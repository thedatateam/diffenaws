# DIFFEN
- Data Lake Ingestion Framework For Enterprises


## Data Source Terminologies

### CDC (Change Data Capture)

CDC is the process of capturing changes made at the data source

- IBM CDC OpTypes
    
    - Insert (I)
    - Before Update (B)
    - After Update (A)
    - Delete (D) 

- Generalized CDC OpTypes

    - Insert
    - Update
    - Delete


## Processing Patterns

Each type of data in the below processing pattern are processed differently in the various stages of the Framework.
    

- Master (Full Dump) 
    
    - Full/Complete Snapshot of the data at a given instance of time
    - Initial loads are always Master load
    - Master load can also be loaded in-between daily loads to refresh the data (maybe due to some recon failure)

- Delta 
    
    - Data containing changes like Insert, Update, Delete in the data
    - Usually all the Dimension tables in data model are processed like delta tables  
    
- Events (Transactions)

    - Always Insert
    - Usually all the Fact tables in data model are processed like event tables


## Supported Source System Files

- CDC Data
    - IBM CDC Data File (data with journal time, transaction id, operation type, user id columns)
    - Generalized CDC Data File (data with journal time and operation type)
    
- Non CDC Data  
    - Data without Operation type (data only with journal time)
    
        Note: Non CDC data supports only Insert and Updates, Delete can't be distinguished from Duplicates without operation type

 
**Note:** All data files are delimited data with UTF-8 encoding  
**TODO:** Check if UTF-16 encoding is supported ??



## Data Layers

The data layers in Diffen framework and the list of processing performed before persisting the data in data layer 

#### Landing Layer

- Source Validation

#### Staging Layer

- Container Level Validation (Rowcount Validation)
- Column Constraints Check
- Data Quality Check

#### Standardization Layer

- Deduplication
- Snapshot Creation


## Setup

### Config Files

Update the source level and table config files in the below path 

1. `bfl_config/bfl_all_param.xml` 
2. `bfl_config/bfl_all_tables_config.xml`
3. `bfl_config/bfl_all_tables_rename.xml` (For Schema evolution, not Used in BFL)


### Creating Code Deployment

- Build Diffen Code Bundle Directory with nifi nars and DIFFEN jars
    ```cmd
    cd nifi-nar-bundles
    mvn clean install
    cd ..\batchIngestion
    sbt assembly
    cd ..
    .\build.bat
    ```
    
- Copy the bundle directory to edge node to deploy the diffen directory
 
### Azure Setup

1. Create a spark HDInsight cluster
    * Search HDinsight under `Create a resource` option
    * Use `Custom` option to create cluster (not `quick create`)
    * Basics
        * Preferably set Cluster name as `bfl` 
        * Cluster Type - **Spark 2.3.0**
        * Preferably set Cluster login password as `1234567890Tdt#`
    * Security + networking
        * Set a existing Virtual Network
        * Skip Identity 
    * Storage
        * Select `bfltdtstorage` as storage account
        * Preferably set Default Container as `hdinsight`
    * Applications 
        * Click next to skip
    * Cluster size
        * select based on requirement
    * Script actions 
        * Click next to skip
         
2. Add Edge Node, use Azure [Template](https://github.com/Azure/azure-quickstart-templates/tree/master/101-hdinsight-linux-add-edge-node) in github
    * Use `Deploy to Azure` button in Readme file.
    
    **Note:** Update the `edmhdpif.config.hive.url` property value in `bfl_all_param.xml` as per new cluster values 
    
3. Copy bundle to edge node
    
    ```sh
    scp -r bundle/ sshuser@<node>:"~"
    ``` 
    
4. Install Nifi under /opt  
        
        
        sudo su  
        cd /opt  
        wget https://archive.apache.org/dist/nifi/1.2.0/nifi-1.2.0-bin.tar.gz  
        tar -xvzf nifi-1.2.0-bin.tar.gz  
        ln -s  /opt/nifi-1.2.0/ /opt/nifi  
        chown -R sshuser. /opt/nifi-1.2.0/  
        chown -R sshuser. /opt/nifi/  
        exit

    
5. Copy the nifi jars and create deployment directory  
    
    
        cp ~/bundle/nifi-nars/* /opt/nifi/lib
        /opt/nifi/bin/nifi.sh start
        mkdir -p ~/deployment/diffen/1.0.0
        cp -r ~/bundle/* ~/deployment/diffen/1.0.0


6. Tunnel 8080 port to access Nifi
    
    ```sh
    ssh -L 8080:localhost:8080 sshuser@edgenode
    ```
    
7. Run diffen-deployer.sh to create hive tables and hdfs directory.  
The Nifi template (DIFFEN-Template-bfl-all.xml) is created under ~/deployment/diffen/1.0.0/conf
 
    ```sh
    ./diffen-deployer.sh sshuser /usr/hdp/current/hive-client/conf/hive-site.xml "" --source bfl --country all --nifi-url localhost:8080/nifi --config-location /home/sshuser/deployment/diffen/1.0.0/conf --distributed-map-cache-port 9000 --nifi-username sshuser
    ```
    
    **Note:** Nifi upload fails in deployer script, but can be done manually.
    
8. Download the Nifi template (`DIFFEN-Template-bfl-all.xml`) under `~/deployment/diffen/1.0.0/conf` and upload to Nifi as 
template.



