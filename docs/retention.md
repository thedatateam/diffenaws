# RETENTION
A scala application is used for applying retention for business data residing in diffen.

## PACKAGE
The retention package is available as part of the batch ingestion library. Retention  application can be found under the package com.tdt.diffen.retention.

## USAGE
```
spark-submit --class com.tdt.diffen.retention.SparkDiffenRetention \
--files $HIVE_CONFIG_RESOURCES \
$DIFFEN_HOME/$DIFFEN_VERSION/lib/IngestionProcessingV2-assembly-1.0.jar $@

      --config-location  <configLocation>     Location on local file system
                                              where config files are available
  -c, --country  <country>                    Country for which deployment is
                                              being done
  -l, --layer  <layer>                        Layer on which retention will be
                                              applied. Allowed values are
                                              <landing|verify_types|invalids|diffen_open|diffen_non_open|>
  -p, --partition-format  <partitionFormat>   Date format of partition.
  -r, --retention-days  <retentionDays>       Number of days worth of data to
                                              retain.
  -s, --source  <source>                      Source for which deployment is
                                              being done
      --start-date  <startDate>               Start date of retention. Format
                                              yyyy-MM-dd
  -t, --table-type  <tableType>               Takes delta or txn value.
      --help                                  Show help message
```