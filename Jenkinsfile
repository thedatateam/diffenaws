def skipRemainingStages = false

// TODO branches for push_to_artifactory should not be present in jenkins file
def branchList = ["develop", "master"]
println("Branch name found to be " + BRANCH_NAME)
def push_to_artifactory = branchList.contains(BRANCH_NAME)
// def push_to_artifactory = true
pipeline {
    agent any

    triggers{
        bitbucketPush()
    }

    stages {
        stage("NotifyBitbucket") {
            steps {
                bitbucketStatusNotify buildState: "INPROGRESS"
            }
        }

        stage('Package_nifi_bundles') {
            when {
                expression {
                    !skipRemainingStages
                }
            }
            steps {
                script{
                    try{
                        dir("nifi-nar-bundles") {
                            sh 'mvn clean install'
                        }
                        bitbucketStatusNotify(buildState: 'SUCCESSFUL',buildKey: 'Package_nifi_bundles',buildName: 'Package_nifi_bundles')
                    }
                    catch(Exception e) {
                        skipRemainingStages = true
                        bitbucketStatusNotify(buildState: 'FAILED',buildKey: 'Package_nifi_bundles',buildName: 'Package_nifi_bundles')
                        echo "FAILED in Package nifi bundles."
                        error('Fail')
                        currentBuild.result = 'FAILED'
                    }
                }
            }
        }

        stage('Package_spark_lib') {
            when {
                expression {
                    !skipRemainingStages
                }
            }
            steps {
                script{
                    try{
                        dir("batchIngestion") {
                            sh "${tool name: 'sbt', type: 'org.jvnet.hudson.plugins.SbtPluginBuilder$SbtInstallation'} clean assembly"
                        }

                        bitbucketStatusNotify(buildState: 'SUCCESSFUL',buildKey: 'Package_spark_lib',buildName: 'Package_spark_lib')
                    }
                    catch(Exception e) {
                        skipRemainingStages = true
                        bitbucketStatusNotify(buildState: 'FAILED',buildKey: 'Package_spark_lib',buildName: 'Package_spark_lib')
                        echo "FAILED in Package"
                        error('Fail')
                        currentBuild.result = 'FAILED'
                    }
                }
            }
        }

        stage('Package_diffen_assembly') {
            when {
                expression {
                    !skipRemainingStages
                }
            }
            steps {
                script{
                    try{
                        dir("diffen-assembly") {
                            sh 'mvn clean package'
                        }
                        bitbucketStatusNotify(buildState: 'SUCCESSFUL',buildKey: 'Package_diffen_assembly',buildName: 'Package_diffen_assembly')
                    }
                    catch(Exception e) {
                        skipRemainingStages = true
                        bitbucketStatusNotify(buildState: 'FAILED',buildKey: 'Package_diffen_assembly',buildName: 'Package_diffen_assembly')
                        echo "FAILED in Package_diffen_assembly."
                        error('Fail')
                        currentBuild.result = 'FAILED'
                    }
                }
            }
        }

        stage('Publish Artifact') {
            when {
                expression {
                    !skipRemainingStages && push_to_artifactory == true
                }
            }
            steps {
                script {
                    try {
                        dir("diffen-assembly") {
                            def readpom = readMavenPom file: 'pom.xml'
                            env.pomVersion = readpom.version
                            rtUpload (
                                serverId: 'jenkinsartifactoryserver',
                                spec: '''{
                                    "files": [
                                        {
                                            "pattern": "target/assembly/*zip",
                                            "target": "diffen/${pomVersion}/"
                                        }
                                    ]
                                }'''
                            )
                        }
                    }
                    catch (Exception e) {
                        skipRemainingStages = true
                        bitbucketStatusNotify(buildState: 'FAILED', buildKey: 'Build Artifact', buildName: 'Build Artifact')
                        echo "FAILED in Build Artifact"
                        error('Fail')
                        currentBuild.result = 'FAILED'
                    }
                }
            }

        }
    }
    post {
        success {
            bitbucketStatusNotify buildState: "SUCCESSFUL"
            }

        failure {
            bitbucketStatusNotify buildState: "FAILED"
            }
    }
}   