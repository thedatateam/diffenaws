#!/usr/bin/env bash

BASEDIR=$(dirname "$0")
source $BASEDIR/env.profile

#<hadoop username> <hive resources> <eod Marker> <business Day> <parameter xml hdfs path> <fulldump|incremental> <rerun Tables List>
#${hdfs.user};${hive.config.resources};${eod.marker.time};${eod.business.time};${hdfs.param.xml.path};${eod.processing.type};""
export HADOOP_USER_NAME=$1
export HIVE_CONFIG_RESOURCES=$2
eodMarker="$3"
businessDate="$4"
paramXmlHdfsPath="$5"
runType="$6"
rerunTable="$7"

$SPARK_HOME/bin/spark-submit --class com.tdt.diffen.processor.Diffen \
--master local[*] \
--deploy-mode client \
--queue default \
--conf "spark.sql.shuffle.partitions=200" \
--conf "spark.default.parallelism=200" \
--files $HIVE_CONFIG_RESOURCES \
$DIFFEN_HOME/$DIFFEN_VERSION/lib/IngestionProcessingV2-assembly-1.0.jar \
"$eodMarker" "$businessDate" "$paramXmlHdfsPath" "$runType" "$rerunTable" > /tmp/diffen.log 2>&1 &

sparkSubmitPid=`echo $!`
sparkSubmitPPid=`echo $$`
echo "SPARK SUBMIT process id : [$sparkSubmitPid]"
echo "SPARK SUBMIT parent process id : [$sparkSubmitPPid]"

while cat /proc/${sparkSubmitPid}/status | grep "$sparkSubmitPPid" >/dev/null 2>&1
do
        sleep 10;
done