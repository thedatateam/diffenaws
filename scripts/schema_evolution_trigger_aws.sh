#!/usr/bin/env bash
BASEDIR=$(dirname "$0")
source $BASEDIR/env.profile

#<hadoop username> <hive resources> <source> <country> <nas config dir> <nas incoming dir> <nas appl dir> <hdfs config dir> <config file name>

export HADOOP_USER_NAME=$1
export HIVE_CONFIG_RESOURCES=$2
source=$3
country=$4
nas_incoming_dir=$5
nas_config_dir=$6
nas_appl_dir=$7
hdfs_config_dir=$8
config_filename=$9


# Trigger spark glue job
/usr/bin/python2 $BASEDIR/schema_evolution_glue.py ${hdfs_config_dir}/${source}_${country}_param.xml ${hdfs_config_dir}/${config_filename}


exit 0

################################################################################################################################################
# END
################################################################################################################################################
