#!/usr/bin/env bash

BASEDIR=$(dirname "$0")
source $BASEDIR/env.profile

if [[ -z "$PLATFORM" ]]
    then
        echo "Missing environment variable PLATFORM"
        exit 1
fi

case $PLATFORM in
    ON_PREM)
        echo "Executing schema evolution for azure."
        sh $BASEDIR/schema_evolution_trigger_onPrem.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9"
        ;;
    AZURE)
        echo "Executing schema evolution for on prem."
        sh $BASEDIR/schema_evolution_trigger_azure.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9"
        ;;
    AWS)
        echo "Executing schema evolution for on prem."
        sh $BASEDIR/schema_evolution_trigger_aws.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9"
        ;;
    *)
        echo -n "unknown"
        ;;
esac