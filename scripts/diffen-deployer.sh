#!/usr/bin/env bash

BASEDIR=$(dirname "$0")
source $BASEDIR/env.profile

export HADOOP_USER_NAME=$1
export HIVE_CONFIG_RESOURCES=$2
export ADDITIONAL_JAVA_OPTIONS=$3
shift 3

spark-submit --class com.tdt.diffen.deployment.DIFFENAWSDeployer \
--master local[*] \
--driver-memory 2g \
--executor-memory 2g \
--executor-cores 1 \
--conf "spark.sql.shuffle.partitions=20" \
--conf "spark.default.parallelism=20" \
$ADDITIONAL_JAVA_OPTIONS \
--queue default \
--files $HIVE_CONFIG_RESOURCES \
$DIFFEN_HOME/$DIFFEN_VERSION/lib/IngestionProcessingV2-assembly-1.0.jar $@


