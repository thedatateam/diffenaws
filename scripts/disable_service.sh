#!/bin/bash

#usage
#sh clean_controller_service.sh process_group_id

hostname=localhost
port=8080

process_group_id=$1


getServiceId() {
curl -s http://${hostname}:${port}/nifi-api/flow/process-groups/${process_group_id}/controller-services | jq -r .controllerServices[].id
}

getComponentId() {
curl -s  http://${hostname}:${port}/nifi-api/controller-services/${1} | jq -r .component.referencingComponents[].id
}


getStopProcessPayload() {
curl -s http://${hostname}:${port}/nifi-api/controller-services/${1} | jq -r '{"id": .id, "state": "STOPPED", "referencingComponentRevisions": [.component.referencingComponents[] | {"key": .id, "value": .revision}] | from_entries}' 
}

stopProcess(){
stopPayLoad=`getStopProcessPayload $1`
curl -X PUT -H "Content-Type: application/json" -d "$stopPayLoad" http://${hostname}:${port}/nifi-api/controller-services/${1}/references
}

getDisableReferenceServicePayLoad(){
curl -s http://${hostname}:${port}/nifi-api/controller-services/${1} | jq -r '{"id": .id, "state": "DISABLED", "referencingComponentRevisions": {}}'
}

disableReferenceService(){
payLoad=`getStopProcessPayload $1`
curl -X PUT -H "Content-Type: application/json" -d "$payLoad" http://${hostname}:${port}/nifi-api/controller-services/${1}/references
}

getDisableControllerServicePayload(){
curl -s http://${hostname}:${port}/nifi-api/controller-services/${1} | jq '{revision, "component": {id, "state": "DISABLED"}}'
}

disableControllerService(){
disablePayload=`getDisableControllerServicePayload $1`
curl -X PUT -H "Content-Type: application/json" -d "$disablePayload" http://${hostname}:${port}/nifi-api/controller-services/${1}
}

for serviceId in `getServiceId $1`
do
	stopProcess $serviceId
	disableReferenceService $serviceId
	disableControllerService $serviceId
done
