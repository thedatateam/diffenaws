#!/usr/bin/env bash

/mnt/deployment/diffen/1.0.0/scripts/diffen-deployer.sh \
    sshuser \
    /usr/hdp/current/hive-client/conf/hive-site.xml \
    --source bfl \
    --country all \
    --nifi-url localhost:8080/nifi \
    --config-location /mnt/deployment/diffen/1.0.0/conf \
    --distributed-map-cache-port 9000 \
    --nifi-username sshuser
