#!/usr/bin/env bash

# If the Nifi instance is not handled by Ambari, uncomment the stop and start steps for nifi.

source $PWD/env.profile

#$NIFI_HOME/bin/nifi.sh stop

rm $NIFI_HOME/lib/nifi-data-cleansing-nar-*
rm $NIFI_HOME/lib/nifi-distributed-cache-services-nar-*
rm $NIFI_HOME/lib/nifi-hadoop-nar-*
rm $NIFI_HOME/lib/nifi-record-serialization-services-nar-*
rm $NIFI_HOME/lib/nifi-scb-edmhdpif-nar-*
rm $NIFI_HOME/lib/nifi-standard-nar-*
rm $NIFI_HOME/lib/nifi-standard-services-api-nar-*


cp $DIFFEN_HOME/$DIFFEN_VERSION/nifi-nars/*.nar $NIFI_HOME/lib/

#$NIFI_HOME/bin/nifi.sh start