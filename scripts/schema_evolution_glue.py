import boto3
import time
import sys
import re
import logging


def load_log_config():
    root = logging.getLogger()
    root.setLevel(logging.INFO)
    return root


def poll_job_run_state(job_name, run_id):
    try:
        jr = glue_client.get_job_run(JobName=job_name, RunId=run_id)
        return jr.get('JobRun').get('JobRunState')
    except glue_client.exceptions.EntityNotFoundException as e:
        print 'Job run not found (RunId:%s).' %run_id


def start_job_run(client, job_name):

    print 'run_job()'
    run_id = -1

    args = {
        '--newTableConfigPath': new_table_config_path,
        '--paramConfigPath': param_config_path,
    }

    try:
        resp = client.start_job_run(JobName=job_name,
                                    Arguments=args,
                                    AllocatedCapacity=MIN_DPU_CAPACITY)
        run_id = resp.get('JobRunId')
        print 'Job run started'
        print 'JobRunId:%s' %run_id

    except client.exceptions.ConcurrentRunsExceededException as e:
        print e
    except client.exceptions.EntityNotFoundException as e:
        print e
    except client.exceptions.InternalServiceException as e:
        print e
    except client.exceptions.InvalidInputException as e:
        print e
    except client.exceptions.OperationTimeoutException as e:
        print e
    except client.exceptions.ResourceNumberLimitExceededException as e:
        print e

    return run_id


def wait_state(exit_pattern, poll_function, *args):

    prev_state=''
    while True:
        state = poll_function(*args)
        if not state:
            sys.exit(1)
        if state != prev_state:
            prev_state = state
            print('\n%s' %state), # discard newline, (python2.x only)
            sys.stdout.flush() 
        else:
            print('.'),
            sys.stdout.flush()
        if exit_pattern.match(state):
            if state in 'SUCCEEDED':
                sys.exit(0)
            else:
                sys.exit(1)

        time.sleep(POLL_INTERVAL)



param_config_path = sys.argv[1]
new_table_config_path = sys.argv[2]
JOB_NAME_DIFFEN='diffen-schema-evolution'
DEFAULT_REGION='ap-southeast-1'
POLL_INTERVAL=10
GLUE_ENDPOINT='glue'
MIN_DPU_CAPACITY=4

logger = load_log_config()

glue_client = boto3.client(service_name='glue',
                           region_name=DEFAULT_REGION,
                           endpoint_url='https://%s.%s.amazonaws.com'
                           %(GLUE_ENDPOINT, DEFAULT_REGION))

if __name__ == '__main__':

    run_id = start_job_run(client=glue_client, job_name=JOB_NAME_DIFFEN)
    exit_pattern = p=re.compile('(?!RUNNING)')
    wait_state(exit_pattern, poll_job_run_state, JOB_NAME_DIFFEN, run_id)

