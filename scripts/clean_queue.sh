get_connections() {
	curl -X GET  localhost:8080/nifi-api/process-groups/${1}/connections | jq -r '.connections | .[].id'
}

drop_requests() {
	curl -X POST localhost:8080/nifi-api/flowfile-queues/${connection}/drop-requests
}

get_process_groups() {
	curl -X GET  localhost:8080/nifi-api/process-groups/${1}/process-groups | jq -r '.processGroups | .[].id'
}


cleanQueue() {
	for connection in `get_connections $1`
	do
		drop_requests connection
	done
	
	for process_group in `get_process_groups $1`
	do
		cleanQueue $process_group
	done
}

cleanQueue $1

