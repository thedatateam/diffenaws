#!/usr/bin/env bash

# USAGE
# ./diffen-retention.sh ./hive-site.xml --source diffen --country all --config-location ./config --retention-days 30 \
# --partition-format yyyy_MM_dd --table-type delta
#
# OPTIONS
#       --config-location  <configLocation>     Location on local file system
#                                              where config files are available
#  -c, --country  <country>                    Country for which deployment is
#                                              being done
#  -p, --partition-format  <partitionFormat>   Date format of partition.
#  -r, --retention-days  <retentionDays>       Number of days worth of data to
#                                              retain.
#  -s, --source  <source>                      Source for which deployment is
#                                              being done
#      --start-date  <startDate>               Start date of retention. Format
#                                              yyyy-MM-dd
#  -t, --table-type  <tableType>               Takes delta or txn value.
#      --help                                  Show help message

BASEDIR=$(dirname "$0")
source $BASEDIR/env.profile

export HIVE_CONFIG_RESOURCES=$1
shift 1

spark-submit --class com.tdt.diffen.retention.FileSystemRetention \
--master local[*] \
--files $HIVE_CONFIG_RESOURCES \
$DIFFEN_HOME/$DIFFEN_VERSION/lib/IngestionProcessingV2-assembly-1.0.jar "$@"