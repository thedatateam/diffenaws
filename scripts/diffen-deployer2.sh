source $PWD/env.profile
export HADOOP_USER_NAME=$1
export HIVE_CONFIG_RESOURCES=$2
NIFI_CERTS=$3
NIFI_PASSWORD_KEY=$4
HIVE_PASSWORD_KEY=$5
HIVE_DR_PASSWORD_KEY=$6


LIB_DIR=$DIFFEN_HOME/$DIFFEN_VERSION/lib

PASSWORD_LOC=$DIFFEN_HOME/$DIFFEN_VERSION/passwords/

CLASSPATH=${LIB_DIR}:${LIB_DIR}/commons-logging.jar:${LIB_DIR}/commons-cli-1.0.jar:${LIB_DIR}/cloveretl.engine.jar:$CLASSPATH:.

export CLASSPATH

echo $CLASSPATH

PATH=${JAVA_HOME}/bin:$PATH:.

NIFI_PASSWORD_ENC=`head -1 ${PASSWORD_LOC}/${NIFI_PASSWORD_KEY}.dat`
export NIFI_PASSWORD=`java EnigmaTest D ${NIFI_PASSWORD_ENC}`

HIVE_PASSWORD_ENC=`head -1 ${PASSWORD_LOC}/${HIVE_PASSWORD_KEY}.dat`
export HIVE_PASSWORD=`java EnigmaTest D ${HIVE_PASSWORD_ENC}`

HIVE_DR_PASSWORD_ENC=`cat ${PASSWORD_LOC}/${HIVE_DR_PASSWORD_KEY}.dat|head -1`
export HIVE_DR_PASSWORD=`java EnigmaTest D ${HIVE_DR_PASSWORD_ENC}`


shift 6

$SPARK_HOME/bin/spark-submit --class com.sc.diffen.deployment.DIFFENDeployer \
--master local[*] \
--driver-memory 10g \
--executor-memory 5g \
--executor-cores 4 \
$NIFI_CERTS \
--files $HIVE_CONFIG_RESOURCES \
$DIFFEN_HOME/$DIFFEN_VERSION/lib/IngestionProcessing-assembly-1.0.jar $@


