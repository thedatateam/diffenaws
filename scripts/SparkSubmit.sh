#!/usr/bin/env bash
BASEDIR=$(dirname "$0")
source $BASEDIR/env.profile

if [[ -z "$PLATFORM" ]]
    then
        echo "Missing environment variable PLATFORM"
        exit 1
fi

case $PLATFORM in
    ON_PREM)
        echo "Executing spark submit for azure."
        sh $BASEDIR/SparkSubmitOnPrem.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7"
        ;;
    AZURE)
        echo "Executing spark submit for on prem."
        sh $BASEDIR/SparkSubmitAzure.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7"
        ;;
    AWS)
        echo "Executing spark submit for on aws."
        python2 $BASEDIR/diffen-snapshot.py "$1" "$2" "$3" "$4" "$5" "$6" "$7"
        #sh $BASEDIR/SparkSubmitOnPrem.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7"
        ;;
    *)
        echo -n "unknown"
        ;;
esac