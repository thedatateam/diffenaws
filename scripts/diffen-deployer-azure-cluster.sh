#!/usr/bin/env bash

BASEDIR=$(dirname "$0")
source $BASEDIR/env.profile

export HADOOP_USER_NAME=$1
export HIVE_CONFIG_RESOURCES=$2
export ADDITIONAL_JAVA_OPTIONS=$3
shift 3

[[ -z "${diffenSource}" ]] && read -p "Enter source name : " diffenSource
[[ -z "${diffenCountry}" ]] && read -p "Enter country name : " diffenCountry
[[ -z "${databricksRegion}" ]] && read -p "Enter databricks region name : " databricksRegion
[[ -z "${databricksKey}" ]] && read -p "Enter databricks key: " databricksKey

#create .netrc file
echo "machine ${databricksRegion}.azuredatabricks.net
login token
password ${databricksKey}" | openssl rsautl -inkey /opt/diffen/databricks/key.txt -encrypt >/mnt/diffen-share/.databricks.bin
openssl rsautl -inkey /opt/diffen/databricks/key.txt -decrypt</mnt/diffen-share/.databricks.bin >/home/diffen/.netrc

baseApiUrl="https://${databricksRegion}.azuredatabricks.net/api"

#List databricks node types
[[ -z "${databricksNode}" ]] && curl -n  ${baseApiUrl}/2.0/clusters/list-node-types | jq '.node_types[] | "NodeId => \(.node_type_id) NodeMemory => \(.memory_mb) NodeCores => \(.num_cores) NodeCategory => \(.category)"'

#get node type
[[ -z "${databricksNode}" ]] && read -p "Enter databricks nodeType: " databricksNode

#create databricks folder upload artifacts
curl -X POST -n ${baseApiUrl}/2.0/dbfs/mkdirs -d '{"path": "/diffen"}'
curl -X POST -n -F contents=@/opt/diffen/diffen/1.0.0/lib/IngestionProcessingV2-assembly-1.0.jar -F path="dbfs:/diffen/IngestionProcessingV2-assembly-1.0.jar" -F overwrite=true ${baseApiUrl}/2.0/dbfs/put
curl -X POST -n -F contents=@/opt/diffen/databricks/set_spark_params.sh -F path="dbfs:/databricks/init/set_spark_params.sh" -F overwrite=true ${baseApiUrl}/2.0/dbfs/put

spark-submit --class com.tdt.diffen.deployment.DIFFENClusterDeployer \
--master local[*] \
--driver-memory 2g \
--executor-memory 2g \
--executor-cores 1 \
--conf "spark.sql.shuffle.partitions=20" \
--conf "spark.default.parallelism=20" \
--conf "spark.driver.userClassPathFirst=true" \
$ADDITIONAL_JAVA_OPTIONS \
--queue default \
--files $HIVE_CONFIG_RESOURCES \
$DIFFEN_HOME/$DIFFEN_VERSION/lib/IngestionProcessingV2-assembly-1.0.jar $@

##replacing data bricks node type
    sed -i s/##dbNode##/$databricksNode/g /mnt/diffen-share/deployment/$diffenSource/$diffenCountry/appl/SparkSubmit.sh
sed -i s/##dbNode##/$databricksNode/g /mnt/diffen-share/deployment/$diffenSource/$diffenCountry/appl/schema_evolution_trigger.sh