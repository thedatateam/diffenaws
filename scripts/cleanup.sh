#!/usr/bin/env bash

hdfs dfs -rm -r -skipTrash /mnt/data/bfl/*

clusterName=`hostname -A | awk -F '[-.]' '{print $2"-"$3"."$4"."$5}'`
beeline -u 'jdbc:hive2://hn0-'${clusterName}'.internal.cloudapp.net:10001/;transportMode=http' -n '' -p '' -e "DROP DATABASE IF EXISTS bfl_all_diffen_nonopen CASCADE; DROP DATABASE IF EXISTS bfl_all_diffen_open CASCADE; DROP DATABASE IF EXISTS bfl_all_storage CASCADE; DROP DATABASE IF EXISTS bfl_common_metadata CASCADE; DROP DATABASE IF EXISTS bfl_demo_landing CASCADE; DROP DATABASE IF EXISTS bfl_ops CASCADE;"

