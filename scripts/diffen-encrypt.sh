source $PWD/env.profile
clear

echo 'Password Entry Screen '
echo '===================== '
echo

read -p "Enter UserName: " Username
read -s -p "Enter Password of ${Username}: " mypassword
echo
read -s -p "Confirm Password of ${Username}: " mypassword1
echo

if [ "${mypassword}" = "${mypassword1}" ]
then
    echo 'Password match'
else
   echo 'Password dont match , ReTry ..'
   exit
fi

LIB_DIR=$DIFFEN_HOME/$DIFFEN_VERSION/lib
PASSWORD_LOC=$DIFFEN_HOME/$DIFFEN_VERSION/passwords/
mkdir -p ${PASSWORD_LOC}

CLASSPATH=${LIB_DIR}:${LIB_DIR}/commons-logging.jar:${LIB_DIR}/commons-cli-1.0.jar:${LIB_DIR}/cloveretl.engine.jar:$CLASSPATH:.

export CLASSPATH

java EnigmaTest E $mypassword > ${PASSWORD_LOC}/${Username}.dat

ls -lrt ${PASSWORD_LOC}/${Username}.dat

echo -e "\n${Username} Password changed successfully..............."

exit 0

