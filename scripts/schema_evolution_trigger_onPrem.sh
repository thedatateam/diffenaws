#!/usr/bin/env bash

BASEDIR=$(dirname "$0")
source $BASEDIR/env.profile

#<hadoop username> <hive resources> <source> <country> <nas config dir> <nas incoming dir> <nas appl dir> <hdfs config dir> <config file name>

export HADOOP_USER_NAME=$1
export HIVE_CONFIG_RESOURCES=$2
source=$3
country=$4
nas_incoming_dir=$5
nas_config_dir=$6
nas_appl_dir=$7
hdfs_config_dir=$8
config_filename=$9

dt=`date +%Y-%m-%d`

script_name=`basename $0 | cut -d "." -f1`
log_file=${nas_appl_dir}/${script_name}_${source}_${country}_$dt.log

$SPARK_HOME/bin/spark-submit --class com.tdt.diffen.processor.SchemaEvolutionV2 \
--master yarn \
--deploy-mode cluster \
--driver-memory 4g \
--executor-memory 4g \
--executor-cores 4 \
--num-executors 3 \
--queue default \
--conf spark.sql.catalogImplementation=hive \
--files $HIVE_CONFIG_RESOURCES  \
$DIFFEN_HOME/$DIFFEN_VERSION/lib/IngestionProcessingV2-assembly-1.0.jar \
--generate-backward-compatible-table-config \
--new-table-config-path ${hdfs_config_dir}/${config_filename} \
--param-config-path ${hdfs_config_dir}/${source}_${country}_param.xml  >/dev/null 2>&1 &

sparkSubmitPid=`echo $!`
sparkSubmitPPid=`echo $$`

echo "SPARK SUBMIT process id : [$sparkSubmitPid]"
echo "SPARK SUBMIT parent process id : [$sparkSubmitPPid]"

    while cat /proc/${sparkSubmitPid}/status | grep "$sparkSubmitPPid" >/dev/null 2>&1
    do
        sleep 10;
    done


#Backup the existing tableConfig file & re-name the new tableConfig file in NAS layer
mv ${nas_config_dir}/${source}_${country}_tables_config.xml ${nas_config_dir}/${source}_${country}_tables_config_$dt.xml
if [ $? -ne 0 ]; then
    echo "ERROR `date +'%Y-%m-%d %T'` : FAILED to run rename NAS" > $log_file
    exit 1;
    else
    echo "INFO `date +'%Y-%m-%d %T'` : SUCCESSFULLY ran rename NAS" >> $log_file
fi

$HADOOP_HOME/bin/hadoop fs -get ${hdfs_config_dir}/${source}_${country}_tables_config.xml ${nas_config_dir}/
if [ $? -ne 0 ]; then
    echo "ERROR `date +'%Y-%m-%d %T'` : FAILED to run backup NAS" >> $log_file
    exit 1;
    else
    echo "INFO `date +'%Y-%m-%d %T'` : SUCCESSFULLY ran backup NAS" >> $log_file
fi

#Move the '.xml' file for Incoming Directory to Config Directory in NAS layer
for xml in `ls ${nas_incoming_dir}/*.xml`
do
file=`basename $xml`
mv $xml ${nas_appl_dir}/${file}_$dt
if [ $? -ne 0 ]; then
    echo "ERROR `date +'%Y-%m-%d %T'` : FAILED to run move NAS" >> $log_file
    exit 1;
    else
    echo "INFO `date +'%Y-%m-%d %T'` : SUCCESSFULLY ran move NAS" >> $log_file
fi
done

rm -f $log_file

exit 0

################################################################################################################################################
# END
################################################################################################################################################
