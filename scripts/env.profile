# Set the values based on the environment DIFFEN is deployed
export SPARK_HOME=/opt/diffen/spark
export DIFFEN_HOME=/opt/diffen/diffen
export DIFFEN_VERSION=1.0.0
export NIFI_HOME=/opt/nifi
export PLATFORM=AWS
export DIFFEN_SOURCE=1302552720
export HADOOP_HOME=/opt/diffen/hadoop
export JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk