import java.io.File
import java.nio.file.StandardOpenOption._
import java.nio.file.{FileSystem, Files, Path}

import scala.xml.Elem

object ConfigCrawler {

  val localFs: FileSystem = java.nio.file.FileSystems.getDefault
  val path: (String) => Path = {
    case str: String => localFs.getPath(str)
  }

  val configFile: String = "sample_configs/gps_all_tables_config.xml"
  val outPath = path("gps.ben.xml")
  val count: String = "50"
  val timeFormat: String = "yyyy-MM-dd HH:mm:ss.SSSSSS"
  val delimiter: String = "\\u0001"

  def main(args: Array[String]) {
    val file: Elem = xml.XML.loadFile(new File(configFile))
    val allProps = file \\ "configuration"
    val seq = allProps \\ "property"

    fileInit()

    val keyColSeq = seq.filter(n => (n \ "name").text.contains("keycols"))
      .map { node =>
        ((node \ "name").text.split("\\.")(2), (node \ "value").text)
      }

    val sourceTypeSeq = seq.filter(n => (n \ "name").text.contains("sourcetype"))
      .map { node =>
        ((node \ "name").text.split("\\.")(2), (node \ "value").text)
      }

    var i = 0
    seq.foreach { node =>
      val propName = (node \ "name").text
      val cond = propName.contains("col_schema")

      val tableName: String = propName.split("\\.")(2)

      val keyColString: String = keyColSeq.filter(_._1.equals(tableName)).head._2
      val sourceType: String = sourceTypeSeq.filter(_._1.equals(tableName)).head._2

      if (cond) {
        processTable(tableName, (node \ "value").text, keyColString, sourceType, i)
        i = i + 1
      }
    }

    fileEnd()
  }

  def fileInit() = {
    Files.deleteIfExists(outPath)
    writeToFile("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n<setup xmlns=\"http://databene.org/benerator/0.7.0\"\n       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n       xsi:schemaLocation=\"http://databene.org/benerator/0.7.0 http://databene.org/benerator-0.7.0.xsd\">\n\n    <import platforms=\"csv\"/>")
  }

  def fileEnd() = {
    writeToFile("<echo>Done !!!</echo></setup>")
  }

  def processTable(tableName: String, colSchema: String, keyColString: String, sourceType: String, index: Int): Unit = {
    val split: Array[String] = colSchema.split(" \\^")


    writeToFile("<bean id=\"datetime_gen" + index + "\" class=\"org.databene.benerator.primitive.datetime.DateTimeGenerator\">\n        <property name=\"minDate\" value=\"2016-01-01\"/>\n        <property name=\"maxDate\" value=\"2016-02-01\"/>\n        <property name=\"dateGranularity\" value=\"0000-00-01\"/>\n        <property name=\"dateDistribution\" value=\"{new RandomSequence()}\"/>\n        <!--<property name=\"minTime\" value=\"00:00:00\"/>-->\n        <!--<property name=\"maxTime\" value=\"23:59:59\"/>-->\n        <property name=\"timeGranularity\" value=\"00:00:01\"/>\n        <property name=\"timeDistribution\" value=\"{new RandomSequence()}\"/>\n    </bean>\n")


    writeToFile("<generate type=\"" + tableName + "\" count=\"" + count + "\">")

    val map: Array[String] = keyColString.split(",").map(_.trim)


    if (sourceType.equals("delta"))
      writeToFile("<attribute name=\"c_jounrnaltime\" type=\"timestamp\" source=\"datetime_gen" + index + "\" />\n <id name=\"c_transactionId\" type=\"string\" maxLength=\"5\" />\n<attribute name=\"c_opType\" type=\"string\" pattern=\"(I|B|A|D)\" />\n<attribute name=\"c_user\" type=\"string\" maxLength=\"5\" />\n        ")
    else
      writeToFile("<attribute name=\"c_jounrnaltime\" type=\"timestamp\" source=\"datetime_gen" + index + "\" />\n <id name=\"c_transactionId\" type=\"string\" maxLength=\"5\" />\n<attribute name=\"c_opType\" type=\"string\" pattern=\"(I)\" />\n<attribute name=\"c_user\" type=\"string\" maxLength=\"5\" />\n        ")


    split.foreach {
      col => {
        val strings = col.split(" ")
        val colname = strings(0)
        val coltype = strings(1)

        val nullable = if (strings.length > 3 && strings(2).equals("NOT") && strings(3).equals("NULL")) {
          true
        } else false

        writeToFile("<attribute name=\"" + colname + "\" type=\"" + getType(coltype, nullable, index) + "\"/>")
      }
    }


    //tablename.D<Year><JulianDay Zero Padded>.T<HHMM MS>.R<ROWCount min 5 0 padded>
    //D2017011.T110110110.R
    writeToFile(" <consumer class=\"CSVEntityExporter\">\n   <property name=\"separator\" value=\"" + delimiter +"\"/>\n         <property name=\"uri\" value=\"" + tableName + ".D2017011.T110110110.R" + count + ".csv\"/>\n            <property name=\"headless\" value=\"true\" />\n            <property name=\"timestampPattern\" value=\"" + timeFormat + "\" />\n        </consumer>\n    </generate> \n <echo>" + tableName + " data Generated</echo> \n")
  }

  def writeToFile(s: String) = {
    Files.write(outPath, s.getBytes, CREATE, APPEND)
  }

  def getType(colType: String, nullable: Boolean, index: Int): String = {
    val s = colType.split(Array('(', ',', ')'))

    s(0) match {
      case "VARCHAR" => "string\" " + "maxLength=\"" + s(1) + (if (nullable) "\" minLength=\"1" else "")
      case "DECIMAL" => "big_decimal\" nullable=\"false\" " + getDecimalSize(s(1).toInt, s(2).toInt)
      case "TIMESTAMP" => """timestamp" source="datetime_gen""" + index
      case _ => throw new Exception("New type")
    }
  }

  def getDecimalSize(intt: Int, decimal: Int): String = {
    """pattern="[0-9]{0,""" + (intt - decimal).toString + "}" + (if (decimal > 0) {"""\.[0-9]{""" + decimal + "}"} else "")
  }
}