# Diffen functionality in DIFFEN

* DIFFEN does not support Intra-day DIFFEN creation (hourly diffen creation is not supported, only supports daily diffen creation)

* DIFFEN supports DIFFEN for following types of data
    * CDC
    * Non CDC (delimited) with record type column (A, B, I, D)  
    * Non CDC (delimited) with no record type (***BFL*** Case)
    
* DIFFEN expects a record time column for all tables to filter the loaded data between the previous EOD and current EOD 
marker. Brief steps for diffen creation:   
    
    * On Diffen trigger, previous day, today and tomorrow verify types data are loaded. 
    * loaded verifyTypes is filtered based on given time column, between previous EOD marker time and current EOD marker
    * Diffen for current day is created based on filtered verify types and previous day diffenopen
    
    To be able to create DIFFEN without a time column I have modified the code 
    [commit](https://bitbucket.org/thedatateam/scb-diffen/commits/8099b819a3fb9a88999e109898109c4130ef8e3a) 
    to take all data in current date (today) partition verify types and create DIFFEN layer. So we need to always 
    set marker time as start of next day of EOD Marker, that as for a EOD Date of `2018-10-14 02:30:00` set marker 
    time as `2018-10-15 00:00:00`  
    

* A marker file/EOD marker file needs to be passed as input file to trigger the DIFFEN
    * Format:  
        
            JOURNALTIME | TRANSACTIONID | OPERATIONTYPE | USERID | EOD_DATE | MARKER_TIME 
        
    * Sample:  
        
            2018-10-14 00:29:40|0|I|BFL|2018-10-14 02:33:34|2018-10-15 00:00:00
        
    * First 4 columns as CDC columns  
    * Opertaion Type can be only `A` or `I`
    * To trigger full dump set User Id as `NOT SET`
    * All date/time/timestamp columns must be in `yyyy-MM-dd HH:mm:ss` format 
    * EOD Date is diffen partition date
    * Marker time is DIFFEN cut-off time

* DIFFEN supports Table level EOD (Needs to be tested)

* Expects input format to be Avro, and output as ORC (always)



---


bfl_admin
Admin@123456


sudo su - oracle

sqlplus sys as sysdba 
-> pass OraPasswd1
> startup


Sql UI, -> ip, (DATA Grip)
service - pdb1
user - bfl
pass - bfl


jdbc url - jdbc:oracle:thin:@//52.172.142.63:1521/pdb1


---

Stream sets

sudo service sdc restart

http://104.211.226.230:18630

