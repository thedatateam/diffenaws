/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.time.format.DateTimeFormatter

def flowFile = session.get()
if(flowFile != null){

    def source = flowFile.getAttribute("source");
    def country = flowFile.getAttribute("country");
    flowFile = session.putAttribute(flowFile,"source",source);
    flowFile = session.putAttribute(flowFile,"country",country);

    def filename = flowFile.getAttribute("filename");

    def fileParts = filename.split("\\.");
    def datePart = fileParts[1].substring(1)
    def timePart = fileParts[2].substring(1,7)
    def formatter = DateTimeFormatter.ofPattern("yyyyDDD HHmmss")
    def date = formatter.parse(datePart + " " + timePart)

    def partitionFormatter = DateTimeFormatter.ofPattern("YYYY_MM_dd_HH")
    def basePartition = partitionFormatter.format(date)
    flowFile = session.putAttribute(flowFile,"base_partition",basePartition)
    flowFile = session.putAttribute(flowFile,"tablename", new String(source+"_"+country+"_"+fileParts[0]).toLowerCase());
    flowFile = session.putAttribute(flowFile,"date",datePart);
    flowFile = session.putAttribute(flowFile,"time",timePart);
    flowFile = session.putAttribute(flowFile,"rowcount",fileParts[3].substring(1));


    session.transfer(flowFile, REL_SUCCESS)
}