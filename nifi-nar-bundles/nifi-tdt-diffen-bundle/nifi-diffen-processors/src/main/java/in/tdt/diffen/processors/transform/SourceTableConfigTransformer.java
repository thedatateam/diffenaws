package in.tdt.diffen.processors.transform;

import in.tdt.diffen.cdc.CDCModels;
import org.apache.nifi.logging.ComponentLog;

import java.io.File;
import java.util.Collection;
import java.util.Map;

public interface SourceTableConfigTransformer {
    Map<String, CDCModels.TableConfiguration> transform(Collection<File> sourceConfigFile, Map<String, String> attributes, ComponentLog logger);
}
