package in.tdt.diffen.processors;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.InputRequirement.Requirement;
import org.apache.nifi.annotation.behavior.TriggerSerially;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@TriggerSerially
@InputRequirement(Requirement.INPUT_REQUIRED)
@Tags({"file", "list", "skip file", "missing file"})
@CapabilityDescription("Workaround for the file skip issue in ListFile processor for filesystems that doesn't support nano time precision")

public class MissingFileCheckProcessor extends AbstractProcessor {

    static final PropertyDescriptor INPUT_DIRECTORY = new PropertyDescriptor.Builder()
            .name("Input Directory")
            .description("The input directory from which files to pull files")
            .required(true)
            .addValidator(StandardValidators.createDirectoryExistsValidator(true, false))
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    static final PropertyDescriptor LOG_DIRECTORY = new PropertyDescriptor.Builder()
            .name("Log Directory")
            .description("The directory in which the logs of this processor is written")
            .required(true)
            .addValidator(StandardValidators.createDirectoryExistsValidator(true, true))
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    static final PropertyDescriptor DELTA_LOG_DIRECTORY = new PropertyDescriptor.Builder()
            .name("Delta Log Directory")
            .description("The directory in which only the new files since the last fetch would be written")
            .required(true)
            .addValidator(StandardValidators.createDirectoryExistsValidator(true, true))
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    static final PropertyDescriptor FILE_FILTER = new PropertyDescriptor.Builder()
            .name("File Filter")
            .description("Only files whose names match the given regular expression will be picked up")
            .required(true)
            .defaultValue("[^\\.].*")
            .addValidator(StandardValidators.REGULAR_EXPRESSION_VALIDATOR)
            .build();

    static final PropertyDescriptor IGNORE_HIDDEN_FILES = new PropertyDescriptor.Builder()
            .name("Ignore Hidden Files")
            .description("Indicates whether or not hidden files should be ignored")
            .allowableValues("true", "false")
            .defaultValue("true")
            .required(true)
            .build();

    static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("All FlowFiles that are received are routed to success")
            .build();

    static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Failure relationship")
            .build();

    private List<PropertyDescriptor> properties;
    private Set<Relationship> relationships;
    private final AtomicReference<FileFilter> fileFilterRef = new AtomicReference<>();


    static final String FILE_FILENAME_ATTRIBUTE = "filename";
    static final String FILE_ABSOLUTE_PATH_ATTRIBUTE = "absolute.path";
    public static final String FILE_CREATION_TIME_ATTRIBUTE = "file.creationTime";
    public static final String FILE_LAST_MODIFY_TIME_ATTRIBUTE = "file.lastModifiedTime";
    public static final String FILE_OWNER_ATTRIBUTE = "file.owner";
    public static final String FILE_GROUP_ATTRIBUTE = "file.group";
    public static final String FILE_SIZE_ATTRIBUTE = "file.size";
    public static final String FILE_MODIFY_DATE_ATTR_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";


    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> properties = new ArrayList<>();
        properties.add(INPUT_DIRECTORY);
        properties.add(LOG_DIRECTORY);
        properties.add(DELTA_LOG_DIRECTORY);
        properties.add(FILE_FILTER);
        properties.add(IGNORE_HIDDEN_FILES);
        this.properties = Collections.unmodifiableList(properties);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return properties;
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relationships;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {
        fileFilterRef.set(createFileFilter(context));
    }

    private FileFilter createFileFilter(final ProcessContext context) {
        final boolean ignoreHidden = context.getProperty(IGNORE_HIDDEN_FILES).asBoolean();
        final Pattern filePattern = Pattern.compile(context.getProperty(FILE_FILTER).getValue());

        return file -> {
                if (ignoreHidden && file.isHidden()) {
                    return false;
                }

                if (!Files.isReadable(file.toPath())) {
                    return false;
                }
                return filePattern.matcher(file.getName()).matches();
        };
            }

    private MiniFileInfo convertToMiniFileInfo(String date, DateTimeFormatter attrTimeFormatter, DateTimeFormatter olaFileDateFormatter, FlowFile flowFile, String status) {

        MiniFileInfo fileInfo = new MiniFileInfo();
        try {
            DateTime modifiedTime = attrTimeFormatter.parseDateTime(flowFile.getAttribute(FILE_LAST_MODIFY_TIME_ATTRIBUTE));
            fileInfo = new MiniFileInfo(
                    flowFile.getAttribute(FILE_ABSOLUTE_PATH_ATTRIBUTE) + flowFile.getAttribute(FILE_FILENAME_ATTRIBUTE),
                    modifiedTime.getMillis(),
                    date,
                    flowFile.getAttribute(FILE_SIZE_ATTRIBUTE),
                    olaFileDateFormatter.print(attrTimeFormatter.parseDateTime(flowFile.getAttribute(FILE_CREATION_TIME_ATTRIBUTE))),
                    olaFileDateFormatter.print(modifiedTime),
                    status
            );
        } catch (Exception e) {
            getLogger().error(e.getMessage(), e);
            e.printStackTrace();
        }
        return fileInfo;
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {

        final List<FlowFile> flowFiles = session.get(1000);
        //If no flow files are coming in, then this processor wouldn't run.  This is to cover for the time when the List file is stopped/no files are coming in for a long time. There's a downside that
        //the older file will never be touched until 'any' file comes in.
        if (flowFiles.isEmpty()) {
            return;
        }

        final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        final DateTimeFormatter olaFileDateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd_hh-mm-ss");
        final DateTimeFormatter fileAttributeTimeFormatter = DateTimeFormat.forPattern(FILE_MODIFY_DATE_ATTR_FORMAT);

        DateTime todayDate = DateTime.now();
        String today = dateFormatter.print(todayDate);

        DateTime yesterdayDate = todayDate.minusDays(1);
        String yesterday = dateFormatter.print(yesterdayDate);

        String time = olaFileDateFormatter.print(todayDate);

        final File logDir = new File(context.getProperty(LOG_DIRECTORY).evaluateAttributeExpressions().getValue());
        final File inputPath = new File(context.getProperty(INPUT_DIRECTORY).evaluateAttributeExpressions().getValue());
        final File deltaLogPath = new File(context.getProperty(DELTA_LOG_DIRECTORY).evaluateAttributeExpressions().getValue());

        final File todayLogFile = new File(logDir, today + ".log");
        final File yesterDayLogFile = new File(logDir, yesterday + ".log");

        try {
            Map<String, MiniFileInfo> filesFromCurrentQueue = flowFiles.stream()
                    .map(flowFile -> convertToMiniFileInfo(today, fileAttributeTimeFormatter, olaFileDateFormatter, flowFile, "Y"))
                    .collect(Collectors.toMap(MiniFileInfo::getAbsolutePath, Function.identity()));

            Map<String, MiniFileInfo> allFilesInIncomingPath = Arrays
                    .stream(inputPath.listFiles(fileFilterRef.get()))
                    .map(file -> MiniFileInfo.constructFromFile(file, today, olaFileDateFormatter))
                    .collect(Collectors.toMap(MiniFileInfo::getAbsolutePath, Function.identity())); //Lookup the incoming directory for the logFileEntrieskjkkkkkkkdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddkkkkkkkd

            Map<String, MiniFileInfo> allTodaysLogFileEntries = new HashMap<>();
            Set<MiniFileInfo> freshFiles = new HashSet<>();

            //Load today's file
            Map<String, MiniFileInfo> todaysMiniFileMapFromFile = constructMiniFileInfoFromLogFile(todayLogFile);
            if (todaysMiniFileMapFromFile.isEmpty()){
                allTodaysLogFileEntries.putAll(allFilesInIncomingPath);
            }
            else {
                allTodaysLogFileEntries.putAll(todaysMiniFileMapFromFile);
            }

            //Load yesterday's file
            Map<String, MiniFileInfo> yesterDaysMiniFileMapFromFile = constructMiniFileInfoFromLogFile(yesterDayLogFile);

            //Update log file with the latest entries
            for (Map.Entry<String, MiniFileInfo> eachFileInIncoming : allFilesInIncomingPath.entrySet()) {
                if (!allTodaysLogFileEntries.containsKey(eachFileInIncoming.getKey())) {
                    allTodaysLogFileEntries.put(eachFileInIncoming.getKey(), eachFileInIncoming.getValue());
                }
            }

            //Traverse through the incoming queue data
            for (Map.Entry<String, MiniFileInfo> freshEntry : filesFromCurrentQueue.entrySet()) {
                freshFiles.add(freshEntry.getValue());
                if (allTodaysLogFileEntries.containsKey(freshEntry.getKey())) {
                    allTodaysLogFileEntries.put(freshEntry.getKey(), freshEntry.getValue());
                }
            }

            //Take max modified time for this cycle
            long maxTimeInCurrentCycle = filesFromCurrentQueue.values().stream().max(Comparator.comparing(MiniFileInfo::getModifiedTime)).get().modifiedTime;

            getLogger().info("Max time in current cycle : "+maxTimeInCurrentCycle);

            Map<String, MiniFileInfo> allEntries = new HashMap<>();
            allEntries.putAll(allTodaysLogFileEntries);
            allEntries.putAll(yesterDaysMiniFileMapFromFile);

            //Update timestamp for all those files whose status is "N" and whose modified time is less than the current time.
            List<MiniFileInfo> filesToBeTouched = allEntries
                    .values()
                    .stream()
                    .filter(mini -> (mini.getStatus().equalsIgnoreCase("N") || mini.getStatus().equalsIgnoreCase("T")) && mini.getModifiedTime() < maxTimeInCurrentCycle)
                    .map(mini -> mini.modifyTS(mini, maxTimeInCurrentCycle, olaFileDateFormatter.print(maxTimeInCurrentCycle)))
                    .collect(Collectors.toList());

            getLogger().info ("Files to be touched : "+ filesToBeTouched);
            filesToBeTouched
                    .stream()
                    .forEach(this::touchFile);

            //Update the log file accordingly
            for (MiniFileInfo touchedFile : filesToBeTouched) {
                if (allTodaysLogFileEntries.containsKey(touchedFile.getAbsolutePath())) {
                    MiniFileInfo miniFileInfo = allTodaysLogFileEntries.get(touchedFile.getAbsolutePath());
                    miniFileInfo.setModifiedTime(touchedFile.modifiedTime);
                    miniFileInfo.setStatus("T");
                    allTodaysLogFileEntries.put(miniFileInfo.getAbsolutePath(), miniFileInfo);
                }
            }

            FileUtils.writeLines(todayLogFile, allTodaysLogFileEntries.values());
            FileUtils.writeLines(new File(deltaLogPath, time + ".log"), freshFiles);

            session.remove(flowFiles);
            FlowFile flowFile = session.create();
            session.transfer(flowFile, REL_SUCCESS);

        }
        catch (Exception e) {
            session.transfer(flowFiles, REL_FAILURE);
            e.printStackTrace();
            throw new ProcessException(e.getMessage(), e);
        }

    }

    private Map<String, MiniFileInfo> constructMiniFileInfoFromLogFile(File logFile) throws IOException {
        Map<String, MiniFileInfo> logFileEntries = new HashMap<>();
        if (logFile.exists()) {
            getLogger().info("LogFile = " + logFile);
            logFileEntries = FileUtils
                    .readLines(logFile, Charset.forName("UTF-8"))
                    .stream()
                    .map(MiniFileInfo::constructFromString)
                    .collect(Collectors.toMap(MiniFileInfo::getAbsolutePath, Function.identity()));
        } else {
            getLogger().info("Log file does not exist. Pushing Fresh entries as log file entries");
        }
        return logFileEntries;
    }

    private void touchFile(MiniFileInfo mini) {
        File file = new File(mini.getAbsolutePath());
        if (file.exists()) {
            try {
                File tempFile = new File("/tmp", file.getName());
                FileUtils.copyFile(file, tempFile);

                Path tempPath = tempFile.toPath();
                FileTime fileTime = FileTime.fromMillis(System.currentTimeMillis());
                java.nio.file.Files.setLastModifiedTime(tempPath, fileTime);

                FileUtils.forceDelete(file);
                FileUtils.copyFile(tempFile, file);
                FileUtils.forceDelete(tempFile);
            } catch (IOException e) {
                getLogger().error(e.getMessage(), e);
            }
        }
    }

    public static class MiniFileInfo {
        private String absolutePath;
        private long modifiedTime;
        private String calendarDay;
        private String size;
        private String creationTimeS;
        private String modifiedTimeS;
        private String status;

        public MiniFileInfo() {
        }

        public MiniFileInfo(String absolutePath, long modifiedTime, String calendarDay, String size, String creationTimeS, String modifiedTimeS, String status) {
            this.absolutePath = absolutePath;
            this.modifiedTime = modifiedTime;
            this.calendarDay = calendarDay;
            this.size = size;
            this.creationTimeS = creationTimeS;
            this.modifiedTimeS = modifiedTimeS;
            this.status = status;
        }

        public String getAbsolutePath() {
            return absolutePath;
        }

        public void setAbsolutePath(String absolutePath) {
            this.absolutePath = absolutePath;
        }

        public long getModifiedTime() {
            return modifiedTime;
        }

        public void setModifiedTime(long modifiedTime) {
            this.modifiedTime = modifiedTime;
        }

        public String getCalendarDay() {
            return calendarDay;
        }

        public void setCalendarDay(String calendarDay) {
            this.calendarDay = calendarDay;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getCreationTimeS() {
            return creationTimeS;
        }

        public void setCreationTimeS(String creationTimeS) {
            this.creationTimeS = creationTimeS;
        }

        public String getModifiedTimeS() {
            return modifiedTimeS;
        }

        public void setModifiedTimeS(String modifiedTimeS) {
            this.modifiedTimeS = modifiedTimeS;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public static MiniFileInfo constructFromString(String miniFileLine) {
            String[] split = StringUtils.splitByWholeSeparatorPreserveAllTokens(miniFileLine, ",", -1);
            try {
                if (split.length == 7) {
                    return new MiniFileInfo(split[0], Long.parseLong(split[1]), split[2], split[3], split[4], split[5], split[6]);
                } else {
                    throw new ProcessException("Input line in file does not have seven tokens separated by a comma");
                }
            } catch (Exception e) {
                throw new ProcessException(e.getMessage(), e);
                }
        }

        static MiniFileInfo constructFromFile(File file, String date, DateTimeFormatter olaFileDateFormatter) {
            MiniFileInfo fileInfo = new MiniFileInfo();
            Path path = file.toPath();
            try {
                BasicFileAttributes fileAttributes = Files.readAttributes(path, BasicFileAttributes.class);
                fileInfo = new MiniFileInfo(
                        file.getAbsolutePath(),
                        file.lastModified(),
                        date,
                        String.valueOf(file.length()),
                        olaFileDateFormatter.print(fileAttributes.creationTime().toMillis()),
                        olaFileDateFormatter.print(file.lastModified()),
                        "N");
            } catch (Exception e) {
                throw new ProcessException(e.getMessage(), e);
            }
            return fileInfo;
        }


        public MiniFileInfo modifyTS(MiniFileInfo mini, long modifiedTime, String modifiedTimeS) {

            return new MiniFileInfo(
                    mini.absolutePath,
                    modifiedTime,
                    mini.calendarDay,
                    mini.size,
                    mini.creationTimeS,
                    modifiedTimeS,
                    mini.status
            );

        }

        @Override
        public String toString() {
            return absolutePath + "," + modifiedTime + "," + calendarDay + "," + size + "," + creationTimeS + "," + modifiedTimeS + "," + status;
        }
    }
}