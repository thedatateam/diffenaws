package in.tdt.diffen.processors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.serialization.DateTimeUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

import static in.tdt.diffen.processors.CSVUtils.*;
import static in.tdt.diffen.processors.NifiAttributesConstants.*;


@ReadsAttributes({ @ReadsAttribute(attribute = "expected.column.count", description = "Expected column count.") })
@WritesAttributes({ @WritesAttribute(attribute = "lastrow.column.count", description = "Actual column count."),
        @WritesAttribute(attribute = "total.row.count", description = "Actual row count file."),
        @WritesAttribute(attribute = "b.row.count", description = "Actual row count file.") })
@SideEffectFree
@SupportsBatching
@Tags({ "csv", "stats" })
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
public class CalculateFileStats extends AbstractProcessor {

    public static final Relationship REL_SUCCESS = new Relationship.Builder().name("success")
            .description("Fetched file stats successfully.").build();

    public static final Relationship REL_FAILURE = new Relationship.Builder().name("failure")
            .description("Failed to fetch file stats.").build();

    public static final int CDC_RECORD_TYPE_POISITION = 2;
    public static final String CDC_IGNORE_RECORD_TYPE = "B";

    @Override
    protected void init(ProcessorInitializationContext context) {
        super.init(context);
    }

    Set<Relationship> relationships = new HashSet<>();

    @Override
    public Set<Relationship> getRelationships() {
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        return relationships;
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        final List<PropertyDescriptor> properties = new ArrayList<>(super.getSupportedPropertyDescriptors());
        properties.add(DateTimeUtils.DATE_FORMAT);
        properties.add(DateTimeUtils.TIME_FORMAT);
        properties.add(DateTimeUtils.TIMESTAMP_FORMAT);
        properties.add(CSV_FORMAT);
        properties.add(VALUE_SEPARATOR);
        properties.add(SKIP_HEADER_LINE);
        properties.add(QUOTE_CHAR);
        properties.add(ESCAPE_CHAR);
        properties.add(COMMENT_MARKER);
        properties.add(NULL_STRING);
        properties.add(TRIM_FIELDS);
        properties.add(QUOTE_MODE);
        properties.add(RECORD_SEPARATOR);
        properties.add(TRAILING_DELIMITER);
        return properties;
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }
        Map<String, String> flowFileAttributes = flowFile.getAttributes();
        int expectedColumnCount = Integer.parseInt(flowFileAttributes.get(EXPECTED_COLUMN_COUNT_ATTRIBUTE));

        CSVFormat csvFormat = createCSVFormat(context);

        Map<String, String> fileStatAttributes = null;
        try {
            fileStatAttributes = getFileStats(flowFile, session, csvFormat, expectedColumnCount);
        } catch (IOException e) {
            getLogger().error("Failed to fetch row and column count {}", new Object[] {flowFile, e});
            session.transfer(flowFile, REL_FAILURE);
            return;
        }
        session.transfer(session.putAllAttributes(flowFile, fileStatAttributes), REL_SUCCESS);
    }

    private Map<String, String> getFileStats(final FlowFile flowFile,
                                             final ProcessSession session,
                                             final CSVFormat csvFormat,
                                             final int expectedColumnCount) throws ProcessException, IOException {
        Map<String, String> attMap = new HashMap<>();
        try (InputStream is = session.read(flowFile);
             BufferedReader isReader = new BufferedReader(new InputStreamReader(is))
        ) {
            CSVParser csvParser = csvFormat.parse(isReader);

            int lastBadColumnCount = -1;
            long bRowCount = 0;
            long rowCount = 0;
            int recordSize = -1;
            for (CSVRecord csvRecord : csvParser) {
                recordSize = csvRecord.size();
                if (recordSize != expectedColumnCount) {
                    lastBadColumnCount = recordSize;
                }
                if(recordSize - 1 >= CDC_RECORD_TYPE_POISITION &&
                        csvRecord.get(CDC_RECORD_TYPE_POISITION).equalsIgnoreCase(CDC_IGNORE_RECORD_TYPE)) {
                    bRowCount++;
                } else {
                    rowCount++;
                }
            }

            if(lastBadColumnCount > 0) recordSize = lastBadColumnCount;

            attMap.put(ACTUAL_COLUMN_COUNT_ATTRIBUTE, String.valueOf(recordSize));
            attMap.put(B_ROW_COUNT_ATTRIBUTE, String.valueOf(bRowCount));
            attMap.put(ACTUAL_ROW_COUNT_ATTRIBUTE, String.valueOf(rowCount));
            return attMap;

        } catch (IOException e) {
            throw e;
        }
    }
}
