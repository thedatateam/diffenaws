/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.verifytypes.DiffenUtils;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.util.StopWatch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static in.tdt.diffen.processors.RowIDAssignment.ROW_METADATA_ADDED;
import static in.tdt.diffen.processors.RowIDAssignment.TOTAL_ROW_COUNT;
import static org.apache.nifi.processor.util.StandardValidators.NON_EMPTY_VALIDATOR;

@Tags({"rowid assignment"})
@CapabilityDescription("Prepends a row ID to every row of data in the incoming flow file content")
@SideEffectFree
@SupportsBatching
@ReadsAttributes({@ReadsAttribute(attribute = "tablename", description = "The table that is being processed"),
                  @ReadsAttribute(attribute = "expected.column.count", description = "The column count for the table")})
@WritesAttributes({
        @WritesAttribute(attribute = TOTAL_ROW_COUNT, description = "This is count of the total number of rows that " +
                                                                    "were processed from the flowfile content"),
        @WritesAttribute(attribute = ROW_METADATA_ADDED, description = "A boolean indicating if the ROWID was added to " +
                                                                 "the flow file(s) from emitted from this processor.")})
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
/**
 * This class is used to populate row id for each row and Prepends to it.
 */
public class RowIDAssignment extends AbstractProcessor {

    static final PropertyDescriptor INPUT_DELIMITER = new PropertyDescriptor.Builder()
            .name("input-delimiter")
            .displayName("Input delimiter")
            .description("Delimiter character for input columns in each record")
            .addValidator(NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .defaultValue("\u0001")
            .build();

    static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("Success relationship")
            .build();


    static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Failure relationship")
            .build();

    private static List<PropertyDescriptor> DESCRIPTORS;
    private static final Set<Relationship> RELATIONSHIPS;
    static final String TOTAL_ROW_COUNT = "total.row.count";
    static final String ROW_METADATA_ADDED = "row.metadata.added";

    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(INPUT_DELIMITER);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        RELATIONSHIPS = Collections.unmodifiableSet(relationships);
    }


    @Override
    public Set<Relationship> getRelationships() {
        return RELATIONSHIPS;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return DESCRIPTORS;
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        final StopWatch stopWatch = new StopWatch(true);
        final char inputDelimiter = DiffenUtils.getDelimiter(context.getProperty(INPUT_DELIMITER).evaluateAttributeExpressions(flowFile).getValue());

        try {
            AtomicReference<Long> totalRowCount = new AtomicReference<>();
            AtomicReference<ArrayList<String>> provEvents = new AtomicReference<>();

            flowFile = session.write(flowFile, (rawIn, rawOut) -> {

                try (final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(rawOut));
                     final BufferedReader reader = new BufferedReader(new InputStreamReader(rawIn))) {
                    long rowcount = 0;
                    String line;
                    ArrayList<String> rowIds = new ArrayList<>();
                    while((line = reader.readLine()) != null) {
                        rowcount++;
                        final String rowId = System.nanoTime() +
                        					 "_"	+
                        					 UUID.randomUUID().toString();

                        writer.write(rowId + inputDelimiter + line);
                        writer.newLine();
                        rowIds.add(rowId);

                    }
                    provEvents.set(rowIds);
                    totalRowCount.set(rowcount);
                    writer.flush();
                }
            });
            stopWatch.stop();
            flowFile = session.putAttribute(flowFile, TOTAL_ROW_COUNT,totalRowCount.get().toString());
            flowFile = session.putAttribute(flowFile, ROW_METADATA_ADDED,"true");
            final String tableName = flowFile.getAttribute("tablename");
            for(final String rowId : provEvents.get()){
                final String provEvent = rowId +"," + tableName;
                session.getProvenanceReporter().modifyContent(flowFile, provEvent);
            }
            session.getProvenanceReporter().modifyContent(flowFile, stopWatch.getDuration(TimeUnit.MILLISECONDS));
            session.transfer(flowFile, REL_SUCCESS);
        } catch (final ProcessException e) {
            session.transfer(flowFile, REL_FAILURE);
            throw e;
        }
    }
}
