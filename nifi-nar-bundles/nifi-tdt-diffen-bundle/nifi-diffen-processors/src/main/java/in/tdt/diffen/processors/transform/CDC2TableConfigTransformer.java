package in.tdt.diffen.processors.transform;

import com.google.common.collect.Lists;
import in.tdt.diffen.cdc.CDCModels;
import in.tdt.diffen.cdc.CDCXmlParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.logging.LogLevel;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static in.tdt.diffen.verifytypes.DiffenConstants.*;

public class CDC2TableConfigTransformer implements SourceTableConfigTransformer {

    @Override
    public Map<String, CDCModels.TableConfiguration> transform(Collection<File> sourceConfigFiles, Map<String, String> attributes, ComponentLog logger) {

        final File keyColumnListPath = new File(attributes.get("key-column-list-path"));
        final File sourceTypePath = new File(attributes.get("source-type-path"));
        final File transformationRulesPath = (attributes.get("transformation-rules-path") != null) ? new File(attributes.get("transformation-rules-path")) : null;
        final File dataTypeMappingPath = new File(attributes.get("datatype-mapping-path"));

        Map<String, CDCModels.TableConfiguration> tableConfigurations = new HashMap<>();
        try {
            //Fetch keycols info
            List<String> keyColFile = FileUtils.readLines(keyColumnListPath);
            keyColFile.removeIf(line -> StringUtils.isEmpty(line));

            Map<String, String> keyColList = keyColFile
                    .stream()
                    .map(line -> new CDCModels.TableKeyColInfo(StringUtils.substringBefore(line, "->").trim(), StringUtils.substringAfter(line, "->").trim()))
                    .collect(Collectors.toMap(CDCModels.TableKeyColInfo::getTableName, CDCModels.TableKeyColInfo::getKeyCols));

            //Fetch source type info
            List<String> tableTypeFile = FileUtils.readLines(sourceTypePath);
            tableTypeFile.removeIf(line -> StringUtils.isEmpty(line));

            Map<String, String> sourceTypeList = tableTypeFile
                    .stream()
                    .map(line -> line.split("->")).map(x -> new CDCModels.TableSourceInfo(x[0].trim(), x[1].trim()))
                    .collect(Collectors.toMap(CDCModels.TableSourceInfo::getTableName, CDCModels.TableSourceInfo::getTypee));

            Map<String, List<CDCModels.ColTransformationRule>> transColList = new HashMap<>();

            //Fetch transformation rules info
            if (transformationRulesPath != null && transformationRulesPath.exists()) {
                List<String> transColFile = FileUtils.readLines(transformationRulesPath);
                transColFile.removeIf(line -> StringUtils.isEmpty(line));
                transColList = transColFile.stream()
                        .map(line -> line.split("->"))
                        .map(x -> new CDCModels.TableTransformationRule(x[0].trim(), Arrays.stream(x[1].split("\\|"))
                                .map(each -> new CDCModels.ColTransformationRule(each))
                                .collect(Collectors.toList())))
                        .collect(Collectors.toMap(CDCModels.TableTransformationRule::getTableName, CDCModels.TableTransformationRule::getRulesList));
            }

            List<String> dataTypeMappingList = FileUtils.readLines(dataTypeMappingPath);
            dataTypeMappingList.removeIf(line -> StringUtils.isEmpty(line));
            Map<String, CDCModels.TableDataTypeMapping> dataTypeMap = new HashMap<>();

            for (String eachDMappingString : dataTypeMappingList) {
                String[] strings = eachDMappingString.split("->");
                if (strings.length > 2) {
                    dataTypeMap.put(strings[0].trim(), new CDCModels.TableDataTypeMapping(strings[0], strings[1], Boolean.parseBoolean(strings[2])));
                } else {
                    dataTypeMap.put(strings[0].trim(), new CDCModels.TableDataTypeMapping(strings[0], strings[1], false));
                }
            }

            for (File sourceConfigFile : sourceConfigFiles) {
                logger.debug("Processing " + sourceConfigFile.getAbsolutePath());

                //Fetch source XML and convert it into NodeList
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(sourceConfigFile);
                NodeList tableMappings = doc.getElementsByTagName("TableMapping");

                tableConfigurations.putAll(getTableProperties(tableMappings, keyColList, sourceTypeList, transColList, dataTypeMap, attributes.get(SOURCE), attributes.get(COUNTRY)));
            }
        } catch (final Exception e) {
            e.printStackTrace();
            logger.error("Error while converting CDC XML to Tableconfig xml : ", e);
        }
        return tableConfigurations;
    }

    private Map<String, CDCModels.TableConfiguration> getTableProperties(NodeList tableMappings, Map<String, String> keyColList, Map<String, String> sourceTypeList, Map<String, List<CDCModels.ColTransformationRule>> transColList, Map<String, CDCModels.TableDataTypeMapping> dataTypeMap, String source, String country) {

        Map<String, CDCModels.TableConfiguration> tableConfigs = new HashMap<>();
        for (int count = 0; count < tableMappings.getLength(); count++) {
            List<CDCModels.Property> properties = new ArrayList<>();
            Node tableMapping = tableMappings.item(count);
            String tableName = source + "_" + country + "_" + CDCXmlParser.getXmlAttribute(tableMapping, "sourceTableName").toLowerCase();
            Element tableMapElem = (Element) tableMapping;
            NodeList sourceColumns = tableMapElem.getElementsByTagName("SourceColumn");

            List<CDCModels.ColTransformationRule> rulesList = transColList.containsKey(tableName) ? transColList.get(tableName) : new ArrayList<>();

            String keyPrefix = DEFAULT_PARAM_PREFIX + ADD_PREFIX_CONFIG_DIFFEN_TABLE + tableName;
            StringBuilder builder = new StringBuilder();
            for (int colCount = 0; colCount < sourceColumns.getLength(); colCount++) {
                Node sourceCol = sourceColumns.item(colCount);
                String schema = CDCXmlParser.getColumn(sourceCol, rulesList, dataTypeMap, true);
                if (null != schema) {
                    builder.append(schema).append("^");
                }
            }
            String colSchema = StringUtils.removeEnd(builder.toString(), "^");
            properties.addAll(Lists.newArrayList(new CDCModels.Property(keyPrefix + SUFFIX_CONFIG_COLSCHEMA, colSchema),
                    new CDCModels.Property(keyPrefix + SUFFIX_CONFIG_SOURCETYPE, sourceTypeList.get(tableName)),
                    new CDCModels.Property(keyPrefix + SUFFIX_CONFIG_KEYCOLS, keyColList.get(tableName))));
            properties.removeIf(property -> StringUtils.isEmpty(property.getValue()));
            Collections.sort(properties);
            tableConfigs.put(tableName, new CDCModels.TableConfiguration(properties));

        }
        return tableConfigs;
    }
}
