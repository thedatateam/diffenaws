/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.services.TransformationRegistry;
import in.tdt.diffen.verifytypes.DiffenUtils;
import in.tdt.diffen.verifytypes.checker.DateTypeValidationResult;
import in.tdt.diffen.verifytypes.checker.VerifySchemaColumn;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.serialization.DateTimeUtils;
import org.apache.nifi.util.StopWatch;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static in.tdt.diffen.processors.CSVUtils.*;
import static in.tdt.diffen.processors.NifiAttributesConstants.*;
import static in.tdt.diffen.verifytypes.DiffenConstants.TIME_ZONE;
import static org.apache.nifi.processor.util.StandardValidators.NON_EMPTY_VALIDATOR;

@Tags({"data verification"})
@CapabilityDescription("Verifies the data types in each column. If the data is not valid it is routed to either an Error " +
        "Type or Warn Type relationship. If the row is not valid, the table name is preprended to it and " +
        "the error or messages are appended to it. This processor assumes that the ROWID has already been " +
        "added to the source data, thus it will add a ROWID to the table schema, before parsing.")
@SideEffectFree
@SupportsBatching
@ReadsAttributes({@ReadsAttribute(attribute = "tablename", description = "The table that is being processed"),
        @ReadsAttribute(attribute = "date", description = "The date from the D part of the file"),
        @ReadsAttribute(attribute = "time", description = "The time from the T part of the file"),
        @ReadsAttribute(attribute = "source", description = "The source system the data is from"),
        @ReadsAttribute(attribute = "country", description = "The country the data is from"),
        @ReadsAttribute(attribute = "row.metadata.added", description = "An idicator if the incoming flow file " +
                "has had a ROW ID pre-prended to the data.")
})
@WritesAttributes({@WritesAttribute(attribute = INVALID_TYPES_WARN_RECORD_COUNT_TOTAL,
        description = "The count of records with type warnings"),
        @WritesAttribute(attribute = INVALID_TYPES_WARN_RECORD_COUNT_COLUMN,
                description = "The count of columns with a type warnings"),
        @WritesAttribute(attribute = INVALID_TYPES_ERROR_RECORD_COUNT_TOTAL,
                description = "The count of records with type errors"),
        @WritesAttribute(attribute = INVALID_TYPES_ERROR_RECORD_COUNT_COLUMN,
                description = "The count of columns with a type error"),
        @WritesAttribute(attribute = INVALID_TYPES_SUCCESS_RECORD_COUNT,
                description = "The count of records with no errors"),
        @WritesAttribute(attribute = INVALID_TYPES_TOTAL_RECORD_COUNT,
                description = "The total number of records processed"),
})
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
/**
 * This class is used to verify the data type of each record  </n>
 * if any data type is mismatching,the input records will be moved either error or warn </n>
 * Get schema from controller service for each table and verifies the type of the data
 */
public class DataTypeVerificationV2 extends AbstractProcessor {

    // Attributes
    static final String INVALID_TYPES_WARN_RECORD_COUNT_TOTAL = "invalidtypes.warn.record.count.total";
    static final String INVALID_TYPES_WARN_RECORD_COUNT_COLUMN = "invalidtypes.warn.record.count.columns";
    static final String INVALID_TYPES_ERROR_RECORD_COUNT_TOTAL = "invalidtypes.error.record.count.total";
    static final String INVALID_TYPES_ERROR_RECORD_COUNT_COLUMN = "invalidtypes.error.record.count.columns";
    static final String INVALID_TYPES_SUCCESS_RECORD_COUNT = "invalidtypes.valid.record.count";
    static final String INVALID_TYPES_TOTAL_RECORD_COUNT = "invalidtypes.total.record.count";

    static final PropertyDescriptor REGISTRY_SERVICE = new PropertyDescriptor.Builder()
            .name("configuration-registry-service")
            .displayName("Conifguration Registry Service")
            .description("The Schema Registry Service for serializing/deserializing messages as well as schema retrieval.")
            .required(true)
            .identifiesControllerService(TransformationRegistry.class)
            .build();

    static final PropertyDescriptor JOURNAL_TS_FORMAT = new PropertyDescriptor.Builder()
            .name("journal-timestamp-format")
            .displayName("Journal TS date format")
            .description("Date format of CDC Jounral timestamp")
            .required(false)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .addValidator(NON_EMPTY_VALIDATOR)
            .defaultValue("yyyy-MM-dd HH:mm:ss")
            .build();

    static final PropertyDescriptor CHARSET = new PropertyDescriptor
            .Builder().name("Input Charset")
            .description("The input character set")
            .required(false)
            .defaultValue("UTF-8")
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .addValidator(NON_EMPTY_VALIDATOR)
            .build();


    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("Success")
            .description("Successfully validated types are sent to this relationship")
            .build();

    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("Failure")
            .description("FlowFiles that experienced general failure are sent to this relationship")
            .build();

    public static final Relationship REL_ERROR_TYPE_FAILURE = new Relationship.Builder()
            .name("ErrorType")
            .description("FlowFiles that failed type validation are sent to this relationship")
            .build();

    public static final Relationship REL_WARN_TYPE_FAILURE = new Relationship.Builder()
            .name("WarnType")
            .description("FlowFiles that had warnings from type validation are sent to this relationship")
            .build();

    public static final Relationship REL_ROW_COUNTS = new Relationship.Builder()
            .name("RowCounts")
            .description("Rowcounts of Verify, Warn and Invalid types get into this relationship")
            .build();

    private static List<PropertyDescriptor> DESCRIPTORS;
    private final char INVALID_TYPE_DATA_DELIMITER = DiffenUtils.getDelimiter("\\u0002");

    @Override
    public Set<Relationship> getRelationships() {
        Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        relationships.add(REL_ERROR_TYPE_FAILURE);
        relationships.add(REL_WARN_TYPE_FAILURE);
        relationships.add(REL_ROW_COUNTS);
        return relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(REGISTRY_SERVICE);
        descriptors.add(JOURNAL_TS_FORMAT);
        descriptors.add(CHARSET);
        descriptors.add(DateTimeUtils.DATE_FORMAT);
        descriptors.add(DateTimeUtils.TIME_FORMAT);
        descriptors.add(DateTimeUtils.TIMESTAMP_FORMAT);
        descriptors.add(CSV_FORMAT);
        descriptors.add(VALUE_SEPARATOR);
        descriptors.add(SKIP_HEADER_LINE);
        descriptors.add(QUOTE_CHAR);
        descriptors.add(ESCAPE_CHAR);
        descriptors.add(COMMENT_MARKER);
        descriptors.add(NULL_STRING);
        descriptors.add(TRIM_FIELDS);
        descriptors.add(QUOTE_MODE);
        descriptors.add(RECORD_SEPARATOR);
        descriptors.add(TRAILING_DELIMITER);
        return descriptors;
    }

    private volatile TransformationRegistry transformationRegistry;

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        this.transformationRegistry = context.getProperty(REGISTRY_SERVICE)
                .asControllerService(TransformationRegistry.class);

        final StopWatch stopWatch = new StopWatch(true);
        final String tableName = getIncomingAttribute(flowFile, "tablename");
        final String source = getIncomingAttribute(flowFile, "source");
        final String country = getIncomingAttribute(flowFile, "country");
        final boolean hasHeader = getIncomingAttribute(flowFile, "has.header")
                .equalsIgnoreCase("true");
        final String timeZoneID = getIncomingAttribute(flowFile, TIME_ZONE);
        final String inputCharset = context.getProperty(CHARSET).evaluateAttributeExpressions().getValue();

        final List<VerifySchemaColumn> schemaColumns = new ArrayList<>();
        //read column directly from table config xml
        schemaColumns
                .addAll(this.transformationRegistry
                        .getVerifySchemaColumnsForTable(tableName));

        final CSVFormat csvFormat = hasHeader ?
                createCSVFormat(context).withFirstRecordAsHeader() : createCSVFormat(context);


        boolean rowMetadataHasBeenAdded = false;
        final String rowMetadataAdded = flowFile.getAttribute("row.metadata.added");
        if (null != rowMetadataAdded) {
            rowMetadataHasBeenAdded = Boolean.parseBoolean(rowMetadataAdded);
        }

        final int firstDataColumnPosition = this.transformationRegistry.getFirstDataColumnPosition();
        final int journalTimeStampPosition = rowMetadataHasBeenAdded ? 3 : 0;
        final int processingColumnCount = rowMetadataHasBeenAdded ? firstDataColumnPosition + 3 : firstDataColumnPosition;

        final DateTimeFormatter formatter = DateTimeFormat.forPattern(context.getProperty(JOURNAL_TS_FORMAT)
                .evaluateAttributeExpressions(flowFile).getValue()).withZone(DateTimeZone.forID(timeZoneID));
        final DateTime currentDateTime = DateTime.now(DateTimeZone.forID(timeZoneID));
        final String time = formatter.print(currentDateTime);

        final AtomicBoolean processingFailure = new AtomicBoolean(false);
        final AtomicInteger recordCount = new AtomicInteger(0);
        final AtomicInteger successfulRecordCount = new AtomicInteger(0);
        final AtomicInteger errorTypeRecordCount = new AtomicInteger(0);
        final AtomicInteger errorTypeColumnCount = new AtomicInteger(0);
        final AtomicInteger warnTypeRecordCount = new AtomicInteger(0);
        final AtomicInteger warnTypeColumnCount = new AtomicInteger(0);
        final FlowFile inputFlowFile = flowFile;

        // Create output flow files
        AtomicReference<FlowFile> successFlowFile = new AtomicReference<>(session.create(inputFlowFile));
        AtomicReference<FlowFile> errorTypeFlowFile = new AtomicReference<>(session.create(inputFlowFile));
        AtomicReference<FlowFile> warnTypeFlowFile = new AtomicReference<>(session.create(inputFlowFile));
        AtomicReference<FlowFile> rowCountsFlowFile = new AtomicReference<>(session.create(inputFlowFile));

        try {
            final String errorWarnPostAmble = time;

            Map<String, String> updatedAttrs = new HashMap<>();
            session.read(inputFlowFile, in -> {
                try (final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                ) {

                    CSVParser csvParser = csvFormat.parse(reader);

                    for (CSVRecord record : csvParser) {
                        recordCount.incrementAndGet();
                        StringBuilder error = new StringBuilder();
                        StringBuilder warn = new StringBuilder();
                        StringBuilder errorColVal = new StringBuilder();
                        StringBuilder warnColVal = new StringBuilder();
                        String columnValue = "";

                        if (record.size() != schemaColumns.size() + processingColumnCount) {
                            error.append("ERROR : Unexpected number of columns. Expected: " + schemaColumns.size()
                                    + "; received: " + (record.size() - processingColumnCount));
                            errorColVal.append(columnValue); }
                        else {
                            //Check the JournalTimeStamp to make sure this record is not late, JournalTimeStamp is column 1 when ROWID is set.
                            try {
                                if (this.transformationRegistry.isCDC()) {
                                    columnValue = record.get(journalTimeStampPosition);
                                    formatter.parseDateTime(columnValue);
                                }
                            } catch (IllegalArgumentException e){
                                getLogger().error(e.getMessage(), e);
                                String errorMessage = "ERROR: INCORRECT JOURNAL TIMESTAMP FORMAT";
                                errorTypeColumnCount.incrementAndGet();
                                if (error.length() == 0) {
                                    error.append(errorMessage);
                                    errorColVal.append(columnValue);
                                } else {
                                    error.append(INVALID_TYPE_DATA_DELIMITER).append(errorMessage);
                                    errorColVal.append(INVALID_TYPE_DATA_DELIMITER).append(columnValue);
                                }
                            }
                        }

                        synchronized (this) {
                            for (int i =0; i <schemaColumns.size(); i ++) {
                                VerifySchemaColumn vsc = schemaColumns.get(i);
                                if (vsc.isDrop()) {
                                    continue;
                                }
                                columnValue = record.get(i+ processingColumnCount);
                                DateTypeValidationResult validationResult = vsc.verify(columnValue);

                                switch (validationResult.getResultType()) {
                                    case ERROR:
                                        errorTypeColumnCount.incrementAndGet();
                                        if (error.length() == 0) {
                                            error.append(validationResult.getErrorMsg());
                                            String strippedData = StreamSupport.stream(record.spliterator(), false)
                                                    .map(Object::toString)
                                                    .collect(Collectors.joining(String.valueOf(INVALID_TYPE_DATA_DELIMITER)));
                                            errorColVal.append(columnValue + "(" + strippedData + ")");
                                        } else {
                                            error.append(INVALID_TYPE_DATA_DELIMITER).append(validationResult.getErrorMsg());
                                            errorColVal.append(INVALID_TYPE_DATA_DELIMITER).append(columnValue);
                                        }
                                        break;

                                    case WARN:
                                        //NEED TO ALSO SEND AS VERIFIED
                                        warnTypeColumnCount.incrementAndGet();
                                        columnValue = validationResult.getOriginalValue();
                                        if (warn.length() == 0) {
                                            warn.append(validationResult.getErrorMsg());
                                            warnColVal.append(columnValue);
                                        } else {
                                            warn.append(INVALID_TYPE_DATA_DELIMITER).append(validationResult.getErrorMsg());
                                            warnColVal.append(INVALID_TYPE_DATA_DELIMITER).append(columnValue);
                                        }
                                        break;
                                    case GOOD:
                                        columnValue = validationResult.getOriginalValue();
                                        break;
                                }
                            }
                        }


                        //now check to see the status of success, error, and warn and write out the correct flow.

                        //If Error or Warning then send to the rel and the format looks like:
                        // table <SEP> rowid <SEP> row_data <SEP> error_message <SEP> D and T concatenated from filename
                        if (0 < error.length()) {
                            errorTypeRecordCount.incrementAndGet();
                            List<Object> values = new ArrayList<>();
                            values.add(source);
                            values.add(country);
                            values.add(tableName);
                            values.add(record.get(0));
                            values.add(errorColVal.toString());
                            values.add(error.toString());
                            values.add(errorWarnPostAmble);
                            appendRecordsToFlowFile(session, values, errorTypeFlowFile, csvFormat);
                        } else {
                            if (0 < warn.length()) {
                                warnTypeRecordCount.incrementAndGet();
                                List<Object> values = new ArrayList<>();
                                values.add(source);
                                values.add(country);
                                values.add(tableName);
                                values.add(record.get("rowid"));
                                values.add(warnColVal.toString());
                                values.add(warn.toString());
                                values.add(errorWarnPostAmble);
                                appendRecordsToFlowFile(session, values, warnTypeFlowFile, csvFormat);
                            }

                            //Warn records still go down the valid path
                            successfulRecordCount.incrementAndGet();
                            appendRecordsToFlowFile(session,
                                    StreamSupport.stream(record.spliterator(), false).collect(Collectors.toList()),
                                    successFlowFile, csvFormat);
                        }

                    }

                } catch (RuntimeException runtimeException) {
                    //This may be thrown from the underlying Hive Library used to do the type verification.
                    getLogger().error(runtimeException.getMessage(), runtimeException);
                    processingFailure.set(true);
                }

            });
            stopWatch.stop();

            if (0 < recordCount.get()) {
                if (0 < successfulRecordCount.get()) {

                    successFlowFile.set(
                            session.putAttribute(successFlowFile.get(), INVALID_TYPES_SUCCESS_RECORD_COUNT,
                                    Integer.toString(successfulRecordCount.get())));

                    successFlowFile.set(
                            session.putAttribute(successFlowFile.get(), INVALID_TYPES_TOTAL_RECORD_COUNT,
                                    Integer.toString(recordCount.get())));

                    successFlowFile.set(
                            session.putAllAttributes(successFlowFile.get(), updatedAttrs)
                    );

                    session.getProvenanceReporter().modifyContent(successFlowFile.get(), stopWatch.getDuration(TimeUnit.MILLISECONDS));
                    session.transfer(successFlowFile.get(), REL_SUCCESS);
                } else {
                    session.remove(successFlowFile.get());
                }
                if (0 < errorTypeRecordCount.get()) {

                    errorTypeFlowFile.set(
                            session.putAttribute(errorTypeFlowFile.get(), INVALID_TYPES_ERROR_RECORD_COUNT_TOTAL,
                                    Integer.toString(errorTypeRecordCount.get())));
                    errorTypeFlowFile.set(
                            session.putAttribute(errorTypeFlowFile.get(), INVALID_TYPES_ERROR_RECORD_COUNT_COLUMN,
                                    Integer.toString(errorTypeColumnCount.get())));

                    errorTypeFlowFile.set(
                            session.putAttribute(errorTypeFlowFile.get(), INVALID_TYPES_TOTAL_RECORD_COUNT,
                                    Integer.toString(recordCount.get())));

                    session.getProvenanceReporter().modifyContent(errorTypeFlowFile.get(), stopWatch.getDuration(TimeUnit.MILLISECONDS));
                    session.transfer(errorTypeFlowFile.get(), REL_ERROR_TYPE_FAILURE);
                } else {
                    session.remove(errorTypeFlowFile.get());
                }
                if (0 < warnTypeRecordCount.get()) {
                    warnTypeFlowFile.set(
                            session.putAttribute(warnTypeFlowFile.get(), INVALID_TYPES_WARN_RECORD_COUNT_TOTAL,
                                    Integer.toString(warnTypeRecordCount.get())));
                    warnTypeFlowFile.set(
                            session.putAttribute(warnTypeFlowFile.get(), INVALID_TYPES_WARN_RECORD_COUNT_COLUMN,
                                    Integer.toString(warnTypeColumnCount.get())));
                    warnTypeFlowFile.set(
                            session.putAttribute(warnTypeFlowFile.get(), INVALID_TYPES_TOTAL_RECORD_COUNT,
                                    Integer.toString(recordCount.get())));

                    session.getProvenanceReporter().modifyContent(warnTypeFlowFile.get(), stopWatch.getDuration(TimeUnit.MILLISECONDS));
                    session.transfer(warnTypeFlowFile.get(), REL_WARN_TYPE_FAILURE);
                } else {
                    session.remove(warnTypeFlowFile.get());
                }

                rowCountsFlowFile.set(session.putAttribute(rowCountsFlowFile.get(), INVALID_TYPES_WARN_RECORD_COUNT_TOTAL,
                        Integer.toString(warnTypeRecordCount.get())));
                rowCountsFlowFile.set(session.putAttribute(rowCountsFlowFile.get(), INVALID_TYPES_ERROR_RECORD_COUNT_TOTAL,
                        Integer.toString(errorTypeRecordCount.get())));
                rowCountsFlowFile.set(session.putAttribute(rowCountsFlowFile.get(), INVALID_TYPES_SUCCESS_RECORD_COUNT,
                        Integer.toString(successfulRecordCount.get())));
                rowCountsFlowFile.set(session.putAttribute(rowCountsFlowFile.get(), INVALID_TYPES_TOTAL_RECORD_COUNT,
                        Integer.toString(recordCount.get())));
                session.transfer(rowCountsFlowFile.get(), REL_ROW_COUNTS);

            } else {
                // No records were processed, so remove the output flow files
                session.remove(successFlowFile.get());
                session.remove(errorTypeFlowFile.get());
                session.remove(warnTypeFlowFile.get());
                session.remove(rowCountsFlowFile.get());

            }

            successFlowFile.set(null);
            errorTypeFlowFile.set(null);
            warnTypeFlowFile.set(null);
            rowCountsFlowFile.set(null);


            if (processingFailure.get()) {
                session.remove(successFlowFile.get());
                session.remove(warnTypeFlowFile.get());
                session.remove(errorTypeFlowFile.get());
                session.remove(rowCountsFlowFile.get());
                session.transfer(inputFlowFile, REL_FAILURE);
            } else {
                session.remove(flowFile);
            }

        } catch (final ProcessException e) {
            session.remove(successFlowFile.get());
            session.remove(warnTypeFlowFile.get());
            session.remove(errorTypeFlowFile.get());
            session.remove(rowCountsFlowFile.get());
            session.transfer(flowFile, REL_FAILURE);
            throw e;
        }
    }

    /**
     * Get input attribute from flow file
     * @param flowFile
     * @param incomingAttributeName
     * @return
     */
    private String getIncomingAttribute(FlowFile flowFile, String incomingAttributeName) {
        final String attrName = flowFile.getAttribute(incomingAttributeName);
        if (null == attrName) {
            throw new ProcessException("Missing attribute " + incomingAttributeName + " on incoming flowfile");
        }
        return attrName;
    }

    private void appendRecordsToFlowFile(ProcessSession session,
                                         List<Object> values,
                                         AtomicReference<FlowFile> appendFlowFile,
                                         CSVFormat csvFormat
    ) {
        appendFlowFile.set(session.append(appendFlowFile.get(), (out) -> {
            try(final PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
                final CSVPrinter csvPrinter = csvFormat.print(writer);) {
                csvPrinter.printRecord(values);
            }
        }));

    }
}
