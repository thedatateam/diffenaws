/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.services.TransformationRegistry;
import in.tdt.diffen.verifytypes.DiffenUtils;
import in.tdt.diffen.verifytypes.checker.DateTypeValidationResult;
import in.tdt.diffen.verifytypes.checker.VerifySchemaColumn;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.StreamCallback;
import org.apache.nifi.processor.util.StandardValidators;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static in.tdt.diffen.verifytypes.DiffenConstants.*;

@Tags({"Flat File Data Extractor", "FlatFile Data", "Header", "Trailer"})
@CapabilityDescription("Extracts header, trailer and data from flow file and sets header and footer info as attributes")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = "tablename", description = "The table that is being processed"),
        @ReadsAttribute(attribute = "absolute.path", description = "Absolute path for the table that is being processed"),
        @ReadsAttribute(attribute = "control_file", description = "Specifies the Control file reading property(Ex: true (or) false)"),
})
@WritesAttributes({@WritesAttribute(attribute = "Rowcount", description = "Actual Rowcount of the flowfile is being processed"),
        @WritesAttribute(attribute = "Header_data", description = "Actual Header Data to be loaded into Recon-Table of the flowfile is being processed"),
        @WritesAttribute(attribute = "Trailer_data", description = "Actual Trailer Data to be loaded into Recon-Table of the flowfile is being processed")})

/*
This class is used to extracts the header and trailer data from flow file
Header and trailer data will be added as flow file attributes
 */
public class HeaderTrailerExtract extends AbstractProcessor {

    private List<PropertyDescriptor> descriptors;
    private Set<Relationship> relationships;

    public static final Relationship SUCCESS = new Relationship.Builder()
            .name("success")
            .description("Data extraction is success")
            .build();

    public static final Relationship FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Data extraction is failure")
            .build();

    private static final PropertyDescriptor DELIMITER = new PropertyDescriptor.Builder()
            .name("Delimiter")
            .description("delimiter value")
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    private static final PropertyDescriptor CONTROL_DELIMITER = new PropertyDescriptor.Builder()
            .name("Control Delimiter")
            .description("control file delimiter value")
            .required(false)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .defaultValue(",")
            .build();

    private static final PropertyDescriptor REMOVE_FIRSTLINE = new PropertyDescriptor.Builder()
            .name("Remove First Line")
            .description("Removes First line from flow file if it is true")
            .required(false)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .defaultValue("false")
            .build();

    private static final PropertyDescriptor FILE_IDENTIFIER_TEXT = new PropertyDescriptor.Builder()
            .name("Controle File Identifier")
            .description("part of the control file name which will be used to identify the control file in the incoming path")
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .required(false)
            .build();

    static final PropertyDescriptor REGISTRY_SERVICE = new PropertyDescriptor.Builder()
            .name("configuration-registry-service")
            .displayName("Conifguration Registry Service")
            .description("The Schema Registry Service for serializing/deserializing messages as well as schema retrieval.")
            .required(true)
            .identifiesControllerService(TransformationRegistry.class)
            .build();

    private volatile TransformationRegistry transformationRegistry;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();

        descriptors.add(DELIMITER);
        descriptors.add(CONTROL_DELIMITER);
        descriptors.add(FILE_IDENTIFIER_TEXT);
        descriptors.add(REGISTRY_SERVICE);
        descriptors.add(REMOVE_FIRSTLINE);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(SUCCESS);
        relationships.add(FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @OnScheduled
    public void onScheduled(ProcessContext context) {
        this.transformationRegistry = context.getProperty(REGISTRY_SERVICE).asControllerService(TransformationRegistry.class);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {

        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }
        AtomicReference<Long> rowCount = new AtomicReference<>();
        rowCount.set(0L);
        String tableName = flowFile.getAttribute("tablename");
        String filePath = flowFile.getAttribute("absolute.path");

        // Added for ControlFile and Recon File Rename Issue
        String controlFile = flowFile.getAttribute(CONTROL_INDICATOR);
        String headerIndicator = transformationRegistry.getTableHeaderIndicator(tableName);
        String trailerIndicator = transformationRegistry.getTableTrailerIndicator(tableName);
        Boolean retainIndicator = Boolean.parseBoolean(flowFile.getAttribute(RETAIN_INDICATOR_IN_DATA));
        String dataIndicatorExpression = Optional.ofNullable(transformationRegistry.getTableDataIndicator(tableName)).orElse("");
        String sourcingType = transformationRegistry.getSourcingType(tableName);
        getLogger().info("Using sourcing type for table : " + tableName + " -- " + sourcingType);

        // Added for RetriveTableProperty DataType Verification Issue
        final List<VerifySchemaColumn> headerSchemaColumns = transformationRegistry.getVerifySchemaColumnsForTableHeader(tableName);
        final List<VerifySchemaColumn> trailerSchemaColumns = transformationRegistry.getVerifySchemaColumnsForTableTrailer(tableName);

        List<String> dataIndicators = null;
        if (StringUtils.contains(dataIndicatorExpression, "|")) {
            dataIndicators = Arrays.asList(StringUtils.replaceChars(dataIndicatorExpression, "[]", "").split("\\|"));
        }
        else{
            dataIndicators = Arrays.asList(dataIndicatorExpression);
        }

        char delimiter = DiffenUtils.getDelimiter(context.getProperty(DELIMITER).evaluateAttributeExpressions(flowFile).getValue());
        char controlDelimiter = DiffenUtils.getDelimiter(context.getProperty(CONTROL_DELIMITER).evaluateAttributeExpressions(flowFile).getValue());
        String fileIdentifier = context.getProperty(FILE_IDENTIFIER_TEXT).evaluateAttributeExpressions(flowFile).getValue();

        // Will be available only for FIXED_WIDTH
        String hParseableRegex = StringEscapeUtils.unescapeJava(flowFile.getAttribute("fixedwidth_header.parseable.regex")); //, hDelimitedRegex, dParseableRegex, dDelimitedRegex, tParseableRegex, tDelimitedRegex = null;
        String hDelimitedRegex = StringEscapeUtils.unescapeJava(flowFile.getAttribute("fixedwidth_header.todelimited.regex"));
        String dParseableRegex = StringEscapeUtils.unescapeJava(flowFile.getAttribute("fixedwidth.parseable.regex"));
        String dDelimitedRegex = StringEscapeUtils.unescapeJava(flowFile.getAttribute("fixedwidth.todelimited.regex"));
        String tParseableRegex = StringEscapeUtils.unescapeJava(flowFile.getAttribute("fixedwidth_trailer.parseable.regex"));
        String tDelimitedRegex = StringEscapeUtils.unescapeJava(flowFile.getAttribute("fixedwidth_trailer.todelimited.regex"));

        List<String> dataIndicatorsWithDelimiter = dataIndicators.stream().map(each -> each.concat(String.valueOf(delimiter))).collect(Collectors.toList());

        // Added for Exceptional Files Issue.
        Set<String> exceptionalTables = transformationRegistry.getExceptionalTables() != null ? transformationRegistry.getExceptionalTables() : new HashSet<>() ;
        boolean isTableExemptedFromHTProcessing = exceptionalTables.contains(tableName.toUpperCase());

        // Added to Remove First Line from FlowFile
        boolean removeFirstLine = Boolean.parseBoolean(context.getProperty(REMOVE_FIRSTLINE).evaluateAttributeExpressions(flowFile).getValue());

        try {

            AtomicReference<Map<String, String>> aMap = new AtomicReference<>();
            Map<String, String> map = new HashMap<>();
            aMap.set(map);

            // Added for Exceptional Files Issue.
            if (isTableExemptedFromHTProcessing){
                session.transfer(flowFile, SUCCESS);
                return;
            }
            else {
                flowFile = session.write(flowFile, new StreamCallback() {
                    @Override
                    public void process(final InputStream rawIn, final OutputStream dataOut) throws IOException {

                        InputStreamReader inputStream = new InputStreamReader(rawIn);
                        BufferedReader bufferedReader = new BufferedReader(inputStream, 65536);
                        Map<String, String> metaInfoMap = aMap.get();
                        //FIXME This currently doesn't support Header and Trailers without indicators. And the Indicator must be a single char
                        String next, line = bufferedReader.readLine();
                        boolean last = ( line == null);
                        boolean firstLine = true;
                        while(!last){
                            last = ((next = bufferedReader.readLine()) == null);
                            if (StringUtils.isNotBlank(line)) {
                                if (firstLine) {
                                    String[] columns = StringUtils.splitByWholeSeparatorPreserveAllTokens(line, String.valueOf(delimiter), -1);
                                    columns[0] =  columns[0].replaceAll("[^\\p{ASCII}]", "");
                                    line = StringUtils.join(columns,delimiter);
                                }
                                if (removeFirstLine && firstLine){
                                    firstLine = false;
                                    line = next;
                                    continue;
                                } else if (StringUtils.isNotBlank(headerIndicator) && line.substring(0, 1).equalsIgnoreCase(headerIndicator) && firstLine) {
                                    line = sourcingType.equalsIgnoreCase(SOURCING_TYPE_BATCH_FIXEDWIDTH) ? toDelimited(line, hParseableRegex, hDelimitedRegex) : line;
                                    // Added for RetriveTableProperty DataType Verification Issue
                                    Map<String, String> headerMap = extractInfoAsMap(delimiter, headerSchemaColumns, line);
                                    String kvPair = convertMapToKVPair(headerMap);
                                    headerMap.put("header_data", kvPair);
                                    metaInfoMap.putAll(headerMap);
                                    firstLine = false;
                                } else if (last && StringUtils.isNotBlank(trailerIndicator) &&
                                        (line.startsWith(trailerIndicator + delimiter) ||
                                                (sourcingType.equalsIgnoreCase(SOURCING_TYPE_BATCH_FIXEDWIDTH) && line.startsWith(trailerIndicator)))) {
                                    line = sourcingType.equalsIgnoreCase(SOURCING_TYPE_BATCH_FIXEDWIDTH) ? toDelimited(line, tParseableRegex, tDelimitedRegex) : line;
                                    // Added for RetriveTableProperty DataType Verification Issue
                                    Map<String, String> trailerMap = extractInfoAsMap(delimiter, trailerSchemaColumns, line);
                                    String kvPair = convertMapToKVPair(trailerMap);
                                    trailerMap.put("trailer_data", kvPair);
                                    metaInfoMap.putAll(trailerMap);
                                } else {
                                    line = sourcingType.equalsIgnoreCase(SOURCING_TYPE_BATCH_FIXEDWIDTH) ? toDelimited(line, dParseableRegex, dDelimitedRegex) : line;
                                    if (retainIndicator) {
                                        dataOut.write((line + "\n").getBytes());
                                    } else if (StringUtils.startsWithAny(line, dataIndicatorsWithDelimiter.toArray(new String[]{}))) {
                                        //FIXME - 2 : 1 for the indicator char and 1 for delimiter char. Revisit.
                                        dataOut.write((StringUtils.substring(line, 2, line.length()) + "\n").getBytes());
                                    } else {
                                        dataOut.write((line + "\n").getBytes());
                                    }
                                }
                            }

                            line = next;
                        }
                        dataOut.flush();
                        aMap.set(metaInfoMap);
                    }

                    private String toDelimited(String line, String sourceRegex, String replaceRegex) {
                        final Pattern searchPattern = Pattern.compile(sourceRegex, Pattern.CASE_INSENSITIVE);
                        final Matcher matcher = searchPattern.matcher(line);
                        String output = matcher.replaceAll(replaceRegex);
                        return output;
                    }
                });

                map = aMap.get();
                flowFile = session.putAllAttributes(flowFile, map);

                // Changed for Read flowfile which having Control file
                if (Boolean.parseBoolean(controlFile)) {
                    Integer controlFileTableNameIndex  = 0;
                    try {
                        controlFileTableNameIndex  = Integer.parseInt(flowFile.getAttribute(CONTROL_FILE_TABLENAME_INDEX));
                    }catch(Exception e){
                        throw new ProcessException("Cannot Parse Value "+flowFile.getAttribute(CONTROL_FILE_TABLENAME_INDEX)+" to Integer.");
                    }

                    String controlFileTableName = flowFile.getAttribute(CONTROL_FILE_TABLENAME);
                    // Added for RetriveTableProperty DataType Verification Issue
                    Map<String, String> metaInfo = getInfoFromControlFile(controlFileTableNameIndex ,controlFileTableName, filePath, headerIndicator, trailerIndicator,
                            headerSchemaColumns, trailerSchemaColumns, controlDelimiter, fileIdentifier);
                    flowFile = session.putAllAttributes(flowFile, metaInfo);

                }
            }
            session.transfer(flowFile, SUCCESS);
        } catch (Exception ae) {
            getLogger().error(ae.getMessage(), ae);
            session.transfer(flowFile, FAILURE);
        }
    }


    private String convertMapToKVPair(Map<String, String> headerMap) {
        return headerMap.entrySet().stream().map(e -> quoted(e.getKey()) + "," + quoted(e.getValue())).collect(Collectors.joining(","));
    }

    private String quoted(String input) {
        return "\"" + input + "\"";
    }

    /**
     * updates the header and trailer as map attribute to flowfile
     * @param delimiter
     * @param format
     * @param valueStr
     * @return
     */

    // Added for RetriveTableProperty DataType Verification Issue
    private Map<String, String> extractInfoAsMap(char delimiter, List<VerifySchemaColumn> format, String valueStr) {

        Map<String, String> map = new HashMap<>();

        if (format != null && valueStr != null) {
            // Added for Double Dollar Issue
            String[] value = StringUtils.splitByWholeSeparatorPreserveAllTokens(valueStr,String.valueOf(delimiter),-1);
            int currentColumnCount =1;
            String columnValue = "";
            if (format.size() == value.length) {
                for (int i = 1; i < format.size(); i++) {
                    final VerifySchemaColumn vsc = format.get(i);
                    if (vsc.isDrop()) {
                        continue;
                    }
                    columnValue = value[currentColumnCount];
                    DateTypeValidationResult validationResult = vsc.verify(columnValue.trim());

                    if(validationResult.getResultType().name().contains("GOOD")){
                        map.put(format.get(i).getName(),validationResult.getOriginalValue());
                        currentColumnCount = currentColumnCount +1;
                    }else {
                        throw new ProcessException("MapFormation Error:--> "+validationResult.getErrorMsg());
                    }
                }
            } else {
                throw new ProcessException("Header/Trailer and Value count doesnt match. Header/Trailer Col Count is " + format.size()
                        + " Value count is " + value.length);
            }
        }
        return map;
    }

    /**
     * For handling control file from acbs
     * @param controlFileTableNameIndex
     * @param controlFileTableName
     * @param filePath
     * @param headerIndicator
     * @param trailerIndicator
     * @param headerFormat
     * @param trailerFormat
     * @param delimiter
     * @param filetype
     * @return
     */
    // Added for RetriveTableProperty DataType Verification Issue
    public Map<String, String> getInfoFromControlFile(int controlFileTableNameIndex ,String controlFileTableName, String filePath, String headerIndicator,
                                                      String trailerIndicator, List<VerifySchemaColumn> headerFormat, List<VerifySchemaColumn> trailerFormat, char delimiter, String filetype) {

        String line = "";
        BufferedReader br = null;
        String cvsSplitBy = String.valueOf(delimiter);
        String cntrlFile = "";
        File folderToScan = new File(filePath);
        File[] listOfFiles = folderToScan.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                cntrlFile = listOfFiles[i].getName();
                if (cntrlFile.contains(filetype)) {
                    cntrlFile = listOfFiles[i].toString();
                    break;
                }
            }
        }
        Map<String, Map<String, String>> metaInfoMap = new HashMap<>();
        Map<String, String> map = new HashMap<>();

        try {

            br = new BufferedReader(new FileReader(cntrlFile));
            String fqTableName = "";
            while ((line = br.readLine()) != null) {
                if (0 != line.length()) {

                    //Control file format
                    //H,COUNTRY,SRC,TABLENAME,[ROWCOUNT] etc
                    String[] colData = StringUtils.splitByWholeSeparatorPreserveAllTokens(line, cvsSplitBy, -1);
                    if (headerIndicator != null && !headerIndicator.isEmpty() && line.substring(0, 1).equalsIgnoreCase(headerIndicator)) {
                        // Added for ControlFile and Recon File Rename Issue

                        String tableName = colData[controlFileTableNameIndex ];

                        fqTableName = tableName;
                        Map<String, String> headerMap = extractInfoAsMap(delimiter, headerFormat, line);
                        String kvPair = convertMapToKVPair(headerMap);
                        headerMap.put("header_data", kvPair);
                        map.putAll(headerMap);

                    } else if (trailerIndicator != null && !trailerIndicator.isEmpty() && line.substring(0, 1).equalsIgnoreCase(trailerIndicator)) {
                        Map<String, String> trailerMap = extractInfoAsMap(delimiter, trailerFormat, line);
                        String kvPair = convertMapToKVPair(trailerMap);
                        trailerMap.put("trailer_data", kvPair);
                        map.putAll(trailerMap);
                    }
                    metaInfoMap.put(fqTableName.toUpperCase(), new HashMap<>(map));
                }
            }
            Set<String> setLst = metaInfoMap.keySet();

            for (String eachString : setLst) {
                if (eachString.contains(controlFileTableName.toUpperCase())){
                    controlFileTableName = eachString;
                    break;
                }
                else continue;
            }

            return metaInfoMap.getOrDefault(controlFileTableName.toUpperCase(), null);

        } catch (IOException e) {
            throw new ProcessException(e.getMessage(), e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    return null;
                }
            }
        }
    }
}