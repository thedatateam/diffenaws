package in.tdt.diffen.processors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.serialization.DateTimeUtils;
import org.apache.nifi.util.StopWatch;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static in.tdt.diffen.processors.CSVUtils.*;
import static in.tdt.diffen.processors.RowIDAssignment.ROW_METADATA_ADDED;
import static in.tdt.diffen.processors.RowIDAssignment.TOTAL_ROW_COUNT;

@Tags({ "rowid assignment" })
@CapabilityDescription("Prepends a row ID to every row of data in the incoming flow file content")
@SideEffectFree
@SupportsBatching
@ReadsAttributes({ @ReadsAttribute(attribute = "tablename", description = "The table that is being processed"),
        @ReadsAttribute(attribute = "expected.column.count", description = "The column count for the table"),
        @ReadsAttribute(attribute = "filename", description = "Name of the file."),
        @ReadsAttribute(attribute = "base_partition", description = "valid partition.") })
@WritesAttributes({
        @WritesAttribute(attribute = TOTAL_ROW_COUNT, description = "This is count of the total number of rows that "
                + "were processed from the flowfile content"),
        @WritesAttribute(attribute = ROW_METADATA_ADDED, description = "A boolean indicating if the ROWID was added to "
                + "the flow file(s) from emitted from this processor.") })
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
/**
 * This class is used to populate row id for each row and Prepends to it.
 */
public class RowIDAssignmentV2 extends AbstractProcessor {

    public static final Relationship REL_SUCCESS = new Relationship.Builder().name("success")
            .description("Fetched file stats successfully.").build();

    public static final Relationship REL_FAILURE = new Relationship.Builder().name("failure")
            .description("Failed to fetch file stats.").build();

    @Override
    protected void init(ProcessorInitializationContext context) {
        super.init(context);
    }

    @Override
    public Set<Relationship> getRelationships() {
        Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        return relationships;
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        final List<PropertyDescriptor> properties = new ArrayList<>(super.getSupportedPropertyDescriptors());
        properties.add(DateTimeUtils.DATE_FORMAT);
        properties.add(DateTimeUtils.TIME_FORMAT);
        properties.add(DateTimeUtils.TIMESTAMP_FORMAT);
        properties.add(CSV_FORMAT);
        properties.add(VALUE_SEPARATOR);
        properties.add(SKIP_HEADER_LINE);
        properties.add(QUOTE_CHAR);
        properties.add(ESCAPE_CHAR);
        properties.add(COMMENT_MARKER);
        properties.add(NULL_STRING);
        properties.add(TRIM_FIELDS);
        properties.add(QUOTE_MODE);
        properties.add(RECORD_SEPARATOR);
        properties.add(TRAILING_DELIMITER);
        return properties;
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        final StopWatch stopWatch = new StopWatch(true);

        CSVFormat csvFormat = createCSVFormat(context);

        try {
            AtomicReference<Long> totalRowCount = new AtomicReference<>();
            AtomicReference<ArrayList<String>> provEvents = new AtomicReference<>();

            String fileName = flowFile.getAttribute("filename");
            String validPartition = flowFile.getAttribute("base_partition");
            flowFile = session.write(flowFile, (rawIn, rawOut) -> {

                try (final PrintWriter writer = new PrintWriter(new OutputStreamWriter(rawOut));
                        final BufferedReader reader = new BufferedReader(new InputStreamReader(rawIn))) {

                    CSVParser csvParser = csvFormat.parse(reader);
                    CSVPrinter csvPrinter = csvFormat.print(writer);

                    long rowcount = 0;
                    ArrayList<String> rowIds = new ArrayList<>();
                    for (CSVRecord csvRecord : csvParser) {
                        final String rowId = String.format("%s_%s", System.nanoTime(), UUID.randomUUID().toString());
                        List<Object> objects = new ArrayList<>();
                        objects.add(rowId);
                        objects.add(fileName);
                        objects.add(validPartition);
                        for (String value : csvRecord) {
                            objects.add(value);
                        }
                        csvPrinter.printRecord(objects);
                        rowcount++;
                        rowIds.add(rowId);
                    }
                    provEvents.set(rowIds);
                    totalRowCount.set(rowcount);
                    writer.flush();
                }
            });
            stopWatch.stop();
            flowFile = session.putAttribute(flowFile, TOTAL_ROW_COUNT, totalRowCount.get().toString());
            flowFile = session.putAttribute(flowFile, ROW_METADATA_ADDED, "true");
            final String tableName = flowFile.getAttribute("tablename");
            for (final String rowId : provEvents.get()) {
                final String provEvent = rowId + "," + tableName;
                session.getProvenanceReporter().modifyContent(flowFile, provEvent);
            }
            session.getProvenanceReporter().modifyContent(flowFile, stopWatch.getDuration(TimeUnit.MILLISECONDS));
            session.transfer(flowFile, REL_SUCCESS);
        } catch (final ProcessException e) {
            session.transfer(flowFile, REL_FAILURE);
            throw e;
        }
    }
}
