/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.services.TransformationRegistry;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.SideEffectFree;
import org.apache.nifi.annotation.behavior.SupportsBatching;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;


import java.util.*;

import static in.tdt.diffen.verifytypes.DiffenConstants.*;
import static java.lang.Thread.sleep;

@Tags({"table config refresh processor to refresh transformation registry based on source config changes"})
@CapabilityDescription("Table config refresh processor to refresh transformation registry based on source config changes")
@SideEffectFree
@SupportsBatching
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
/**
 * This class is used to refresh table properties from controller service{@link  TransformationRegistry}based on table name
 * and unlock table post schema evolution.
 */
public class TableConfigRefreshProcessor extends AbstractProcessor {

    static final PropertyDescriptor REGISTRY_SERVICE = new PropertyDescriptor.Builder()
            .name("configuration-registry-service")
            .displayName("Conifguration Registry Service")
            .description("The Schema Registry Service for serializing/deserializing messages as well as schema retrieval.")
            .required(true)
            .identifiesControllerService(TransformationRegistry.class)
            .build();

    static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("Success")
            .description("Success relationship")
            .build();

    private static final List<PropertyDescriptor> DESCRIPTORS;
    private static final Set<Relationship> RELATIONSHIPS;

    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(REGISTRY_SERVICE);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        RELATIONSHIPS = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return RELATIONSHIPS;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return DESCRIPTORS;
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        final TransformationRegistry registry = context.getProperty(REGISTRY_SERVICE).asControllerService(TransformationRegistry.class);
        try {
            final String tableName = getIncomingAttribute(flowFile, TABLE_NAME_ATTRIBUTE);
            getLogger().info("Unlocking for table : " + tableName);
            //Unlock for table so that processing is done
            registry.refreshConfig();
            registry.unlockTable(tableName);

        } catch (final Exception e) {
            registry.unlockTable(getIncomingAttribute(flowFile, TABLE_NAME_ATTRIBUTE));
        	getLogger().error(e.toString(), e);
        }
        session.transfer(flowFile, REL_SUCCESS);
    }

    private String getIncomingAttribute(FlowFile flowFile, String incomingAttributeName) {
        final String value = flowFile.getAttribute(incomingAttributeName);
        if (value == null) {
            throw new ProcessException("Missing attribute " + incomingAttributeName + " on incoming flowfile");
        }
        return value;
    }
}
