package in.tdt.diffen.processors;

import in.tdt.diffen.cdc.CDCModels;
import in.tdt.diffen.processors.transform.SourceTableConfigTransformerV2;
import in.tdt.diffen.verifytypes.DiffenConstants;
import org.apache.commons.io.IOUtils;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.components.PropertyValue;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.processor.io.OutputStreamCallback;
import org.apache.nifi.processor.util.StandardValidators;

import java.io.*;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import static org.apache.nifi.processor.util.StandardValidators.NON_EMPTY_VALIDATOR;

public class SourceConfig2TableConfigProcessorWithMappingV2 extends AbstractProcessor {

   /* static final PropertyDescriptor SOURCE_CONFIG_PATH = new PropertyDescriptor.Builder()
            .name("Source Schema Path")
            .description("Path to the Source Schema files")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();*/

    static final PropertyDescriptor SOURCE_CONFIG_FILE_NAME_FILTER = new PropertyDescriptor.Builder()
            .name("source-config-file-filter")
            .displayName("Source Config File Filter")
            .description("file name filter to be used get the source config file from incoming folder")
            .addValidator(NON_EMPTY_VALIDATOR)
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor SOURCE_CONFIG_TRANSFORM_CLASS_NAME = new PropertyDescriptor.Builder()
            .name("source-config-transform-class-name")
            .displayName("Source Config Transform Class Name")
            .description("Class Name which transforms source table config xml to SDM format")
            .addValidator(NON_EMPTY_VALIDATOR)
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();


    static final PropertyDescriptor SOURCE_TYPE_PATH = new PropertyDescriptor.Builder()
            .name("source-type-path")
            .displayName("Source Type Path for CDC")
            .description("This identifies what kind of source this source table is either a transaction or a delta table")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor KEY_COLUMN_LIST_PATH = new PropertyDescriptor.Builder()
            .name("key-column-list-path")
            .displayName("Key Column List Path for CDC")
            .description("The keycol.list file contains the list of columns that compose the primary key of the source table")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor TRANSFORMATION_RULES_PATH = new PropertyDescriptor.Builder().
            name("transformation-rules-path")
            .displayName("Transformation Rules Path")
            .description("This file contains all the constraints and the validation rules that must be applied on the input data file.")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor DATATYPE_MAPPING_FILE_PATH = new PropertyDescriptor.Builder()
            .name("datatype-mapping-path")
            .displayName("Datatype mapping file path")
            .description("Path to the file that has the source and the target Avro datatype delimited by ->")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor DEFAULTS_MAPPING_FILE_PATH = new PropertyDescriptor.Builder()
            .name("defaults-mapping-path")
            .displayName("Defaults mapping file path for table configs")
            .description("Path to the file that has the default values for table config attributes delimited by ->")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor SOURCE = new PropertyDescriptor.Builder()
            .name(DiffenConstants.SOURCE)
            .displayName("Source")
            .description("Source")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor COUNTRY = new PropertyDescriptor.Builder()
            .name(DiffenConstants.COUNTRY)
            .displayName("Country")
            .description("Country")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    public static final Relationship REL_SUCCESS =
            new Relationship.Builder()
                    .name("success")
                    .description("Successfully Generated Table config XMLs from CDC XML are transferred to this relationship")
                    .build();

    public static final Relationship REL_FAILURE =
            new Relationship.Builder()
                    .name("failure")
                    .description("CDC XML Files that could not be converted into Table config XMLs are transferred to this relationship")
                    .build();

    private static final Set<Relationship> RELATIONSHIPS;
    private static final List<PropertyDescriptor> DESCRIPTORS;

    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(SOURCE);
        descriptors.add(COUNTRY);
        descriptors.add(SOURCE_CONFIG_FILE_NAME_FILTER);
        descriptors.add(SOURCE_CONFIG_TRANSFORM_CLASS_NAME);
        descriptors.add(DATATYPE_MAPPING_FILE_PATH);
        descriptors.add(SOURCE_TYPE_PATH);
        descriptors.add(KEY_COLUMN_LIST_PATH);
        descriptors.add(DEFAULTS_MAPPING_FILE_PATH);
        descriptors.add(TRANSFORMATION_RULES_PATH);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);

        Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        RELATIONSHIPS = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.RELATIONSHIPS;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return this.DESCRIPTORS;
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {

        FlowFile parentFlowFile = session.get();
        if (parentFlowFile == null) {
            return;
        }
        final AtomicReference<String> contentsRef = new AtomicReference<>(null);

        session.read(parentFlowFile, new InputStreamCallback() {
            @Override
            public void process(final InputStream in) throws IOException {
                final String contents = IOUtils.toString(in, "UTF-8");
                contentsRef.set(contents);
            }
        });

        final String contents = contentsRef.get();


        try {

            String filename = context.getProperty(SOURCE_CONFIG_FILE_NAME_FILTER).evaluateAttributeExpressions(parentFlowFile).getValue();
            String transformClassName = context.getProperty(SOURCE_CONFIG_TRANSFORM_CLASS_NAME).evaluateAttributeExpressions(parentFlowFile).getValue();
            SourceTableConfigTransformerV2 transformer = getConfigTransformer(transformClassName);

            Map<String, CDCModels.TableConfiguration> tableConfigs = transformer.transform(filename, contents, getAttributes(context, parentFlowFile), getLogger());

            final List<FlowFile> childFlowFiles = new ArrayList<>();
            Map<String, String> attributes = parentFlowFile.getAttributes();
            for (Map.Entry<String, CDCModels.TableConfiguration> entry: tableConfigs.entrySet()) {
                FlowFile childFlowFile = session.create();
                childFlowFile = session.write(childFlowFile, new OutputStreamCallback() {
                    @Override
                    public void process(OutputStream rawOut) throws IOException {
                        IOUtils.write(entry.getValue().toString(), rawOut);
                    }
                });
                childFlowFile = session.putAttribute(childFlowFile, DiffenConstants.FLOW_FILE_NAME, entry.getKey()+"_tables_config.xml");
                childFlowFile = session.putAllAttributes(childFlowFile, attributes);
                childFlowFiles.add(childFlowFile);
            }
            session.transfer(childFlowFiles, REL_SUCCESS);
            session.remove(parentFlowFile);
        } catch (final Exception e) {
            e.printStackTrace();
            getLogger().error("Error while converting CDC XML to Tableconfig xml.", e);
            session.transfer(parentFlowFile, REL_FAILURE);
        }
    }

    private Map<String,String> getAttributes(ProcessContext context, FlowFile flowFile) {
        Map<String, String> map = new HashMap<>();
        for(PropertyDescriptor descriptor: getPropertyDescriptors()) {
            PropertyValue propertyValue =  context.getProperty(descriptor)
                    .evaluateAttributeExpressions(flowFile);
            map.put(descriptor.getName(), propertyValue.getValue());
        }
        return map;
    }

    private static Collection<File> getSourceConfigFiles(File incomingPath, final Pattern configFilePattern) {
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(final File file) {
                return configFilePattern.matcher(file.getName()).matches();
            }
        };
        return Arrays.asList(incomingPath.listFiles(filter));
    }

    private SourceTableConfigTransformerV2 getConfigTransformer(String transformClassName) throws Exception {
        Class<?> clazz = Class.forName(transformClassName);
        Constructor<?> ctor = clazz.getConstructor();
        return (SourceTableConfigTransformerV2) ctor.newInstance();
    }
}