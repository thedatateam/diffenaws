/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.services.TransformationRegistry;
import in.tdt.diffen.verifytypes.DiffenUtils;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.distributed.cache.client.DistributedMapCacheClient;
import org.apache.nifi.distributed.cache.client.Serializer;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.util.StopWatch;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.tz.DateTimeZoneBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static in.tdt.diffen.processors.EODProcessing.*;
import static in.tdt.diffen.verifytypes.DiffenConstants.*;
import static org.apache.nifi.processor.util.StandardValidators.NON_EMPTY_VALIDATOR;

@Tags({ "data transformation" })
@CapabilityDescription("Processes EOD Marker file")
@SideEffectFree
@SupportsBatching
@ReadsAttributes({
        @ReadsAttribute(attribute = "file.lastAccessTime", description = "This is used to understand "
                + "how long ago the EOD file was read. " + "This is used when processing the file, "
                + "if this date is less than the configured " + "Max Wait Time property then the Flow File will "
                + "be penalized."),
        @ReadsAttribute(attribute = "tablename", description = "The EOD table name"),
        @ReadsAttribute(attribute = "source", description = "The source system the EOD is for") })
@WritesAttributes({
        @WritesAttribute(attribute = EOD_MARKER_TIME, description = "This is marker time from the EOD file."),
        @WritesAttribute(attribute = EOD_BUSINESS_TIME, description = "This is business date for the marker."),
        @WritesAttribute(attribute = PROCESSING_TYPE, description = "This property will be true if the file is detected to be "
                + "a full dump otherwise it will be false.") })
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
/**
 * This class is used to extract the EOD information from EOD file <\n> It will
 * add the EOD data to the flow file attributes
 */
public class EODProcessing extends AbstractProcessor {

    static final PropertyDescriptor DELIMITER = new PropertyDescriptor.Builder().name("csv-delimiter")
            .displayName("CSV delimiter").description("Delimiter character for CSV records")
            .addValidator(NON_EMPTY_VALIDATOR).expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES).defaultValue("\u0001").build();

    static final PropertyDescriptor CHARSET = new PropertyDescriptor.Builder().name("Input Charset")
            .description("The input character set").required(false).defaultValue("UTF-8")
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY).addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

    static final PropertyDescriptor EOD_WAIT_TIME = new PropertyDescriptor.Builder().name("eod-wait-time")
            .displayName("EOD Wait Time")
            .description("The amount of time in minutes between the EOD Marker and now. This allows "
                    + "us to wait for data before processing DIFFEN")
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES).addValidator(StandardValidators.INTEGER_VALIDATOR).defaultValue("120")
            .build();

    static final PropertyDescriptor CACHE_SERVICE = new PropertyDescriptor.Builder()
            .name("distributed-map-cache-client").displayName("Distributed Map Cache")
            .description("The distributed cache to store the EOD Marker in").required(true)
            .identifiesControllerService(DistributedMapCacheClient.class).build();

    static final PropertyDescriptor REGISTRY_SERVICE = new PropertyDescriptor.Builder()
            .name("configuration-registry-service").displayName("Conifguration Registry Service")
            .description(
                    "The Schema Registry Service for serializing/deserializing messages as well as schema retrieval.")
            .required(true).identifiesControllerService(TransformationRegistry.class).build();

    static final Relationship REL_SUCCESS = new Relationship.Builder().name("success")
            .description("Success relationship").build();

    static final Relationship REL_FAILURE = new Relationship.Builder().name("failure")
            .description("Failure relationship").build();

    private static List<PropertyDescriptor> DESCRIPTORS;
    private static final Set<Relationship> RELATIONSHIPS;
    static final String EOD_MARKER_TIME = "eod.marker.time";
    static final String EOD_BUSINESS_TIME = "eod.business.time";
    static final String PROCESSING_TYPE = "eod.processing.type";

    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(REGISTRY_SERVICE);
        descriptors.add(CACHE_SERVICE);
        descriptors.add(DELIMITER);
        descriptors.add(CHARSET);
        descriptors.add(EOD_WAIT_TIME);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        RELATIONSHIPS = Collections.unmodifiableSet(relationships);
    }

    private volatile TransformationRegistry transformationRegistry;

    @Override
    public Set<Relationship> getRelationships() {
        return RELATIONSHIPS;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return DESCRIPTORS;
    }

    @OnScheduled
    public void onScheduled(ProcessContext context) {
        this.transformationRegistry = context.getProperty(REGISTRY_SERVICE)
                .asControllerService(TransformationRegistry.class);
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        try {
            AtomicReference<String> eodMarkerTime = new AtomicReference<>();
            AtomicReference<String> eodBusinessTime = new AtomicReference<>();

            final StopWatch stopWatch = new StopWatch(true);
            final char inputDelimiter = DiffenUtils
                    .getDelimiter(context.getProperty(DELIMITER).evaluateAttributeExpressions(flowFile).getValue());
            final String inputCharset = context.getProperty(CHARSET).evaluateAttributeExpressions().getValue();
            final int eodWaitTime = context.getProperty(EOD_WAIT_TIME).evaluateAttributeExpressions(flowFile)
                    .asInteger();
            final String tableName = getIncomingAttribute(flowFile, "tablename");
            final String source = getIncomingAttribute(flowFile, "source");
            final String country = getIncomingAttribute(flowFile, "country");
            final String timeStampColFormat = getIncomingAttribute(flowFile, TIMESTAMP_COL_FORMAT);
            final String markerTimeColumn = getIncomingAttribute(flowFile, MARKERTIME_COLUMN);
            final String eodDateColumn = getIncomingAttribute(flowFile, EODDATE_COLUMN);
            final String journalTimeColumn = getIncomingAttribute(flowFile, JOURNALTIME_COLUMN);
            final String userIdColumn = getIncomingAttribute(flowFile, USERID_COLUMN);
            final String timeZoneID = getIncomingAttribute(flowFile, TIME_ZONE);

            final int journalTimeStampPosition = this.transformationRegistry.getIndexForColumn(tableName,
                    journalTimeColumn);
            final int userIdColumnPosition = this.transformationRegistry.getIndexForColumn(tableName, userIdColumn);

            final TimeZone timeZone = TimeZone.getTimeZone(timeZoneID);
            final DateTimeFormatter formatter = DateTimeFormat.forPattern(timeStampColFormat)
                    .withZone(DateTimeZone.forTimeZone(timeZone));
            final int markerColumn = this.transformationRegistry.getIndexForColumn(tableName, markerTimeColumn);
            final int eodColumn = this.transformationRegistry.getIndexForColumn(tableName, eodDateColumn);

            AtomicReference<Boolean> penalizeFlow = new AtomicReference<>(false);
            AtomicReference<Boolean> receivedARecord = new AtomicReference<>(false);
            final AtomicReference<DateTime> mostRecentJournalTime = new AtomicReference<>();
            final AtomicReference<String> markerValue = new AtomicReference<>();

            final AtomicReference<String> processingType = new AtomicReference<>("incremental");
            final AtomicReference<String> eodValue = new AtomicReference<>();
            session.read(flowFile, in -> {

                try (final BufferedReader reader = new BufferedReader(new InputStreamReader(in, inputCharset))) {
                    boolean haveCheckedFullDump = false;
                    String line;
                    while ((line = reader.readLine()) != null) {
                        final String[] columns = StringUtils.splitByWholeSeparatorPreserveAllTokens(line,
                                String.valueOf(inputDelimiter), -1);

                        // do two things here...
                        // 1. Only look at A records
                        // 2. If the markerDate is more recent than the latest, update the latest and
                        // keep track
                        // of the record offset.
                        if (columns.length < 5) {
                            getLogger().error("Record is missing columns! " + line);
                            continue;
                        }
                        if (columns[5].contentEquals("A") || columns[5].contentEquals("I")) {

                            // do check for full dump or incremental
                            if (!haveCheckedFullDump) {
                                if (0 == "NOT SET".compareToIgnoreCase(columns[userIdColumnPosition].trim())) {
                                    processingType.set("fulldump");
                                }
                                haveCheckedFullDump = true;
                            }

                            receivedARecord.set(true);
                            final DateTime currentJournalTime = formatter
                                    .parseDateTime(columns[journalTimeStampPosition]);

                            if (null == mostRecentJournalTime.get()
                                    || currentJournalTime.isAfter(mostRecentJournalTime.get())) {
                                mostRecentJournalTime.set(currentJournalTime);
                                markerValue.set(columns[markerColumn]);
                                eodValue.set(columns[eodColumn]);
                            }
                        }

                    }
                    // by now we should have the latest record and everything else we need...
                    // final String currentMarkerValue = cvsRecord.get(markerColumn);
                    // final DateTime markerDate = formatter.parseDateTime(markerValue);
                    if (null == markerValue.get()) {
                        getLogger().error("Marker value is not set!!!!!");
                        throw new ProcessException("Marker value is not set!!!!!");
                    }

                    final DateTime currentDate = DateTime.now(DateTimeZone.forTimeZone(timeZone));
                    Duration duration = new Duration(mostRecentJournalTime.get(), currentDate);
                    if (eodWaitTime > duration.getStandardMinutes()) {
                        penalizeFlow.set(true);
                    } else {
                        eodBusinessTime.set(eodValue.get());
                        eodMarkerTime.set(markerValue.get());
                    }
                }
            });
            stopWatch.stop();

            session.getProvenanceReporter().modifyContent(flowFile, stopWatch.getDuration(TimeUnit.MILLISECONDS));

            if (!receivedARecord.get()) {
                getLogger().error("Never received an A record for table: " + tableName + " source:" + source
                        + " country: " + country);
            }
            if (penalizeFlow.get()) {
                session.rollback(true);
            } else {
                flowFile = session.putAttribute(flowFile, EOD_MARKER_TIME, eodMarkerTime.get());
                flowFile = session.putAttribute(flowFile, EOD_BUSINESS_TIME, eodBusinessTime.get());
                flowFile = session.putAttribute(flowFile, PROCESSING_TYPE, processingType.get());
                session.transfer(flowFile, REL_SUCCESS);
            }
        } catch (final ProcessException e) {
            session.transfer(flowFile, REL_FAILURE);
            throw e;
        }
    }

    private String getIncomingAttribute(FlowFile flowFile, String incomingAttributeName) {
        final String attrName = flowFile.getAttribute(incomingAttributeName);
        if (null == attrName) {
            throw new ProcessException("Missing attribute " + incomingAttributeName + " on incoming flowfile");
        }
        return attrName;
    }
}
