/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.services;

import in.tdt.diffen.cdc.CDCModels;
import in.tdt.diffen.verifytypes.checker.TransformRule;
import in.tdt.diffen.verifytypes.checker.VerifySchemaColumn;
import groovy.lang.Script;
import org.apache.nifi.controller.ControllerService;
import org.apache.nifi.reporting.InitializationException;

import java.util.List;
import java.util.Map;
import java.util.Set;

//FIXME This seems to be loaded now. Need to group and segregate.
/** Interface for LocalFile transformation registry. */
public interface TransformationRegistry extends ControllerService, AutoCloseable {

    String[] getDataHeaderForTable(final String tableName);
    List<VerifySchemaColumn> getVerifySchemaColumnsForTable(String tableName);
    TransformRule retrieveRule(String ruleName);
    Script getScriptForRule(final TransformRule rule);
    String[] getFullDataHeaderForTable(final String tableName);
    int getFirstDataColumnPosition();
    String getGlobalConfig(String configName);
    String getTableSchemaAsAvro(final String tableName);
    String getTableSchemaAsAvro(final String tableName, final String[] headerColumns);
    String getFixedWidthParseableRegexForHeader(String tableName);
    String getFixedWidthToDelimitedRegexForHeader(String tableName);
    String getFixedWidthParseableRegexForTrailer(String tableName);
    String getFixedWidthToDelimitedRegexForTrailer(String tableName);
    String getInvalidTableSchemaAsAvro(final String tableName);
    String getWarnTableSchemaAsAvro(final String tableName);
    String getRowHistoryTableSchemaAsAvro(final String tableName);
    String getTranslatedTableName(String tableName);
    boolean isTableExist(final String tableName);
    String getFixedWidthParseableRegexForData(final String tableName);
    String getFixedWidthToDelimitedRegexForData(final String tableName);
    boolean isCDC();
    String getTableHeaderFormat(String tableName);
    String getTableTrailerFormat(String tableName);
    String getTableHeaderIndicator(String tableName);
    String getTableTrailerIndicator(String tableName);
    String getSourcingType(String tableName);
    String getTableDataIndicator(String tableName);
    Map<String, String> getAllGlobalConfigs();
    int getIndexForColumn(final String tableName, final String columnName);
    String getKeyColumnsForTable(String tableName);
    String getTableColAllStringSchema(final String tableName);
    String getTableColAllStringSchemaAsAvro(final String tableName);
    String getTableAfterValueIndicator(String tableName);
    String getTableBeforeValueIndicator(String tableName);
    String getTableInsertValueIndicator(String tableName);
    String getTableDeleteValueIndicator(String tableName);
    // Added for ControlFile and Recon File Rename Issue
    String getControlFileTableNameIndex(String tableName);
    String getControlFileTableName(String tableName);
    String getreconfilerename(String tableName);
    // Added for RetriveTableProperty DataType Verification Issue
    List<VerifySchemaColumn> getVerifySchemaColumnsForTableHeader(String tableName);
    List<VerifySchemaColumn> getVerifySchemaColumnsForTableTrailer(String tableName);
    // Added for Exceptional Tables Issue.
    Set<String> getExceptionalTables();
    void lockTable(String tableName);
    void unlockTable(String tableName);
    boolean isTableLocked(String tableName);
    CDCModels.TableConfiguration getTableConfig(String tableName);
    void refreshConfig() throws InitializationException;
}