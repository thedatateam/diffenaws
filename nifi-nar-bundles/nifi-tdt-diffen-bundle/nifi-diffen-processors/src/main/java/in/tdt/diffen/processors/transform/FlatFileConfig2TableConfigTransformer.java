package in.tdt.diffen.processors.transform;

import in.tdt.diffen.cdc.CDCModels;
import in.tdt.diffen.cdc.CDCXmlParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.logging.ComponentLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static in.tdt.diffen.verifytypes.DiffenConstants.*;

public class FlatFileConfig2TableConfigTransformer implements SourceTableConfigTransformer {

    @Override
    public Map<String, CDCModels.TableConfiguration> transform(Collection<File> sourceConfigFiles, Map<String, String> attributes, ComponentLog logger) {

        final File transformationRulesPath = (attributes.get("transformation-rules-path") != null) ? new File(attributes.get("transformation-rules-path")) : null;
        final File dataTypeMappingPath = new File(attributes.get("datatype-mapping-path"));
        final File defaultMappingPath = new File(attributes.get("defaults-mapping-path"));

        Map<String, CDCModels.TableConfiguration> tableConfigurations = new HashMap<>();
        try {
            Map<String, List<CDCModels.ColTransformationRule>> transColList = new HashMap<>();

            //Fetch transformation rules info
            if (transformationRulesPath != null && transformationRulesPath.exists()) {
                List<String> transColFile = FileUtils.readLines(transformationRulesPath);
                transColFile.removeIf(line -> StringUtils.isEmpty(line));
                transColList = transColFile.stream()
                        .map(line -> line.split("->"))
                        .map(x -> new CDCModels.TableTransformationRule(x[0].trim(), Arrays.stream(x[1].split("\\|"))
                                .map(each -> new CDCModels.ColTransformationRule(each))
                                .collect(Collectors.toList())))
                        .collect(Collectors.toMap(CDCModels.TableTransformationRule::getTableName, CDCModels.TableTransformationRule::getRulesList));
            }

            List<String> dataTypeMappingList = FileUtils.readLines(dataTypeMappingPath);
            dataTypeMappingList.removeIf(line -> StringUtils.isEmpty(line));
            Map<String, CDCModels.TableDataTypeMapping> dataTypeMap = new HashMap<>();

            for (String eachDMappingString : dataTypeMappingList) {
                String[] strings = eachDMappingString.split("->");
                if (strings.length > 2) {
                    dataTypeMap.put(strings[0].trim(), new CDCModels.TableDataTypeMapping(strings[0], strings[1], Boolean.parseBoolean(strings[2])));
                } else {
                    dataTypeMap.put(strings[0].trim(), new CDCModels.TableDataTypeMapping(strings[0], strings[1], false));
                }
            }

            List<String> defaultsMappingList = FileUtils.readLines(defaultMappingPath);
            defaultsMappingList.removeIf(line -> StringUtils.isEmpty(line));
            Map<String, String> defaultsMap = new HashMap<>();
            for (String eachDMappingString : defaultsMappingList) {
                String[] strings = eachDMappingString.split("->");
                defaultsMap.put(strings[0].trim(), strings[1].trim());
            }

            for (File sourceConfigFile : sourceConfigFiles) {
                logger.debug("Processing " + sourceConfigFile.getAbsolutePath());

                //Fetch source XML and convert it into NodeList
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(sourceConfigFile);
                NodeList tableMappings = doc.getElementsByTagName("TableMapping");

                tableConfigurations.putAll(getTableProperties(tableMappings, transColList, dataTypeMap, defaultsMap, sourceConfigFile, attributes.get(SOURCE), attributes.get(COUNTRY)));
            }

        } catch (final Exception e) {
            e.printStackTrace();
            logger.error("Error while converting CDC XML to Tableconfig xml : ", e);
        }
        return tableConfigurations;
    }

    private Map<String, CDCModels.TableConfiguration> getTableProperties(NodeList tableMappings, Map<String, List<CDCModels.ColTransformationRule>> transColList, Map<String, CDCModels.TableDataTypeMapping> dataTypeMap, Map<String, String> defaultsMap, File sourceConfigFile, String source, String country) {

        Map<String, CDCModels.TableConfiguration> tableConfigs = new HashMap<>();
        for (int count = 0; count < tableMappings.getLength(); count++) {
            Node tableMapping = tableMappings.item(count);
            String tableName = source + "_" + country + "_" + resolveTableName(sourceConfigFile);
            Element tableMapElem = (Element) tableMapping;
            NodeList sourceColumns = tableMapElem.getElementsByTagName("SourceColumn");

            List<CDCModels.ColTransformationRule> rulesList = transColList.containsKey(tableName) ? transColList.get(tableName) : new ArrayList<>();

            String keyPrefix = DEFAULT_PARAM_PREFIX + ADD_PREFIX_CONFIG_DIFFEN_TABLE + tableName;
            StringBuilder colSchemaBuilder = new StringBuilder();
            StringBuilder keyColBuilder = new StringBuilder();
            for (int colCount = 0; colCount < sourceColumns.getLength(); colCount++) {
                Node column = sourceColumns.item(colCount);
                colSchemaBuilder.append(CDCXmlParser.getColumn(column, rulesList, dataTypeMap, false)).append("^");
                if(CDCXmlParser.resolveBoolean(CDCXmlParser.getXmlAttribute(column, "Primarykey"))) {
                    keyColBuilder.append(CDCXmlParser.getXmlAttribute(column, "columnName")).append("^");
                }
            }
            String colSchema = StringUtils.removeEnd(colSchemaBuilder.toString(), "^");
            String keyCol = StringUtils.removeEnd(keyColBuilder.toString(), "^");

            List<CDCModels.Property> properties = new ArrayList<>();
            properties.add(new CDCModels.Property(keyPrefix + SUFFIX_CONFIG_COLSCHEMA, colSchema));
            properties.add(new CDCModels.Property(keyPrefix + SUFFIX_CONFIG_KEYCOLS, keyCol));
            properties.add(new CDCModels.Property(keyPrefix + SUFFIX_CONFIG_RUNTYPE, resolveRunType(sourceConfigFile)));
            properties.add(new CDCModels.Property(keyPrefix + SUFFIX_CONFIG_SOURCETYPE, resolveSourceType(CDCXmlParser.getXmlAttribute(tableMapping, "table_type"))));
            for (Map.Entry<String, String> entry : defaultsMap.entrySet()) {
                properties.add(new CDCModels.Property(keyPrefix + "." + entry.getKey(), entry.getValue()));
            }

            properties.removeIf(property -> StringUtils.isEmpty(property.getValue()));
            Collections.sort(properties);
            tableConfigs.put(tableName, new CDCModels.TableConfiguration(properties));
        }
        return tableConfigs;
    }

    private String resolveTableName(File sourceConfigFile) {
        String[] fileNameSplit = sourceConfigFile.getName().split("_");
        assert fileNameSplit.length >= 5 : "Schema Filename should be of the " +
                "format <>_<>_<>_tableName_runType.xml";

        String tableName = Arrays.stream(fileNameSplit)
                .collect(Collectors.toList())
                .subList(3, fileNameSplit.length - 1)
                .stream()
                .collect(Collectors.joining("_"));
        return tableName;
    }

    private String resolveRunType(File sourceConfigFile) {
        if(sourceConfigFile.getName().toUpperCase().endsWith("_DI.XML")) {
            return "incremental";
        } else if(sourceConfigFile.getName().toUpperCase().endsWith("_DF.XML")) {
            return "fulldump";
        }
        throw new RuntimeException("Not able to resolve run type from the config file name: " + sourceConfigFile.getName());
    }

    private String resolveSourceType(String input) {
        if("Transaction".equalsIgnoreCase(input)) {
            return "txn";
        } else if("Master".equalsIgnoreCase(input)) {
            return "delta";
        }
        throw new RuntimeException("Passed value doesn't match with any expected value for sourceType : " + input);
    }
}
