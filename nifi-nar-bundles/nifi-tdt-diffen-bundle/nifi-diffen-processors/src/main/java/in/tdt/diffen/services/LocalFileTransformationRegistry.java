/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package in.tdt.diffen.services;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import in.tdt.diffen.cdc.CDCModels;
import in.tdt.diffen.verifytypes.DiffenConstants;
import in.tdt.diffen.verifytypes.checker.TransformRule;
import in.tdt.diffen.verifytypes.checker.VerifySchema;
import in.tdt.diffen.verifytypes.checker.VerifySchemaColumn;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyObjectInspectorParameters;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyObjectInspectorParametersImpl;
import org.apache.hadoop.hive.serde2.typeinfo.PrimitiveTypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoFactory;
import org.apache.nifi.annotation.lifecycle.OnDisabled;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.AbstractControllerService;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.distributed.cache.client.Serializer;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.reporting.InitializationException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

import static in.tdt.diffen.verifytypes.DiffenConstants.*;

/**
 * Serves as a general lookup map.
 *
 * If we would like a replacement for this Controller service, it performs the following functions :
 *
 * 1. Internal Avro schema registry for all the tables in a source country
 * 2. Store for all table and global configs
 * 3. Store for FixedWidth to Delimited conversion regular expression
 * 4. Table rename mappings
 */
public class LocalFileTransformationRegistry extends AbstractControllerService implements TransformationRegistry {

    protected static final PropertyDescriptor CONFIG_FILE_PATH = new PropertyDescriptor.Builder()
            .name("Configuration File Path")
            .description("The path to the conf file on the local file system")
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.NONE)
            .addValidator(StandardValidators.FILE_EXISTS_VALIDATOR)
            .build();

    protected static final PropertyDescriptor RULE_FILE_PATH = new PropertyDescriptor.Builder()
            .name("Rule File Path")
            .description("The path to the rule file on the local file system")
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.NONE)
            .addValidator(StandardValidators.FILE_EXISTS_VALIDATOR)
            .build();

    protected static final PropertyDescriptor PARAM_FILE_PATH = new PropertyDescriptor.Builder()
            .name("Parameter file Path")
            .description("The path to the global parameter configuration file")
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.NONE)
            .addValidator(StandardValidators.FILE_EXISTS_VALIDATOR)
            .build();

    protected static final PropertyDescriptor TABLE_RENAME_FILE_PATH = new PropertyDescriptor.Builder()
            .name("Table rename file Path")
            .description("The path to the config file that has the source and the target table name")
            .required(false)
            .expressionLanguageSupported(ExpressionLanguageScope.NONE)
            .addValidator(StandardValidators.FILE_EXISTS_VALIDATOR)
            .build();

    private static List<PropertyDescriptor> DESCRIPTORS;

    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(CONFIG_FILE_PATH);
        descriptors.add(RULE_FILE_PATH);
        descriptors.add(PARAM_FILE_PATH);
        descriptors.add(TABLE_RENAME_FILE_PATH);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);
    }

    protected Configuration conf;

    protected final Map<String, String> tableFixedWidthParseableRegexData;
    protected final Map<String, String> tableFixedWidthToDelimitedRegexData;
    protected final Map<String, String> tableFixedWidthParseableRegexHeader;
    protected final Map<String, String> tableFixedWidthToDelimitedRegexHeader;
    protected final Map<String, String> tableFixedWidthParseableRegexTrailer;
    protected final Map<String, String> tableFixedWidthToDelimitedRegexTrailer;

    protected final Map<String,String> dataIndicatorMap;
    protected final Map<String,String> headerIndicatorMap;
    protected final Map<String,String> trailerIndicatorMap;
    protected final Map<String,String> tableLevelSourcingTypeMap;
    protected final Map<String,String> headerColSchemaMap;
    protected final Map<String,String> trailerColSchemaMap;
    protected final Map<String, String> tableRawSchema;
    protected final Map<String, String> tableGeneralConfigs;
    protected final Map<String, String> tableRenameConfigs;
    protected final Map<String, String> keyColumnsMap;
    protected final Map<String, String> afterValueIndicatorMap;
    protected final Map<String, String> beforeValueIndicatorMap;
    protected final Map<String, String> deleteValueIndicatorMap;

    // Added for ControlFile and Recon File Rename Issue
    protected final Map<String, String> controlFileTableNameIndex;
    protected final Map<String, String> controlFileTableName;
    protected final Map<String, String> reconFileRename;

    protected final Map<String, String> insertValueIndicatorMap;

    protected final Map<String, List<VerifySchemaColumn>> tableSchemaColumns;

    // Added for RetriveTableProperty DataType Verification Issue
    protected final Map<String, List<VerifySchemaColumn>> tableHeaderSchemaColumns;
    protected final Map<String, List<VerifySchemaColumn>> tableTrailerSchemaColumns;

    protected final Map<String, String[]> tableSchemaHeader;
    private Map<String, TransformRule> ruleMap = new HashMap<>();
    private Map<String, Script> gcsMap = new HashMap<>();
    private Map<String, Boolean> semaphoreMap;
    private AtomicReference<GroovyShell> engine = new AtomicReference<>(new GroovyShell());
    private final static ObjectMapper objectMapper = new ObjectMapper();

    private static final String GROUP_PREFIX = "(.{";
    private static final String GROUP_SUFFIX = "})";

    private String tableConfigParamPrefix = null;
    private String globalConfigParamPrefix = null;
    private String renameConfigParamPrefix = null;
    private String rulesConfigPrefix = null;
    private String configFilePath;
    private String ruleFilePath;
    private String paramFilePath;
    private String tableRenameFilePath;
    LazyObjectInspectorParameters inspectorParameters;

    public LocalFileTransformationRegistry() {

        this.tableRawSchema = new HashMap<>();
        this.tableSchemaColumns = new HashMap<>();
        // Added for RetriveTableProperty DataType Verification Issue
        this.tableHeaderSchemaColumns = new HashMap<>();
        this.tableTrailerSchemaColumns = new HashMap<>();

        this.tableSchemaHeader = new HashMap<>();
        this.tableGeneralConfigs = new HashMap<>();
        this.tableRenameConfigs = new HashMap<>();
        this.tableFixedWidthParseableRegexData = new HashMap<>();
        this.tableFixedWidthToDelimitedRegexData = new HashMap<>();
        this.tableFixedWidthParseableRegexHeader = new HashMap<>();
        this.tableFixedWidthToDelimitedRegexHeader = new HashMap<>();
        this.tableFixedWidthParseableRegexTrailer = new HashMap<>();
        this.tableFixedWidthToDelimitedRegexTrailer = new HashMap<>();
        this.headerColSchemaMap = new HashMap<>();
        this.trailerColSchemaMap = new HashMap<>();
        this.dataIndicatorMap = new HashMap<>();
        this.headerIndicatorMap = new HashMap<>();
        this.trailerIndicatorMap = new HashMap<>();
        this.tableLevelSourcingTypeMap = new HashMap<>();
        this.keyColumnsMap = new HashMap<>();
        this.afterValueIndicatorMap = new HashMap<>();
        this.beforeValueIndicatorMap = new HashMap<>();
        this.deleteValueIndicatorMap = new HashMap<>();

        // Added for ControlFile and Recon File Rename Issue
        this.controlFileTableNameIndex = new HashMap<>();
        this.controlFileTableName = new HashMap<>();
        this.reconFileRename = new HashMap<>();

        this.insertValueIndicatorMap = new HashMap<>();

    }

    public void refreshConfig() throws InitializationException {

        this.conf = new Configuration(false);
        File tableConfigDir = new  File(this.configFilePath);
        if(tableConfigDir.isDirectory()) {
            for(File file : tableConfigDir.listFiles()) {
                String fileName = file.getName().toLowerCase();
                if(fileName.endsWith("tables_config.xml")) {
                    getLogger().info("Loading file : " + fileName);
                    this.conf.addResource(new Path(file.getAbsolutePath()));
                }
            }
        } else {
            this.conf.addResource(new Path(this.configFilePath));
        }

        this.conf.addResource(new Path(this.ruleFilePath));
        this.conf.addResource(new Path(this.paramFilePath));

        if (StringUtils.isNotEmpty(this.tableRenameFilePath)) {
            this.conf.addResource(new Path(this.tableRenameFilePath));
        }

        tableConfigParamPrefix = conf.get("param.prefix", DEFAULT_PARAM_PREFIX) + ADD_PREFIX_CONFIG_DIFFEN_TABLE;
        globalConfigParamPrefix = conf.get("param.prefix", DEFAULT_PARAM_PREFIX) + ADD_PREFIX_DIFFEN_GLOBAL_CONFIG;
        renameConfigParamPrefix = conf.get("param.prefix", DEFAULT_PARAM_PREFIX) + ADD_PREFIX_DIFFEN_RENAME_TABLE;
        rulesConfigPrefix = conf.get("param.prefix", DEFAULT_PARAM_PREFIX) + ADD_PREFIX_DIFFEN_RULES;

        final String globalConfigPattern = globalConfigParamPrefix + "(.*)";
        final String tablePattern = tableConfigParamPrefix + "(.*)" + SUFFIX_CONFIG_COLSCHEMA;
        final String renameTableConfigPattern = renameConfigParamPrefix + "(.*)";
        final String keyColsPattern = tableConfigParamPrefix + "(.*)" + SUFFIX_CONFIG_KEYCOLS;

        final String headerColSchemaPattern = tableConfigParamPrefix + "(.*)"+ HEADER_COLSCHEMA_SUFFIX;
        final String trailerColSchemaPattern = tableConfigParamPrefix + "(.*)"+ TRAILER_COLSCHEMA_SUFFIX;
        final String dataIndicatorPattern = tableConfigParamPrefix + "(.*)"+ DATA_INDICATOR;
        final String headerIndicatorPattern = tableConfigParamPrefix + "(.*)"+ HEADER_INDICATOR;
        final String trailerIndicatorPattern = tableConfigParamPrefix + "(.*)"+ TRAILER_INDICATOR;
        final String sourcingTypePattern = tableConfigParamPrefix + "(.*)"+ SOURCING_TYPE;
        final String afterValueIndicatorPattern = tableConfigParamPrefix + "(.*)"+ SUFFIX_CONFIG_AFTERVALUE;
        final String beforeValueIndicatorPattern = tableConfigParamPrefix + "(.*)"+ SUFFIX_CONFIG_BEFOREVALUE;
        final String insertValueIndicatorPattern = tableConfigParamPrefix + "(.*)"+ SUFFIX_CONFIG_INSERTVALUE;
        final String deleteValueIndicatorPattern = tableConfigParamPrefix + "(.*)"+ SUFFIX_CONFIG_DELETEVALUE;
        // Added for ControlFile and Recon File Rename Issue
        final String controlFileTableNameIndexPattern = tableConfigParamPrefix + "(.*)"+ CONTROL_FILE_TABLENAME_INDEX;
        final String controlFileTableNamePattern = tableConfigParamPrefix + "(.*)"+ CONTROL_FILE_TABLENAME;
        final String reconFileRenamePattern = tableConfigParamPrefix + "(.*)"+ RECON_FILE_RENAME;

        Map<String, String> globalConfigs = conf.getValByRegex(globalConfigPattern);
        Map<String, String> tableConfigs = conf.getValByRegex(tablePattern);
        Map<String, String> renameTableConfigs = conf.getValByRegex(renameTableConfigPattern);
        Map<String,String> headerColSchemaConfigs = conf.getValByRegex(headerColSchemaPattern);
        Map<String,String> trailerColSchemaConfigs = conf.getValByRegex(trailerColSchemaPattern);
        Map<String,String> dataIndicatorConfigs = conf.getValByRegex(dataIndicatorPattern);
        Map<String,String> headerIndicatorConfigs = conf.getValByRegex(headerIndicatorPattern);
        Map<String,String> trailerIndicatorConfigs = conf.getValByRegex(trailerIndicatorPattern);
        Map<String,String> sourcingTypeConfigs = conf.getValByRegex(sourcingTypePattern);
        Map<String,String> keyColsConfigs = conf.getValByRegex(keyColsPattern);
        Map<String,String> afterValueConfigs = conf.getValByRegex(afterValueIndicatorPattern);
        Map<String,String> beforeValueConfigs = conf.getValByRegex(beforeValueIndicatorPattern);
        Map<String,String> insertValueConfigs = conf.getValByRegex(insertValueIndicatorPattern);
        Map<String,String> deleteValueConfigs = conf.getValByRegex(deleteValueIndicatorPattern);
        // Added for ControlFile and Recon File Rename Issue
        Map<String,String> controlFileTableNameIndexConfigs = conf.getValByRegex(controlFileTableNameIndexPattern);
        Map<String,String> controlFileTableNameConfigs = conf.getValByRegex(controlFileTableNamePattern);
        Map<String,String> reconFileRenameConfigs = conf.getValByRegex(reconFileRenamePattern);

        tableGeneralConfigs.putAll(getTableRawSchema(globalConfigs, globalConfigParamPrefix, ""));
        tableRawSchema.putAll(getTableRawSchema(tableConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_COLSCHEMA));
        tableRenameConfigs.putAll(getTableRawSchema(renameTableConfigs, renameConfigParamPrefix, ""));
        keyColumnsMap.putAll(getTableRawSchema(keyColsConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_KEYCOLS));

        headerColSchemaMap.putAll(getTableRawSchema(headerColSchemaConfigs, tableConfigParamPrefix, HEADER_COLSCHEMA_SUFFIX));
        trailerColSchemaMap.putAll(getTableRawSchema(trailerColSchemaConfigs,tableConfigParamPrefix, TRAILER_COLSCHEMA_SUFFIX));

        dataIndicatorMap.putAll(getTableRawSchema(dataIndicatorConfigs,tableConfigParamPrefix, DATA_INDICATOR));
        headerIndicatorMap.putAll(getTableRawSchema(headerIndicatorConfigs,tableConfigParamPrefix, HEADER_INDICATOR));
        trailerIndicatorMap.putAll(getTableRawSchema(trailerIndicatorConfigs,tableConfigParamPrefix, TRAILER_INDICATOR));
        tableLevelSourcingTypeMap.putAll(getTableRawSchema(sourcingTypeConfigs,tableConfigParamPrefix, "."+SOURCING_TYPE));

        afterValueIndicatorMap.putAll(getTableRawSchema(afterValueConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_AFTERVALUE));
        beforeValueIndicatorMap.putAll(getTableRawSchema(beforeValueConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_BEFOREVALUE));
        insertValueIndicatorMap.putAll(getTableRawSchema(insertValueConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_INSERTVALUE));
        deleteValueIndicatorMap.putAll(getTableRawSchema(deleteValueConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_DELETEVALUE));

        // Added for ControlFile and Recon File Rename Issue
        controlFileTableNameIndex.putAll(getTableRawSchema(controlFileTableNameIndexConfigs, tableConfigParamPrefix, CONTROL_FILE_TABLENAME_INDEX));
        controlFileTableName.putAll(getTableRawSchema(controlFileTableNameConfigs, tableConfigParamPrefix, CONTROL_FILE_TABLENAME));
        reconFileRename.putAll(getTableRawSchema(reconFileRenameConfigs, tableConfigParamPrefix, RECON_FILE_RENAME));


        String fileDelimiter = tableGeneralConfigs.get(FILE_DELIMITER);
        String timestampFormat = tableGeneralConfigs.get(TIMESTAMP_COL_FORMAT);
        List<String> timestampFormats = new ArrayList<>();
        timestampFormats.add(timestampFormat);
        LazyObjectInspectorParameters inspectorParameters =
                new LazyObjectInspectorParametersImpl(false, (byte)0, false, timestampFormats, null, null);
        if (StringUtils.isNotBlank(fileDelimiter)) tableGeneralConfigs.put("escaped.filedelimiter", StringEscapeUtils.escapeXml11(StringEscapeUtils.unescapeJava(fileDelimiter)));

        inspectorParameters =
                new LazyObjectInspectorParametersImpl(false, (byte)0, false, timestampFormats, null, null);

        //This is applicable only for Valid types.
        //Load up the columns per table
        for (final Map.Entry<String, String> tableEntry : tableConfigs.entrySet()) {

            final String fqtn = tableEntry.getKey();
            final String tableName = fqtn.substring(tableConfigParamPrefix.length(),
                    fqtn.length() - SUFFIX_CONFIG_COLSCHEMA.length());

            final VerifySchema tableSchema = new VerifySchema(tableEntry.getValue(), true, inspectorParameters);
            final List<VerifySchemaColumn> dataSchemaColumns = tableSchema.getSchemaColumns();
            //build up the header that can be used to parse a row of data for this table.
            final String[] headerRow = new String[dataSchemaColumns.size()];
            for (int i = 0; i < dataSchemaColumns.size(); i++) {
                final VerifySchemaColumn verifySchemaColumn = dataSchemaColumns.get(i);
                headerRow[i] = verifySchemaColumn.getName();
            }

            // Added for RetriveTableProperty DataType Verification Issue
            //for Header VerifySchemaColumn usage
            if(headerColSchemaConfigs.containsKey(tableConfigParamPrefix+tableName+HEADER_COLSCHEMA_SUFFIX)){
                final VerifySchema tblHeaderSchema = new VerifySchema(headerColSchemaConfigs.get(tableConfigParamPrefix+tableName+HEADER_COLSCHEMA_SUFFIX),true, inspectorParameters);
                List<VerifySchemaColumn> headerSchemaColumns = tblHeaderSchema.getSchemaColumns();
                final String[] hdrRow = new String[headerSchemaColumns.size()];
                for (int i=1;i<headerSchemaColumns.size();i++){
                    final VerifySchemaColumn verifyHeaderSchemaColumn = headerSchemaColumns.get(i);
                    hdrRow[i]=verifyHeaderSchemaColumn.getName();
                }
                tableHeaderSchemaColumns.put(tableName.toLowerCase(),headerSchemaColumns);
            }

            // Added for RetriveTableProperty DataType Verification Issue
            //for Trailer VerifySchemaColumn usage
            if(trailerColSchemaConfigs.containsKey(tableConfigParamPrefix+tableName+TRAILER_COLSCHEMA_SUFFIX)) {
                final VerifySchema tblTrailerSchema = new VerifySchema(trailerColSchemaConfigs.get(tableConfigParamPrefix+tableName+TRAILER_COLSCHEMA_SUFFIX), true, inspectorParameters);
                List<VerifySchemaColumn> trailerSchemaColumns = tblTrailerSchema.getSchemaColumns();
                final String[] tlrRow = new String[trailerSchemaColumns.size()];
                for (int i = 1; i < trailerSchemaColumns.size(); i++) {
                    final VerifySchemaColumn verifyHeaderSchemaColumn = trailerSchemaColumns.get(i);
                    tlrRow[i] = verifyHeaderSchemaColumn.getName();
                }
                tableTrailerSchemaColumns.put(tableName.toLowerCase(),trailerSchemaColumns);
            }


            //Construct Fixed Width to Delimited
            if (getSourcingType().equals(SOURCING_TYPE_BATCH_FIXEDWIDTH)) {
                //TODO Fix the bottom and the top portion using streams as well

                getLogger().debug ("Table column schema : "+ tableEntry.getValue());
                //Data
                String dataIndicator = dataIndicatorMap.get(tableName);
                String dataParseableRegexStr = getParseableRegex(dataIndicator, dataSchemaColumns);
                StringBuilder dataToDelimitedRegex = getDelimitedRegex(dataIndicator, fileDelimiter, dataSchemaColumns);

                tableFixedWidthParseableRegexData.put(tableName.toLowerCase(), dataParseableRegexStr);
                tableFixedWidthToDelimitedRegexData.put(tableName.toLowerCase(), StringUtils.removeEnd(dataToDelimitedRegex.toString(), fileDelimiter));

                //Header
                if (headerColSchemaMap.containsKey(tableName) && StringUtils.isNotBlank(headerColSchemaMap.get(tableName))) {
                    final VerifySchema headerSchema = new VerifySchema(headerColSchemaMap.get(tableName), true, inspectorParameters);
                    final List<VerifySchemaColumn> headerSchemaCols = headerSchema.getSchemaColumns();

                    String headerIndicator = headerIndicatorMap.get(tableName);
                    String headerParseableRegexStr = getParseableRegex(headerIndicator, headerSchemaCols);
                    StringBuilder headerToDelimitedRegex = getDelimitedRegex(headerIndicator, fileDelimiter, headerSchemaCols);

                    tableFixedWidthParseableRegexHeader.put(tableName.toLowerCase(), headerParseableRegexStr);
                    tableFixedWidthToDelimitedRegexHeader.put(tableName.toLowerCase(), StringUtils.removeEnd(headerToDelimitedRegex.toString(), fileDelimiter));
                }

                //Trailer
                if (trailerColSchemaMap.containsKey(tableName) && StringUtils.isNotBlank(trailerColSchemaMap.get(tableName))) {
                    final VerifySchema trailerSchema = new VerifySchema(trailerColSchemaMap.get(tableName), true, inspectorParameters);
                    final List<VerifySchemaColumn> trailerSchemaCols = trailerSchema.getSchemaColumns();

                    String trailerIndicator = trailerIndicatorMap.get(tableName);
                    String trailerParseableRegexStr = getParseableRegex(trailerIndicator, trailerSchemaCols);
                    StringBuilder trailerToDelimitedRegex = getDelimitedRegex(trailerIndicator, fileDelimiter, trailerSchemaCols);

                    tableFixedWidthParseableRegexTrailer.put(tableName.toLowerCase(), trailerParseableRegexStr);
                    tableFixedWidthToDelimitedRegexTrailer.put(tableName.toLowerCase(), StringUtils.removeEnd(trailerToDelimitedRegex.toString(), fileDelimiter));
                }

            }

            tableSchemaHeader.put(tableName.toLowerCase(), headerRow);
            tableSchemaColumns.put(tableName.toLowerCase(), dataSchemaColumns);

        }
    }

    @OnEnabled
    public void enable(final ConfigurationContext configurationContext) throws InitializationException {
        this.semaphoreMap = new HashMap<>();
        this.configFilePath = configurationContext.getProperty(CONFIG_FILE_PATH).getValue();
        this.ruleFilePath = configurationContext.getProperty(RULE_FILE_PATH).getValue();
        this.paramFilePath = configurationContext.getProperty(PARAM_FILE_PATH).getValue();
        if (configurationContext.getProperty(TABLE_RENAME_FILE_PATH) != null && StringUtils.isNotEmpty(configurationContext.getProperty(TABLE_RENAME_FILE_PATH).getValue())) {
            this.tableRenameFilePath = configurationContext.getProperty(TABLE_RENAME_FILE_PATH).getValue();
        }
        refreshConfig();
    }

    @OnDisabled
    public void disable(final ConfigurationContext configurationContext) throws InitializationException {
        this.semaphoreMap.clear();
    }

    /**
     * Get delimited regex
     * @param indicator
     * @param fileDelimiter
     * @param schemaColumns
     * @return
     */
    protected StringBuilder getDelimitedRegex(String indicator, String fileDelimiter, List<VerifySchemaColumn> schemaColumns) {
        StringBuilder toDelimitedRegex = new StringBuilder("");
        int colSize = schemaColumns.size();
        for (int i = 0; i < colSize; i++) {
            toDelimitedRegex.append("$").append(i + 1).append(fileDelimiter);
        }
        return toDelimitedRegex;
    }

    /**
     * Get parsable regex
     * @param indicator
     * @param schemaColumns
     * @return
     */
    protected String getParseableRegex(String indicator, List<VerifySchemaColumn> schemaColumns) {
        StringBuilder parseableRegex = new StringBuilder("^");
        int startIndex = 0;
        if (StringUtils.isNotBlank(indicator)){
            parseableRegex.append("(" + indicator + ")");
            //Start from Index 1. Skipping the first column because of indicator
            startIndex = 1;
        }

        for (int n = startIndex; n < schemaColumns.size(); n++) {
            parseableRegex.append(GROUP_PREFIX).append(schemaColumns.get(n).getWidth()).append(GROUP_SUFFIX);
        }

        return parseableRegex.toString();
    }

    /**
     *
     * To get the getTableRawSchema as Map
     * @param configs
     * @param prefix
     * @param suffix
     * @return
     */
    protected Map<String, String> getTableRawSchema(Map<String, String> configs, String prefix, String suffix) {
        Map<String, String> tableRawSchemaLocal = new HashMap<>();
        for (final Map.Entry<String, String> tableEntry : configs.entrySet()) {
            final String fqtn = tableEntry.getKey();
            final String tableName = fqtn.substring(prefix.length(),
                    fqtn.length() - suffix.length());

            tableRawSchemaLocal.put(tableName.toLowerCase(), tableEntry.getValue());
            //getLogger().info (tableName.toLowerCase() +"---->" + tableEntry.getValue());
        }
        return tableRawSchemaLocal;
    }

    @Override
    protected PropertyDescriptor getSupportedDynamicPropertyDescriptor(final String propertyDescriptorName) {
        return new PropertyDescriptor.Builder().required(false).name(propertyDescriptorName)
                .addValidator(StandardValidators.NON_EMPTY_VALIDATOR).dynamic(true).expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
                .build();
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return DESCRIPTORS;
    }

    @Override
    public boolean isTableExist(final String tableName) {
        String localTableName = getTranslatedTableName(tableName);
        if (tableRawSchema.containsKey(localTableName)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<VerifySchemaColumn> getVerifySchemaColumnsForTable(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (tableSchemaColumns.containsKey(localTableName)) {
            return tableSchemaColumns.get(localTableName);
        }
        return new ArrayList<>();
    }

    // Added for RetriveTableProperty DataType Verification Issue
    @Override
    public List<VerifySchemaColumn> getVerifySchemaColumnsForTableHeader(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (tableHeaderSchemaColumns.containsKey(localTableName)) {
            return tableHeaderSchemaColumns.get(localTableName);
        }
        return new ArrayList<>();
    }

    // Added for RetriveTableProperty DataType Verification Issue
    @Override
    public List<VerifySchemaColumn> getVerifySchemaColumnsForTableTrailer(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (tableTrailerSchemaColumns.containsKey(localTableName)) {
            return tableTrailerSchemaColumns.get(localTableName);
        }
        return new ArrayList<>();
    }

    // Added for Exceptional Table Issue
    @Override
    public Set<String> getExceptionalTables() {
        if (StringUtils.isNotBlank(tableGeneralConfigs.get(EXCEPTIONAL_TABLES_LIST))) {
            return new HashSet<String>(Arrays.asList(tableGeneralConfigs.get(EXCEPTIONAL_TABLES_LIST).toUpperCase().split(",")));
        } else {
            return new HashSet<String>();
        }
    }

    @Override
    public String[] getDataHeaderForTable(final String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!tableSchemaHeader.containsKey(localTableName)) {
            return null;
        }
        return tableSchemaHeader.get(localTableName);
    }


    public boolean isCDC() {
        return getSourcingType().equals(SOURCING_TYPE_CDC);
    }

    protected String getSourcingType() {
        if (!tableGeneralConfigs.containsKey(SOURCING_TYPE)) {
            getLogger().error("Sourcing type parameter " + globalConfigParamPrefix + SOURCING_TYPE + "is Mandatory");
            return globalConfigParamPrefix + SOURCING_TYPE + "_NOT_SET";
        } else {
            return tableGeneralConfigs.get(SOURCING_TYPE);
        }
    }

    @Override
    public int getFirstDataColumnPosition() {
        if (isCDC()) {
            return 4;
        } else {
            return 0;
        }
    }
    /**
     * This will return the data header with the source columns pre-pended to it.
     * @param tableName - the name of the table to return the data for
     * @return An String[] that contains the data and source type headers.
     */
    @Override
    public String[] getFullDataHeaderForTable(final String tableName) {

        String localTableName=getTranslatedTableName(tableName);

        getLogger().info ("Lowercase TableName : "+localTableName);

        final String[] dataHeaders = getDataHeaderForTable(localTableName);

        if (isCDC()) {
            final int cdcHeadersSize = 4;
            final String[] fullHeader = new String[cdcHeadersSize + dataHeaders.length];
            //add the CDC headers
            fullHeader[0] = "c_journaltime";
            fullHeader[1] = "c_transactionid";
            fullHeader[2] = "c_operationtype";
            fullHeader[3] = "c_userid";

            System.arraycopy(dataHeaders, 0, fullHeader, 4, dataHeaders.length);
            return fullHeader;
        } else {
            return dataHeaders;
        }

    }
    /**
     * Retrieves the rule from conf
     *
     * @param ruleName
     *            The rule to retrieve
     * @return A TransformRule object with the rule details
     */
    @Override
    public TransformRule retrieveRule(String ruleName) {
        TransformRule rule = ruleMap.get(ruleName);
        if (rule == null) {
            rule = new TransformRule();
            String cleanName = ruleName;
            if (ruleName.contains("(")) {
                cleanName = ruleName.substring(0, ruleName.indexOf('('));
            }
            rule.setName(cleanName);
            rule.setParameters(new HashMap<String, String>());
            String params = conf.get(rulesConfigPrefix + cleanName + PARAMS);
            if (params != null) {
                String[] paramKeys = params.split(",", -1);
                String[] paramValues = null;
                if (ruleName.contains("(")) {
                    String values = ruleName.substring(ruleName.indexOf('(') + 1, ruleName.indexOf(")"));
                    paramValues = values.split(",", -1);
                }
                for (int i = 0; i < paramKeys.length; i++) {
                    if (paramValues != null && paramValues.length > i) {
                        rule.getParameters().put(paramKeys[i], cleanValue(paramValues[i]));
                    } else {
                        rule.getParameters().put(paramKeys[i], null);
                    }
                }
            }

            rule.setCode(conf.get(rulesConfigPrefix + cleanName + CODE));

            if (null == rule.getCode()) {
                throw new RuntimeException("Rule " + rule.getName() + " not defined.");
            }

            ruleMap.put(ruleName, rule);
            if (!gcsMap.containsKey(rule.getName())) {
                gcsMap.put(ruleName, engine.get().parse(rule.getCode()));
            }
        }
        return rule;
    }

    private String cleanValue(String value) {
        String result = value;
        if (result.startsWith("'") && result.endsWith("'")) {
            result = result.substring(1, result.length() - 1);
        }
        if (result.startsWith("\"") && result.endsWith("\"")) {
            result = result.substring(1, result.length() - 1);
        }
        return result;
    }

    @Override
    public Script getScriptForRule(final TransformRule rule){
        if (!gcsMap.containsKey(rule.getName())) {
            gcsMap.put(rule.getName(), engine.get().parse(rule.getCode()));
        }

        return gcsMap.get(rule.getName());
    }

    @Override
    public String getTableColAllStringSchema(final String tableName) {

        String localTableName=getTranslatedTableName(tableName);

        if (!tableRawSchema.containsKey(localTableName)) {
            return null;
        }

        final StringBuilder schemaString = new StringBuilder();

        if (isCDC()) {
            schemaString.append("c_journaltime STRING^c_transactionid STRING^c_operationtype STRING^c_userid STRING NULLABLE^");
        }

        String cols[] = tableRawSchema.get(localTableName).split(DiffenConstants.PROP_LINE_SEPARATOR);
        for (String col : cols) {
            VerifySchemaColumn column = new VerifySchemaColumn(col, inspectorParameters);
            if (!column.isDrop()) {
                schemaString.append(column.getName()).append(" ").append("STRING").append(" ").append("NULLABLE").append("^");
            }
        }

        return schemaString.toString().substring(0, schemaString.length() - 1);

    }

    @Override
    public String getTableColAllStringSchemaAsAvro(final String tableName){
        String lCaseTableName = tableName.toLowerCase();
        String localTableName = getTranslatedTableName(lCaseTableName);

        final String rawTableSchema = getTableColAllStringSchema(localTableName);
        return convertToAvroSchema(localTableName, rawTableSchema);
    }

    @Override
    public String getGlobalConfig(final String configName) {
        return tableGeneralConfigs.getOrDefault(configName, "vds");
    }

    @Override
    public Map<String, String> getAllGlobalConfigs() {
        return new HashMap<>(tableGeneralConfigs);
    }

    public String getTableSchema(final String tableName) {

        String localTableName=getTranslatedTableName(tableName);

        if (!tableRawSchema.containsKey(localTableName)) {
            return null;
        }

        final StringBuilder schemaString = new StringBuilder("ROWID STRING^FILENAME STRING^");
        schemaString.append(getGlobalConfig(STORAGE_PARTITION_NAME)).append(" STRING^");

        if (isCDC()) {
            schemaString.append("c_journaltime TIMESTAMP^c_transactionid STRING^c_operationtype VARCHAR(1)^c_userid STRING NULLABLE^");
        }

        String cols[] = tableRawSchema.get(localTableName).split(DiffenConstants.PROP_LINE_SEPARATOR);
        for (String col : cols) {
            VerifySchemaColumn column = new VerifySchemaColumn(col, inspectorParameters);
            if (!column.isDrop()) {
                schemaString.append(column.getName()).append(" ").append(column.getDatatype()).append(" ").append(column.isNotNull() ? "" : "NULLABLE").append("^");
            }
        }
        return schemaString.toString().substring(0, schemaString.length() - 1);
    }

    public String getInvalidTableSchema(String tableName) {
        if (!tableGeneralConfigs.containsKey(INVALID_TYPE_COLSCHEMA)) {
            return null;
        } else {
            return tableGeneralConfigs.get(INVALID_TYPE_COLSCHEMA);
        }
    }

    public String getWarnTableSchema(String tableName) {
        if (!tableGeneralConfigs.containsKey(WARN_TYPE_COLSCHEMA)) {
            return null;
        } else {
            return tableGeneralConfigs.get(WARN_TYPE_COLSCHEMA);
        }
    }

    public String getRowHistoryTableSchema(String tableName) {
        if (!tableGeneralConfigs.containsKey(ROWHISTORY_COLSCHEMA)) {
            return null;
        } else {
            return tableGeneralConfigs.get(ROWHISTORY_COLSCHEMA);
        }
    }

    @Override
    public String getTableSchemaAsAvro(final String tableName) {
        String lCaseTableName = tableName.toLowerCase();
        String localTableName = getTranslatedTableName(lCaseTableName);

        final String rawTableSchema = getTableSchema(localTableName);
        return convertToAvroSchema(localTableName, rawTableSchema);
    }

    @Override
    public String getTableSchemaAsAvro(final String tableName,
                                                 final String[] headerColumns) {
        String lCaseTableName = tableName.toLowerCase();
        String localTableName = getTranslatedTableName(lCaseTableName);

        String rawTableSchema = getTableSchema(localTableName);
        //map of column name and datatype
        //for ex: col1 -> col1 String
        Map<String, String> configColumnMap =
                Arrays.stream(rawTableSchema.split(DiffenConstants.PROP_LINE_SEPARATOR))
                .collect(Collectors.toMap(x -> x.split(" ")[0].trim().toUpperCase(),
                        Function.identity()));

        String headerRawSchema = Arrays.stream(headerColumns)
                .map(x -> configColumnMap.get(x.trim().toUpperCase()))
                .collect(Collectors.joining(DiffenConstants.PROP_LINE_SEPARATOR.replace("\\", "")));
        final StringBuilder schemaString = new StringBuilder("ROWID STRING^FILENAME STRING^");
        schemaString.append(getGlobalConfig(STORAGE_PARTITION_NAME)).append(" STRING^");

        if (isCDC()) {
            schemaString.append("c_journaltime TIMESTAMP^c_transactionid STRING^c_operationtype VARCHAR(1)^c_userid STRING NULLABLE^");
        }
        schemaString.append(headerRawSchema);
        return convertToAvroSchema(localTableName, schemaString.toString());
    }

    public String getTranslatedTableName(String tableName) {
        String localTableName=tableName.toLowerCase();
        return tableRenameConfigs.containsKey(localTableName) ? tableRenameConfigs.get(localTableName).toLowerCase() : localTableName;
    }

    @Override
    public String getFixedWidthParseableRegexForData(String tableName) {
        return tableFixedWidthParseableRegexData.get(tableName);
    }

    @Override
    public String getFixedWidthToDelimitedRegexForData(String tableName) {
        return tableFixedWidthToDelimitedRegexData.get(tableName);
    }

    @Override
    public String getFixedWidthParseableRegexForHeader(String tableName) {
        return tableFixedWidthParseableRegexHeader.get(tableName);
    }

    @Override
    public String getFixedWidthToDelimitedRegexForHeader(String tableName) {
        return tableFixedWidthToDelimitedRegexHeader.get(tableName);
    }

    @Override
    public String getFixedWidthParseableRegexForTrailer(String tableName) {
        return tableFixedWidthParseableRegexTrailer.get(tableName);
    }

    @Override
    public String getFixedWidthToDelimitedRegexForTrailer(String tableName) {
        return tableFixedWidthToDelimitedRegexTrailer.get(tableName);
    }

    //
    @Override
    public String getInvalidTableSchemaAsAvro(final String tableName) {

        String localTableName=getTranslatedTableName(tableName);

        final String rawTableSchema = getInvalidTableSchema(localTableName);
        return convertToAvroSchema(localTableName, rawTableSchema);
    }

    @Override
    public String getWarnTableSchemaAsAvro(final String tableName) {

        String localTableName=getTranslatedTableName(tableName);

        final String rawTableSchema = getWarnTableSchema(localTableName);
        return convertToAvroSchema(localTableName, rawTableSchema);
    }

    @Override
    public String getRowHistoryTableSchemaAsAvro(final String tableName) {

        String localTableName=getTranslatedTableName(tableName);

        final String rawTableSchema = getRowHistoryTableSchema(localTableName);
        return convertToAvroSchema(localTableName, rawTableSchema);
    }

    @Override
    public String getTableHeaderFormat(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        getLogger().info("table name before replace:"+tableName+"table name after replace:"+localTableName);
        if (!headerColSchemaMap.containsKey(localTableName)) {
            return null;
        }
        getLogger().info("Header format for tablename"+ headerColSchemaMap.get(tableName));
        getLogger().info("map is "+ headerColSchemaMap.toString());
        getLogger().info(tableName);
        return headerColSchemaMap.get(localTableName);
    }

    @Override
    public String getTableTrailerFormat(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!trailerColSchemaMap.containsKey(localTableName)) {
            return null;
        }
        return trailerColSchemaMap.get(localTableName);
    }

    @Override
    public String getTableDataIndicator(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!dataIndicatorMap.containsKey(localTableName)) {
            return null;
        }
        return dataIndicatorMap.get(localTableName);
    }

    @Override
    public String getTableHeaderIndicator(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        getLogger().info("table name before replace:"+tableName+"table name after replace:"+localTableName);
        if (!headerIndicatorMap.containsKey(localTableName)) {
            return null;
        }
        getLogger().info("map is "+ headerIndicatorMap.toString());
        getLogger().info(tableName);
        getLogger().info("Header Indicator for tablename"+ headerIndicatorMap.get(tableName));
        return headerIndicatorMap.get(localTableName);
    }

 @Override
    public String getSourcingType(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!tableLevelSourcingTypeMap.containsKey(localTableName)) {
            return getSourcingType();
        }
        return tableLevelSourcingTypeMap.get(localTableName);
    }

    @Override
    public String getTableTrailerIndicator(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!trailerIndicatorMap.containsKey(localTableName)) {
            return null;
        }
        return trailerIndicatorMap.get(localTableName);
    }

    @Override
    public String getKeyColumnsForTable(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!keyColumnsMap.containsKey(localTableName)) {
            return null;
        }
        return keyColumnsMap.get(localTableName);
    }

    @Override
    public String getTableAfterValueIndicator(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!afterValueIndicatorMap.containsKey(localTableName)) {
            return null;
        }
        return afterValueIndicatorMap.get(localTableName);
    }

    @Override
    public String getTableBeforeValueIndicator(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!beforeValueIndicatorMap.containsKey(localTableName)) {
            return null;
        }
        return beforeValueIndicatorMap.get(localTableName);
    }

    @Override
    public String getTableInsertValueIndicator(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!insertValueIndicatorMap.containsKey(localTableName)) {
            return null;
        }
        return insertValueIndicatorMap.get(localTableName);
    }

    @Override
    public String getTableDeleteValueIndicator(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!deleteValueIndicatorMap.containsKey(localTableName)) {
            return null;
        }
        return deleteValueIndicatorMap.get(localTableName);
    }

    // Added for ControlFile and Recon File Rename Issue
    @Override
    public String getControlFileTableNameIndex(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!controlFileTableNameIndex.containsKey(localTableName)) {
            return null;
        }
        return controlFileTableNameIndex.get(localTableName);
    }

    // Added for ControlFile and Recon File Rename Issue
    @Override
    public String getControlFileTableName(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!controlFileTableName.containsKey(localTableName)) {
            return null;
        }
        return controlFileTableName.get(localTableName);
    }

    // Added for ControlFile and Recon File Rename Issue
    @Override
    public String getreconfilerename(String tableName) {
        String localTableName=getTranslatedTableName(tableName);
        if (!reconFileRename.containsKey(localTableName)) {
            return null;
        }
        return reconFileRename.get(localTableName);
    }

    @Override
    public int getIndexForColumn(final String tableName, final String columnName) {
        ArrayList<String> columnNames = new ArrayList<String>();
        String tableSchema = getTableSchema(tableName);
        final String[] columnsWithType = tableSchema.split("\\^", -1);
        for (int i = 0; i < columnsWithType.length; i++) {
            columnNames.add(columnsWithType[i].split(" ", -1)[0].toLowerCase());
        }
        return columnNames.indexOf(columnName.toLowerCase());
    }

    private String convertToAvroSchema(final String tableName, final String rawTableSchema) {
        getLogger().debug("Raw table schema is " + rawTableSchema);
        if (null == rawTableSchema) {
            return null;
        }
        //now walk through it and build up the Avro schema
        final String[] schemaFields = rawTableSchema.split(DiffenConstants.PROP_LINE_SEPARATOR);

        final ByteArrayOutputStream schemaByteArrayOutputStream = new ByteArrayOutputStream();
        try (JsonGenerator schemaJsonGenerator = objectMapper.getFactory().createGenerator(schemaByteArrayOutputStream, JsonEncoding.UTF8)) {
            schemaJsonGenerator.writeStartObject();
            writeAvroSchemaHeader(tableName, schemaJsonGenerator);

            startAvroSchemaFields(schemaJsonGenerator);
            for (final String schemaField : schemaFields) {
                final String[] nameType = schemaField.split(" ", -1);
                boolean isNullable = false;
                if (nameType.length > 2 && nameType[2].equals("NULLABLE")) {
                    isNullable = true;
                }

                PrimitiveTypeInfo hiveDataType = toHiveTypeInfo(nameType[1]);
                writeSchemaElementAsJson(schemaJsonGenerator, nameType[0], nameType[1], hiveDataType, isNullable);
            }

            endAvroSchemaFields(schemaJsonGenerator);
            schemaJsonGenerator.writeEndObject();
            schemaJsonGenerator.flush();
        } catch (IOException e) {
            getLogger().error("Caught exception trying to generate the Avro schema", e);
            return null;
        }
        return schemaByteArrayOutputStream.toString().toLowerCase();
    }

    private PrimitiveTypeInfo toHiveTypeInfo (String dbDataType) {
        return TypeInfoFactory.getPrimitiveTypeInfo(dbDataType.toLowerCase());
    }

    private void writeAvroSchemaHeader(final String name, final JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.writeStringField("name", name);
        jsonGenerator.writeStringField("type", "record");
    }

    private void startAvroSchemaFields(final JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.writeArrayFieldStart("fields");
    }
    private void endAvroSchemaFields(final JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.writeEndArray();
    }
    private void writeSchemaElementAsJson(
            final JsonGenerator jsonGenerator,
            final String fieldName,
            final String rawType,
            final PrimitiveTypeInfo fieldType,
            final boolean nullable) throws IOException {

        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("name", fieldName);

        switch (fieldType.getPrimitiveCategory()) {

            case BOOLEAN:
                writePrimitiveType(jsonGenerator,"boolean", nullable);
                break;
            case SHORT:
            case INT:
                writePrimitiveType(jsonGenerator,"int", nullable);
                break;
            case LONG:
                writePrimitiveType(jsonGenerator,"long", nullable);
                break;
            case FLOAT:
                writePrimitiveType(jsonGenerator,"float", nullable);
                break;
            case DOUBLE:
                writePrimitiveType(jsonGenerator,"double", nullable);
                break;
            case DECIMAL: //For lack of binding between nifi and Avro decimal type, Decimals are stored as strings on Storage and cast back as decimal on Diffen.
            case CHAR:
            case VARCHAR:
            case STRING:
            case DATE:
            case TIMESTAMP:
                writePrimitiveType(jsonGenerator,"string", nullable);
                break;
            case UNKNOWN:
                throw new IllegalArgumentException("Invalid type: " + fieldType.getTypeName());
        }

        jsonGenerator.writeEndObject();

    }

    private void writeTimeLogicalType(JsonGenerator jsonGenerator, String logicalType, String underlyingType, boolean nullable) throws IOException {
        if (nullable){
            jsonGenerator.writeStringField("default", null);
            jsonGenerator.writeArrayFieldStart("type");
            jsonGenerator.writeString("null");
            jsonGenerator.writeStartObject();
            writeTimeLogicalTypeInner(jsonGenerator, logicalType, underlyingType);
            jsonGenerator.writeEndObject();
            jsonGenerator.writeEndArray();
        }
        else {
            jsonGenerator.writeObjectFieldStart("type");
            writeTimeLogicalTypeInner(jsonGenerator, logicalType, underlyingType);
            jsonGenerator.writeEndObject();
        }
    }

    private void writeTimeLogicalTypeInner(JsonGenerator jsonGenerator, String logicalType, String underlyingType) throws IOException {
        jsonGenerator.writeStringField("type", underlyingType);
        jsonGenerator.writeStringField("logicalType", logicalType);
    }

    private void writePrimitiveType(JsonGenerator jsonGenerator, String typee, boolean nullable) throws IOException {
        if (nullable){
            jsonGenerator.writeArrayFieldStart("type");
            jsonGenerator.writeString("null");
            jsonGenerator.writeString(typee);
            jsonGenerator.writeEndArray();
            jsonGenerator.writeStringField("default", null);
        }
        else jsonGenerator.writeStringField("type", typee);
    }

    private void writeDecimalLogicalType(JsonGenerator jsonGenerator, String typee, boolean nullable) throws IOException {
        if (nullable){
            jsonGenerator.writeStringField("default", null);
            jsonGenerator.writeArrayFieldStart("type");
            jsonGenerator.writeString("null");
            jsonGenerator.writeStartObject();
            writeDecimalLogicalTypeInner(jsonGenerator, typee);
            jsonGenerator.writeEndObject();
            jsonGenerator.writeEndArray();
        }
        else {
            jsonGenerator.writeObjectFieldStart("type");
            writeDecimalLogicalTypeInner(jsonGenerator, typee);
            jsonGenerator.writeEndObject();
        }
    }

    //FIXME Find a better name for this
    private void writeDecimalLogicalTypeInner(JsonGenerator jsonGenerator, String decimalWithPrecScale) throws IOException {
        int precision = 0;
        int scale = 0;
        if (StringUtils.contains(decimalWithPrecScale, ",")){
            String [] precisionAndScale = StringUtils.substringBetween(decimalWithPrecScale, "(", ")").split(",");
            precision = Integer.parseInt(precisionAndScale[0]);
            scale =  Integer.parseInt(precisionAndScale[1]);
        }
        else {
            String precisionStr = StringUtils.substringBetween(decimalWithPrecScale, "(", ")");
            precision = Integer.parseInt(precisionStr);
            scale =  0;
        }

        jsonGenerator.writeStringField("logicalType", "decimal");
        jsonGenerator.writeNumberField("precision", precision);
        jsonGenerator.writeNumberField("scale", scale);
        jsonGenerator.writeStringField("type", "bytes");
    }

    @Override
    @OnDisabled
    public void close() throws Exception {
        this.conf = null;
        this.tableSchemaColumns.clear();
        this.ruleMap.clear();
    }
    private static class StringSerializer implements Serializer<String> {
        @Override
        public void serialize(final String value, final OutputStream output) throws SerializationException, IOException {
            output.write(value.getBytes(StandardCharsets.UTF_8));
        }
    }

    public synchronized void lockTable(String tableName) {
        if(semaphoreMap.containsKey(tableName) && semaphoreMap.get(tableName)) {
            throw new ProcessException("Trying to lock already locked table : " + tableName);
        }
        getLogger().info("Got lock for table : " + tableName);
        semaphoreMap.put(tableName, true);
    }

    public synchronized void unlockTable(String tableName) {
        getLogger().info("Releasing lock for table : " + tableName);
        semaphoreMap.put(tableName, false);
    }

    public synchronized boolean isTableLocked(String tableName) {
        return semaphoreMap.containsKey(tableName) && semaphoreMap.get(tableName);
    }

    @Override
    public CDCModels.TableConfiguration getTableConfig(String tableName) {
        List<CDCModels.Property> properties = new ArrayList<>();
        for(Map.Entry<String, String> entry: this.conf.getValByRegex(tableConfigParamPrefix+tableName+"\\.").entrySet()) {
            properties.add(new CDCModels.Property(entry.getKey(), entry.getValue()));
        }
        Collections.sort(properties);
        return new CDCModels.TableConfiguration(properties);
    }
}
