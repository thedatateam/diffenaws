/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package in.tdt.diffen.processors;

import in.tdt.diffen.services.TransformationRegistry;
import in.tdt.diffen.verifytypes.DiffenUtils;
import in.tdt.diffen.verifytypes.checker.TransformRule;
import in.tdt.diffen.verifytypes.checker.VerifySchemaColumn;
import groovy.lang.Binding;
import groovy.lang.Script;
import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.util.StopWatch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.apache.nifi.processor.util.StandardValidators.NON_EMPTY_VALIDATOR;

@Tags({"data transformation"})
@CapabilityDescription("Provides the ability to process Groovy rules against a column from a record in the flow file")
@SideEffectFree
@SupportsBatching
@Restricted("Provides the operator the ability to execute arbitrary code assuming all permissions that NiFi has.")
@ReadsAttributes({@ReadsAttribute(attribute = "tablename", description = "The table that is being processed"),
                  @ReadsAttribute(attribute = "row.metadata.added", description = "An idicator if the incoming flow file " +
                                                                            "has had a ROW ID pre-prended to the data.")})
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)

/**
 *This class is used to validate the records against rules provided in table config xml </n>
 *If any record validation got failure the flow files will be moved to failure relationship </n>
 *This class uses controller service  {@link TransformationRegistry} to get the rules </n>
 */
public class DataTransformation extends AbstractProcessor {

    static final PropertyDescriptor REGISTRY_SERVICE = new PropertyDescriptor.Builder()
            .name("configuration-registry-service")
            .displayName("Configuration Registry Service")
            .description("The Schema Registry Service for serializing/deserializing messages as well as schema retrieval.")
            .required(true)
            .identifiesControllerService(TransformationRegistry.class)
            .build();


    static final PropertyDescriptor DELIMITER = new PropertyDescriptor.Builder()
            .name("csv-delimiter")
            .displayName("CSV delimiter")
            .description("Delimiter character for CSV records")
            .addValidator(NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .defaultValue("\u0001")
            .build();

    static final PropertyDescriptor CHARSET = new PropertyDescriptor
            .Builder().name("Input Charset")
                      .description("The input character set")
                      .required(false)
                      .defaultValue("UTF-8")
                      .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
                      .addValidator(NON_EMPTY_VALIDATOR)
                      .build();

    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("Successfully retrieved schema from Schema Registry")
            .build();

    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("FlowFiles that failed to find a schema are sent to this relationship")
            .build();


    private static final Set<Relationship> RELATIONSHIPS;
    private static List<PropertyDescriptor> DESCRIPTORS;

    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(REGISTRY_SERVICE);
        descriptors.add(DELIMITER);
        descriptors.add(CHARSET);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);

        Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        RELATIONSHIPS = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.RELATIONSHIPS;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return this.DESCRIPTORS;
    }

    private AtomicReference<TransformationRegistry> transformationRegistry;

    @OnScheduled
    public void onScheduled(ProcessContext context) {
        this.transformationRegistry = new AtomicReference<>(context.getProperty(REGISTRY_SERVICE).asControllerService(TransformationRegistry.class));
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        final StopWatch stopWatch = new StopWatch(true);
        final char inputDelimiter = DiffenUtils.getDelimiter(context.getProperty(DELIMITER).evaluateAttributeExpressions(flowFile).getValue());
        final String inputCharset = context.getProperty(CHARSET).evaluateAttributeExpressions().getValue();
        final AtomicReference<String> tableName = new AtomicReference<>(getIncomingAttribute(flowFile, "tablename"));
        final AtomicReference<String> rowMetadataAdded = new AtomicReference<>(flowFile.getAttribute("row.metadata.added"));

        try {

            final AtomicReference<Map<String, Map<String, Long>>> columnRuleTimings = new AtomicReference<>(new HashMap<>());
            flowFile = session.write(flowFile, (rawIn, rawOut) -> {

                try (final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(rawOut));
                     final BufferedReader reader = new BufferedReader(new InputStreamReader(rawIn,inputCharset))) {

                    final List<VerifySchemaColumn> schemaColumns = transformationRegistry.get().getVerifySchemaColumnsForTable(tableName.get());

                    int processingColumnCount;

                    boolean rowMetadataHasBeenAdded = false;

                    if (null != rowMetadataAdded.get()) {
                        rowMetadataHasBeenAdded = Boolean.parseBoolean(rowMetadataAdded.get());
                    }
                    getLogger().info ("RowMetadata Added? "+ rowMetadataHasBeenAdded);
                    if (rowMetadataHasBeenAdded) {
                        processingColumnCount = transformationRegistry.get().getFirstDataColumnPosition() + 3;
                    } else{
                        processingColumnCount = transformationRegistry.get().getFirstDataColumnPosition();
                    }

                    String line;
                    while((line = reader.readLine()) != null) {

                        final String[] columns = StringUtils.splitByWholeSeparatorPreserveAllTokens(line,String.valueOf(inputDelimiter),-1);
                        if (columns.length != schemaColumns.size() + processingColumnCount ) {
                            throw new ProcessException("ERROR : Unexpected number of columns for table:" + tableName.get() +" Expected: " +
                                    (schemaColumns.size() + processingColumnCount) + " columns but record has: " + columns.length);

                        }


                        final Map.Entry<String, Map<String, Map<String, Long>>> result = processRecord(columns,processingColumnCount,
                                                                                                       schemaColumns, inputDelimiter);
                        writer.write(result.getKey());
                        writer.newLine();
                        columnRuleTimings.get().putAll(result.getValue());

                    }
                    writer.flush();
                }catch (final ProcessException e) {
                    getLogger().error(e.getMessage(),e);
                    throw e;
                }
            });
            stopWatch.stop();


            final StringBuilder provEventData = new StringBuilder();
            provEventData.append(String.format("Rule processing results for Table: %s ", tableName));
            provEventData.append(System.lineSeparator());
            for (Map.Entry<String, Map<String, Long>> columnRuleTiming : columnRuleTimings.get().entrySet()) {

                for (Map.Entry<String, Long> ruleTiming : columnRuleTiming.getValue().entrySet()) {
                    provEventData.append(String.format("Column %s Rule %s took %d ms to process",
                                                       columnRuleTiming.getKey(), ruleTiming.getKey(), ruleTiming.getValue()));
                    provEventData.append(System.lineSeparator());
                }
            }
            session.getProvenanceReporter().modifyContent(flowFile, provEventData.toString(), stopWatch.getDuration(TimeUnit.MILLISECONDS));
            session.transfer(flowFile, REL_SUCCESS);
        } catch (final ProcessException e) {
            session.transfer(flowFile, REL_FAILURE);
            throw e;
        }
    }

    /**
     * Process each records against rules
     * @param columns
     * @param processingColumnCount
     * @param schemaColumns
     * @param outputDelimiter
     * @return
     */
    private Map.Entry<String, Map<String, Map<String, Long>>> processRecord(final String[] columns, final int processingColumnCount, List<VerifySchemaColumn> schemaColumns, final char outputDelimiter) {

        final StringBuilder transformedRow = new StringBuilder();
        //pre-prend the processingColumns
        for(int i = 0; i < processingColumnCount; i++){
            transformedRow.append(columns[i]).append(outputDelimiter);
        }
        final Map<String, Map<String, Long>> columnRuleTimings = new HashMap<>();
        for(int i = 0; i < schemaColumns.size(); i++){

            final Map.Entry<String, Map<String, Map<String, Long>>> result = processColumn(outputDelimiter, schemaColumns.get(i), columns[i + processingColumnCount]);
            transformedRow.append(result.getKey());
            columnRuleTimings.putAll(result.getValue());

        }

        if (transformedRow.charAt(transformedRow.length() - 1) == outputDelimiter) {
            transformedRow.deleteCharAt(transformedRow.length() - 1);
        }
        return new AbstractMap.SimpleEntry<>(transformedRow.toString(), columnRuleTimings);
    }

    /**
     * Retreive rules for each columns from controller service
     * @param outputDelimiter
     * @param schemaColumn
     * @param columnValue
     * @return
     */
    private Map.Entry<String, Map<String, Map<String, Long>>> processColumn(final char outputDelimiter, VerifySchemaColumn schemaColumn, String columnValue) {
        final StringBuilder transformedRow = new StringBuilder();
        final StopWatch recordStopWatch = new StopWatch(true);
        final Map<String, Map<String, Long>> columnRuleTimings = new HashMap<>();
        final Map<String, Long> ruleTiming = new HashMap<>();
        final List<String> columnRules = schemaColumn.getRules();

        if (0 < columnRules.size()) {
            for (final String ruleName : columnRules) {
                final TransformRule rule = transformationRegistry.get().retrieveRule(ruleName);

                try {
                    recordStopWatch.stop();
                    columnValue = applyRule(rule, columnValue);

                    if (columnRules.indexOf(ruleName) == columnRules.size() - 1) {
                        transformedRow.append(columnValue);
                        transformedRow.append(outputDelimiter);
                    }

                    ruleTiming.put(ruleName, recordStopWatch.getDuration(TimeUnit.MILLISECONDS));
                    recordStopWatch.start();
                } catch (Exception ex) {
                    getLogger().error("Caught exception processing rule!", new Object[]{rule, ex});
                }
            }
            columnRuleTimings.put(schemaColumn.getName(), ruleTiming);
        } else {
            transformedRow.append(columnValue);
            transformedRow.append(outputDelimiter);
        }

        return new AbstractMap.SimpleEntry<>(transformedRow.toString(), columnRuleTimings);
    }

    /**
     * Applies a rule over certain column data
     *
     * @param rule The TransformRule object with the rule details
     * @param data The data of the column where apply the rule
     * @return The transformed data
     * @throws Exception
     */
    private String applyRule(TransformRule rule, String data) throws Exception {
        if (data == null) {
            return null;
        }
        try {
            // Send parameters to Groovy
            Binding binding = new Binding(rule.getParameters());
            binding.setVariable("DATA", data);
            Script script = transformationRegistry.get().getScriptForRule(rule);
            script.setBinding(binding);
            script.run();
            Object result = binding.getVariable("result");
            return result == null ? null : result.toString();
        } catch (Exception e) {
            throw new Exception("Rule: " + rule.getName() + " Data:" + data + " Error:" + e.getMessage());
        }

    }

    private String getIncomingAttribute(FlowFile flowFile, String incomingAttributeName) {
        final String attrName = flowFile.getAttribute(incomingAttributeName);
        if (null == attrName) {
            throw new ProcessException("Missing attribute " + incomingAttributeName + " on incoming flowfile");
        }
        return attrName;
    }
}
