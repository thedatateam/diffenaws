/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.services.TransformationRegistry;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Tags({"set global configs", "flowfile attributes"})
@CapabilityDescription("Fetches all the Global configs as flowfile attributes")
/**
 * This class is used to fetch the global config
 */
public class SetGlobalConfigsAsAttributes extends AbstractProcessor {

    private static final Logger logger = LoggerFactory.getLogger(SetGlobalConfigsAsAttributes.class);

    static final PropertyDescriptor REGISTRY_SERVICE = new PropertyDescriptor.Builder()
            .name("configuration-registry-service")
            .displayName("Configuration Registry Service")
            .description("The Schema Registry Service for serializing/deserializing messages as well as schema retrieval.")
            .required(true)
            .identifiesControllerService(TransformationRegistry.class)
            .build();

    static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("If the properties are all set as attributes")
            .build();


    static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Exception thrown when setting the attributes to the flow")
            .build();


    private static List<PropertyDescriptor> descriptors;
    private static Set<Relationship> relationships;
    private volatile TransformationRegistry transformationRegistry;

    static {
        List<PropertyDescriptor> props = new ArrayList<>();
        props.add(REGISTRY_SERVICE);
        descriptors = Collections.unmodifiableList(props);

        Set<Relationship> rels = new HashSet<>();
        rels.add(REL_SUCCESS);
        rels.add(REL_FAILURE);
        relationships = Collections.unmodifiableSet(rels);
    }

    @OnScheduled
    public void onScheduled(ProcessContext context) {
        this.transformationRegistry = context.getProperty(REGISTRY_SERVICE).asControllerService(TransformationRegistry.class);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relationships;
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        Map<String, String> allGlobalConfigs = transformationRegistry.getAllGlobalConfigs();
        FlowFile flowFile = session.get();
        if (flowFile == null) return;

        try {
            if (allGlobalConfigs == null || allGlobalConfigs.isEmpty()) {
                logger.debug("No Global Configs available in the TransformationRegistry");
            } else {
                flowFile = session.putAllAttributes(flowFile, allGlobalConfigs);
                logger.debug("All Global configs successfully set to the FlowFile");
            }
            session.transfer(flowFile, REL_SUCCESS);
        } catch (Exception e) {
            logger.error("Unable to set all global configs as attributes", e);
            session.transfer(flowFile, REL_FAILURE);
        }

    }
}
