/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.cdc.CDCModels;
import in.tdt.diffen.processors.transform.SourceTableConfigTransformer;
import in.tdt.diffen.services.TransformationRegistry;
import org.apache.commons.io.FileUtils;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;

import java.io.File;
import java.io.FileFilter;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.regex.Pattern;

import static in.tdt.diffen.verifytypes.DiffenConstants.*;
import static java.lang.Thread.sleep;
import static org.apache.nifi.processor.util.StandardValidators.NON_EMPTY_VALIDATOR;

@Tags({"table config processor to consume source config changes"})
@CapabilityDescription("Table config Processor to consume source xml config files whenever available in incoming directory.")
@SideEffectFree
@SupportsBatching
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
/**
 * This class is used to retrieve the table properties from controller service{@link  TransformationRegistry}based on table name </n>
 * and keep this properties in flow file attributes for further processing
 */
public class TableConfigProcessor extends AbstractProcessor {

    private static final Comparator<File> LAST_MODIFIER_COMPARATOR = new Comparator<File>() {
        @Override
        public int compare(File o1, File o2) {
            return (int)(o1.lastModified() - o2.lastModified());
        }
    };

    static final PropertyDescriptor REGISTRY_SERVICE = new PropertyDescriptor.Builder()
            .name("configuration-registry-service")
            .displayName("Conifguration Registry Service")
            .description("The Schema Registry Service for serializing/deserializing messages as well as schema retrieval.")
            .required(true)
            .identifiesControllerService(TransformationRegistry.class)
            .build();

    static final PropertyDescriptor SOURCE_CONFIG_FILE_NAME_FILTER = new PropertyDescriptor.Builder()
            .name("source-config-file-filter")
            .displayName("Source Config File Filter")
            .description("file name filter to be used get the source config file from incoming folder")
            .addValidator(NON_EMPTY_VALIDATOR)
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor SOURCE_CONFIG_TRANSFORM_CLASS_NAME = new PropertyDescriptor.Builder()
            .name("source-config-transform-class-name")
            .displayName("Source Config Transform Class Name")
            .description("Class Name which transforms source table config xml to SDM format")
            .addValidator(NON_EMPTY_VALIDATOR)
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor NEW_TABLE_CONFIG_PATH = new PropertyDescriptor.Builder()
            .name("new-table-config-path")
            .displayName("Path for Generating New Table Config File")
            .description("Path where the new config file generated by processor should be placed")
            .addValidator(NON_EMPTY_VALIDATOR)
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor ARCHIVE_SOURCE_CONFIG_FILE_FLAG = new PropertyDescriptor.Builder()
            .name("archive-source-config-file-flag")
            .displayName("Source Config File to be archived (Y/N)")
            .description("Whether the source config file should be archived")
            .addValidator(NON_EMPTY_VALIDATOR)
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor DATATYPE_MAPPING_FILE_PATH = new PropertyDescriptor.Builder()
            .name("datatype-mapping-path")
            .displayName("Datatype mapping file path")
            .description("Path to the file that has the source and the target Avro datatype delimited by ->")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor SOURCE_TYPE_PATH = new PropertyDescriptor.Builder()
            .name("source-type-path")
            .displayName("Source Type Path")
            .description("This identifies what kind of source this source table is either a transaction or a delta table")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor KEY_COLUMN_LIST_PATH = new PropertyDescriptor.Builder()
            .name("key-column-list-path")
            .displayName("Key Column List Path")
            .description("The keycol.list file contains the list of columns that compose the primary key of the source table")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor TRANSFORMATION_RULES_PATH = new PropertyDescriptor.Builder().
            name("transformation-rules-path")
            .displayName("Transformation Rules Path")
            .description("This file contains all the constraints and the validation rules that must be applied on the input data file.")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor DEFAULTS_MAPPING_FILE_PATH = new PropertyDescriptor.Builder()
            .name("defaults-mapping-path")
            .displayName("Defaults mapping file path for table configs")
            .description("Path to the file that has the default values for table config attributes delimited by ->")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final Relationship REL_SCHEMA_EVOLUTION_REQUIRED = new Relationship.Builder()
            .name("Schema Evolution")
            .description("Schema Evolution Required Relationship")
            .build();

    static final Relationship REL_SCHEMA_EVOLUTION_NOT_REQUIRED = new Relationship.Builder()
            .name("No Schema Evolution")
            .description("Schema Evolution Not Required Relationship")
            .build();


    static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Failure Relationship")
            .build();

    private static final List<PropertyDescriptor> DESCRIPTORS;
    private static final Set<Relationship> RELATIONSHIPS;

    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(REGISTRY_SERVICE);
        descriptors.add(SOURCE_CONFIG_FILE_NAME_FILTER);
        descriptors.add(SOURCE_CONFIG_TRANSFORM_CLASS_NAME);
        descriptors.add(ARCHIVE_SOURCE_CONFIG_FILE_FLAG);
        descriptors.add(NEW_TABLE_CONFIG_PATH);
        descriptors.add(DATATYPE_MAPPING_FILE_PATH);
        descriptors.add(SOURCE_TYPE_PATH);
        descriptors.add(KEY_COLUMN_LIST_PATH);
        descriptors.add(DEFAULTS_MAPPING_FILE_PATH);
        descriptors.add(TRANSFORMATION_RULES_PATH);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SCHEMA_EVOLUTION_REQUIRED);
        relationships.add(REL_SCHEMA_EVOLUTION_NOT_REQUIRED);
        relationships.add(REL_FAILURE);
        RELATIONSHIPS = Collections.unmodifiableSet(relationships);
    }


    @Override
    public Set<Relationship> getRelationships() {
        return RELATIONSHIPS;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return DESCRIPTORS;
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }
        final TransformationRegistry registry = context.getProperty(REGISTRY_SERVICE).asControllerService(TransformationRegistry.class);

        try {
            final String tableName = getIncomingAttribute(flowFile, TABLE_NAME_ATTRIBUTE);
            final File incomingPath = new File(getIncomingAttribute(flowFile, INCOMING_PATH));
            final File currentFile = new File(incomingPath, getIncomingAttribute(flowFile, FLOW_FILE_NAME));

            Pattern configFilePattern = Pattern.compile(context.getProperty(SOURCE_CONFIG_FILE_NAME_FILTER).evaluateAttributeExpressions(flowFile).getValue());
            String transformClassName = context.getProperty(SOURCE_CONFIG_TRANSFORM_CLASS_NAME).evaluateAttributeExpressions(flowFile).getValue();
            String archiveSourceConfigFileAttribute = context.getProperty(ARCHIVE_SOURCE_CONFIG_FILE_FLAG).evaluateAttributeExpressions(flowFile).getValue();
            String newTableConfigPath = context.getProperty(NEW_TABLE_CONFIG_PATH).evaluateAttributeExpressions(flowFile).getValue();

            getLogger().info("new Table Config file Location: " + newTableConfigPath);

            //Get the lock for table so that processing can be done further
            getTableLock(registry, tableName);

            Collection<File> sourceConfigFiles = getSourceConfigFiles(incomingPath, configFilePattern, currentFile);
            if(sourceConfigFiles.isEmpty()) {
                registry.unlockTable(tableName);
                getLogger().info("No table config file found for " + tableName);
                session.transfer(flowFile, REL_SCHEMA_EVOLUTION_NOT_REQUIRED);
            } else {
                getLogger().info("Initializing transformer class : " + transformClassName);
                SourceTableConfigTransformer transformer = getConfigTransformer(transformClassName);
                CDCModels.TableConfiguration newTableConfig = transformer.transform(sourceConfigFiles, getAttributes(flowFile, context), getLogger()).get(tableName);
                CDCModels.TableConfiguration existingConfig = registry.getTableConfig(tableName);

                getLogger().info("New table config : " + newTableConfig);
                getLogger().info("Existing table config : " + existingConfig);

                if(newTableConfig == null || newTableConfig.equals(existingConfig)) {
                    registry.unlockTable(tableName);
                    getLogger().info("No difference between current version and new version of table config for " + tableName);
                    session.transfer(flowFile, REL_SCHEMA_EVOLUTION_NOT_REQUIRED);
                } else {
                    waitForExistingFilesProcessingCompletion(incomingPath, tableName, currentFile, sourceConfigFiles);
                    //Write new table config file to do schema evolution
                    writeNewConfigFile(newTableConfigPath, tableName, newTableConfig);
                    //Table will be unlocked in next processor after schema evolution is completed.
                    session.transfer(flowFile, REL_SCHEMA_EVOLUTION_REQUIRED);
                }

                if("Y".equals(archiveSourceConfigFileAttribute)) {
                    File archivalFolder = new File(incomingPath.getParent(), "archival");
                    for(File file: sourceConfigFiles) {
                        getLogger().info("Moving new configuration file " + file + " to " + archivalFolder.getAbsolutePath());
                        FileUtils.moveFile(file, new File(archivalFolder, file.getName()+"_"+new Date().getTime()));
                    }
                }
            }
        } catch (final Exception e) {
            registry.unlockTable(getIncomingAttribute(flowFile, TABLE_NAME_ATTRIBUTE));
        	getLogger().error(e.toString(), e);
            session.transfer(flowFile, REL_FAILURE);
        }
    }

    private Map<String,String> getAttributes(FlowFile flowFile, ProcessContext context) {
        Map<String, String> map = new HashMap<>(flowFile.getAttributes());
        for(PropertyDescriptor descriptor: getPropertyDescriptors()) {
            String value = context.getProperty(descriptor).evaluateAttributeExpressions(flowFile).getValue();
            map.put(descriptor.getName(), value);
        }
        return map;
    }

    private void writeNewConfigFile(String newTableConfigPath, String tableName, CDCModels.TableConfiguration newTableConfig) throws Exception {
        String configFileName = tableName + "_tables_config.xml";
        FileUtils.forceMkdir(new File(newTableConfigPath));
        getLogger().info("Writing new config file to : " + newTableConfigPath + " - " + configFileName);
        PrintWriter pw = new PrintWriter(new File(newTableConfigPath, configFileName));
        pw.println(newTableConfig.toString());
        pw.flush();
        pw.close();
    }

    private void waitForExistingFilesProcessingCompletion(File incomingPath, String tableName, File currentFile, Collection<File> configFiles) throws InterruptedException {
        while(true) {
            List<File> pendingDataFiles = getPendingFiles(incomingPath, tableName, currentFile, configFiles);
            if(pendingDataFiles.isEmpty()) {
                break;
            }
            Thread.sleep(10000);
        }
    }

    private SourceTableConfigTransformer getConfigTransformer(String transformClassName) throws Exception {
        Class<?> clazz = Class.forName(transformClassName);
        Constructor<?> ctor = clazz.getConstructor();
        return (SourceTableConfigTransformer) ctor.newInstance();
    }

    private static Collection<File> getSourceConfigFiles(File incomingPath, final Pattern configFilePattern, File currentFile) {
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(final File file) {
                return configFilePattern.matcher(file.getName()).matches() && (file.lastModified() <= currentFile.lastModified());
            }
        };
        return Arrays.asList(incomingPath.listFiles(filter));
    }

    private void getTableLock(TransformationRegistry registry, String tableName) throws Exception {
        while(registry.isTableLocked(tableName)) {
            sleep(10000);
        }
        registry.lockTable(tableName);
    }

    private String getIncomingAttribute(FlowFile flowFile, String incomingAttributeName) {
        final String attrName = flowFile.getAttribute(incomingAttributeName);
        if (attrName == null) {
            throw new ProcessException("Missing attribute " + incomingAttributeName + " on incoming flowfile");
        }
        return attrName;
    }
    private static List<File> getPendingFiles(File incomingPath, String tableName, File currentFile, Collection<File> configFiles) {
        //TODO : Use table name to get the files only for that table
        final File latestConfigFile = Collections.max(configFiles, LAST_MODIFIER_COMPARATOR);
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(final File file) {
                return !configFiles.contains(file) && !file.equals(currentFile) && file.lastModified() < latestConfigFile.lastModified();
            }
        };
        return Arrays.asList(incomingPath.listFiles(filter));
    }
}
