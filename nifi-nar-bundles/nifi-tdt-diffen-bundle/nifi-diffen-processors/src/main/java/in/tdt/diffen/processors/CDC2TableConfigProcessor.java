/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import com.google.common.collect.Lists;
import in.tdt.diffen.cdc.CDCXmlParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.InputRequirement.Requirement;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.OutputStreamCallback;
import org.apache.nifi.processor.util.StandardValidators;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static in.tdt.diffen.cdc.CDCModels.*;
import static in.tdt.diffen.verifytypes.DiffenConstants.*;
import static org.apache.nifi.processor.util.StandardValidators.NON_EMPTY_VALIDATOR;

@InputRequirement(Requirement.INPUT_REQUIRED)
@Tags({"CDC", "XML", "convert"})
@CapabilityDescription("Converts a CDC Schema XML to a Table config XML")

/**
 *This class is used to convert CDC schema xml to Table config XML
 */

public class CDC2TableConfigProcessor extends AbstractProcessor {

    static final PropertyDescriptor CDC_XML_PATH = new PropertyDescriptor.Builder()
            .name("CDC XML Schema Path")
            .description("Path to the CDC Schema XML file")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor SOURCE_TYPE_PATH = new PropertyDescriptor.Builder()
            .name("Source Type Path")
            .description("This identifies what kind of source this source table is either a transaction or a delta table")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor KEY_COLUMN_LIST_PATH = new PropertyDescriptor.Builder()
            .name("Key Column List Path")
            .description("The keycol.list file contains the list of columns that compose the primary key of the source table")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor TRANSFORMATION_RULES_PATH = new PropertyDescriptor.Builder().
            name("Transformation Rules Path")
            .description("This file contains all the constraints and the validation rules that must be applied on the input data file.")
            .required(false)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();

    static final PropertyDescriptor MERGE_FLAG = new PropertyDescriptor.Builder().
            name("Merge CDC XMls Indicator")
            .description("The Merge flag indicator if true will combine multiple CDC XMLs")
            .required(false)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .defaultValue("true")
            .addValidator(NON_EMPTY_VALIDATOR)
            .build();


    public static final Relationship REL_SUCCESS =
            new Relationship.Builder()
                    .name("success")
                    .description("Successfully Generated Table config XMLs from CDC XML are transferred to this relationship")
                    .build();

    public static final Relationship REL_FAILURE =
            new Relationship.Builder()
                    .name("failure")
                    .description("CDC XML Files that could not be converted into Table config XMLs are transferred to this relationship")
                    .build();

    private static final Set<Relationship> RELATIONSHIPS;
    private static final List<PropertyDescriptor> DESCRIPTORS;

    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(CDC_XML_PATH);
        descriptors.add(SOURCE_TYPE_PATH);
        descriptors.add(KEY_COLUMN_LIST_PATH);
        descriptors.add(TRANSFORMATION_RULES_PATH);
        descriptors.add(MERGE_FLAG);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);

        Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        RELATIONSHIPS = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.RELATIONSHIPS;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return this.DESCRIPTORS;
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {

        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        final File cdcXMLFile = new File(context.getProperty(CDC_XML_PATH).evaluateAttributeExpressions(flowFile).getValue());
        final boolean mergeIndicator = context.getProperty(MERGE_FLAG).evaluateAttributeExpressions().asBoolean();
        final AtomicReference<String> source = new AtomicReference<>(getIncomingAttribute(flowFile, "source"));
        final AtomicReference<String> country = new AtomicReference<>(getIncomingAttribute(flowFile, "country"));

        try {
            File dir = cdcXMLFile.getParentFile();
            String xmlFile = cdcXMLFile.getName().toLowerCase();
            File[] cdcConfigXmlFiles;

            //Fetch keycols info
            List<String> keyColFile = FileUtils.readLines(new File(context.getProperty(KEY_COLUMN_LIST_PATH).evaluateAttributeExpressions(flowFile).getValue()));
            keyColFile.removeIf(line -> StringUtils.isEmpty(line));

            Map<String, String> keyColList = keyColFile
                    .stream()
                    .map(line -> new TableKeyColInfo(StringUtils.substringBefore(line, "->").trim(), StringUtils.substringAfter(line, "->").trim()))
                    .collect(Collectors.toMap(TableKeyColInfo::getTableName, TableKeyColInfo::getKeyCols));

            //Fetch source type info
            List<String> tableTypeFile = FileUtils.readLines(new File(context.getProperty(SOURCE_TYPE_PATH).evaluateAttributeExpressions(flowFile).getValue()));
            tableTypeFile.removeIf(line -> StringUtils.isEmpty(line));

            Map<String, String> sourceTypeList = tableTypeFile
                    .stream()
                    .map(line -> line.split("->")).map(x -> new TableSourceInfo(x[0].trim(), x[1].trim()))
                    .collect(Collectors.toMap(TableSourceInfo::getTableName, TableSourceInfo::getTypee));

            Map<String, List<ColTransformationRule>> transColList = new HashMap<>();

            //Fetch transformation rules info
            if (Optional.ofNullable(context.getProperty(TRANSFORMATION_RULES_PATH).getValue()).isPresent()) {
                List<String> transColFile = FileUtils.readLines(new File(context.getProperty(TRANSFORMATION_RULES_PATH).evaluateAttributeExpressions(flowFile).getValue()));
                transColFile.removeIf(line -> StringUtils.isEmpty(line));
                transColList = transColFile.stream()
                        .map(line -> line.split("->"))
                        .map(x -> new TableTransformationRule(x[0].trim(), Arrays.stream(x[1].split("\\|"))
                                .map(each -> new ColTransformationRule(each))
                                .collect(Collectors.toList())))
                        .collect(Collectors.toMap(TableTransformationRule::getTableName, TableTransformationRule::getRulesList));

            }

            if (mergeIndicator) {
                //Fetch XML cdcConfigXmlFiles from input directory
                cdcConfigXmlFiles = dir.listFiles((d, name) -> name.toLowerCase().endsWith(".xml"));
            } else {
                cdcConfigXmlFiles = dir.listFiles((d, name) -> name.toLowerCase().endsWith(xmlFile));
            }

            List<Property> properties = new ArrayList<>();

            for (File eachCdcXmlFile : cdcConfigXmlFiles) {
                getLogger().debug("Processing " + eachCdcXmlFile.getAbsolutePath());
                //Fetch CDC XML and convert it into NodeList
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(eachCdcXmlFile);

                NodeList tableMappings = doc.getElementsByTagName("TableMapping");
                properties.addAll(getTableProperties(tableMappings, keyColList, sourceTypeList, transColList, source.get(), country.get()));
            }


            FlowFile successFlowFile = session.write(flowFile, new OutputStreamCallback() {
                @Override
                public void process(final OutputStream out) throws IOException {
                    IOUtils.write(new TableConfiguration(properties).toString(), out);
                }
            });

            session.transfer(successFlowFile, REL_SUCCESS);

        } catch (final Exception e) {
            e.printStackTrace();
            getLogger().error("Error while converting CDC XML to Tableconfig xml : {}", new Object[]{e.getMessage()});
            session.transfer(flowFile, REL_FAILURE);
        }
    }

    /**
     * Returns the property for each table level
     *
     * @param tableMappings
     * @param keyColList
     * @param sourceTypeList
     * @param transColList
     * @param source
     * @param country
     * @return properties
     */
    private List<Property> getTableProperties(NodeList tableMappings, Map<String, String> keyColList, Map<String, String> sourceTypeList, Map<String, List<ColTransformationRule>> transColList, String source, String country) {

        List<Property> properties = new ArrayList<Property>();

        for (int count = 0; count < tableMappings.getLength(); count++) {

            Node tableMapping = tableMappings.item(count);
            String tableName = source + "_" + country + "_" + CDCXmlParser.getXmlAttribute(tableMapping, "sourceTableName").toLowerCase();
            Element tableMapElem = (Element) tableMapping;
            NodeList sourceColumns = tableMapElem.getElementsByTagName("SourceColumn");

            List<ColTransformationRule> rulesList = transColList.containsKey(tableName) ? transColList.get(tableName) : new ArrayList<>();

            //FIXME Need to accept the prefix as a parameter
            String keyPrefix = DEFAULT_PARAM_PREFIX + ADD_PREFIX_CONFIG_DIFFEN_TABLE + tableName;

            StringBuilder builder = new StringBuilder();
            for (int colCount = 0; colCount < sourceColumns.getLength(); colCount++) {
                Node sourceCol = sourceColumns.item(colCount);
                String schema = CDCXmlParser.getColumn(sourceCol, rulesList, new HashMap<>(), true);
                if (null != schema) {
                    builder.append(schema).append("^");
                }
            }
            String colSchema = StringUtils.removeEnd(builder.toString(), "^");

            //Validate the Primary columns against the Table columns
            List<String> colSchemaList = Arrays.asList(colSchema.split("\\^"));
            String colNames="";

            for(int i=0;i<colSchemaList.size();i++){
                colNames = colNames + colSchemaList.get(i).split(" ")[0] + ",";
            }

            List<String> tableColList = Arrays.asList(colNames.split(",",-1));
            String keyCols = keyColList.get(tableName);
            List<String> keyColsList = Arrays.asList(keyCols.split(",",-1));

            if(!tableColList.containsAll(keyColsList)){
                throw new ProcessException("Invalid Primary key defined for table:" +tableName);
            }

            getLogger().info("Tablename {}, columns {} ", new Object[]{tableName, colSchema});
            properties.addAll(Lists.newArrayList(new Property(keyPrefix + SUFFIX_CONFIG_COLSCHEMA, colSchema),
                    new Property(keyPrefix + SUFFIX_CONFIG_SOURCETYPE, sourceTypeList.get(tableName)),
                    new Property(keyPrefix + SUFFIX_CONFIG_KEYCOLS, keyColList.get(tableName)),
                    new Property(keyPrefix + SUFFIX_CONFIG_RECONQUERY, "")));
        }
        return properties;
    }

    /**
     * Get input values from flow file's attributes
     *
     * @param flowFile
     * @param incomingAttributeName
     * @return attrName
     */
    private String getIncomingAttribute(FlowFile flowFile, String incomingAttributeName) {
        final String attrName = flowFile.getAttribute(incomingAttributeName);
        if (null == attrName) {
            throw new ProcessException("Missing attribute " + incomingAttributeName + " on incoming flowfile");
        }
        return attrName;
    }
}