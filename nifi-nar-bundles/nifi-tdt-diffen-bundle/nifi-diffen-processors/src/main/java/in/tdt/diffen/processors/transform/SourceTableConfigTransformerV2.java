package in.tdt.diffen.processors.transform;

import in.tdt.diffen.cdc.CDCModels;
import org.apache.nifi.logging.ComponentLog;

import java.util.Map;

public interface SourceTableConfigTransformerV2 {
    Map<String, CDCModels.TableConfiguration> transform(String filename, String contents, Map<String, String> attributes, ComponentLog logger);
}
