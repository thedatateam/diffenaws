package in.tdt.diffen.processors;

public class NifiAttributesConstants {
    // Attributes
    public static final String INVALID_TYPES_WARN_RECORD_COUNT_TOTAL = "invalidtypes.warn.record.count.total";
    public static final String INVALID_TYPES_WARN_RECORD_COUNT_COLUMN = "invalidtypes.warn.record.count.columns";
    public static final String INVALID_TYPES_ERROR_RECORD_COUNT_TOTAL = "invalidtypes.error.record.count.total";
    public static final String INVALID_TYPES_ERROR_RECORD_COUNT_COLUMN = "invalidtypes.error.record.count.columns";
    public static final String INVALID_TYPES_SUCCESS_RECORD_COUNT = "invalidtypes.valid.record.count";
    public static final String INVALID_TYPES_TOTAL_RECORD_COUNT = "invalidtypes.total.record.count";

    public static final String EXPECTED_COLUMN_COUNT_ATTRIBUTE = "expected.column.count";
    public static final String ACTUAL_COLUMN_COUNT_ATTRIBUTE = "lastrow.column.count";
    public static final String ACTUAL_ROW_COUNT_ATTRIBUTE = "total.row.count";
    public static final String B_ROW_COUNT_ATTRIBUTE = "b.row.count";
}
