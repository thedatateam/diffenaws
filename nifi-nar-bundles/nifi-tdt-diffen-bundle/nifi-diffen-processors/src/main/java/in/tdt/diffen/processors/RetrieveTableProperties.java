/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.services.TransformationRegistry;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;

import java.util.*;

import static in.tdt.diffen.processors.RetrieveTableProperties.*;
import static in.tdt.diffen.verifytypes.DiffenConstants.*;

@Tags({"data properties lookup"})
@CapabilityDescription("Based on the table name this will add all of the processing properties as attributes to " +
                       "the flow file.")
@SideEffectFree
@SupportsBatching
@ReadsAttributes({@ReadsAttribute(attribute = "tablename", description = "The table that is being processed")})
@WritesAttributes({
        @WritesAttribute(attribute = EXPECTED_COLUMN_COUNT, description = "This is count of the total number of columns " +
                                                                          "that is expected for this table. This include " +
                                                                          "source columns such as CDC fields"),
        @WritesAttribute(attribute = VALID_AVRO_SCHEMA, description = "The Avro schema to use to serialize this table"),
        @WritesAttribute(attribute = INVALID_AVRO_SCHEMA, description = "The Avro schema usee to serialize invliad data for " +
                                                                        "this table")})
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
/**
 * This class is used to retrieve the table properties from controller service{@link  TransformationRegistry}based on table name </n>
 * and keep this properties in flow file attributes for further processing
 */
public class RetrieveTableProperties extends AbstractProcessor {

    static final PropertyDescriptor REGISTRY_SERVICE = new PropertyDescriptor.Builder()
            .name("configuration-registry-service")
            .displayName("Conifguration Registry Service")
            .description("The Schema Registry Service for serializing/deserializing messages as well as schema retrieval.")
            .required(true)
            .identifiesControllerService(TransformationRegistry.class)
            .build();

    static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("Success relationship")
            .build();


    static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Failure relationship")
            .build();

    private static List<PropertyDescriptor> DESCRIPTORS;
    private static final Set<Relationship> RELATIONSHIPS;
    static final String EXPECTED_COLUMN_COUNT = "expected.column.count";
    static final String VALID_AVRO_SCHEMA = "valid.avro.schema";
    static final String INVALID_AVRO_SCHEMA = "invalid.avro.schema";
    static final String WARN_AVRO_SCHEMA = "warn.avro.schema";
    static final String ROWHISTORY_AVRO_SCHEMA = "rowhistory.avro.schema";
    static final String FIXEDWIDTH_PARSEABLE_REGEX="fixedwidth.parseable.regex";
    static final String FIXEDWIDTH_TODELIMITED_REGEX="fixedwidth.todelimited.regex";
    static final String FIXEDWIDTH_PARSEABLE_REGEX_HEADER="fixedwidth_header.parseable.regex";
    static final String FIXEDWIDTH_TODELIMITED_REGEX_HEADER="fixedwidth_header.todelimited.regex";
    static final String FIXEDWIDTH_PARSEABLE_REGEX_TRAILER="fixedwidth_trailer.parseable.regex";
    static final String FIXEDWIDTH_TODELIMITED_REGEX_TRAILER="fixedwidth_trailer.todelimited.regex";
    static final String TABLE_KEY_COLS = "table.key.columns";
    static final String TABLE_COLSCHEMA = "col_schema";
    static final String TABLE_AFTERVALUE_INDICATOR = "table.aftervalue.indicator";
    static final String TABLE_BEFOREVALUE_INDICATOR = "table.beforevalue.indicator";
    static final String TABLE_INSERTVALUE_INDICATOR = "table.insertvalue.indicator";
    static final String TABLE_DELETEVALUE_INDICATOR = "table.deletevalue.indicator";



    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(REGISTRY_SERVICE);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        RELATIONSHIPS = Collections.unmodifiableSet(relationships);
    }


    @Override
    public Set<Relationship> getRelationships() {
        return RELATIONSHIPS;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return DESCRIPTORS;
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        try {
            final String tableName = getIncomingAttribute(flowFile, TABLE_NAME_ATTRIBUTE);
            final TransformationRegistry registry = context.getProperty(REGISTRY_SERVICE).asControllerService(TransformationRegistry.class);

			//Check if the table name is valid
			if(!registry.isTableExist(tableName)){
            	throw new ProcessException("Table Name does not exist in Config : " +tableName);
            }

            final Map<String, String> updatedAttrs = new HashMap<>();
            updatedAttrs.put(EXPECTED_COLUMN_COUNT, Integer.toString(registry.getFullDataHeaderForTable(tableName).length));
            updatedAttrs.put(VALID_AVRO_SCHEMA, registry.getTableSchemaAsAvro(tableName));
            updatedAttrs.put(INVALID_AVRO_SCHEMA, registry.getInvalidTableSchemaAsAvro(tableName));
            updatedAttrs.put(WARN_AVRO_SCHEMA, registry.getWarnTableSchemaAsAvro(tableName));
            updatedAttrs.put(ROWHISTORY_AVRO_SCHEMA, registry.getRowHistoryTableSchemaAsAvro(tableName));
            updatedAttrs.put(STORAGE_PARTITION_NAME, registry.getGlobalConfig(STORAGE_PARTITION_NAME));
            updatedAttrs.put(SOURCE, registry.getGlobalConfig(SOURCE));
            updatedAttrs.put(COUNTRY, registry.getGlobalConfig(COUNTRY));
            updatedAttrs.put(HDFS_PARENT_DIRECTORY, registry.getGlobalConfig(HDFS_PARENT_DIRECTORY));
            updatedAttrs.put(VERIFY_TYPES_SCHEMA, registry.getGlobalConfig(VERIFY_TYPES_SCHEMA));
            updatedAttrs.put(OPS_SCHEMA_NAME, registry.getGlobalConfig(OPS_SCHEMA_NAME));
            updatedAttrs.put(DIFFEN_JOB_TRIGGER_FILE_PATTERN, registry.getGlobalConfig(DIFFEN_JOB_TRIGGER_FILE_PATTERN));
            updatedAttrs.put(TABLE_NAME_ATTRIBUTE, registry.getTranslatedTableName(tableName)); //Renames, if needed to the translated tablename
            updatedAttrs.put(FIXEDWIDTH_PARSEABLE_REGEX, registry.getFixedWidthParseableRegexForData(tableName));
            updatedAttrs.put(FIXEDWIDTH_TODELIMITED_REGEX, registry.getFixedWidthToDelimitedRegexForData(tableName));
            updatedAttrs.put(FIXEDWIDTH_PARSEABLE_REGEX_HEADER, registry.getFixedWidthParseableRegexForHeader(tableName));
            updatedAttrs.put(FIXEDWIDTH_TODELIMITED_REGEX_HEADER, registry.getFixedWidthToDelimitedRegexForHeader(tableName));
            updatedAttrs.put(FIXEDWIDTH_PARSEABLE_REGEX_TRAILER, registry.getFixedWidthParseableRegexForTrailer(tableName));
            updatedAttrs.put(FIXEDWIDTH_TODELIMITED_REGEX_TRAILER, registry.getFixedWidthToDelimitedRegexForTrailer(tableName));

            updatedAttrs.put(SOURCING_TYPE,registry.getGlobalConfig(SOURCING_TYPE));
            /*updatedAttrs.put(HEADER_COLSCHEMA_SUFFIX,registry.getTableHeaderFormat(tableName));
            updatedAttrs.put(TRAILER_COLSCHEMA_SUFFIX,registry.getTableTrailerFormat(tableName));*/
            updatedAttrs.put(HEADER_INDICATOR,registry.getTableHeaderIndicator(tableName));
            updatedAttrs.put(TRAILER_INDICATOR,registry.getTableTrailerIndicator(tableName));
            updatedAttrs.put(DATA_INDICATOR,registry.getTableDataIndicator(tableName));
            updatedAttrs.put(TIMESTAMP_COL_FORMAT, registry.getGlobalConfig(TIMESTAMP_COL_FORMAT));
            updatedAttrs.put(TABLE_KEY_COLS,registry.getKeyColumnsForTable(tableName));
            updatedAttrs.put(TABLE_COLSCHEMA,registry.getTableColAllStringSchemaAsAvro((tableName))); //FIXME To be renamed to something more meaningful

            updatedAttrs.put(TABLE_AFTERVALUE_INDICATOR,registry.getTableAfterValueIndicator((tableName)));
            updatedAttrs.put(TABLE_BEFOREVALUE_INDICATOR,registry.getTableBeforeValueIndicator((tableName)));
            updatedAttrs.put(TABLE_INSERTVALUE_INDICATOR,registry.getTableInsertValueIndicator((tableName)));
            updatedAttrs.put(TABLE_DELETEVALUE_INDICATOR,registry.getTableDeleteValueIndicator((tableName)));

            // Added for ControlFile and Recon File Rename Issue
            updatedAttrs.put(CONTROL_FILE_TABLENAME_INDEX,registry.getControlFileTableNameIndex (tableName));
            updatedAttrs.put(CONTROL_FILE_TABLENAME,registry.getControlFileTableName((tableName)));
            updatedAttrs.put(RECON_FILE_RENAME,registry.getreconfilerename((tableName)));

            flowFile = session.putAllAttributes(flowFile, updatedAttrs);

            session.getProvenanceReporter().modifyAttributes(flowFile, "Added properties for table being processed");
            session.transfer(flowFile, REL_SUCCESS);
        } catch (final ProcessException e) {
        	getLogger().error(e.toString());
            session.transfer(flowFile, REL_FAILURE);
        }
    }
    private String getIncomingAttribute(FlowFile flowFile, String incomingAttributeName) {
        final String attrName = flowFile.getAttribute(incomingAttributeName);
        if (null == attrName) {
            throw new ProcessException("Missing attribute " + incomingAttributeName + " on incoming flowfile");
        }
        return attrName;
    }
}
