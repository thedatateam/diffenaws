package in.tdt.diffen.processors;

import java.util.HashSet;
import java.util.Set;

import org.apache.nifi.components.ValidationContext;
import org.apache.nifi.components.ValidationResult;
import org.apache.nifi.components.Validator;

import static in.tdt.diffen.processors.CSVUtils.unescape;

public class SingleCharacterValidator implements Validator {
    private static final Set<String> illegalChars = new HashSet<>();
    static {
        illegalChars.add("\r");
        illegalChars.add("\n");
    }

    @Override
    public ValidationResult validate(final String subject, final String input, final ValidationContext context) {
        final String unescaped = unescape(input);
        if (unescaped.length() != 1) {
            return new ValidationResult.Builder()
                    .input(input)
                    .subject(subject)
                    .valid(false)
                    .explanation("Value must be exactly 1 character but was " + input.length() + " in length")
                    .build();
        }

        if (illegalChars.contains(input)) {
            return new ValidationResult.Builder()
                    .input(input)
                    .subject(subject)
                    .valid(false)
                    .explanation(input + " is not a valid character for this property")
                    .build();
        }

        return new ValidationResult.Builder()
                .input(input)
                .subject(subject)
                .valid(true)
                .build();
    }

}
