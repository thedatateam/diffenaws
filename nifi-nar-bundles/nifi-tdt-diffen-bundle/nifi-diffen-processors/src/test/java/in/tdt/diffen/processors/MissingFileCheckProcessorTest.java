package in.tdt.diffen.processors;

import org.apache.commons.io.FileUtils;
import org.apache.nifi.flowfile.attributes.CoreAttributes;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static in.tdt.diffen.processors.MissingFileCheckProcessor.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MissingFileCheckProcessorTest {

    final File inputDir = new File("target/test/data/in");
    final File logDir = new File("target/test/data/log");
    final File deltaLogDir = new File("target/test/data/deltalog");
    MissingFileCheckProcessor processor;
    TestRunner runner;
    ProcessContext context;
    String logFileName = null;

    Long time0millis, time1millis, time2millis, time3millis, time4millis, time5millis;

    @Before
    public void setUp() throws Exception {
        processor = new MissingFileCheckProcessor();
        runner = TestRunners.newTestRunner(processor);
        context = runner.getProcessContext();
        final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        logFileName = formatter.format(new Date()) + ".log";
        deleteDirectory(inputDir);
        deleteDirectory(logDir);
        deleteDirectory(deltaLogDir);
        assertTrue("Unable to create test data directory " + inputDir.getAbsolutePath(), inputDir.exists() || inputDir.mkdirs());
        resetAges();
    }

    @Test
    public void testPickAllFiles() throws Exception {

        final File file1 = new File(inputDir, "file1.txt");
        assertTrue(file1.createNewFile());

        final File file2 = new File(inputDir, "file2.txt");
        assertTrue(file2.createNewFile());

        final File file3 = new File(inputDir, "file3.txt");
        assertTrue(file3.createNewFile());

        resetAges();
        file1.setLastModified(time1millis);
        file2.setLastModified(time2millis);
        file3.setLastModified(time3millis);

        List<File> fileList = Arrays.asList(file1, file2, file3);
        fileList
                .stream()
                .map(this::createFlowFileAttributesFromFile)
                .forEach(attr -> runner.enqueue("Dummy".getBytes(Charset.forName("UTF-8")), attr));

        // check all files
        runner.setProperty(MissingFileCheckProcessor.INPUT_DIRECTORY, inputDir.getAbsolutePath());
        runner.setProperty(MissingFileCheckProcessor.LOG_DIRECTORY, logDir.getAbsolutePath());
        runner.setProperty(MissingFileCheckProcessor.DELTA_LOG_DIRECTORY, deltaLogDir.getAbsolutePath());

        runner.run();
        runner.assertTransferCount(MissingFileCheckProcessor.REL_SUCCESS, 1);

        List<String> logLines = FileUtils.readLines(new File(logDir, logFileName), Charset.forName("UTF-8"));
        assertTrue(logLines.size() == 3);

        /*List<String> deltaLogDir = FileUtils.readLines(new File(logDir, logFileName), Charset.forName("UTF-8"));
        assertTrue(logLines.size() == 3);*/
    }

    @Test
    public void testPickAllFilesMakeEntryForNewFile() throws Exception {

        final File file1 = new File(inputDir, "file1.txt");
        assertTrue(file1.createNewFile());

        final File file2 = new File(inputDir, "file2.txt");
        assertTrue(file2.createNewFile());

        final File file3 = new File(inputDir, "file3.txt");
        assertTrue(file3.createNewFile());

        resetAges();
        file1.setLastModified(time1millis);
        file2.setLastModified(time2millis);
        file3.setLastModified(time3millis);

        List<File> fileList = Arrays.asList(file1, file2, file3);
        List<MissingFileCheckProcessor.MiniFileInfo> miniFiles = fileList.stream().map(MissingFileCheckProcessorTest::constructFromFile).collect(Collectors.toList());
        FileUtils.writeLines(new File(logDir, logFileName), miniFiles);

        final File file4 = new File(inputDir, "file4.txt");
        assertTrue(file4.createNewFile());
        file2.setLastModified(time4millis);
        final File file5 = new File(inputDir, "file5.txt");
        assertTrue(file5.createNewFile());
        file3.setLastModified(time5millis);

        List<File> newFileList = Arrays.asList(file4, file5);
        newFileList
                .stream()
                .map(this::createFlowFileAttributesFromFile)
                .forEach(attr -> runner.enqueue("Dummy".getBytes(Charset.forName("UTF-8")), attr));

        runner.setProperty(MissingFileCheckProcessor.INPUT_DIRECTORY, inputDir.getAbsolutePath());
        runner.setProperty(MissingFileCheckProcessor.LOG_DIRECTORY, logDir.getAbsolutePath());
        runner.setProperty(MissingFileCheckProcessor.DELTA_LOG_DIRECTORY, deltaLogDir.getAbsolutePath());

        runner.run();
        runner.assertTransferCount(MissingFileCheckProcessor.REL_SUCCESS, 1);

        List<String> logLines = FileUtils.readLines(new File(logDir, logFileName), Charset.forName("UTF-8"));
        assertTrue(logLines.size() == 5);
        assertEquals(logLines
                .stream()
                .map(MissingFileCheckProcessor.MiniFileInfo::constructFromString)
                .filter(mini -> mini.getStatus().equals("N"))
                .collect(Collectors.toList()).size(),0);

        assertEquals(logLines
                .stream()
                .map(MissingFileCheckProcessor.MiniFileInfo::constructFromString)
                .filter(mini -> mini.getStatus().equals("T"))
                .collect(Collectors.toList()).size(),3);

        assertTrue(logLines
                .stream()
                .map(MissingFileCheckProcessor.MiniFileInfo::constructFromString)
                .filter(mini -> mini.getStatus().equals("T"))
                .allMatch(mini -> mini.getModifiedTime() <= file5.lastModified()));
    }

    final static DateFormat olaFileDateFormatter = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss", Locale.US);

    public static MissingFileCheckProcessor.MiniFileInfo constructFromFile(File file) {
        return new MissingFileCheckProcessor.MiniFileInfo(file.getAbsolutePath(), file.lastModified(), "2018-04-30", String.valueOf(file.length()), "", olaFileDateFormatter.format(file.lastModified()), "N");
    }

    public void resetAges() {
        long currentTime = System.currentTimeMillis();

        time0millis = currentTime - 0L;
        time1millis = currentTime - 1000L;
        time2millis = currentTime - 2000L;
        time3millis = currentTime - 3000L;
        time4millis = currentTime - 4000L;
        time5millis = currentTime - 5000L;

    }

    final DateFormat attrTimeFormatter = new SimpleDateFormat(MissingFileCheckProcessor.FILE_MODIFY_DATE_ATTR_FORMAT, Locale.US);

    public Map<String, String> createFlowFileAttributesFromFile(final File file) {
        final Map<String, String> attributes = new HashMap<>();
        attributes.put(CoreAttributes.MIME_TYPE.key(), "application/plain-text");
        attributes.put(FILE_ABSOLUTE_PATH_ATTRIBUTE, file.getParentFile().getAbsolutePath() + File.separator);
        attributes.put(FILE_FILENAME_ATTRIBUTE, file.getName());
        attributes.put(FILE_LAST_MODIFY_TIME_ATTRIBUTE, attrTimeFormatter.format(new Date(file.lastModified())));
        attributes.put(FILE_CREATION_TIME_ATTRIBUTE, attrTimeFormatter.format(new Date(file.lastModified()-10000)));
        attributes.put(FILE_SIZE_ATTRIBUTE, "0");
        return attributes;
    }


    private void deleteDirectory(final File directory) throws IOException {
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (final File file : files) {
                    if (file.isDirectory()) {
                        deleteDirectory(file);
                    }
                    assertTrue("Could not delete " + file.getAbsolutePath(), file.delete());
                }
            }
        }
    }
}
