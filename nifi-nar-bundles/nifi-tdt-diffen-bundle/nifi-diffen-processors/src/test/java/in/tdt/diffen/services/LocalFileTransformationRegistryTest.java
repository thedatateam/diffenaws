/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package in.tdt.diffen.services;

import in.tdt.diffen.verifytypes.DiffenConstants;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.util.MockComponentLog;
import org.apache.nifi.util.MockPropertyValue;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class LocalFileTransformationRegistryTest {

    @Test
    public void getTableSchemaAsAvro() throws Exception {

        ConfigurationContext configContext = mock(ConfigurationContext.class);
        when(configContext.getProperty(LocalFileTransformationRegistry.CONFIG_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ECDPFACTCA__STATUS_INFO_TABLECONFIG.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.RULE_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ColumnTransformationRules.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.PARAM_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ECDPFACTCA_PARAM.xml"));

        LocalFileTransformationRegistry delegate = new LocalFileTransformationRegistry(){
            @Override
            protected ComponentLog getLogger() {
                return new MockComponentLog("LocalfileTransformationRegistry", this);
            }
        };
        delegate.enable(configContext);

        String locatedSchemaText = delegate.getTableSchemaAsAvro("ecddp_all_copi_fatca_status_info_crs");

        final String validSchema = "{\"name\":\"ecddp_all_copi_fatca_status_info_crs\",\"type\":\"record\"," +
                "\"fields\":[{\"name\":\"rowid\",\"type\":\"string\"},{\"name\":\"filename\",\"type\":\"string\"}," +
                "{\"name\":\"vds\",\"type\":\"string\"},{\"name\":\"c_journaltime\",\"type\":\"string\"}," +
                "{\"name\":\"c_transactionid\",\"type\":\"string\"},{\"name\":\"c_operationtype\",\"type\":\"string\"}," +
                "{\"name\":\"c_userid\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"fatca_status_seq_id\",\"type\":\"string\"}," +
                "{\"name\":\"crs_id\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"rulebase_name\",\"type\":\"string\"}," +
                "{\"name\":\"rulebase_version\",\"type\":[\"null\",\"string\"],\"default\":null}," +
                "{\"name\":\"sdate\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"stimestamp\",\"type\":[\"null\",\"string\"],\"default\":null}]}";

        assertEquals(validSchema, locatedSchemaText);

        final String notFound = delegate.getTableSchemaAsAvro("barSchema");
        Assert.assertNotNull(notFound, "Expected a the table name to be null and it was not");

        delegate.close();

    }

    @Test
    public void getInvalidTableSchemaAsAvro() throws Exception {
        ConfigurationContext configContext = mock(ConfigurationContext.class);
        when(configContext.getProperty(LocalFileTransformationRegistry.CONFIG_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/"));
        when(configContext.getProperty(LocalFileTransformationRegistry.RULE_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ColumnTransformationRules.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.PARAM_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ECDPFACTCA_PARAM.xml"));

        LocalFileTransformationRegistry delegate = new LocalFileTransformationRegistry(){
            @Override
            protected ComponentLog getLogger() {
                return new MockComponentLog("LocalfileTransformationRegistry", this);
            }
        };
        delegate.enable(configContext);

        String locatedSchemaText = delegate.getInvalidTableSchemaAsAvro("ecddp_all_copi_fatca_status_info_crs");

        final String validSchema = "{\"name\":\"ecddp_all_copi_fatca_status_info_crs\",\"type\":\"record\",\"fields\":[" +
                "{\"name\":\"source\",\"type\":\"string\"},{\"name\":\"country\",\"type\":\"string\"}," +
                "{\"name\":\"tablename\",\"type\":\"string\"},{\"name\":\"rowid\",\"type\":\"string\"}," +
                "{\"name\":\"data\",\"type\":\"string\"},{\"name\":\"errormsg\",\"type\":\"string\"}," +
                "{\"name\":\"partitioncolumn\",\"type\":\"string\"},{\"name\":\"ts\",\"type\":\"string\"}]}";

        assertEquals(validSchema, locatedSchemaText);


        delegate.close();
    }


    @Test
    public void getInvalidTableSchemaAsAvroForRenamedTable() throws Exception {
        ConfigurationContext configContext = mock(ConfigurationContext.class);
        when(configContext.getProperty(LocalFileTransformationRegistry.CONFIG_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/"));
        when(configContext.getProperty(LocalFileTransformationRegistry.RULE_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ColumnTransformationRules.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.PARAM_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ECDPFACTCA_PARAM.xml"));

        LocalFileTransformationRegistry delegate = new LocalFileTransformationRegistry(){
            @Override
            protected ComponentLog getLogger() {
                return new MockComponentLog("LocalfileTransformationRegistry", this);
            }
        };
        delegate.enable(configContext);

        String locatedSchemaText = delegate.getInvalidTableSchemaAsAvro("ecddp_all_ACCOUNT_ASSOC_INFO");

        final String validSchema = "{\"name\":\"ecddp_all_account_assoc_info\",\"type\":\"record\",\"fields\":[" +
                "{\"name\":\"source\",\"type\":\"string\"}," +
                "{\"name\":\"country\",\"type\":\"string\"}," +
                "{\"name\":\"tablename\",\"type\":\"string\"},{\"name\":\"rowid\",\"type\":\"string\"},{\"name\":\"data\",\"type\":\"string\"},{\"name\":\"errormsg\",\"type\":\"string\"}," +
                "{\"name\":\"partitioncolumn\",\"type\":\"string\"},{\"name\":\"ts\",\"type\":\"string\"}]}";

        assertEquals(validSchema, locatedSchemaText);


        delegate.close();
    }


    @Test
    public void getRenamedTableSchemaAsAvro() throws Exception {
        ConfigurationContext configContext = mock(ConfigurationContext.class);
        when(configContext.getProperty(LocalFileTransformationRegistry.CONFIG_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ECDPFACTCA__STATUS_INFO_TABLECONFIG.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.RULE_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ColumnTransformationRules.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.TABLE_RENAME_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ECDPFACTCA_RENAME_TABLE.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.PARAM_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ECDPFACTCA_PARAM.xml"));

        LocalFileTransformationRegistry delegate = new LocalFileTransformationRegistry(){
            @Override
            protected ComponentLog getLogger() {
                return new MockComponentLog("LocalfileTransformationRegistry", this);
            }
        };
        delegate.enable(configContext);

        String locatedSchemaText = delegate.getTableSchemaAsAvro("ecddp_all_COPI_FATCA_STATUS_INFO_CRS");

        final String validSchema = "{\"name\":\"ecddp_all_copi_fatca_status_info_crs\",\"type\":\"record\",\"fields\":[{\"name\":\"rowid\",\"type\":\"string\"}," +
                "{\"name\":\"filename\",\"type\":\"string\"},{\"name\":\"vds\",\"type\":\"string\"},{\"name\":\"c_journaltime\",\"type\":\"string\"}," +
                "{\"name\":\"c_transactionid\",\"type\":\"string\"},{\"name\":\"c_operationtype\",\"type\":\"string\"}," +
                "{\"name\":\"c_userid\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"fatca_status_seq_id\",\"type\":\"string\"}," +
                "{\"name\":\"crs_id\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"rulebase_name\",\"type\":\"string\"}," +
                "{\"name\":\"rulebase_version\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"sdate\",\"type\":[\"null\",\"string\"],\"default\":null}," +
                "{\"name\":\"stimestamp\",\"type\":[\"null\",\"string\"],\"default\":null}]}";

        assertEquals(validSchema, locatedSchemaText);

        final String notFound = delegate.getTableSchemaAsAvro("barSchema");
        Assert.assertNotNull(notFound, "Expected a the table name to be null and it was not");


        delegate.close();
    }

    @Test
    public void getRowHistoryTableSchemaAsAvro() throws Exception {
        ConfigurationContext configContext = mock(ConfigurationContext.class);
        when(configContext.getProperty(LocalFileTransformationRegistry.CONFIG_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/"));
        when(configContext.getProperty(LocalFileTransformationRegistry.RULE_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ColumnTransformationRules.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.PARAM_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ECDPFACTCA_PARAM.xml"));

        LocalFileTransformationRegistry delegate = new LocalFileTransformationRegistry(){
            @Override
            protected ComponentLog getLogger() {
                return new MockComponentLog("LocalfileTransformationRegistry", this);
            }
        };
        delegate.enable(configContext);

        String locatedSchemaText = delegate.getRowHistoryTableSchemaAsAvro("ecddp_all_copi_fatca_status_info_crs");

        final String validSchema = "{\"name\":\"ecddp_all_copi_fatca_status_info_crs\",\"type\":\"record\",\"fields\":[" +
                "{\"name\":\"rowid\",\"type\":\"string\"},{\"name\":\"filename\",\"type\":\"string\"}]}";

        assertEquals(validSchema, locatedSchemaText);


        delegate.close();
    }

    @Test
    public void getWarnTableSchemaAsAvroForRenamedTable() throws Exception {
        ConfigurationContext configContext = mock(ConfigurationContext.class);
        when(configContext.getProperty(LocalFileTransformationRegistry.CONFIG_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/"));
        when(configContext.getProperty(LocalFileTransformationRegistry.RULE_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ColumnTransformationRules.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.PARAM_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ECDPFACTCA_PARAM.xml"));

        LocalFileTransformationRegistry delegate = new LocalFileTransformationRegistry(){
            @Override
            protected ComponentLog getLogger() {
                return new MockComponentLog("LocalfileTransformationRegistry", this);
            }
        };
        delegate.enable(configContext);

        String locatedSchemaText = delegate.getWarnTableSchemaAsAvro("ecddp_all_copi_fatca_status_info_crs");

        final String validSchema = "{\"name\":\"ecddp_all_copi_fatca_status_info_crs\",\"type\":\"record\",\"fields\":[" +
                "{\"name\":\"source\",\"type\":\"string\"}," +
                "{\"name\":\"country\",\"type\":\"string\"}," +
                "{\"name\":\"tablename\",\"type\":\"string\"},{\"name\":\"rowid\",\"type\":\"string\"},{\"name\":\"data\",\"type\":\"string\"},{\"name\":\"errormsg\",\"type\":\"string\"}," +
                "{\"name\":\"partitioncolumn\",\"type\":\"string\"},{\"name\":\"ts\",\"type\":\"string\"}]}";

        assertEquals(validSchema, locatedSchemaText);
        delegate.close();
    }

    @Test
    public void getTableLevelSourceType() throws Exception {
        ConfigurationContext configContext = mock(ConfigurationContext.class);
        when(configContext.getProperty(LocalFileTransformationRegistry.CONFIG_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/acb_ca_tables_config.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.RULE_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/ColumnTransformationRules.xml"));
        when(configContext.getProperty(LocalFileTransformationRegistry.PARAM_FILE_PATH)).thenReturn(new MockPropertyValue("src/test/resources/acb_ca_param.xml"));

        LocalFileTransformationRegistry delegate = new LocalFileTransformationRegistry();
        delegate.enable(configContext);

        String sourcingType = delegate.getSourcingType("acb_ca_cashflows");
        System.out.println("Tablename : acb_ca_cashflows -- Sourcing Type : " + sourcingType);
        assertEquals(DiffenConstants.SOURCING_TYPE_BATCH_FIXEDWIDTH, sourcingType);
        sourcingType = delegate.getSourcingType("acb_ca_customers");
        System.out.println("Tablename : acb_ca_customers -- Sourcing Type : " + sourcingType);
        assertEquals(DiffenConstants.SOURCING_TYPE_BATCH_DELIMITED, sourcingType);
        delegate.close();
    }
}