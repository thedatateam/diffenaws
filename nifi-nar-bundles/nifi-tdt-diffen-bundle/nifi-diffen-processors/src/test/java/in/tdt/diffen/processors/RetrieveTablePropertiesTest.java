package in.tdt.diffen.processors;

import in.tdt.diffen.services.LocalFileTransformationRegistry;
import in.tdt.diffen.verifytypes.checker.VerifySchema;
import in.tdt.diffen.verifytypes.checker.VerifySchemaColumn;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.AbstractControllerService;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.distributed.cache.client.Deserializer;
import org.apache.nifi.distributed.cache.client.DistributedMapCacheClient;
import org.apache.nifi.distributed.cache.client.Serializer;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.reporting.InitializationException;
import org.apache.nifi.state.MockStateManager;
import org.apache.nifi.util.*;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static in.tdt.diffen.processors.RetrieveTableProperties.REGISTRY_SERVICE;
import static in.tdt.diffen.verifytypes.DiffenConstants.*;


public class RetrieveTablePropertiesTest {
	private MockLocalFileTransformationRegistry service = new MockLocalFileTransformationRegistry();
	private TestRunner runner = TestRunners.newTestRunner(RetrieveTableProperties.class);
	static DistributedMapCacheClientImpl distributedMapCacheClient;

	@Before
	public void initialize() throws InitializationException {

	}

	@After
	public void shutdown() throws Exception {
		service.close();
	}
	
	@BeforeClass
	public static void classSetup() throws InitializationException {
		distributedMapCacheClient = createClient();
	}

	@Test
	public void testSuccessCase() throws InitializationException {

		Map<String, String> properties = new HashMap<>();
		properties.put("Configuration File Path", "src/test/resources/acb_ca_tables_config.xml");
		properties.put("Rule File Path", "src/test/resources/ColumnTransformationRules.xml");
		properties.put("Table rename file Path", "src/test/resources/acb_ca_tables_rename.xml");
		properties.put("Parameter file Path", "src/test/resources/acb_ca_param.xml");
		properties.put("Distributed Map Cache Client", "distributed-map-cache-client");
		
		final Map<String, String> clientProperties = new HashMap<>();
		runner.addControllerService("distributed-map-cache-client", distributedMapCacheClient, clientProperties);
		runner.enableControllerService(distributedMapCacheClient);
		runner.setValidateExpressionUsage(true);

		runner.addControllerService("configuration-registry-service", service, properties);
		runner.enableControllerService(service);
		runner.setProperty(REGISTRY_SERVICE, "configuration-registry-service");
		runner.assertValid();

		Map<String, String> attributes = new HashMap<>();
		attributes.put("tablename","acb_ca_cashflows");
		attributes.put("has.header","false");

		final String sampleRow = "26053898836518_dc3ca216-ee47-405e-972c-1fa8eda81b40,ACB_CA_CASHFLOWS_20161230_D_1.csv,2016_12_30,MU,CA,20161230,75000564008,75000564008000445351600,20111115,1,0,13396,0,0,USD,0";
		runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
		runner.run();

		runner.assertTransferCount(RetrieveTableProperties.REL_SUCCESS, 1);
		runner.assertTransferCount(RetrieveTableProperties.REL_FAILURE, 0);
		final List<MockFlowFile> result = runner.getFlowFilesForRelationship(RetrieveTableProperties.REL_SUCCESS);
		result.get(0).assertContentEquals("26053898836518_dc3ca216-ee47-405e-972c-1fa8eda81b40,ACB_CA_CASHFLOWS_20161230_D_1.csv,2016_12_30,MU,CA,20161230,75000564008,75000564008000445351600,20111115,1,0,13396,0,0,USD,0"
		, StandardCharsets.UTF_8);
	}

	@Test
	public void testFailureCase() throws InitializationException {

		Map<String, String> properties = new HashMap<>();
		properties.put("Configuration File Path", "src/test/resources/acb_ca_tables_config.xml");
		properties.put("Rule File Path", "src/test/resources/ColumnTransformationRules.xml");
		properties.put("Table rename file Path", "src/test/resources/acb_ca_tables_rename.xml");
		properties.put("Parameter file Path", "src/test/resources/acb_ca_param.xml");
		properties.put("Distributed Map Cache Client", "distributed-map-cache-client");

		final Map<String, String> clientProperties = new HashMap<>();
		runner.addControllerService("distributed-map-cache-client", distributedMapCacheClient, clientProperties);
		runner.enableControllerService(distributedMapCacheClient);
		runner.setValidateExpressionUsage(true);

		runner.addControllerService("configuration-registry-service", service, properties);
		runner.enableControllerService(service);
		runner.setProperty(REGISTRY_SERVICE, "configuration-registry-service");
		runner.assertValid();

		Map<String, String> attributes = new HashMap<>();
		attributes.put("tablename","acb");


		final String sampleRow = "26053898836518_dc3ca216-ee47-405e-972c-1fa8eda81b40,ACB_CA_CASHFLOWS_20161230_D_1.csv,2016_12_30,MU,CA,20161230,75000564008,75000564008000445351600,20111115,1,0,13396,0,0,USD,0";
		runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
		runner.run();

		runner.assertTransferCount(RetrieveTableProperties.REL_SUCCESS, 0);
		runner.assertTransferCount(RetrieveTableProperties.REL_FAILURE, 1);
		final List<MockFlowFile> result = runner.getFlowFilesForRelationship(RetrieveTableProperties.REL_FAILURE);
		result.get(0).assertContentEquals("26053898836518_dc3ca216-ee47-405e-972c-1fa8eda81b40,ACB_CA_CASHFLOWS_20161230_D_1.csv,2016_12_30,MU,CA,20161230,75000564008,75000564008000445351600,20111115,1,0,13396,0,0,USD,0"
				, StandardCharsets.UTF_8);
	}

	private class MockLocalFileTransformationRegistry extends LocalFileTransformationRegistry {

		private ConfigurationContext mockConfigContext;
		private volatile DistributedMapCacheClient distributedMapCacheClient;

		public MockLocalFileTransformationRegistry() {
			Map<PropertyDescriptor, String> properties = new HashMap<>();
			
			PropertyDescriptor fileCongfig = new PropertyDescriptor.Builder()
			.name("Configuration File Path")
			.dynamic(false)
			.build();

			PropertyDescriptor ruleFileCongfig = new PropertyDescriptor.Builder()
			.name("Rule File Path")
			.dynamic(false)
			.build();

			PropertyDescriptor tableRenameCongfig = new PropertyDescriptor.Builder()
			.name("Table rename file Path")
			.dynamic(false)
			.build();

			PropertyDescriptor paramFileCongfig = new PropertyDescriptor.Builder()
			.name("Param file Path")
			.dynamic(false)
			.build();
			
			PropertyDescriptor cacheCongfig = new PropertyDescriptor.Builder()
			.name("Distributed Map Cache Client")
			.dynamic(false)
			.identifiesControllerService(DistributedMapCacheClient.class)
			.build();
			

			properties.put(fileCongfig, "acb_ca_tables_config.xml");
			properties.put(ruleFileCongfig, "ColumnTransformationRules.xml");
			properties.put(tableRenameCongfig, "acb_ca_tables_rename.xml");
			properties.put(paramFileCongfig,"acb_ca_param.xml");
			mockConfigContext = new MockConfigurationContext(properties, null);
		}


		private String tableConfigParamPrefix = null;
		private String globalConfigParamPrefix = null;
		private String renameConfigParamPrefix = null;
		private String rulesConfigPrefix = null;

		@OnEnabled
		public void enable(final ConfigurationContext configurationContext) throws InitializationException {{
			this.conf = new Configuration(false);
			this.conf.addResource(new Path(configurationContext.getProperty(CONFIG_FILE_PATH).getValue()));
			this.conf.addResource(new Path(configurationContext.getProperty(RULE_FILE_PATH).getValue()));
			this.conf.addResource(new Path(configurationContext.getProperty(PARAM_FILE_PATH).getValue()));

			if (configurationContext.getProperty(TABLE_RENAME_FILE_PATH) != null && StringUtils.isNotEmpty(configurationContext.getProperty(TABLE_RENAME_FILE_PATH).getValue())) {
				this.conf.addResource(new Path(configurationContext.getProperty(TABLE_RENAME_FILE_PATH).getValue()));
			}

			tableConfigParamPrefix = conf.get("param.prefix", DEFAULT_PARAM_PREFIX) + ADD_PREFIX_CONFIG_DIFFEN_TABLE;
			globalConfigParamPrefix = conf.get("param.prefix", DEFAULT_PARAM_PREFIX) + ADD_PREFIX_DIFFEN_GLOBAL_CONFIG;
			renameConfigParamPrefix = conf.get("param.prefix", DEFAULT_PARAM_PREFIX) + ADD_PREFIX_DIFFEN_RENAME_TABLE;
			rulesConfigPrefix = conf.get("param.prefix", DEFAULT_PARAM_PREFIX) + ADD_PREFIX_DIFFEN_RENAME_TABLE;

			final String globalConfigPattern = globalConfigParamPrefix + "(.*)";
			final String tablePattern = tableConfigParamPrefix + "(.*)" + SUFFIX_CONFIG_COLSCHEMA;
			final String renameTableConfigPattern = renameConfigParamPrefix + "(.*)";
			final String keyColsPattern = tableConfigParamPrefix + "(.*)" + SUFFIX_CONFIG_KEYCOLS;

			final String headerColSchemaPattern = tableConfigParamPrefix + "(.*)"+ HEADER_COLSCHEMA_SUFFIX;
			final String trailerColSchemaPattern = tableConfigParamPrefix + "(.*)"+ TRAILER_COLSCHEMA_SUFFIX;
			final String dataIndicatorPattern = tableConfigParamPrefix + "(.*)"+ DATA_INDICATOR;
			final String headerIndicatorPattern = tableConfigParamPrefix + "(.*)"+ HEADER_INDICATOR;
			final String trailerIndicatorPattern = tableConfigParamPrefix + "(.*)"+ TRAILER_INDICATOR;
			final String afterValueIndicatorPattern = tableConfigParamPrefix + "(.*)"+ SUFFIX_CONFIG_AFTERVALUE;
			final String beforeValueIndicatorPattern = tableConfigParamPrefix + "(.*)"+ SUFFIX_CONFIG_BEFOREVALUE;
			final String insertValueIndicatorPattern = tableConfigParamPrefix + "(.*)"+ SUFFIX_CONFIG_INSERTVALUE;
			final String deleteValueIndicatorPattern = tableConfigParamPrefix + "(.*)"+ SUFFIX_CONFIG_DELETEVALUE;
			final String controlFileTableNameIndexPattern = tableConfigParamPrefix + "(.*)"+ CONTROL_FILE_TABLENAME_INDEX;
			final String controlFileTablenamePattern = tableConfigParamPrefix + "(.*)"+ CONTROL_FILE_TABLENAME;
			final String reconFileRenamePattern = tableConfigParamPrefix + "(.*)"+ RECON_FILE_RENAME;

			Map<String, String> globalConfigs = conf.getValByRegex(globalConfigPattern);
			Map<String, String> tableConfigs = conf.getValByRegex(tablePattern);
			Map<String, String> renameTableConfigs = conf.getValByRegex(renameTableConfigPattern);
			Map<String,String> headerColSchemaConfigs = conf.getValByRegex(headerColSchemaPattern);
			Map<String,String> trailerColSchemaConfigs = conf.getValByRegex(trailerColSchemaPattern);
			Map<String,String> dataIndicatorConfigs = conf.getValByRegex(dataIndicatorPattern);
			Map<String,String> headerIndicatorConfigs = conf.getValByRegex(headerIndicatorPattern);
			Map<String,String> trailerIndicatorConfigs = conf.getValByRegex(trailerIndicatorPattern);
			Map<String,String> keyColsConfigs = conf.getValByRegex(keyColsPattern);
			Map<String,String> afterValueConfigs = conf.getValByRegex(afterValueIndicatorPattern);
			Map<String,String> beforeValueConfigs = conf.getValByRegex(beforeValueIndicatorPattern);
			Map<String,String> insertValueConfigs = conf.getValByRegex(insertValueIndicatorPattern);
			Map<String,String> deleteValueConfigs = conf.getValByRegex(deleteValueIndicatorPattern);
			Map<String,String> controlFileTableNameIndexConfigs = conf.getValByRegex(controlFileTableNameIndexPattern);
			Map<String,String> controlFileTableNameConfigs = conf.getValByRegex(controlFileTablenamePattern);
			Map<String,String> reconFileRenameConfigs = conf.getValByRegex(reconFileRenamePattern);

			tableGeneralConfigs.putAll(getTableRawSchema(globalConfigs, globalConfigParamPrefix, ""));
			tableRawSchema.putAll(getTableRawSchema(tableConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_COLSCHEMA));
			tableRenameConfigs.putAll(getTableRawSchema(renameTableConfigs, renameConfigParamPrefix, ""));
			keyColumnsMap.putAll(getTableRawSchema(keyColsConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_KEYCOLS));

			headerColSchemaMap.putAll(getTableRawSchema(headerColSchemaConfigs, tableConfigParamPrefix, HEADER_COLSCHEMA_SUFFIX));
			trailerColSchemaMap.putAll(getTableRawSchema(trailerColSchemaConfigs,tableConfigParamPrefix, TRAILER_COLSCHEMA_SUFFIX));

			dataIndicatorMap.putAll(getTableRawSchema(dataIndicatorConfigs,tableConfigParamPrefix, DATA_INDICATOR));
			headerIndicatorMap.putAll(getTableRawSchema(headerIndicatorConfigs,tableConfigParamPrefix, HEADER_INDICATOR));
			trailerIndicatorMap.putAll(getTableRawSchema(trailerIndicatorConfigs,tableConfigParamPrefix, TRAILER_INDICATOR));

			afterValueIndicatorMap.putAll(getTableRawSchema(afterValueConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_AFTERVALUE));
			beforeValueIndicatorMap.putAll(getTableRawSchema(beforeValueConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_BEFOREVALUE));
			insertValueIndicatorMap.putAll(getTableRawSchema(insertValueConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_INSERTVALUE));
			deleteValueIndicatorMap.putAll(getTableRawSchema(deleteValueConfigs, tableConfigParamPrefix, SUFFIX_CONFIG_DELETEVALUE));
			controlFileTableNameIndex.putAll(getTableRawSchema(controlFileTableNameIndexConfigs, tableConfigParamPrefix, CONTROL_FILE_TABLENAME_INDEX));
			controlFileTableName.putAll(getTableRawSchema(controlFileTableNameConfigs, tableConfigParamPrefix, CONTROL_FILE_TABLENAME));
			reconFileRename.putAll(getTableRawSchema(reconFileRenameConfigs, tableConfigParamPrefix, RECON_FILE_RENAME));


			String fileDelimiter = tableGeneralConfigs.get(FILE_DELIMITER);
			if (StringUtils.isNotBlank(fileDelimiter)) tableGeneralConfigs.put("escaped.filedelimiter", StringEscapeUtils.escapeXml11(StringEscapeUtils.unescapeJava(fileDelimiter)));

			//This is applicable only for Valid types.
			//Load up the columns per table
			for (final Map.Entry<String, String> tableEntry : tableConfigs.entrySet()) {

				final String fqtn = tableEntry.getKey();
				final String tableName = fqtn.substring(tableConfigParamPrefix.length(),
						fqtn.length() - SUFFIX_CONFIG_COLSCHEMA.length());

				final VerifySchema tableSchema = new VerifySchema(tableEntry.getValue(), true);
				final List<VerifySchemaColumn> dataSchemaColumns = tableSchema.getSchemaColumns();
				//build up the header that can be used to parse a row of data for this table.
				final String[] headerRow = new String[dataSchemaColumns.size()];
				for (int i = 0; i < dataSchemaColumns.size(); i++) {
					final VerifySchemaColumn verifySchemaColumn = dataSchemaColumns.get(i);
					headerRow[i] = verifySchemaColumn.getName();
				}

				//Construct Fixed Width to Delimited
				if (getSourcingType().equals(SOURCING_TYPE_BATCH_FIXEDWIDTH)) {
					//TODO Fix the bottom and the top portion using streams as well

					getLogger().debug ("Table column schema : "+ tableEntry.getValue());
					//Data
					String dataIndicator = dataIndicatorMap.get(tableName);
					String dataParseableRegexStr = getParseableRegex(dataIndicator, dataSchemaColumns);
					StringBuilder dataToDelimitedRegex = getDelimitedRegex(dataIndicator, fileDelimiter, dataSchemaColumns);

					tableFixedWidthParseableRegexData.put(tableName.toLowerCase(), dataParseableRegexStr);
					tableFixedWidthToDelimitedRegexData.put(tableName.toLowerCase(), StringUtils.removeEnd(dataToDelimitedRegex.toString(), fileDelimiter));

					//Header
					if (headerColSchemaMap.containsKey(tableName) && StringUtils.isNotBlank(headerColSchemaMap.get(tableName))) {
						final VerifySchema headerSchema = new VerifySchema(headerColSchemaMap.get(tableName), true);
						final List<VerifySchemaColumn> headerSchemaCols = headerSchema.getSchemaColumns();

						String headerIndicator = headerIndicatorMap.get(tableName);
						String headerParseableRegexStr = getParseableRegex(headerIndicator, headerSchemaCols);
						StringBuilder headerToDelimitedRegex = getDelimitedRegex(headerIndicator, fileDelimiter, headerSchemaCols);

						tableFixedWidthParseableRegexHeader.put(tableName.toLowerCase(), headerParseableRegexStr);
						tableFixedWidthToDelimitedRegexHeader.put(tableName.toLowerCase(), StringUtils.removeEnd(headerToDelimitedRegex.toString(), fileDelimiter));
					}

					//Trailer
					if (trailerColSchemaMap.containsKey(tableName) && StringUtils.isNotBlank(trailerColSchemaMap.get(tableName))) {
						final VerifySchema trailerSchema = new VerifySchema(trailerColSchemaMap.get(tableName), true);
						final List<VerifySchemaColumn> trailerSchemaCols = trailerSchema.getSchemaColumns();

						String trailerIndicator = trailerIndicatorMap.get(tableName);
						String trailerParseableRegexStr = getParseableRegex(trailerIndicator, trailerSchemaCols);
						StringBuilder trailerToDelimitedRegex = getDelimitedRegex(trailerIndicator, fileDelimiter, trailerSchemaCols);

						tableFixedWidthParseableRegexTrailer.put(tableName.toLowerCase(), trailerParseableRegexStr);
						tableFixedWidthToDelimitedRegexTrailer.put(tableName.toLowerCase(), StringUtils.removeEnd(trailerToDelimitedRegex.toString(), fileDelimiter));
					}

				}

				tableSchemaHeader.put(tableName.toLowerCase(), headerRow);
				tableSchemaColumns.put(tableName.toLowerCase(), dataSchemaColumns);

			}

		}}

	}

	private static class StringSerializer implements Serializer<String> {

		@Override
		public void serialize(final String value, final OutputStream output) throws SerializationException, IOException {
			output.write(value.getBytes(StandardCharsets.UTF_8));
		}
	}
	private static DistributedMapCacheClientImpl createClient() throws InitializationException {

		final DistributedMapCacheClientImpl client = new DistributedMapCacheClientImpl();
		final ComponentLog logger = new MockComponentLog("client", client);
		final MockControllerServiceInitializationContext clientInitContext = new MockControllerServiceInitializationContext(client, "client", logger, new MockStateManager(client));
		client.initialize(clientInitContext);

		return client;
	}
	static final class DistributedMapCacheClientImpl extends AbstractControllerService implements DistributedMapCacheClient {

		boolean exists = false;
		private Object cacheValue;

		@Override
		public void close() throws IOException {
		}

		@Override
		public void onPropertyModified(final PropertyDescriptor descriptor, final String oldValue, final String newValue) {
		}

		@Override
		public <K, V> boolean putIfAbsent(final K key, final V value, final Serializer<K> keySerializer, final Serializer<V> valueSerializer) throws IOException {
			if (exists) {
				return false;
			}

			cacheValue = value;
			exists = true;
			return true;
		}

		@Override
		@SuppressWarnings("unchecked")
		public <K, V> V getAndPutIfAbsent(final K key, final V value, final Serializer<K> keySerializer, final Serializer<V> valueSerializer,
				final Deserializer<V> valueDeserializer) throws IOException {
			if (exists) {
				return (V) cacheValue;
			}
			cacheValue = value;
			exists = true;
			return null;
		}

		@Override
		public <K> boolean containsKey(final K key, final Serializer<K> keySerializer) throws IOException {
			return exists;
		}

		@Override
		public <K, V> V get(final K key, final Serializer<K> keySerializer, final Deserializer<V> valueDeserializer) throws IOException {
			if (exists) {
				return (V) cacheValue;
			} else {
				return null;
			}
		}

		@Override
		public <K> boolean remove(final K key, final Serializer<K> serializer) throws IOException {
			exists = false;
			return true;
		}

		@Override
		public long removeByPattern(String s) throws IOException {
			return 0;
		}

		@Override
		public <K, V> void put(final K key, final V value, final Serializer<K> keySerializer, final Serializer<V> valueSerializer) throws IOException {
			cacheValue = value;
			exists = true;
		}
	}
}