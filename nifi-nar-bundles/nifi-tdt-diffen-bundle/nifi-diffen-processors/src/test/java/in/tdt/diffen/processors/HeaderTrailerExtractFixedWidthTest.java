/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.services.LocalFileTransformationRegistry;
import in.tdt.diffen.verifytypes.checker.VerifySchema;
import in.tdt.diffen.verifytypes.checker.VerifySchemaColumn;
import org.apache.commons.lang3.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.AbstractControllerService;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.distributed.cache.client.Deserializer;
import org.apache.nifi.distributed.cache.client.DistributedMapCacheClient;
import org.apache.nifi.distributed.cache.client.Serializer;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.reporting.InitializationException;
import org.apache.nifi.state.MockStateManager;
import org.apache.nifi.util.*;
import org.apache.nifi.util.StringUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static in.tdt.diffen.verifytypes.DiffenConstants.*;

public class HeaderTrailerExtractFixedWidthTest {
	private TestRunner runner = TestRunners.newTestRunner(HeaderTrailerExtract.class);
	Map<String, String> attributes = new HashMap<>();
	static DistributedMapCacheClientImpl distributedMapCacheClient;
	private LocalFileTransformationRegistry service = new LocalFileTransformationRegistry();

	@BeforeClass
	public static void classSetup() throws InitializationException {
		distributedMapCacheClient = createClient();
	}

	@Before
	public void initialize() throws InitializationException {

		Map<String, String> properties = new HashMap<>();

		properties.put("Configuration File Path", "src/test/resources/address_cr_hk_tables.config.xml");
		properties.put("Rule File Path", "src/test/resources/ColumnTransformationRules.xml");
		properties.put("Parameter file Path", "src/test/resources/address_cr_hk_tables.config.xml");
		properties.put("Distributed Map Cache Client", "distributed-map-cache-client");
		runner.setProperty("Delimiter", ";");


		final Map<String, String> clientProperties = new HashMap<>();
		runner.addControllerService("distributed-map-cache-client", distributedMapCacheClient, clientProperties);
		runner.enableControllerService(distributedMapCacheClient);

		runner.addControllerService("configuration-registry-service", service, properties);
		runner.enableControllerService(service);
		runner.setProperty(DataTransformation.REGISTRY_SERVICE, "configuration-registry-service");

		runner.assertValid();
	}

	@Test
	public void testValidFlow() throws InitializationException{

		attributes.put("tablename", "hog_hk_acacrel");
		attributes.put("absolute.path", "src\\test\\resources\\headertrailer"); //ACBS
		attributes.put("sourcing.type", "BATCH_FIXEDWIDTH");
		attributes.put("fixedwidth_header.parseable.regex", "^(H)(.{7})(.{7})");
		attributes.put("fixedwidth_header.todelimited.regex", "$1\\u0001$2\\u0001$3");
		attributes.put("fixedwidth_trailer.parseable.regex", "^(T)(.{45})");
		attributes.put("fixedwidth_trailer.todelimited.regex", "$1\\u0001$2");
		attributes.put("fixedwidth.parseable.regex", "^([D|X])(.{3})(.{16})(.{2})(.{3})(.{3})(.{16})(.{2})");
		//attributes.put("fixedwidth.delimited.regex", "$1;$2;$3;$4;$5;$6;$7;$8");
		attributes.put("fixedwidth.todelimited.regex","$1\\u0001$2\\u0001$3\\u0001$4\\u0001$5\\u0001$6\\u0001$7\\u0001$8");
		attributes.put("retain.indicator.in.data", "true");
		attributes.put(CONTROL_FILE_TABLENAME_INDEX,"2");
		attributes.put(CONTROL_FILE_TABLENAME,"CAE_FT_ADDENDUM_1");
		runner.setProperty("Delimiter", "\u0001");


		final String sampleRow = "" +
				"HACACREL1170113                              \n" +
				"XDDA0000029180003140BMHKDDDA0000028200001189BN\n" +
				"XDDA0000029180003141RMCNYDDA0000028210002234RO\n" +
				"XDDA0000029180003141RMHKDDDA0000028210002226RN\n" +
				"DDDA0000029180003210BMHKDDDA0000028200001197BN\n" +
				"DDDA0000097280021321RMUSDDDA0000028210017355RO\n" +
				"T000004403                                    \n";
		runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
		runner.run();

		runner.assertTransferCount(HeaderTrailerExtract.SUCCESS, 1);
		runner.assertTransferCount(HeaderTrailerExtract.FAILURE, 0);
		final List<MockFlowFile> result = runner.getFlowFilesForRelationship(HeaderTrailerExtract.SUCCESS);
		result.get(0).assertContentEquals(
				"X\u0001DDA\u00010000029180003140\u0001BM\u0001HKD\u0001DDA\u00010000028200001189\u0001BN\n" +
						"X\u0001DDA\u00010000029180003141\u0001RM\u0001CNY\u0001DDA\u00010000028210002234\u0001RO\n" +
						"X\u0001DDA\u00010000029180003141\u0001RM\u0001HKD\u0001DDA\u00010000028210002226\u0001RN\n" +
						"D\u0001DDA\u00010000029180003210\u0001BM\u0001HKD\u0001DDA\u00010000028200001197\u0001BN\n" +
						"D\u0001DDA\u00010000097280021321\u0001RM\u0001USD\u0001DDA\u00010000028210017355\u0001RO\n",
				StandardCharsets.UTF_8);
		result.get(0).assertAttributeExists("header_data");
		result.get(0).assertAttributeExists("trailer_data");
		result.get(0).assertAttributeEquals("header_data", "\"date\",\"1170113\",\"htablenames\",\"ACACREL\"");
		result.get(0).assertAttributeEquals("trailer_data", "\"rowcount\",\"000004403\"");
	}

	@Test
	public void testValidFlowPipeDelimiter() throws InitializationException{

		attributes.put("tablename", "hog_hk_acacrel");
		attributes.put("absolute.path", "src\\test\\resources\\headertrailer"); //ACBS
		attributes.put("sourcing.type", "BATCH_FIXEDWIDTH");
		attributes.put("fixedwidth_header.parseable.regex", "^(H)(.{7})(.{7})");
		attributes.put("fixedwidth_header.todelimited.regex", "$1|$2|$3");
		attributes.put("fixedwidth_trailer.parseable.regex", "^(T)(.{45})");
		attributes.put("fixedwidth_trailer.todelimited.regex", "$1|$2");
		attributes.put("fixedwidth.parseable.regex", "^([D|X])(.{3})(.{16})(.{2})(.{3})(.{3})(.{16})(.{2})");
		//attributes.put("fixedwidth.delimited.regex", "$1;$2;$3;$4;$5;$6;$7;$8");
		attributes.put("fixedwidth.todelimited.regex","$1|$2|$3|$4|$5|$6|$7|$8");
		attributes.put("retain.indicator.in.data", "true");
		attributes.put(CONTROL_FILE_TABLENAME_INDEX,"2");
		attributes.put(CONTROL_FILE_TABLENAME,"CAE_FT_ADDENDUM_1");
		runner.setProperty("Delimiter", "|");


		final String sampleRow = "" +
				"HACACREL1170113                              \n" +
				"XDDA0000029180003140BMHKDDDA0000028200001189BN\n" +
				"XDDA0000029180003141RMCNYDDA0000028210002234RO\n" +
		"T000004403                                    \n";
		runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
		runner.run();

		runner.assertTransferCount(HeaderTrailerExtract.SUCCESS, 1);
		runner.assertTransferCount(HeaderTrailerExtract.FAILURE, 0);
		final List<MockFlowFile> result = runner.getFlowFilesForRelationship(HeaderTrailerExtract.SUCCESS);
		result.get(0).assertContentEquals(
				"X|DDA|0000029180003140|BM|HKD|DDA|0000028200001189|BN\n" +
						"X|DDA|0000029180003141|RM|CNY|DDA|0000028210002234|RO\n",
				StandardCharsets.UTF_8);
		result.get(0).assertAttributeExists("header_data");
		result.get(0).assertAttributeExists("trailer_data");
		result.get(0).assertAttributeEquals("header_data", "\"date\",\"1170113\",\"htablenames\",\"ACACREL\"");
		result.get(0).assertAttributeEquals("trailer_data", "\"rowcount\",\"000004403\"");
	}

	@Ignore
	public void testValidFlowWithNoIndicatorRetained() throws InitializationException{

		attributes.put("tablename", "hog_hk_acacrel");
		attributes.put("absolute.path", "src\\test\\resources\\headertrailer");
		attributes.put("sourcing.type", "BATCH_FIXEDWIDTH");
		attributes.put("fixedwidth_header.parseable.regex", "^(.{1})(.{10})(.{7})");
		attributes.put("fixedwidth_header.todelimited.regex", "$1;$2;$3");
		attributes.put("fixedwidth_trailer.parseable.regex", "^(.{1})(.{45})");
		attributes.put("fixedwidth_trailer.todelimited.regex", "$1;$2");
		attributes.put("fixedwidth.parseable.regex", "^(.{1})(.{3})(.{16})(.{2})(.{3})(.{3})(.{16})(.{2})");
		attributes.put("fixedwidth.todelimited.regex", "$1;$2;$3;$4;$5;$6;$7;$8");
		attributes.put("retain.indicator.in.data", "false");
		attributes.put(CONTROL_FILE_TABLENAME_INDEX,"3");
		attributes.put(CONTROL_FILE_TABLENAME,"CAE_FT_ADDENDUM_1");


		final String sampleRow = "" +
				"HACAC REL1170113                              \n" +
				"XDDA0000029180003140BMHKDDDA0000028200001189BN\n" +
				"XDDA0000029180003141RMCNYDDA0000028210002234RO\n" +
				"XDDA0000029180003141RMHKDDDA0000028210002226RN\n" +
				"DDDA0000029180003210BMHKDDDA0000028200001197BN\n" +
				"DDDA0000097280021321RMUSDDDA0000028210017355RO\n" +
				"T000004403                                    \n";
		runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
		runner.run();

		runner.assertTransferCount(HeaderTrailerExtract.SUCCESS, 1);
		runner.assertTransferCount(HeaderTrailerExtract.FAILURE, 0);
		final List<MockFlowFile> result = runner.getFlowFilesForRelationship(HeaderTrailerExtract.SUCCESS);
		result.get(0).assertContentEquals(
				  "DDA;0000029180003140;BM;HKD;DDA;0000028200001189;BN\n" +
						"DDA;0000029180003141;RM;CNY;DDA;0000028210002234;RO\n" +
						"DDA;0000029180003141;RM;HKD;DDA;0000028210002226;RN\n" +
						"DDA;0000029180003210;BM;HKD;DDA;0000028200001197;BN\n" +
						"DDA;0000097280021321;RM;USD;DDA;0000028210017355;RO\n",
				StandardCharsets.UTF_8);
		result.get(0).assertAttributeExists("header_data");
		result.get(0).assertAttributeExists("trailer_data");
		result.get(0).assertAttributeEquals("header_data", "\"date\",\"70113\",\"htablenames\",\"ACAC REL11\"");
		result.get(0).assertAttributeEquals("trailer_data", "\"rowcount\",\"000004403\"");
	}

	private static DistributedMapCacheClientImpl createClient() throws InitializationException {

		final DistributedMapCacheClientImpl client = new DistributedMapCacheClientImpl();
		final ComponentLog logger = new MockComponentLog("client", client);
		final MockControllerServiceInitializationContext clientInitContext = new MockControllerServiceInitializationContext(client, "client", logger, new MockStateManager(client));
		client.initialize(clientInitContext);

		return client;
	}
	static final class DistributedMapCacheClientImpl extends AbstractControllerService implements DistributedMapCacheClient {

		boolean exists = false;
		private Object cacheValue;

		@Override
		public void close() throws IOException {
		}

		@Override
		public void onPropertyModified(final PropertyDescriptor descriptor, final String oldValue, final String newValue) {
		}

		@Override
		public <K, V> boolean putIfAbsent(final K key, final V value, final Serializer<K> keySerializer, final Serializer<V> valueSerializer) throws IOException {
			if (exists) {
				return false;
			}

			cacheValue = value;
			exists = true;
			return true;
		}

		@Override
		@SuppressWarnings("unchecked")
		public <K, V> V getAndPutIfAbsent(final K key, final V value, final Serializer<K> keySerializer, final Serializer<V> valueSerializer,
										  final Deserializer<V> valueDeserializer) throws IOException {
			if (exists) {
				return (V) cacheValue;
			}
			cacheValue = value;
			exists = true;
			return null;
		}

		@Override
		public <K> boolean containsKey(final K key, final Serializer<K> keySerializer) throws IOException {
			return exists;
		}

		@Override
		public <K, V> V get(final K key, final Serializer<K> keySerializer, final Deserializer<V> valueDeserializer) throws IOException {
			if (exists) {
				return (V) cacheValue;
			} else {
				return null;
			}
		}

		@Override
		public <K> boolean remove(final K key, final Serializer<K> serializer) throws IOException {
			exists = false;
			return true;
		}

		@Override
		public long removeByPattern(String s) throws IOException {
			return 0;
		}

		@Override
		public <K, V> void put(final K key, final V value, final Serializer<K> keySerializer, final Serializer<V> valueSerializer) throws IOException {
			cacheValue = value;
			exists = true;
		}
	}
}