/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import in.tdt.diffen.services.LocalFileTransformationRegistry;
import in.tdt.diffen.verifytypes.DiffenConstants;
import in.tdt.diffen.verifytypes.checker.VerifySchema;
import in.tdt.diffen.verifytypes.checker.VerifySchemaColumn;
import org.apache.hadoop.conf.Configuration;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.AbstractControllerService;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.distributed.cache.client.Deserializer;
import org.apache.nifi.distributed.cache.client.DistributedMapCacheClient;
import org.apache.nifi.distributed.cache.client.Serializer;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.reporting.InitializationException;
import org.apache.nifi.state.MockStateManager;
import org.apache.nifi.util.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static in.tdt.diffen.verifytypes.DiffenConstants.*;
import static org.junit.Assert.assertTrue;

public class EODProcessingTest {

    private MockLocalFileTransformationRegistry service = new MockLocalFileTransformationRegistry();
    private TestRunner runner = TestRunners.newTestRunner(EODProcessing.class);
    private DateTimeFormatter formatter;

    Map<String, String> attributes = new HashMap<>();

    @Before
    public void initialize() throws InitializationException {

        Map<String, String> properties = new HashMap<>();

        properties.put("Configuration File Path", "src/test/resources/ColumnTransformationRules.xml");
        properties.put("Rule File Path", "src/test/resources/abc_tables.xml");
        properties.put("Table rename file Path", "src/test/resources/acb_ca_tables_rename.xml");
        properties.put("Parameter file Path", "src/test/resources/acb_ca_param.xml");
        properties.put("Distributed Map Cache Client", "distributed-map-cache-client");

        runner.setValidateExpressionUsage(true);
        runner.addControllerService("configuration-registry-service", service, properties);
        runner.enableControllerService(service);
        final DistributedMapCacheClientImpl client = createClient();
        final Map<String, String> clientProperties = new HashMap<>();
        runner.addControllerService("distributed-map-cache-client", client, clientProperties);
        runner.enableControllerService(client);
        runner.setProperty(EODProcessing.REGISTRY_SERVICE, "configuration-registry-service");
        runner.setProperty(EODProcessing.CACHE_SERVICE, "distributed-map-cache-client");
        runner.setProperty(EODProcessing.DELIMITER, ",");
        runner.assertValid();

        attributes.clear();
        attributes.put("source", "abc");
        attributes.put("country", "all");
        attributes.put("tablename", "abc_all_tl_eod_marker");
        attributes.put("source", "ABC");
        attributes.put("date", "2017_01_02");
        attributes.put("time", "13:00:12");
        attributes.put("row.metadata.added", "true");
        attributes.put(DiffenConstants.TIMESTAMP_COL_FORMAT, "yyyy-MM-dd HH:mm:ss.SSSSSS");
        attributes.put("markertime.column", "MARKER_TIME");
        attributes.put("eoddate.column", "EOD_DATE");
        attributes.put("journaltime.column", "C_JOURNALTIME");
        attributes.put("userid.column", "C_USERID");
        attributes.put("timezone", "Asia/Kolkata");

        formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
                .withZone(DateTimeZone.forID(attributes.get("timezone")));
    }

    @After
    public void shutdown() throws Exception {
        service.close();
    }

    @Test
    public void TestMultiRecordEODMarker() throws InitializationException {

        final DateTime currentDate = DateTime.now(DateTimeZone.forID(attributes.get("timezone")));
        final String journalTime = formatter.print(currentDate.minusMinutes(2));
        // final String sampleRow =
        // "ROWID\u0001JTS\u0001TrxID\u0001OpType\u0001UserID\u000120170225\u000120170325\u000120170425\u0001SG\n";
        final String input = "rowid,FNAME,vds," + journalTime
                + ",A,A,KFOZM,2016-01-20 10:10:37,2016-01-20 16:54:28,2016-01-22 12:45:50,S\n"
                + "rowid,FNAME,vds,2016-01-01 10:47:25.123456,B,B,KFOZM,2016-01-12 12:24:49,2016-01-08 10:28:32,2016-02-01 15:24:48,ETQJ\n"
                + "rowid,FNAME,vds,2016-01-21 14:47:44.123456,D,B,UJ,2016-01-13 14:23:37,2016-01-20 10:28:05,2016-01-09 10:21:40,LV\n"
                + "rowid,FNAME,vds,2016-01-05 11:14:13.123456,E,I,SDFGU,2016-01-01 16:10:34,2016-01-15 10:44:50,2016-01-06 14:36:00,JGVPBG\n"
                + "rowid,FNAME,vds,2016-01-08 09:10:32.123456,C,A,LNAP,2016-02-01 13:12:21,2016-01-24 14:13:14,2016-01-08 14:34:45,PX\n"
                + "rowid,FNAME,vds,2016-01-25 11:14:29.123456,F,D,XKTN,2016-01-02 12:08:19,2016-01-12 15:09:12,2016-01-05 16:09:48,FYSSOZW\n"
                + "rowid,FNAME,vds,2016-01-04 11:40:30.123456,G,A,RBGAT,2016-01-30 12:25:59,2016-01-19 13:23:10,2016-01-07 09:08:37,ZVZF\n"
                + "rowid,FNAME,vds,2016-02-01 10:23:18.123456,H,I,W,2016-01-25 12:15:51,2016-01-19 10:31:54,2016-01-17 14:42:32,AGPUM\n"
                + "rowid,FNAME,vds,2016-01-28 15:58:14.123456,I,I,H,2016-01-06 10:51:13,2016-01-21 12:28:09,2016-01-29 12:57:52,WZBCLOLZSO\n"
                + "rowid,FNAME,vds,2016-01-02 09:16:44.123456,J,D,HRQ,2016-01-26 12:29:52,2016-01-16 14:53:09,2016-01-11 16:36:51,QIHDI\n"
                + "rowid,FNAME,vds,2016-01-05 12:13:02.123456,K,I,PIHHR,2016-01-01 14:49:45,2016-01-20 12:38:30,2016-01-05 09:15:18,XITXV\n"
                + "rowid,FNAME,vds,2016-01-26 15:16:44.123456,L,D,DCS,2016-01-31 16:56:30,2016-01-12 10:50:51,2016-01-01 15:07:12,UFJ\n"
                + "rowid,FNAME,vds,2016-01-20 11:23:00.123456,M,D,OG,2016-01-20 12:49:57,2016-01-29 15:31:00,2016-01-08 16:39:34,TWSXQREJGD\n"
                + "rowid,FNAME,vds,2016-01-13 11:25:01.123456,N,A,NZP,2016-01-04 15:32:02,2016-01-02 13:49:56,2016-01-29 09:47:08,EA\n";

        runner.setProperty(EODProcessing.EOD_WAIT_TIME, "1");
        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);
        runner.run();

        runner.assertTransferCount(EODProcessing.REL_SUCCESS, 1);
        final MockFlowFile successFlow = runner.getFlowFilesForRelationship(EODProcessing.REL_SUCCESS).get(0);

        final String markerTime = successFlow.getAttribute(EODProcessing.EOD_MARKER_TIME);
        final String businessTime = successFlow.getAttribute(EODProcessing.EOD_BUSINESS_TIME);
        assert (!markerTime.isEmpty());
        assertTrue("2016-01-22 12:45:50".contentEquals(markerTime));
        assert (!businessTime.isEmpty());
        assertTrue("2016-01-20 10:10:37".contentEquals(businessTime));

    }

    @Test
    public void TestTwoRecordEODMarker() throws InitializationException {
        final DateTime currentDate = DateTime.now(DateTimeZone.forID(attributes.get("timezone")));
        final String journalTime = formatter.print(currentDate.minusMinutes(2));
        final String input = "ROWID,FNAME,vds," + journalTime
                + ",A,A,K,2016-01-20 10:10:37.123456,2016-01-20 16:54:28,2016-01-22 12:45:50.123456,S\n"
                + "ROWID,FNAME,vds,"
                + "2016-01-21 15:19:16,D,B,UJ,2016-01-13 14:23:37,2016-01-20 10:28:05,2016-01-09 10:21:40,LV\n";

        runner.setProperty(EODProcessing.EOD_WAIT_TIME, "1");
        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);
        runner.run();

        runner.assertTransferCount(EODProcessing.REL_SUCCESS, 1);
        final MockFlowFile successFlow = runner.getFlowFilesForRelationship(EODProcessing.REL_SUCCESS).get(0);

        final String markerTime = successFlow.getAttribute(EODProcessing.EOD_MARKER_TIME);
        final String businessTime = successFlow.getAttribute(EODProcessing.EOD_BUSINESS_TIME);
        assert (!markerTime.isEmpty());
        assertTrue("2016-01-22 12:45:50.123456".contentEquals(markerTime));
        assert (!businessTime.isEmpty());
        assertTrue("2016-01-20 10:10:37.123456".contentEquals(businessTime));
    }

    @Test
    public void TestPenalizeEODMarker() throws InitializationException {

        final DateTime currentDate = DateTime.now(DateTimeZone.forID(attributes.get("timezone")));
        final String journalTime = formatter.print(currentDate.minusSeconds(1));
        final String input = "ROWID\u0001FNAME\u0001vds\u0001" + journalTime
                + "\u0001A\u0001A\u0001K\u00012016-01-20 10:10:37\u00012016-01-20 16:54:28\u00012016-01-22 12:45:50\u0001S\n"
                + "2016-01-21 15:19:16\u0001D\u0001B\u0001UJ\u00012016-01-13 14:23:37\u00012016-01-20 10:28:05\u00012016-01-09 10:21:40\u0001LV\n";

        runner.removeProperty(DataTransformation.DELIMITER);
        runner.setProperty(DataTransformation.DELIMITER, "\u0001");

        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);
        runner.run();

        runner.assertPenalizeCount(1);

    }

    @Test
    public void TestVeryOldEODMarker() throws InitializationException {

        final DateTime currentDate = DateTime.now(DateTimeZone.UTC);
        final String input = "ROWID\u0001FNAME\u0001vds\u00012017-01-01 23:15:00.123456"
                + "\u0001A\u0001A\u0001K\u00012016-01-20 10:10:37\u00012016-01-20 16:54:28\u00012016-01-22 12:45:50\u0001S\n"
                + "ROWID\u0001FNAME\u0001vds\u00012016-01-21 15:19:16.123456\u0001D\u0001B\u0001UJ\u00012016-01-13 14:23:37\u00012016-01-20 10:28:05\u00012016-01-09 10:21:40\u0001LV\n";

        runner.removeProperty(DataTransformation.DELIMITER);
        runner.setProperty(DataTransformation.DELIMITER, "\u0001");

        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);
        runner.run();

        runner.assertPenalizeCount(0);
        runner.assertAllFlowFilesTransferred(DataTransformation.REL_SUCCESS, 1);
    }

    // 2017-01-11 23:15:00\u000116588363205\u0001A\u0001BHOPALAP\u00012017-01-11
    // 23:15:00\u00012017-01-12 23:11:53\u00012017-01-11 23:50:39\u0001all
    @Test
    public void TestNPE() throws InitializationException {

        final DateTime currentDate = DateTime.now(DateTimeZone.forID(attributes.get("timezone")));
        final String input = "ROWID\u0001FNAME\u0001vds\u00012017-01-11 23:15:00.123456\u000116588363205\u0001A\u0001BHOPALAP\u00012017-01-11 23:15:00\u00012017-01-12 23:11:53\u00012017-01-11 23:50:39\u0001all\n";

        runner.removeProperty(DataTransformation.DELIMITER);
        runner.setProperty(DataTransformation.DELIMITER, "\u0001");

        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);
        runner.run();

        runner.assertPenalizeCount(0);
        runner.assertAllFlowFilesTransferred(DataTransformation.REL_SUCCESS, 1);
    }

    @Test
    public void testFullDumpFile() throws InitializationException {

        final DateTime currentDate = DateTime.now(DateTimeZone.forID(attributes.get("timezone")));
        final String input = "ROWID\u0001FNAME\u0001vds\u00012017-01-11 23:15:00.123456\u000116588363205\u0001A\u0001NOT SET   \u00012017-01-11 23:15:00\u00012017-01-12 23:11:53\u00012017-01-11 23:50:39\u0001all\n";

        runner.removeProperty(DataTransformation.DELIMITER);
        runner.setProperty(DataTransformation.DELIMITER, "\u0001");

        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);
        runner.run();

        runner.assertPenalizeCount(0);
        runner.assertAllFlowFilesTransferred(DataTransformation.REL_SUCCESS, 1);

        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(EODProcessing.REL_SUCCESS);
        final String processingType = result.get(0).getAttribute(EODProcessing.PROCESSING_TYPE);
        assertTrue(processingType.contentEquals("fulldump"));
    }

    @Test
    public void testIncrementalProcessing() throws InitializationException {

        final DateTime currentDate = DateTime.now(DateTimeZone.forID(attributes.get("timezone")));
        final String input = "ROWID\u0001FNAME\u0001vds\u00012017-01-11 23:15:00.123456\u000116588363205\u0001A\u0001SomeUser\u00012017-01-11 23:15:00\u00012017-01-12 23:11:53\u00012017-01-11 23:50:39\u0001all\n";

        runner.removeProperty(DataTransformation.DELIMITER);
        runner.setProperty(DataTransformation.DELIMITER, "\u0001");

        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);
        runner.run();

        runner.assertPenalizeCount(0);
        runner.assertAllFlowFilesTransferred(DataTransformation.REL_SUCCESS, 1);

        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(EODProcessing.REL_SUCCESS);
        final String processingType = result.get(0).getAttribute(EODProcessing.PROCESSING_TYPE);
        assertTrue(processingType.contentEquals("incremental"));
    }

    @Test
    public void testIncrementalProcessingPipeDelimiter() throws InitializationException {

        final DateTime currentDate = DateTime.now(DateTimeZone.forID(attributes.get("timezone")));
        final String input = "ROWID|FNAME|vds|2017-01-11 23:15:00.123456|16588363205|A|SomeUser|2017-01-11 23:15:00|2017-01-12 23:11:53|2017-01-11 23:50:39|all\n";

        runner.removeProperty(DataTransformation.DELIMITER);
        runner.setProperty(DataTransformation.DELIMITER, "|");

        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);
        runner.run();

        runner.assertPenalizeCount(0);
        runner.assertAllFlowFilesTransferred(DataTransformation.REL_SUCCESS, 1);

        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(EODProcessing.REL_SUCCESS);
        final String processingType = result.get(0).getAttribute(EODProcessing.PROCESSING_TYPE);
        assertTrue(processingType.contentEquals("incremental"));
    }

    @Test
    public void testIncrementalProcessingCtrlADelimiter() throws InitializationException {

        final DateTime currentDate = DateTime.now(DateTimeZone.forID(attributes.get("timezone")));
        final String input = "ROWID\u0001FNAME\u0001vds\u00012017-01-11 23:15:00.123456\u000116588363205\u0001A\u0001SomeUser\u00012017-01-11 23:15:00\u00012017-01-12 23:11:53\u00012017-01-11 23:50:39\u0001all\n";

        runner.removeProperty(DataTransformation.DELIMITER);
        runner.setProperty(DataTransformation.DELIMITER, "\\u0001");

        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);
        runner.run();

        runner.assertPenalizeCount(0);
        runner.assertAllFlowFilesTransferred(DataTransformation.REL_SUCCESS, 1);

        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(EODProcessing.REL_SUCCESS);
        final String processingType = result.get(0).getAttribute(EODProcessing.PROCESSING_TYPE);
        assertTrue(processingType.contentEquals("incremental"));
    }

    private class MockLocalFileTransformationRegistry extends LocalFileTransformationRegistry {

        private ConfigurationContext mockConfigContext;

        public MockLocalFileTransformationRegistry() {
            Map<PropertyDescriptor, String> properties = new HashMap<>();
            properties.put(CONFIG_FILE_PATH, "abc_tables.xml");
            properties.put(RULE_FILE_PATH, "ColumnTransformationRules.xml");
            properties.put(TABLE_RENAME_FILE_PATH, "acb_ca_tables_rename.xml");
            properties.put(PARAM_FILE_PATH, "acb_ca_param.xml");

            mockConfigContext = new MockConfigurationContext(properties, null);
        }

        @OnEnabled
        public void enable(final ConfigurationContext configurationContext) throws InitializationException {
            this.conf = new Configuration(false);
            ClassLoader classLoader = getClass().getClassLoader();
            this.conf.addResource(classLoader.getResourceAsStream(
                    new File(configurationContext.getProperty(CONFIG_FILE_PATH).getValue()).getName()));
            this.conf.addResource(classLoader.getResourceAsStream(
                    new File(configurationContext.getProperty(PARAM_FILE_PATH).getValue()).getName()));
            this.conf.addResource(classLoader.getResourceAsStream(
                    new File(configurationContext.getProperty(RULE_FILE_PATH).getValue()).getName()));

            final String globalConfigPattern = DEFAULT_PARAM_PREFIX + ADD_PREFIX_DIFFEN_GLOBAL_CONFIG + "(.*)";
            Map<String, String> globalConfigs = conf.getValByRegex(globalConfigPattern);

            final String tablePattern = DEFAULT_PARAM_PREFIX + ADD_PREFIX_CONFIG_DIFFEN_TABLE + "(.*)"
                    + SUFFIX_CONFIG_COLSCHEMA;
            Map<String, String> configs = conf.getValByRegex(tablePattern);

            Map<String, String> tableConfigs = conf.getValByRegex(tablePattern);

            tableGeneralConfigs.putAll(
                    getTableRawSchema(globalConfigs, DEFAULT_PARAM_PREFIX + ADD_PREFIX_DIFFEN_GLOBAL_CONFIG, ""));
            tableRawSchema.putAll(getTableRawSchema(tableConfigs, DEFAULT_PARAM_PREFIX + ADD_PREFIX_CONFIG_DIFFEN_TABLE,
                    SUFFIX_CONFIG_COLSCHEMA));

            // Load up the columns per table
            for (final Map.Entry<String, String> tableEntry : configs.entrySet()) {

                final String fqtn = tableEntry.getKey();
                final String tableName = fqtn.substring(
                        (DEFAULT_PARAM_PREFIX + ADD_PREFIX_CONFIG_DIFFEN_TABLE).length(),
                        fqtn.length() - SUFFIX_CONFIG_COLSCHEMA.length());

                final VerifySchema tableSchema = new VerifySchema(tableEntry.getValue(), true);
                final List<VerifySchemaColumn> schemaColumns = tableSchema.getSchemaColumns();
                // build up the header that can be used to parse a row of data for this table.
                final String[] headerRow = new String[schemaColumns.size()];
                for (int i = 0; i < schemaColumns.size(); i++) {
                    final VerifySchemaColumn verifySchemaColumn = schemaColumns.get(i);
                    headerRow[i] = verifySchemaColumn.getName();
                }
                tableSchemaHeader.put(tableName, headerRow);
                tableSchemaColumns.put(tableName, schemaColumns);

            }
        }
    }

    private DistributedMapCacheClientImpl createClient() throws InitializationException {

        final DistributedMapCacheClientImpl client = new DistributedMapCacheClientImpl();
        final ComponentLog logger = new MockComponentLog("client", client);
        final MockControllerServiceInitializationContext clientInitContext = new MockControllerServiceInitializationContext(
                client, "client", logger, new MockStateManager(client));
        client.initialize(clientInitContext);

        return client;
    }

    static final class DistributedMapCacheClientImpl extends AbstractControllerService
            implements DistributedMapCacheClient {

        boolean exists = false;
        private Object cacheValue;

        @Override
        public void close() throws IOException {
        }

        @Override
        public void onPropertyModified(final PropertyDescriptor descriptor, final String oldValue,
                final String newValue) {
        }

        @Override
        public <K, V> boolean putIfAbsent(final K key, final V value, final Serializer<K> keySerializer,
                final Serializer<V> valueSerializer) throws IOException {
            if (exists) {
                return false;
            }

            cacheValue = value;
            exists = true;
            return true;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <K, V> V getAndPutIfAbsent(final K key, final V value, final Serializer<K> keySerializer,
                final Serializer<V> valueSerializer, final Deserializer<V> valueDeserializer) throws IOException {
            if (exists) {
                return (V) cacheValue;
            }
            cacheValue = value;
            exists = true;
            return null;
        }

        @Override
        public <K> boolean containsKey(final K key, final Serializer<K> keySerializer) throws IOException {
            return exists;
        }

        @Override
        public <K, V> V get(final K key, final Serializer<K> keySerializer, final Deserializer<V> valueDeserializer)
                throws IOException {
            if (exists) {
                return (V) cacheValue;
            } else {
                return null;
            }
        }

        @Override
        public <K> boolean remove(final K key, final Serializer<K> serializer) throws IOException {
            exists = false;
            return true;
        }

        @Override
        public long removeByPattern(String s) throws IOException {
            return 0;
        }

        @Override
        public <K, V> void put(final K key, final V value, final Serializer<K> keySerializer,
                final Serializer<V> valueSerializer) throws IOException {
            cacheValue = value;
            exists = true;
        }
    }
}