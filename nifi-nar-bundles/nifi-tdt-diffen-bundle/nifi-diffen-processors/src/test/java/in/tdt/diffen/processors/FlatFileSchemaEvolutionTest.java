package in.tdt.diffen.processors;

import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static in.tdt.diffen.processors.CDC2TableConfigProcessor.REL_FAILURE;
import static in.tdt.diffen.processors.CDC2TableConfigProcessor.REL_SUCCESS;
import static org.junit.Assert.assertEquals;

import static in.tdt.diffen.processors.SourceConfig2TableConfigProcessorWithMapping.*;

public class FlatFileSchemaEvolutionTest {
    @Test
    public void flatFileTest() {
        SourceConfig2TableConfigProcessorWithMapping processor =
                new SourceConfig2TableConfigProcessorWithMapping();
        TestRunner runner = TestRunners.newTestRunner(processor);
        Map<String, String> attributes = new HashMap<>();
        attributes.put("source", "diffen");
        attributes.put("country", "all");
        runner.setValidateExpressionUsage(false);
        runner.enqueue("Some content", attributes);
        runner.setProperty(SOURCE, "${source}");
        runner.setProperty(COUNTRY, "all");
        runner.setProperty(SOURCE_CONFIG_PATH, "src/test/resources/file_schema_evolution");
        runner.setProperty(DEFAULTS_MAPPING_FILE_PATH, "src/test/resources/file_schema_evolution/datamapping.txt");
        runner.setProperty(SOURCE_CONFIG_FILE_NAME_FILTER, ".*xml");
        runner.setProperty(SOURCE_CONFIG_TRANSFORM_CLASS_NAME, "in.tdt.diffen.processors.transform.FlatFileConfig2TableConfigTransformer");
        runner.setProperty(DATATYPE_MAPPING_FILE_PATH, "src/test/resources/file_schema_evolution/datamapping.txt");


        runner.run(1);
        runner.assertTransferCount(REL_SUCCESS, 2);
        runner.assertTransferCount(REL_FAILURE, 0);

        List<MockFlowFile> flowFiles = runner.getFlowFilesForRelationship(REL_SUCCESS);
        for (MockFlowFile flowFile : flowFiles) {
            String resultValue = new String(runner.getContentAsByteArray(flowFile));
            String expectedValue = "";
            System.out.println(resultValue);
//            assertEquals(resultValue, expectedValue);
        }
    }
}
