/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

public class RowIDAssignmentTest {

    @Test
    public void TestSpecialCharacterDelimiter(){
        final String input = "2016/11/14\u0002abcde\u0002oxoooo\u0002abc\u0002Anne\u000229568\u000282569\u0002abc1\u00022016/11/14\u00022016/11/14\u0002abcde\u00022016/11/14\u00022016/11/14\n" +
                             "2016/11/14\u0002abcde\u0002oxoooo\u0002abc\u0002Anne\u000280111\u000252966\u0002abc1\u00022016/11/14\u00022016/11/14\u0002abcde\u00022016/11/14\u00022016/11/14\n" +
                             "2016/11/14\u0002abcde\u0002oxoooo\u0002abc\u0002Anne\u00023844\u000262645\u0002abc1\u00022016/11/14\u00022016/11/14\u0002abcde\u00022016/11/14\u00022016/11/14\n" +
                             "2016/11/14\u0002abcde\u0002oxoooo\u0002abc\u0002Anne\u000278991\u000262974\u0002abc1\u00022016/11/14\u00022016/11/14\u0002abcde\u00022016/11/14\u00022016/11/14\n" +
                             "2016/11/14\u0002abcde\u0002oxoooo\u0002abc\u0002Anne\u000223773\u000217965\u0002abc1\u00022016/11/14\u00022016/11/14\u0002abcde\u00022016/11/14\u00022016/11/14\n" +
                             "2016/11/14\u0002abcde\u0002oxoooo\u0002abc\u0002Anne\u000290656\u000217434\u0002abc1\u00022016/11/14\u00022016/11/14\u0002abcde\u00022016/11/14\u00022016/11/14";

        final TestRunner runner = TestRunners.newTestRunner(new RowIDAssignment());
        runner.setProperty(RowIDAssignment.INPUT_DELIMITER, Character.toString((char) 2));

        runner.enqueue(input.getBytes(StandardCharsets.UTF_8));

        runner.run();

        runner.assertTransferCount(RowIDAssignment.REL_SUCCESS, 1);
        final MockFlowFile successFlow = runner.getFlowFilesForRelationship(RowIDAssignment.REL_SUCCESS).get(0);

        final long totalRowCount = Long.parseLong(successFlow.getAttribute(RowIDAssignment.TOTAL_ROW_COUNT));
        assert(6 == totalRowCount);


    }

    @Test
    public void TestPipeDelimiter(){
        final String input = "2016/11/14|abcde|oxoooo|abc|Anne|29568|82569|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14\n" +
                "2016/11/14|abcde|oxoooo|abc|Anne|80111|52966|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14\n" +
                "2016/11/14|abcde|oxoooo|abc|Anne|3844|62645|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14\n" +
                "2016/11/14|abcde|oxoooo|abc|Anne|78991|62974|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14\n" +
                "2016/11/14|abcde|oxoooo|abc|Anne|23773|17965|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14\n" +
                "2016/11/14|abcde|oxoooo|abc|Anne|90656|17434|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14";

        final TestRunner runner = TestRunners.newTestRunner(new RowIDAssignment());
        runner.setProperty(RowIDAssignment.INPUT_DELIMITER, "|");

        runner.enqueue(input.getBytes(StandardCharsets.UTF_8));

        runner.run();

        runner.assertTransferCount(RowIDAssignment.REL_SUCCESS, 1);
        final MockFlowFile successFlow = runner.getFlowFilesForRelationship(RowIDAssignment.REL_SUCCESS).get(0);

        final long totalRowCount = Long.parseLong(successFlow.getAttribute(RowIDAssignment.TOTAL_ROW_COUNT));
        assert(6 == totalRowCount);


    }

}