package in.tdt.diffen.processors;

import in.tdt.diffen.services.LocalFileTransformationRegistry;
import in.tdt.diffen.verifytypes.checker.VerifySchema;
import in.tdt.diffen.verifytypes.checker.VerifySchemaColumn;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.AbstractControllerService;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.distributed.cache.client.Deserializer;
import org.apache.nifi.distributed.cache.client.DistributedMapCacheClient;
import org.apache.nifi.distributed.cache.client.Serializer;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.reporting.InitializationException;
import org.apache.nifi.state.MockStateManager;
import org.apache.nifi.util.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static in.tdt.diffen.verifytypes.DiffenConstants.*;

public class DataTypeVerificationProcessorTest {

    public static final String ABC_EOD_MARKER = "abc_EOD_MARKER";
    public static final String ABC_EOD_MARKER_VALUE = "2017-03-02 17:00:32";
    //JTS before EOD marker - this would be the late arrivals - since EOD file has arrived and the time has been logged but we have a file with JTS with time in the past
    public static final String JTS_BEFORE_ABC_EOD_MARKER_VALUE = "2017-03-02 17:00:00";
    public static final String JTS_AFTER_ABC_EOD_MARKER_VALUE = "2017-03-02 17:00:42"; //JTS after EOD marker
    public static final String lineSeparator = java.security.AccessController.doPrivileged(
            new sun.security.action.GetPropertyAction("line.separator"));
    private MockLocalFileTransformationRegistry service = new MockLocalFileTransformationRegistry();
    private TestRunner runner;
    Map<String, String> attributes = new HashMap<>();
    final DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZoneUTC();


    @Before
    public void initialize() throws InitializationException {

        runner = TestRunners.newTestRunner(DataTypeVerification.class);
        Map<String, String> properties = new HashMap<>();

        properties.put("Rule File Path", "src/test/resources/ColumnTransformationRules.xml");
        properties.put("Configuration File Path", "src/test/resources/abc_tables.xml");
        properties.put("Table rename file Path", "src/test/resources/acb_ca_tables_rename.xml");
        properties.put("Parameter file Path", "src/test/resources/acb_ca_param.xml");
        properties.put("Configuration Registry Service", "configuration-registry-service");

        runner.setValidateExpressionUsage(true);
        runner.addControllerService("configuration-registry-service", service, properties);
        runner.enableControllerService(service);
        runner.setProperty(DataTypeVerification.REGISTRY_SERVICE, "configuration-registry-service");
        final Map<String, String> clientProperties = new HashMap<>();
        runner.setProperty(DataTypeVerification.DELIMITER, ",");
        runner.assertValid();

        attributes.clear();
        attributes.put("source", "abc");
        attributes.put("country", "all");
        attributes.put("tablename","abc_all_tl_eod_marker");
        attributes.put("source","ABC");
        attributes.put("date","2017_01_02");
        attributes.put("time","13:00:12");
        attributes.put("row.metadata.added","true");
        attributes.put("has.header", "false");
        attributes.put("timezone", "Asia/Kolkata");
    }

    @After
    public void shutdown() throws Exception {
        runner.shutdown();
        service.close();
    }

    @Test
    public void testErrorTypes() throws InitializationException{

        final DateTime currentDate = DateTime.now(DateTimeZone.UTC);
        final String time = formatter.print(currentDate);

        final String sampleRow = "ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,ABCDEFGH,2017-03-25,2017-04-25,SG";
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

		runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
		runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 1);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_ERROR_TYPE_FAILURE);
        result.get(0).assertContentEquals("ABC,all,abc_all_tl_eod_marker,ROWID,ABCDEFGH(ROWIDFNAMEvds2017-03-02 17:00:42TrxIDOpTypeUserIDABCDEFGH2017-03-252017-04-25SG),ERROR: 'ABCDEFGH' for column 'EOD_DATE' not of type DATE, type enforced='null',"+time+lineSeparator, StandardCharsets.UTF_8);

    }

    @Test
    public void testSuccessTypes() throws InitializationException{

        //distributedMapCacheClient.put(ABC_EOD_MARKER, ABC_EOD_MARKER_VALUE, stringSerializer, stringSerializer);
        final String sampleRow = "ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,2017-03-25,2017-03-25,2017-04-25,SG";
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 1);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_SUCCESS);
        result.get(0).assertContentEquals("ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,2017-03-25,2017-03-25,2017-04-25,SG" + lineSeparator, StandardCharsets.UTF_8);

    }

    @Test
    public void testSuccessTypesPipeDelimiter() throws InitializationException{

        runner.setProperty(DataTypeVerification.DELIMITER, "|");
        final String sampleRow = "ROWID|FNAME|vds|"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +"|TrxID|OpType|UserID|2017-03-25|2017-03-25|2017-04-25|SG";
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 1);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_SUCCESS);
        result.get(0).assertContentEquals("ROWID|FNAME|vds|"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +"|TrxID|OpType|UserID|2017-03-25|2017-03-25|2017-04-25|SG" + lineSeparator, StandardCharsets.UTF_8);

    }

    @Test
    public void testSchemaRecordMismatch() throws InitializationException{

        final DateTime currentDate = DateTime.now(DateTimeZone.UTC);
        final String time = formatter.print(currentDate);

        final String sampleRow = "ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,2017-03-25,2017-03-25,2017-04-25,SG,EXTRA_COLUMN";
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 1);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_ERROR_TYPE_FAILURE);
        result.get(0).assertContentEquals("ABC,all,abc_all_tl_eod_marker,ROWID,,ERROR : Unexpected number of columns. Expected: 4; received: 5," + time + lineSeparator, StandardCharsets.UTF_8);

    }

    @Test
    public void testProcessZeroRecords() throws InitializationException{

        final String sampleRow = "";
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 0);
    }

    @Test
    public void testSuccessWithMaker() throws IOException {

        StringSerializer stringSerializer = new StringSerializer();

        final String sampleRow = "ROWID\u0001FNAME\u0001vds\u0001" + JTS_AFTER_ABC_EOD_MARKER_VALUE +"\u0001TrxID\u0001OpType\u0001UserID\u00012017-03-25\u00012017-03-25\u00012017-04-25\u0001SG";
        runner.removeProperty(DataTransformation.DELIMITER);
        runner.setProperty(DataTransformation.DELIMITER, "\u0001");
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 1);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_SUCCESS);
        result.get(0).assertContentEquals(sampleRow + lineSeparator, StandardCharsets.UTF_8);
    }

    @Test
    public void testJournalTime() throws InitializationException{

        final DateTime currentDate = DateTime.now(DateTimeZone.UTC);
        final String time = formatter.print(currentDate);

        final String sampleRow = "ROWID,FNAME,vds,"+ "2017-03-02 17:00:42.00000" +",TrxID,OpType,UserID,2017-03-25,2017-03-25,2017-04-25,SG";

        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 1);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_ERROR_TYPE_FAILURE);
        result.get(0).assertContentEquals("ABC,all,abc_all_tl_eod_marker,ROWID,2017-03-02 17:00:42.00000,ERROR: INCORRECT JOURNAL TIMESTAMP FORMAT," + time + lineSeparator, StandardCharsets.UTF_8);
    }


    @Test(expected = AssertionError.class)
    public void testMissingIncomingAttribute() throws InitializationException {

        final String sampleRow = "";
        attributes.clear();
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();
    }


    @Test
    public void testNumericTypesWithPlusSign() throws InitializationException{
        final String sampleRow = "ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,+.000,+.000";
        attributes.clear();
        attributes.put("source", "abc");
        attributes.put("country", "all");
        attributes.put("tablename","abc_all_tl_dummy_table");
        attributes.put("date","2017_01_02");
        attributes.put("time","13:00:12");
        attributes.put("row.metadata.added","true");
        attributes.put("has.header", "false");
        attributes.put("timezone", "Asia/Kolkata");
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 1);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_SUCCESS);
        result.get(0).assertContentEquals("ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,+.000,0.000" + lineSeparator, StandardCharsets.UTF_8);

    }

    @Test
    public void testNumericTypesWithMinusSign() throws InitializationException{
        final String sampleRow = "ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,+.000,-.001";
        attributes.clear();
        attributes.put("source", "abc");
        attributes.put("country", "all");
        attributes.put("tablename","abc_all_tl_dummy_table");
        attributes.put("date","2017_01_02");
        attributes.put("time","13:00:12");
        attributes.put("row.metadata.added","true");
        attributes.put("has.header", "false");
        attributes.put("timezone", "Asia/Kolkata");
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 1);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_SUCCESS);
        result.get(0).assertContentEquals("ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,+.000,-0.001" + lineSeparator, StandardCharsets.UTF_8);

    }

    @Test
    public void testNumericTypesWithUnsignedValue() throws InitializationException{
        final String sampleRow = "ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,+.000,0.0";
        attributes.clear();
        attributes.put("source", "abc");
        attributes.put("country", "all");
        attributes.put("tablename","abc_all_tl_dummy_table");
        attributes.put("date","2017_01_02");
        attributes.put("time","13:00:12");
        attributes.put("row.metadata.added","true");
        attributes.put("has.header", "false");
        attributes.put("timezone", "Asia/Kolkata");
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 1);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_SUCCESS);
        result.get(0).assertContentEquals("ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,+.000,0.0" + lineSeparator, StandardCharsets.UTF_8);

    }


    @Test
    public void testInvalidForInvalidBigDecimal() throws InitializationException{

        final String time = formatter.print(DateTime.now(DateTimeZone.UTC));
        final String sampleRow = "ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,+.000,0-52";
        attributes.clear();
        attributes.put("source", "abc");
        attributes.put("country", "all");
        attributes.put("tablename","abc_all_tl_dummy_table");
        attributes.put("date","2017_01_02");
        attributes.put("time","13:00:12");
        attributes.put("row.metadata.added","true");
        attributes.put("has.header", "false");
        attributes.put("timezone", "Asia/Kolkata");
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 1);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_ERROR_TYPE_FAILURE);
        result.get(0).assertContentEquals("abc,all,abc_all_tl_dummy_table,ROWID,0-52(ROWID\u0002FNAME\u0002vds\u00022017-03-02 17:00:42\u0002TrxID\u0002OpType\u0002UserID\u0002+.000\u00020-52),ERROR: 0-52 is not a valid decimal for column DEC1.,"+time  + lineSeparator, StandardCharsets.UTF_8);

    }

    @Test
    public void testValidForValidBigDecimalLength() throws InitializationException{

        final String time = formatter.print(DateTime.now(DateTimeZone.UTC));
        final String sampleRow = "ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,+.000,5000";
        attributes.clear();
        attributes.put("source", "abc");
        attributes.put("country", "all");
        attributes.put("tablename","abc_all_tl_dummy_table2");
        attributes.put("date","2017_01_02");
        attributes.put("time","13:00:12");
        attributes.put("row.metadata.added","true");
        attributes.put("has.header", "false");
        attributes.put("timezone", "Asia/Kolkata");
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 1);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_ERROR_TYPE_FAILURE);
    }

    @Test
    public void testValidForColumnsOfDifferentOrder() throws InitializationException{

        final String time = formatter.print(DateTime.now(DateTimeZone.UTC));
        final String sampleRow =
                "ROWID,FILENAME,vds,c_journaltime,c_transactionid,c_operationtype,c_userid,STRING,DBL1,DEC1"
                + System.lineSeparator()
                        + "ROWID,FNAME,2017_01_02,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,abc,+.000,5000"     + System.lineSeparator()
                        + "ROWID,FNAME,2017_01_02,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,abc2,+.001,6000"
                ;
        attributes.clear();
        attributes.put("source", "abc");
        attributes.put("country", "all");
        attributes.put("tablename","abc_all_tl_dummy_table3");
        attributes.put("date","2017_01_02");
        attributes.put("time","13:00:12");
        attributes.put("row.metadata.added","true");
        attributes.put("has.header", "true");
        attributes.put("storage.partition.name", "vds");
        attributes.put("timezone", "Asia/Kolkata");
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 1);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
    }

    @Test
    public void testInvalidForInvalidBigDecimalLength() throws InitializationException{

        final String time = formatter.print(DateTime.now(DateTimeZone.UTC));
        final String sampleRow = "ROWID,FNAME,vds,"+ JTS_AFTER_ABC_EOD_MARKER_VALUE +",TrxID,OpType,UserID,+.000,500000";
        attributes.clear();
        attributes.put("source", "abc");
        attributes.put("country", "all");
        attributes.put("tablename","abc_all_tl_dummy_table2");
        attributes.put("date","2017_01_02");
        attributes.put("time","13:00:12");
        attributes.put("row.metadata.added","true");
        attributes.put("has.header", "false");
        attributes.put("timezone", "Asia/Kolkata");
        runner.enqueue(sampleRow.getBytes(StandardCharsets.UTF_8),attributes);
        runner.run();

        runner.assertTransferCount(DataTypeVerification.REL_SUCCESS, 0);
        runner.assertTransferCount(DataTypeVerification.REL_WARN_TYPE_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_FAILURE, 0);
        runner.assertTransferCount(DataTypeVerification.REL_ERROR_TYPE_FAILURE, 1);
        runner.assertTransferCount(DataTypeVerification.REL_ROW_COUNTS, 1);
        final List<MockFlowFile> result = runner.getFlowFilesForRelationship(DataTypeVerification.REL_ERROR_TYPE_FAILURE);
    }


    protected static class MockLocalFileTransformationRegistry extends LocalFileTransformationRegistry {

        private ConfigurationContext mockConfigContext;

        public MockLocalFileTransformationRegistry() {
            Map<PropertyDescriptor, String> properties = new HashMap<>();
            PropertyDescriptor fileConfig = new PropertyDescriptor.Builder()
                    .name("Configuration File Path")
                    .dynamic(false)
                    .build();

            PropertyDescriptor ruleFileConfig = new PropertyDescriptor.Builder()
                    .name("Rule File Path")
                    .dynamic(false)
                    .build();

            properties.put(ruleFileConfig, "ColumnTransformationRules.xml");
            properties.put(fileConfig, "abc_tables.xml");
            mockConfigContext = new MockConfigurationContext(properties, null);
        }


        @OnEnabled
        public void enable(final ConfigurationContext configurationContext) throws InitializationException {
            this.conf = new Configuration(false);
            this.conf.addResource(new Path(configurationContext.getProperty(CONFIG_FILE_PATH).getValue()));
            this.conf.addResource(new Path(configurationContext.getProperty(RULE_FILE_PATH).getValue()));
            if (configurationContext.getProperty(TABLE_RENAME_FILE_PATH) != null && StringUtils.isNotEmpty(configurationContext.getProperty(TABLE_RENAME_FILE_PATH).getValue())) {
                this.conf.addResource(new Path(configurationContext.getProperty(TABLE_RENAME_FILE_PATH).getValue()));
            }

            final String globalConfigPattern = DEFAULT_PARAM_PREFIX + ADD_PREFIX_DIFFEN_GLOBAL_CONFIG + "(.*)";
            final String tablePattern = DEFAULT_PARAM_PREFIX + ADD_PREFIX_CONFIG_DIFFEN_TABLE + "(.*)" + SUFFIX_CONFIG_COLSCHEMA;
            final String renameTableConfigPattern = DEFAULT_PARAM_PREFIX + ADD_PREFIX_DIFFEN_RENAME_TABLE + "(.*)";

            Map<String, String> globalConfigs = conf.getValByRegex(globalConfigPattern);
            Map<String, String> tableConfigs = conf.getValByRegex(tablePattern);
            Map<String, String> renameTableConfigs = conf.getValByRegex(renameTableConfigPattern);

            tableGeneralConfigs.putAll(getTableRawSchema(globalConfigs, DEFAULT_PARAM_PREFIX + ADD_PREFIX_DIFFEN_GLOBAL_CONFIG, ""));
            tableRawSchema.putAll(getTableRawSchema(tableConfigs, DEFAULT_PARAM_PREFIX + ADD_PREFIX_CONFIG_DIFFEN_TABLE, SUFFIX_CONFIG_COLSCHEMA));
            tableRenameConfigs.putAll(getTableRawSchema(renameTableConfigs, DEFAULT_PARAM_PREFIX + ADD_PREFIX_DIFFEN_RENAME_TABLE, ""));

            //Load up the columns per table
            for (final Map.Entry<String, String> tableEntry : tableConfigs.entrySet()) {

                final String fqtn = tableEntry.getKey();
                final String tableName = fqtn.substring((DEFAULT_PARAM_PREFIX + ADD_PREFIX_CONFIG_DIFFEN_TABLE).length(),
                        fqtn.length() - SUFFIX_CONFIG_COLSCHEMA.length());

                final VerifySchema tableSchema = new VerifySchema(tableEntry.getValue(), true);
                final List<VerifySchemaColumn> schemaColumns = tableSchema.getSchemaColumns();
                //build up the header that can be used to parse a row of data for this table.
                final String[] headerRow = new String[schemaColumns.size()];
                for (int i = 0; i < schemaColumns.size(); i++) {
                    final VerifySchemaColumn verifySchemaColumn = schemaColumns.get(i);
                    headerRow[i] = verifySchemaColumn.getName();
                }

                tableSchemaHeader.put(tableName.toLowerCase(), headerRow);
                tableSchemaColumns.put(tableName.toLowerCase(), schemaColumns);
            }
        }

    }
    private static class StringSerializer implements Serializer<String> {

        @Override
        public void serialize(final String value, final OutputStream output) throws SerializationException, IOException {
            output.write(value.getBytes(StandardCharsets.UTF_8));
        }
    }
}