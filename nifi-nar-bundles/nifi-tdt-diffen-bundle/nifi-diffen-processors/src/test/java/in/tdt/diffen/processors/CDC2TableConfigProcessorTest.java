/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.processors;

import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static in.tdt.diffen.processors.CDC2TableConfigProcessor.*;
import static org.junit.Assert.*;

public class CDC2TableConfigProcessorTest {
    //@Test
    public void constructTableConfigXMLTest() {
        try {
            CDC2TableConfigProcessor processor = new CDC2TableConfigProcessor();
            TestRunner runner = TestRunners.newTestRunner(processor);

           /* runner.setProperty(CDC_XML_PATH, Resources.getResource("sample_cdc.xml").getPath());
            runner.setProperty(SOURCE_TYPE_PATH,Resources.getResource("sourcetype.list").getPath());
            runner.setProperty(KEY_COLUMN_LIST_PATH,Resources.getResource("keycol.list").getPath());*/

            runner.setProperty(CDC_XML_PATH, "C:\\Temp\\dotopal\\OPLAES1_OPAL_AE_76_out_of_76_tables_20180401.xml");
            runner.setProperty(SOURCE_TYPE_PATH, "C:\\Temp\\dotopal\\dotopal_ae_sourcetype.list");
            runner.setProperty(KEY_COLUMN_LIST_PATH, "C:\\Temp\\dotopal\\dotopal_ae_keycols.list");

            Map<String, String> attributes = new HashMap<>();
            attributes.put("source", "dotopal");
            attributes.put("country", "ae");
            //runner.setProperty(TransformationRules, "") //currently set as optional because the NiFi processors doesn't do any data transformation.

            runner.setValidateExpressionUsage(false);
            runner.enqueue("Some content", attributes);
            runner.run(1);
            runner.assertTransferCount(REL_SUCCESS, 1);
            runner.assertTransferCount(REL_FAILURE, 0);


            List<MockFlowFile> flowFiles = runner.getFlowFilesForRelationship(REL_SUCCESS);
            assertEquals(1, flowFiles.size());
            for (MockFlowFile flowFile : flowFiles) {
                String resultValue = new String(runner.getContentAsByteArray(flowFile));
                String expectedValue = "<configuration>\n" +
                        "<property>\n" +
                        "     <name>diffen.table.DS_BANK_REF.col_schema</name>\n" +
                        "     <value>BANKCODE VARCHAR(11) NOT NULL^BNKCTRYCODE VARCHAR(2) NOT NULL^BNKDES VARCHAR(105) NOT NULL</value>\n" +
                        "</property>\n" +
                        "<property>\n" +
                        "     <name>diffen.table.DS_BANK_REF.sourcetype</name>\n" +
                        "     <value>delta</value>\n" +
                        "</property>\n" +
                        "<property>\n" +
                        "     <name>diffen.table.DS_BANK_REF.keycols</name>\n" +
                        "     <value>BANKCODE</value>\n" +
                        "</property>\n" +
                        "</configuration>";


                System.out.println("Result XML ######################### \n" + resultValue);
                //System.out.println("Expected XML ######################### \n" + expectedValue);

                Diff myDiff = DiffBuilder.compare(Input.fromString(expectedValue)).withTest(Input.fromString(resultValue))
                        .checkForSimilar()
                        .ignoreWhitespace()
                        .build();

                assertFalse("XML similar " + myDiff.toString(), myDiff.hasDifferences());
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }


    //@Test
    public void testSomething() {
        CDC2TableConfigProcessor processor = new CDC2TableConfigProcessor();
        TestRunner runner = TestRunners.newTestRunner(processor);

        runner.setProperty(CDC_XML_PATH, "C:\\Temp\\dotopal");
        runner.setProperty(SOURCE_TYPE_PATH, "C:\\Temp\\dotopal\\type_detica_sr_test.txt");
        runner.setProperty(KEY_COLUMN_LIST_PATH, "C:\\Temp\\dotopal\\primary_keys_detica_sr_test.txt");

        Map<String, String> attributes = new HashMap<>();
        attributes.put("source", "dtcts");
        attributes.put("country", "sr");

        //runner.setProperty(TransformationRules, "") //currently set as optional because the NiFi processors doesn't do any data transformation.

        runner.setValidateExpressionUsage(false);
        runner.enqueue("Some content", attributes);
        runner.run(1);
        runner.assertTransferCount(REL_SUCCESS, 1);
        runner.assertTransferCount(REL_FAILURE, 0);

    }
}
