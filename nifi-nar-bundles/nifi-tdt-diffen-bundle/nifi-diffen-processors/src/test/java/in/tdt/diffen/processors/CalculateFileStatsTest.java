package in.tdt.diffen.processors;

import org.apache.nifi.reporting.InitializationException;
import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static in.tdt.diffen.processors.CalculateFileStats.REL_SUCCESS;
import static in.tdt.diffen.processors.NifiAttributesConstants.*;

public class CalculateFileStatsTest {

    Map<String, String> attributes = new HashMap<>();
    private TestRunner runner;

    @Before
    public void initialize() {
        runner = TestRunners.newTestRunner(CalculateFileStats.class);
        attributes.put("col_schema", "{\"name\":\"diffen_all_txn_2\",\"type\":\"record\",\"fields\":[{\"name\":\"id\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"message\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"value\",\"type\":[\"null\",\"string\"],\"default\":null}]}");
        attributes.put(EXPECTED_COLUMN_COUNT_ATTRIBUTE, "3");
    }

    @Test
    public void testSuccessScenario() throws InitializationException {
        runner.assertValid();
        runner.setProperty("CSV Format", "rfc-4180");
        runner.enqueue("\"id-1\", \"m-1\", \"v-1\"\n", attributes);
        runner.run();

        runner.assertTransferCount(REL_SUCCESS, 1);

        final MockFlowFile out = runner.getFlowFilesForRelationship(REL_SUCCESS).get(0);

        out.assertAttributeEquals(ACTUAL_ROW_COUNT_ATTRIBUTE, "1");
        out.assertAttributeEquals(ACTUAL_COLUMN_COUNT_ATTRIBUTE, "3");
    }

    @Test
    public void testFailureScenario() throws InitializationException {
        runner.assertValid();
        runner.setProperty("CSV Format", "rfc-4180");
        runner.enqueue("\"id-1\"\n\"id-1\",\"m-1\",\"v-1\"", attributes);
        runner.run();

        runner.assertTransferCount(REL_SUCCESS, 1);

        final MockFlowFile out = runner.getFlowFilesForRelationship(REL_SUCCESS).get(0);

        out.assertAttributeEquals(ACTUAL_ROW_COUNT_ATTRIBUTE, "2");
        out.assertAttributeEquals(ACTUAL_COLUMN_COUNT_ATTRIBUTE, "1");
    }

    @Test
    public void testSkipRecordScenario() throws InitializationException {
        runner.assertValid();
        runner.setProperty("CSV Format", "rfc-4180");
        runner.enqueue("id-1,m-1,B\nid-1,m-1,I", attributes);
        runner.run();

        runner.assertTransferCount(REL_SUCCESS, 1);

        final MockFlowFile out = runner.getFlowFilesForRelationship(REL_SUCCESS).get(0);

        out.assertAttributeEquals(ACTUAL_ROW_COUNT_ATTRIBUTE, "1");
        out.assertAttributeEquals(B_ROW_COUNT_ATTRIBUTE, "1");
        out.assertAttributeEquals(ACTUAL_COLUMN_COUNT_ATTRIBUTE, "3");
    }

    @After
    public void shutdown() throws Exception {
        runner.shutdown();
    }
}
