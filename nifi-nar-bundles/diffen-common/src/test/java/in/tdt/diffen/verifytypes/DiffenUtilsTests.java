package in.tdt.diffen.verifytypes;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DiffenUtilsTests {

    @Test
    public void testUnicodePipeDelimiter() {
        assertEquals('|', DiffenUtils.getDelimiter("\\u007C"));
    }

    @Test
    public void testStringPipeDelimiter() {
        assertEquals('|', DiffenUtils.getDelimiter("|"));
    }

    @Test
    public void testCommaDelimiter() {
        assertEquals(',', DiffenUtils.getDelimiter(","));
    }
}
