/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.verifytypes;

/** Common constants **/
public class DiffenConstants {

    public static final String PROP_LINE_SEPARATOR = "\\^";
    public static final String DEFAULT_PARAM_PREFIX = "diffen";
    public static final String ADD_PREFIX_CONFIG_DIFFEN_TABLE = ".table.";
    public static final String ADD_PREFIX_DIFFEN_GLOBAL_CONFIG = ".config.";
    public static final String ADD_PREFIX_DIFFEN_RENAME_TABLE = ".rename.";
    public static final String ADD_PREFIX_DIFFEN_RULES = ".rules.";
    public static final String PARAMS = ".params";
    public static final String CODE = ".code";
    public static final String SUFFIX_CONFIG_COLSCHEMA = ".col_schema";
    public static final String STORAGE_PARTITION_NAME = "storage.partition.name";
    public static final String SOURCE = "source";
    public static final String COUNTRY = "country";
    public static final String HDFS_PARENT_DIRECTORY = "hdfs.data.parent.dir";
    public static final String VERIFY_TYPES_SCHEMA = "storage.schema";
    public static final String OPS_SCHEMA_NAME = "ops.schema";
    public static final String DIFFEN_JOB_TRIGGER_FILE_PATTERN = "diffen.job.trigger.file.pattern";
    public static final String INVALID_TYPE_COLSCHEMA = "invalidtypes.schema";
    public static final String WARN_TYPE_COLSCHEMA = "warntypes.schema";
    public static final String ROWHISTORY_COLSCHEMA = "rowhistory.schema";
    public static final String SOURCING_TYPE = "sourcing.type";
    public static final String SUFFIX_CONFIG_KEYCOLS = ".keycols";
    public static final String SOURCING_TYPE_CDC = "CDC";
    public static final String SOURCING_TYPE_BATCH_DELIMITED = "BATCH_DELIMITED";
    public static final String SOURCING_TYPE_BATCH_FIXEDWIDTH = "BATCH_FIXEDWIDTH";
    public static final String FILE_DELIMITER = "filedelimiter";
    public static final String HEADER_COLSCHEMA_SUFFIX = ".headercolschema";
    public static final String TRAILER_COLSCHEMA_SUFFIX = ".trailercolschema";
    public static final String DATA_INDICATOR = ".dataindicator";
    public static final String HEADER_INDICATOR = ".headerindicator";
    public static final String TRAILER_INDICATOR = ".trailerindicator";
    public static final String CONTROL_INDICATOR = "control_file";
    public static final String MARKERTIME_COLUMN = "markertime.column";
    public static final String EODDATE_COLUMN = "eoddate.column";
    public static final String JOURNALTIME_COLUMN = "journaltime.column";
    public static final String USERID_COLUMN = "userid.column";
    public static final String TIMESTAMP_COL_FORMAT = "timestampcolformat";
    public static final String TABLE_NAME_ATTRIBUTE = "tablename";
    public static final String RETAIN_INDICATOR_IN_DATA = "retain.indicator.in.data";
    public static final String SUFFIX_CONFIG_RUNTYPE = ".runtype";
    public static final String SUFFIX_CONFIG_SOURCETYPE = ".sourcetype";
    public static final String SUFFIX_CONFIG_RECONQUERY = ".query";
    public static final String SUFFIX_CONFIG_AFTERVALUE = ".aftervalue";
    public static final String SUFFIX_CONFIG_BEFOREVALUE = ".beforevalue";
    public static final String SUFFIX_CONFIG_INSERTVALUE = ".insertvalue";
    public static final String SUFFIX_CONFIG_DELETEVALUE = ".deletevalue";
    public static final String CONTROL_FILE_TABLENAME_INDEX = ".controlfile.tablenameindex";
    public static final String CONTROL_FILE_TABLENAME = ".controlfile.tablename";
    public static final String RECON_FILE_RENAME = ".reconfilerename";
    public static final String EXCEPTIONAL_TABLES_LIST = "exceptional.table.list";
    public static final String INCOMING_PATH = "absolute.path";
    public static final String FLOW_FILE_NAME = "filename";
    public static final String TIME_ZONE = "timezone";
}