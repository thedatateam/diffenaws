/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.verifytypes.checker;

import in.tdt.diffen.verifytypes.DiffenConstants;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyObjectInspectorParameters;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyObjectInspectorParametersImpl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Java representation of a table schema. it wraps a collection of {@link VerifySchemaColumn} that represents each
 * column of the table and the validation attached to it.
 */
public class VerifySchema {

    private final List<VerifySchemaColumn> schemaColumns = new ArrayList<VerifySchemaColumn>();

    private final DecimalFormat dfInput = new DecimalFormat();

    private final DecimalFormat dfInputWithPostive = new DecimalFormat();

    private final DecimalFormat dfOutput = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

    public VerifySchema(String colSchema, boolean includeDropColumns, LazyObjectInspectorParameters lazyObjectInspectorParameters) {

        dfInput.setMaximumFractionDigits(Integer.MAX_VALUE);
        dfInput.setMinimumFractionDigits(1);
        dfInput.setParseBigDecimal(true);

        dfInputWithPostive.setPositivePrefix("+");
        if (colSchema == null) {
            throw new RuntimeException("Null column schema");
        }

        String cols[] = colSchema.split(DiffenConstants.PROP_LINE_SEPARATOR);
        for (String col : cols) {
            VerifySchemaColumn column = new VerifySchemaColumn(col, lazyObjectInspectorParameters);
            if (includeDropColumns || !column.isDrop()) {
                schemaColumns.add(column);
            }
        }
        dfOutput.setMaximumFractionDigits(340);
    }

    public VerifySchema(String colSchema, boolean includeDropColumns) {
        this(colSchema, includeDropColumns, new LazyObjectInspectorParametersImpl());
    }

    public List<VerifySchemaColumn> getSchemaColumns() {
        return schemaColumns;
    }
}
