/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.cdc;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import java.util.List;
import java.util.Map;

/**
 * Parses CDC XML and generates table-configs
 */
public class CDCXmlParser {

    public static String getColumn(Node sourceCol, List<CDCModels.ColTransformationRule> rulesList, Map<String, CDCModels.TableDataTypeMapping> dataTypeMap, boolean isSelectAttributeAvailable) {
        String columnName = getXmlAttribute(sourceCol, "columnName");
        String dataType = getXmlAttribute(sourceCol, "dataType");
        boolean selected = resolveBoolean((isSelectAttributeAvailable) ? getXmlAttribute(sourceCol, "selected") : "true");

        boolean nullable = resolveBoolean(getXmlAttribute(sourceCol, "nullable"));
        String length = getXmlAttribute(sourceCol, "length");
        String scale = getXmlAttribute(sourceCol, "scale");

        String hiveDataType = "";
        if (!dataTypeMap.containsKey(dataType)) {
            hiveDataType = getHiveDataType(dataType, length, scale);
        } else if (dataTypeMap.get(dataType).isPrecisionType()) {
            String avroDataType = dataTypeMap.get(dataType).getAvroType();
            hiveDataType = getDecimalType(avroDataType, length, scale);
        } else {
            hiveDataType = dataTypeMap.get(dataType).getAvroType();
        }

        String rules = rulesList.isEmpty() ? "" : rulesList.stream().filter(eachRule -> eachRule.getColumnName().equals(columnName)).findFirst().get().getColumnName();

        if ((columnName.equalsIgnoreCase("filename") ||
                columnName.equalsIgnoreCase("rowid") ||
                columnName.equalsIgnoreCase("s_startdt") ||
                columnName.equalsIgnoreCase("s_starttime") ||
                columnName.equalsIgnoreCase("s_enddt") ||
                columnName.equalsIgnoreCase("s_endtime") ||
                columnName.equalsIgnoreCase("s_deleted_flag")) && selected) {
            return columnName + "_1" + " " + hiveDataType + (nullable ? "" : " NOT NULL") + rules;
        } else if (selected) {
            return columnName + " " + hiveDataType + (nullable ? "" : " NOT NULL") + rules;
        } else {
            return null;
        }
    }

    public static boolean resolveBoolean(String input) {
        switch(input) {
            case "true":
            case "TRUE":
            case "Y":
            case "y":
            case "yes":
            case "YES":return true;
            default:return false;
        }
    }

    public static String getXmlAttribute(Node sourceCol, String attributeName) {
        return (sourceCol.getAttributes().getNamedItem(attributeName) == null)? null : sourceCol.getAttributes().getNamedItem(attributeName).getNodeValue();
    }

    public static String getDecimalType(String targetType, String length, String scale) {
        scale = StringUtils.isBlank(scale) ? "0" : scale;
        if (StringUtils.equalsIgnoreCase(targetType, "DECIMAL")) {
            return "DECIMAL(" + length + "," + scale + ")";
        } else {
            return "PRECISION_TYPE_NOT_DECIMAL";
        }
    }

    /**
     * Maps Database types to Hive types
     *
     * @param dataType Database datatype
     * @param length   scale for Decimal types
     * @param scale    precision for Decimal types
     * @return
     */
    public static String getHiveDataType(String dataType, String length, String scale) {
        if (dataType.equals("CHAR") ||
                dataType.equals("VARCHAR") ||
                dataType.equals("VARCHAR2") ||
                dataType.equals("NVARCHAR") ||
                dataType.equals("CHARACTER") ||
                dataType.equals("NVARCHAR2")) {
            return "STRING";
        } else if (dataType.equals("NUMBER") ||
                dataType.equals("DECIMAL") ||
                dataType.equals("DECIMAL IDENTITY")) {
            return "DECIMAL(" + length + "," + scale + ")";
        } else if (dataType.equals("NUMBER FLOATING POINT")) {
            return "FLOAT";
        } else if (dataType.equals("INTEGER") ||
                dataType.equals("INTEGER IDENTITY") ||
                dataType.equals("SMALLINT") ||
                dataType.equals("TINYINT") ) {
            return "INT";
        } else if (dataType.equals("CLOB") ||
                dataType.equals("BLOB") ||
                //dataType.equals("DATE") ||
                dataType.equals("TIME") ||
                dataType.equals("UNIQUEIDENTIFIER") ||
                dataType.equals("XML") ||
                dataType.equals("VARBINARY")) {
            return "STRING";
        } else if (dataType.equals("DATETIME")){
            return "TIMESTAMP";
        }
         else if (dataType.equals("BIT")){
             return "BOOLEAN";
        }
          else return dataType;
    }
}
