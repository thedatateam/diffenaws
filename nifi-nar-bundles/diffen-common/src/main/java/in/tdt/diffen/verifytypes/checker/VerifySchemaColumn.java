/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.verifytypes.checker;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hive.serde2.io.DateWritable;
import org.apache.hadoop.hive.serde2.lazy.ByteArrayRef;
import org.apache.hadoop.hive.serde2.lazy.LazyFactory;
import org.apache.hadoop.hive.serde2.lazy.LazyPrimitive;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.AbstractPrimitiveLazyObjectInspector;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyObjectInspectorParameters;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyObjectInspectorParametersImpl;
import org.apache.hadoop.hive.serde2.lazy.objectinspector.primitive.LazyPrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.typeinfo.PrimitiveTypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoFactory;
import org.apache.hadoop.io.Text;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Performs cell level validation. Encapsulates all the data validation parameters of the column and accepts a column value.
 * This class is encapsulated in {@link VerifySchema}
 */
public class VerifySchemaColumn {

    public static boolean stringSupportEnabled = true;
    private String name;
    private String datatype;
    private String comment;
    private boolean notNull = false;
    private boolean drop = false;
    private Integer width = 0;
    private boolean Default = false;
    private String defaultValue = null;
    private Map<String, String> defaultColumnValue = new HashMap<String, String>();
    private List<String> rules = new ArrayList<>();
    private final ByteArrayRef _byteRef = new ByteArrayRef();
    private final DecimalFormat dfInput = new DecimalFormat();
    private final DecimalFormat dfInputWithPostive = new DecimalFormat();
    private final DecimalFormat dfOutput = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
    private LazyObjectInspectorParameters lazyObjectInspectorParameters;


    private final Pattern pattern = Pattern.compile("([^\\s]+)\\s+([^\\s]+)");
    private final String otherThanCloseParenthesis = " [^\\)] ";
    private final String paranthesisString = String.format(" \\( %s* \\) ", otherThanCloseParenthesis);
    private final String regex = String.format("(?x)" + // enable comments,
                    // ignore white spaces
                    "," + // match a comma
                    "(?=" + // start positive look ahead
                    "(" + // start group 1
                    "%s*" + // match 'otherThanParenthesis' zero or more times
                    "%s" + // match 'parenthesisString'
                    ")*" + // end group 1 and repeat it zero or more times
                    "%s* " + // match 'otherThanParenthesis'
                    "$ " + // match the end of the string
                    ") ", // stop positive look ahead
            otherThanCloseParenthesis, paranthesisString, otherThanCloseParenthesis);


    public VerifySchemaColumn(String columnLine) {
        this(columnLine, new LazyObjectInspectorParametersImpl());
    }

    /**
     * Accepts the column description as declared in the table config. Parses each component and holds them in this bean
     */
    public VerifySchemaColumn(String columnLine, LazyObjectInspectorParameters inspectorParameters) {

        if (columnLine == null) {
            throw new RuntimeException("Null column schema");
        }
        Matcher m = pattern.matcher(columnLine);
        if (!m.find()) {
            throw new RuntimeException("Invalid column schema: " + columnLine);
        }
        name = m.group(1);
        datatype = m.group(2);
        if (columnLine.contains("NOT NULL")) {
            notNull = true;
        }
        // Definition words
        String[] words = columnLine.split("\\s+");
        for (int i = 1; i < words.length; i++) {
            if ("DROP".equals(words[i])) {
                drop = true;
                continue;
            }
            if ("WIDTH".equals(words[i])) {
                i++;
                width = Integer.valueOf(words[i]);
                continue;
            }
            if ("RULES".equals(words[i])) {
                i++;
                String[] ruleNames = words[i].split(regex);
                for (String ruleName : ruleNames) {
                    rules.add(ruleName);
                }
                continue;
            }
            if ("DEFAULT".equals(words[i])) {
                i++;

                defaultValue = words[i];
                // Trim quotes
                if ((defaultValue.startsWith("'") && defaultValue.endsWith("'"))
                        || (defaultValue.startsWith("\"") && defaultValue.endsWith("\""))) {
                    defaultValue = defaultValue.substring(1, defaultValue.length() - 1);
                }
                defaultColumnValue.put(name, defaultValue);
                Default = true;

            }
        }

        // Decimal part with decimal
        if (StringUtils.startsWithIgnoreCase(datatype, "DECIMAL") && !datatype.contains(",")) {
            datatype = datatype.replace(")", ",0)");
        }

        this.lazyObjectInspectorParameters = inspectorParameters;
    }

    public String getName() {
        return name;
    }

    public String getDatatype() {
        return datatype;
    }

    public boolean isNotNull() {
        return notNull;
    }

    public boolean hasDefault() {
        return Default;
    }

    public String getComment() {
        return comment;
    }

    public boolean isDrop() {
        return drop;
    }

    public Integer getWidth() {
        return width;
    }

    public List<String> getRules() {
        return rules;
    }

    /**
     * Performs the verification of the preProcessedData.
     *
     * @param originalData The original Data that was untransformed
     * @return null, if the preProcessedData is valid, or a String describing the error,
     * if the preProcessedData is invalid.
     * @throws RuntimeException
     */
    public DateTypeValidationResult verify(String originalData) throws RuntimeException {
        DateTypeValidationResult preProcessedDataResult = getTrimmedNumber(originalData, getDatatype());
        String preProcessedData = null;
        if (preProcessedDataResult.getResultType() == ValidationResultType.ERROR && StringUtils.isNotEmpty(originalData)){
            return preProcessedDataResult;
        }
        else{
            preProcessedData = preProcessedDataResult.getOriginalValue();
        }
        String returnValue = preProcessedData;
        if (preProcessedData == null || preProcessedData.isEmpty()) {
            if (isNotNull()) {
                if (defaultColumnValue.get(name) != null) {
                    preProcessedData = defaultColumnValue.get(name);
                    return new DateTypeValidationResult(preProcessedData, false, "WARNING: Replacing NULL value with DEFAULT value for " + name + " Data$: " + preProcessedData, ValidationResultType.WARN);
                } else {
                    return new DateTypeValidationResult(returnValue, false, "ERROR: NULL value for NOT NULL column " + name + " Null? " + isNotNull() + " HasDefault? " + hasDefault(), ValidationResultType.ERROR);
                }
            } else {
                return new DateTypeValidationResult("", true, null, ValidationResultType.GOOD);
            }
        }

        String dataTypeValidation = isValOfType(originalData, preProcessedData, datatype);
        if (dataTypeValidation == null) { // Valid
            return new DateTypeValidationResult(preProcessedData, true, null, ValidationResultType.GOOD);
        } else {
            if (defaultColumnValue.get(name) != null) {
                preProcessedData = defaultColumnValue.get(name);
                return new DateTypeValidationResult(returnValue, false, "WARNING: Replacing NULL value with DEFAULT value for " + name + dataTypeValidation + " Data$: " + preProcessedData, ValidationResultType.WARN);
            } else {
                return new DateTypeValidationResult(returnValue, false, "ERROR: " + dataTypeValidation, ValidationResultType.ERROR);
            }
        }
    }

    /**
     * Checks the string with its Hive data type object.
     *
     * @param preProcessedData The string to verify.
     * @param datatype         The Hive data type.
     * @return null, if the string is valid, or a String describing the error,
     * if the string is invalid.
     */
    private String isValOfType(String originalData, String preProcessedData, String datatype) {

        //Ducking for String datatype, since String should allows all characters
        if (datatype.equalsIgnoreCase("string")) {
            if (stringSupportEnabled)
                return null;
            else {
                RuntimeException up = new RuntimeException("STRING support for source table fields is disabled. Please enable the same, or use CHAR/VARCHAR instead.");
                throw up;
            }
        }

        Text t = new Text(preProcessedData);
        _byteRef.setData(t.getBytes());

        // capture and rethrow with descriptive error msg, if type not
        // recognized
        PrimitiveTypeInfo p;

        try {
            p = TypeInfoFactory.getPrimitiveTypeInfo(datatype.toLowerCase());
        } catch (RuntimeException e) {
            throw new RuntimeException("'" + datatype + "' not recognized" + ", only Hive Primitive types supported");
        }

        // this will get object inspector already cached in this execution;
        // if not, will create and add to cache only as many objects as there
        // are distinct types are created
        AbstractPrimitiveLazyObjectInspector<?> oi = LazyPrimitiveObjectInspectorFactory.getLazyObjectInspector(p,
                lazyObjectInspectorParameters);
        LazyPrimitive<?, ?> lp = LazyFactory.createLazyPrimitiveClass(oi);
        lp.init(_byteRef, 0, t.getLength());

        String lpString = lp.toString();

        String d;
        if(datatype.contains("TIMESTAMP") &&
                preProcessedData != null &&
                lazyObjectInspectorParameters.getTimestampFormats() != null &&
                !lazyObjectInspectorParameters.getTimestampFormats().isEmpty()) {
            final SimpleDateFormat formatter = new SimpleDateFormat(lazyObjectInspectorParameters
                    .getTimestampFormats().get(0));
            if(!preProcessedData.equals(lpString)) {
                try{
                    formatter.parse(t.toString());
                    d = t.toString();
                } catch(Exception e) {
                    e.printStackTrace();
                    d = lpString;
                }
            } else d = lpString;
        } else {
            d = lpString;
        }

        if (d == null || !d.equals(preProcessedData)) {
            // HIVE-8102: Partitions of type 'date' behave incorrectly with
            // daylight saving time
            // If the type if DateWritable then do our own checking to avoid
            // that bug of Hive 0.13. The bug is fixed in Hive 0.14 so if using
            // Hive 0.14 this code is no longer needed
            if (lp.getWritableObject() instanceof DateWritable) {
                if (hive8102Check(preProcessedData)) {
                    return null;
                }
            } else if (datatype.toLowerCase().contains("decimal")) {
                if (validateDecimal(preProcessedData, datatype)) {
                    return null;
                }
            }

            try {
                if (datatype.contains("CHAR")) {
                    int maxLength = Integer.parseInt(datatype.substring(5, datatype.length() - 1));
                    if (preProcessedData.length() > maxLength)
                        return "'" + originalData + "' for column '" + name + "' not of type " + datatype + ", type enforced='" + d + "'";
                    else if (preProcessedData.length() == maxLength) {
                        if (preProcessedData.compareTo(d) == 0)
                            return null;
                    } else {
                        preProcessedData = org.apache.hadoop.hive.common.type.HiveBaseChar.getPaddedValue(preProcessedData, maxLength);
                        if (preProcessedData.equals(d))
                            return null;
                    }
                }

                if (datatype.equalsIgnoreCase("bigint")) {
                    if (new BigInteger(preProcessedData).compareTo(new BigInteger(d)) == 0) {
                        return null;
                    }
                }
                if (datatype.toLowerCase().contains("int")) {
                    if (new Integer(preProcessedData).compareTo(new Integer(d)) == 0) {
                        return null;
                    }
                }
                if (datatype.equalsIgnoreCase("float")) {
                    if (new Float(preProcessedData).compareTo(new Float(d)) == 0) {
                        return null;
                    }
                }
                if (datatype.equalsIgnoreCase("boolean")) {
                    if (new Boolean(preProcessedData).compareTo(new Boolean(d)) == 0) {
                        return null;
                    }
                }
                if (datatype.equalsIgnoreCase("double")) {
                    if (new Double(preProcessedData).compareTo(new Double(d)) == 0) {
                        return null;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return "'" + originalData + "' for column '" + name + "' not of type " + datatype + ", type enforced='" + d + "'";
            }
            //checking for DECIMAL with precision check due to hive error on decimal checks

            return "'" + originalData + "' for column '" + name + "' not of type " + datatype + ", type enforced='" + d + "'";
        }

        return null;
    }

    /**
     * HIVE-8102: Partitions of type 'date' behave incorrectly with daylight
     * saving time. If the type if DateWritable then do our own checking to
     * avoid that bug of Hive 0.13. The bug is fixed in Hive 0.14 so if using
     * Hive 0.14 this code is no longer needed.
     *
     * @param d The date to check.
     * @return True if the date is correct, false otherwise.
     */
    private boolean hive8102Check(String d) {
        final long MILLIS_PER_DAY = TimeUnit.DAYS.toMillis(1);
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        final TimeZone LOCAL_TIMEZONE = Calendar.getInstance().getTimeZone();

        java.sql.Date sqlDate = new java.sql.Date(0);
        try {
            sqlDate.setTime(formatter.parse(d).getTime());
        } catch (ParseException e) {
            return false;
        }

        // Date to Days
        long millisLocal = sqlDate.getTime();
        long millisUtc = millisLocal + LOCAL_TIMEZONE.getOffset(millisLocal);
        // Hive check
        int j;
        if (millisUtc >= 0L) {
            j = (int) (millisUtc / MILLIS_PER_DAY);
        } else {
            j = (int) ((millisUtc - 86399999) / MILLIS_PER_DAY);
        }
        // Non-hive check
        // int j = (int) Math.round((double) millisUtc / MILLIS_PER_DAY);

        // Days to millis
        long millisUtcBack = j * MILLIS_PER_DAY;
        long tmpTime = millisUtcBack - LOCAL_TIMEZONE.getOffset(millisUtcBack);
        long milis = millisUtcBack - LOCAL_TIMEZONE.getOffset(tmpTime);

        sqlDate.setTime(milis);

        return d.equals(formatter.format(sqlDate));
    }

    // Fix for HIVE-7373 (https://issues.apache.org/jira/i#browse/HIVE-7373)
    public DateTypeValidationResult getTrimmedNumber(String val, String datatype) {
        try {
            if (hive7373Check(val, datatype)) {
                if (StringUtils.startsWithIgnoreCase(datatype, "DECIMAL")) {
                    String trimmedVal = null;
                    boolean negate = false;
                    if (StringUtils.startsWith(val, "+")) {
                        trimmedVal = StringUtils.substringAfter(val.trim(), "+");
                    }
                    else if (StringUtils.startsWith(val, "-")) {
                        negate = true;
                        trimmedVal = StringUtils.substringAfter(val.trim(), "-");
                    }
                    else trimmedVal = val.trim();

                    try {
                        BigDecimal bd = new BigDecimal(trimmedVal);
                        if (negate) bd = bd.negate();
                        return new DateTypeValidationResult(bd.toPlainString(), true, null, ValidationResultType.GOOD);
                    }
                    catch (NumberFormatException ex){
                        return new DateTypeValidationResult(val, false, "ERROR: " + val + " is not a valid decimal for column " + name  + ".", ValidationResultType.ERROR);
                    }

                } else {
                    return new DateTypeValidationResult(String.valueOf(new BigInteger(val)), true, null, ValidationResultType.GOOD);
                }
            }
        } catch (Exception e) {
            if (val.isEmpty())
                return new DateTypeValidationResult(val, true, null, ValidationResultType.GOOD);
        }
        return new DateTypeValidationResult(val, true, null, ValidationResultType.GOOD);
    }


    /**
     * Validates the Decimal string value for precision and scale
     * @param preProcessedData Decimal string data that is stripped off of leading '+' indicator.
     * @param dataType
     * @return
     */
    private boolean validateDecimal(String preProcessedData, String dataType) {
        Integer characteristic;
        Integer mantissa;
        if (dataType.contains(",")) {
            String[] precisionScaleArray = StringUtils.substringBetween(dataType, "(", ")").split(",");
            characteristic = Integer.parseInt(precisionScaleArray[0].trim());
            mantissa = Integer.parseInt(precisionScaleArray[1].trim());
        } else {
            String precisionStr = StringUtils.substringBetween(dataType, "(", ")");
            characteristic = Integer.parseInt(precisionStr.trim());
            mantissa = 0;
        }

        String [] numberArray = StringUtils.split(preProcessedData, ".", -1);
        Integer scale = Optional.ofNullable(numberArray[0]).orElse("0").length();
        Integer precision;
        if (numberArray.length > 1) {
            precision = Optional.ofNullable(numberArray[1]).orElse("0").length();
        } else{
            precision = 0;
        }

        if (scale <= characteristic && precision <= mantissa) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * DECIMAL with precision is not handled correctly when there are trailing
     * zeros Even though HIVE-7373 is marked as Resolved in Hive-0.14, a comment
     * later confirms that the resolution has been reverted by HIVE-8745
     *
     * @param string   The string to verify.
     * @param datatype The expected datatype of the string.
     * @return
     */
    private boolean hive7373Check(String string, String datatype) {
        return (datatype.equals("TINYINT") || datatype.startsWith("SMALLINT") || datatype.startsWith("INT")
                || datatype.startsWith("BIGINT") || datatype.startsWith("DECIMAL"));
    }
}
