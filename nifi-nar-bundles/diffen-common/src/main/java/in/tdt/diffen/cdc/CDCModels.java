/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.tdt.diffen.cdc;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;

/**
 * Container for all Java bindings for Table Configs
 */
public class CDCModels {
    /**
     * Java binding for the keycol.list file
     */
    public static class TableKeyColInfo {
        private final String tableName;
        private final String keyCols;

        public TableKeyColInfo(String tableName, String keyCols) {
            this.tableName = tableName;
            this.keyCols = keyCols;
        }

        public String getTableName() {
            return tableName;
        }

        public String getKeyCols() {
            return keyCols;
        }
    }

    /**
     * Java binding for the sourcetype.list file
     */
    public static class TableSourceInfo {
        private final String tableName;
        private final String typee;

        public TableSourceInfo(String tableName, String typee) {
            this.tableName = tableName;
            this.typee = typee;
        }

        public String getTableName() {
            return tableName;
        }

        public String getTypee() {
            return typee;
        }
    }

    /**
     * Java binding for transformationrules.list
     */
    public static class TableTransformationRule {
        private final String tableName;
        private final List<ColTransformationRule> rulesList;

        public TableTransformationRule(String tableName, List<ColTransformationRule> rulesList) {
            this.tableName = tableName;
            this.rulesList = rulesList;
        }

        public String getTableName() {
            return tableName;
        }

        public List<ColTransformationRule> getRulesList() {
            return rulesList;
        }
    }

    /**
     * Transformation rules corresponding to a single column
     */
    public static class ColTransformationRule {
        private final String columnName;
        private final String rules;

        public ColTransformationRule(String columnNameRuleString) {
            System.out.println("columnNameRuleString = " + columnNameRuleString);
            String[] split = StringUtils.split(columnNameRuleString.trim(), "[]");
            this.columnName = split[0];
            this.rules = split[1];
        }

        public String getColumnName() {
            return columnName;
        }

        public String getRules() {
            return rules;
        }
    }

    /**
     * Java binding for Datatype mapping
     */
    public static class TableDataTypeMapping {
        private final String sourceType;
        private final String avroType;
        private final boolean isPrecisionType;

        public TableDataTypeMapping(String sourceType, String avroType, boolean isPrecisionType) {
            this.sourceType = sourceType;
            this.avroType = avroType;
            this.isPrecisionType = isPrecisionType;
        }

        public String getSourceType() {
            return sourceType;
        }

        public String getAvroType() {
            return avroType;
        }

        public boolean isPrecisionType() {
            return isPrecisionType;
        }
    }

    /**
     * Java binding of each property in the output configuration XML
     */
    public static class Property implements Comparable{
        private final String name;
        private final String value;

        public Property(String name, String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Property property = (Property) o;
            return Objects.equals(name, property.name) &&
                    Objects.equals(value, property.value);
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, value);
        }

        @Override
        public String toString() {
            return "\n<property>\n\t<name>" + name + "</name>\n\t<value>" + value + "</value>\n</property>";
        }

        @Override
        public int compareTo(Object o) {
            return this.name.compareTo(((Property)o).name);
        }
    }

    /**
     * Java binding of the configuration XML
     */
    public static class TableConfiguration {
        private final List<Property> list;

        public TableConfiguration(List<Property> list) {
            this.list = list;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TableConfiguration that = (TableConfiguration) o;
            return Objects.equals(list, that.list);
        }

        @Override
        public int hashCode() {
            return Objects.hash(list);
        }

        @Override
        public String toString() {
            return "<configuration>" + list.stream().map(prop -> prop.toString()).reduce("", String::concat) + "\n</configuration>";
        }
    }
}
