/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.nifi.processors.rowstandardizers;


import org.apache.commons.io.IOUtils;
import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;


public class EmbeddedNewLineRemoverTest {

    private TestRunner testRunner;

    @Before
    public void init() {
        testRunner = TestRunners.newTestRunner(EmbeddedNewLineRemover.class);
    }

    @Test
    public void testProcessor() {

    }

    @Test
    public void TestValidLastRowColumnCount() {
        final String input = "2016/11/14,abcde,oxoooo,abc,Anne\n" +
                "Jones,29568,82569,abc1,\n" +
                "2016/11/14,2016/11/14,abcde,2016/11/14,2016/11/14\n" +
                "2016/11/14,abcde,oxoooo,abc,Anne,80111,52966,abc1,2016/11/14,2016/11/14,abcde,2016/11/14,2016/11/14\n" +
                "2016/11/14,abcde,oxoooo,abc,Anne,3844,62645,abc1,2016/11/14,2016/11/14,abcde,2016/11/14,\n" +
                "\n" +
                "2016/11/14\n" +
                "\n" +
                "\n" +
                "2016/11/14,abcde,\n" +
                "oxoooo,abc,\n" +
                "\n" +
                "Anne,78991,62974,abc1,2016/11/14,2016/11/14,abcde,2016/11/14,2016/11/14\n" +
                "2016/11/14,abcde,oxoooo,abc,Anne,23773,17965,abc1,2016/11/14,2016/11/14,abcde,2016/11/14,2016/11/14\n" +
                "2016/11/14,abcde,oxoooo,abc,Anne,90656,\n" +
                "17434,abc1,2016/11/14,2016/11/14,abcde,2016/11/14,\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "2016/11/14";

        final TestRunner runner = TestRunners.newTestRunner(new EmbeddedNewLineRemover());
        runner.setProperty(EmbeddedNewLineRemover.DELIMITER, ",");
        Map<String, String> attributes = new HashMap<>();

        attributes.put("expected.column.count", "13");
        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);

        runner.run();

        runner.assertTransferCount(EmbeddedNewLineRemover.REL_SUCCESS, 1);
        final MockFlowFile successFlow = runner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_SUCCESS).get(0);

        final String expectedColumnCount = successFlow.getAttribute(EmbeddedNewLineRemover.EXPECTED_COLUMN_COUNT);
        final String lastRowColumnCount = successFlow.getAttribute(EmbeddedNewLineRemover.LAST_ROW_COLUMN_COUNT);
        successFlow.assertAttributeEquals(EmbeddedNewLineRemover.LAST_ROW_COLUMN_COUNT, expectedColumnCount);


    }

    @Test
    public void TestValidLastRowColumnCountPipe() {
        final String input = "2016/11/14|abcde|oxoooo|abc|Anne\n" +
                "Jones|29568|82569|abc1|\n" +
                "2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14\n" +
                "2016/11/14|abcde|oxoooo|abc|Anne|80111|52966|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14\n" +
                "2016/11/14|abcde|oxoooo|abc|Anne|3844|62645|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|\n" +
                "\n" +
                "2016/11/14\n" +
                "\n" +
                "\n" +
                "2016/11/14|abcde|\n" +
                "oxoooo|abc|\n" +
                "\n" +
                "Anne|78991|62974|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14\n" +
                "2016/11/14|abcde|oxoooo|abc|Anne|23773|17965|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|2016/11/14\n" +
                "2016/11/14|abcde|oxoooo|abc|Anne|90656|\n" +
                "17434|abc1|2016/11/14|2016/11/14|abcde|2016/11/14|\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "2016/11/14";

        final TestRunner runner = TestRunners.newTestRunner(new EmbeddedNewLineRemover());
        runner.setProperty(EmbeddedNewLineRemover.DELIMITER, "|");
        Map<String, String> attributes = new HashMap<>();

        attributes.put("expected.column.count", "13");
        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);

        runner.run();

        runner.assertTransferCount(EmbeddedNewLineRemover.REL_SUCCESS, 1);
        final MockFlowFile successFlow = runner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_SUCCESS).get(0);

        final String expectedColumnCount = successFlow.getAttribute(EmbeddedNewLineRemover.EXPECTED_COLUMN_COUNT);
        final String lastRowColumnCount = successFlow.getAttribute(EmbeddedNewLineRemover.LAST_ROW_COLUMN_COUNT);
        successFlow.assertAttributeEquals(EmbeddedNewLineRemover.LAST_ROW_COLUMN_COUNT, expectedColumnCount);


    }

    @Test
    public void TestInValidLastRowColumnCount() {
        final String input = "2016/11/14,abcde,oxoooo,abc,Anne\n" +
                "Jones,29568,82569,abc1,\n" +
                "2016/11/14,2016/11/14,abcde,2016/11/14,2016/11/14\n" +
                "2016/11/14,abcde,oxoooo,abc,Anne,80111,52966,abc1,2016/11/14,2016/11/14,abcde,2016/11/14,2016/11/14\n" +
                "2016/11/14,abcde,oxoooo,abc,Anne,3844,62645,abc1,2016/11/14,2016/11/14,abcde,2016/11/14,\n" +
                "\n" +
                "2016/11/14\n" +
                "\n" +
                "\n" +
                "2016/11/14,abcde,\n" +
                "oxoooo,abc,\n" +
                "\n" +
                "Anne,78991,62974,abc1,2016/11/14,2016/11/14,abcde,2016/11/14,2016/11/14\n" +
                "2016/11/14,abcde,oxoooo,abc,Anne,23773,17965,abc1,2016/11/14,2016/11/14,abcde,2016/11/14,2016/11/14\n" +
                "2016/11/14,abcde,oxoooo,abc,Anne,90656,\n" +
                "17434,abc1,2016/11/14,2016/11/14,abcde,2016/11/14";

        final TestRunner runner = TestRunners.newTestRunner(new EmbeddedNewLineRemover());
        runner.setProperty(EmbeddedNewLineRemover.DELIMITER, ",");
        Map<String, String> attributes = new HashMap<>();

        attributes.put("expected.column.count", "13");
        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);

        runner.run();

        runner.assertTransferCount(EmbeddedNewLineRemover.REL_SUCCESS, 0);
        final MockFlowFile successFlow = runner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_FAILURE).get(0);

        final int expectedColumnCount = Integer.parseInt(successFlow.getAttribute(EmbeddedNewLineRemover.EXPECTED_COLUMN_COUNT));
        final int lastRowColumnCount = Integer.parseInt(successFlow.getAttribute(EmbeddedNewLineRemover.LAST_ROW_COLUMN_COUNT));
        assert (lastRowColumnCount < expectedColumnCount);

    }

    @Test
    public void TestSpecialCharacterDelimiter() {
        final String input = "2016-01-27 12:07:56\u0001A\u0001D\u0001UPNC\u0001YNSJYR\u0001UYEHU\u0001Z\u0001OXD\u00019\u0001816455111\u00014626187.070\u00012016-01-06 09:59:13\u00012016-01-28 16:37:06\u00012016-01-15 09:07:58\u0001362\u0001WH\u0001TWJY\u0001YMUBKGVANJEEQVJF\u0001IWFDLTRB\u0001MRKC\u0001FRK\u0001SASRTNTY\u0001VOFOGTVF\u0001OCLKCRMHH\u0001KJNWEHSHJEUQ\u0001PEHRCSXTREIRZBCVEHVPZFZIZELGWWVMRZDDMCTYEJN\u00012016-01-31 11:12:39\u0001YFC\u00019201409772584.902\u0001B\u0001NCMCUFMA\u0001L\u0001J\n" +
                "2016-01-21 15:30:03\u0001B\u0001A\u0001ZDOZ\u0001V\u0001LCNZBWE\u0001A\u0001T\u0001634\u0001188\u0001513438124515.499\u00012016-02-01 12:32:57\u00012016-01-09 12:43:19\u00012016-01-19 09:33:03\u00017\u0001HNYERMRFUMUO\u0001HZYXRZYWFBMJ\u0001KRVNDPEGYMACOBP\u0001Z\u0001YAHOMRRIV\u0001CUC\u0001JMVTIOLGV\u0001ARVOZWLECPEG\u0001DESFQQEEXTJD\u0001GMNKTYIMN\u0001EQSECWKLZXUEICEQLCFPRMXOJNUBNVVNSCYNXKBKFROYIUCTLDXHYFLUHZOHHILZTCZMZO\u00012016-01-12 16:18:13\u0001Y\u0001991488295306274.522\u0001W\u0001YTBRFCA\u0001I\u0001J\n";


        final TestRunner runner = TestRunners.newTestRunner(new EmbeddedNewLineRemover());
        runner.setProperty(EmbeddedNewLineRemover.DELIMITER, "\u0001");


        Map<String, String> attributes = new HashMap<>();

        attributes.put("expected.column.count", "33");
        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);

        runner.run();

        runner.assertTransferCount(EmbeddedNewLineRemover.REL_SUCCESS, 1);
        final MockFlowFile successFlow = runner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_SUCCESS).get(0);

        final int expectedColumnCount = Integer.parseInt(successFlow.getAttribute(EmbeddedNewLineRemover.EXPECTED_COLUMN_COUNT));
        final int lastRowColumnCount = Integer.parseInt(successFlow.getAttribute(EmbeddedNewLineRemover.LAST_ROW_COLUMN_COUNT));
        final long totalRowCount = Long.parseLong(successFlow.getAttribute(EmbeddedNewLineRemover.TOTAL_ROW_COUNT));
        assert (2 == totalRowCount);
        assert (lastRowColumnCount == expectedColumnCount);

    }

    @Test
    public void TestSpecialCharacterDelimiter500Lines() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        final String input = IOUtils.toString(classLoader.getResourceAsStream("gps_all_tl_pymt_batch.D2017011.T110110110.R500.csv"), StandardCharsets.UTF_8);


        final TestRunner runner = TestRunners.newTestRunner(new EmbeddedNewLineRemover());
        runner.setProperty(EmbeddedNewLineRemover.DELIMITER, "\u0001");


        Map<String, String> attributes = new HashMap<>();

        attributes.put("expected.column.count", "33");
        runner.enqueue(input.getBytes(StandardCharsets.UTF_8), attributes);

        runner.run();

        runner.assertTransferCount(EmbeddedNewLineRemover.REL_SUCCESS, 1);
        final MockFlowFile successFlow = runner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_SUCCESS).get(0);

        final int expectedColumnCount = Integer.parseInt(successFlow.getAttribute(EmbeddedNewLineRemover.EXPECTED_COLUMN_COUNT));
        final int lastRowColumnCount = Integer.parseInt(successFlow.getAttribute(EmbeddedNewLineRemover.LAST_ROW_COLUMN_COUNT));
        final long totalRowCount = Long.parseLong(successFlow.getAttribute(EmbeddedNewLineRemover.TOTAL_ROW_COUNT));
        assert (500 == totalRowCount);
        assert (lastRowColumnCount == expectedColumnCount);

    }

    @Test
    public void TestValidColCnt() throws FileNotFoundException {
        // inputs
        String toBeSent = "2016-10-12 03:20:52,0,I,NOT SET   ,OB50200610010001,999,0.000,2008-02-22-00.32.34.556184,0.000\n" +
                "2016-10-12 03:20:52,0,I,NOT SET   ,OB5020061001\n0001,999,0.000,2008-02-22-00.32.34.556184,0.000\n";

        testRunner.setProperty(EmbeddedNewLineRemover.DELIMITER, ",");

        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put("expected.column.count", "9");

        // test run
        testRunner.enqueue(toBeSent, attributes);
        testRunner.run();
        // assertions
        testRunner.assertQueueEmpty();
        List<MockFlowFile> successflowFile = testRunner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_SUCCESS);
        assertTrue(successflowFile.size() == 1);

        List<MockFlowFile> failedflowFile = testRunner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_FAILURE);
        assertTrue(failedflowFile.size() == 0);

        final int lastRowColumnCount = Integer.parseInt(successflowFile.get(0).getAttribute(EmbeddedNewLineRemover.LAST_ROW_COLUMN_COUNT));
        final long totalRowCount = Long.parseLong(successflowFile.get(0).getAttribute(EmbeddedNewLineRemover.TOTAL_ROW_COUNT));
        assert (2 == totalRowCount);
        assert (lastRowColumnCount == 9);
    }

    @Test
    public void TestInvalidlessColCnt() throws FileNotFoundException {
        // inputs
        String toBeSent = "2016-10-12 03:20:52,0,I,NOT SET   ,OB50200610010001,999,0.000,2008-02-22-00.32.34.556184,0.000\n" +
                "2016-10-12 03:20:52,0,I,NOT SET   ,OB5020061001\n0001,999,0.000,2008-02-22-00.32.34.556184\n";

        testRunner.setProperty(EmbeddedNewLineRemover.DELIMITER, ",");

        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put("expected.column.count", "9");

        // test run
        testRunner.enqueue(toBeSent, attributes);
        testRunner.run();
        // assertions
        testRunner.assertQueueEmpty();
        List<MockFlowFile> successflowFile = testRunner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_SUCCESS);
        assertTrue(successflowFile.size() == 0);

        List<MockFlowFile> failedflowFile = testRunner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_FAILURE);
        assertTrue(failedflowFile.size() == 1);
    }

    @Test
    public void TestInvalidmoreColCnt() throws FileNotFoundException {
        // inputs
        String toBeSent = "2016-10-12 03:20:52,0,I,NOT SET   ,OB50200610010001,999,0.000,2008-02-22-00.32.34.556184,0.000\n" +
                "2016-10-12 03:20:52,0,I,NOT SET   ,OB5020061001\n0001,999,0.000,2008-02-22-00.32.34.556184,0.000,0.0\n" +
                "2016-10-12 03:20:52,0,I,NOT SET   ,OB5020061001\n0001,999,0.000,2008-02-22-00.32.34.556184,0.000\n";

        testRunner.setProperty(EmbeddedNewLineRemover.DELIMITER, ",");

        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put("expected.column.count", "9");

        // test run
        testRunner.enqueue(toBeSent, attributes);
        testRunner.run();
        // assertions
        testRunner.assertQueueEmpty();
        List<MockFlowFile> successflowFile = testRunner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_SUCCESS);
        assertTrue(successflowFile.size() == 0);

        List<MockFlowFile> failedflowFile = testRunner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_FAILURE);
        assertTrue(failedflowFile.size() == 1);
    }

    @Ignore
    public void TestEmptyFile() throws FileNotFoundException {
        // inputs
        String toBeSent = "";

        testRunner.setProperty(EmbeddedNewLineRemover.DELIMITER, ",");

        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put("expected.column.count", "9");

        // test run
        testRunner.enqueue(toBeSent, attributes);
        testRunner.run();
        // assertions
        testRunner.assertQueueEmpty();
        List<MockFlowFile> successflowFile = testRunner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_SUCCESS);
        assertTrue(successflowFile.size() == 0);

        List<MockFlowFile> failedflowFile = testRunner.getFlowFilesForRelationship(EmbeddedNewLineRemover.REL_FAILURE);
        System.out.println(failedflowFile.get(0).getAttribute(EmbeddedNewLineRemover.TOTAL_ROW_COUNT));
        assertTrue(failedflowFile.get(0).getAttribute(EmbeddedNewLineRemover.TOTAL_ROW_COUNT).equals("0"));
        assertTrue(failedflowFile.get(0).getAttribute(EmbeddedNewLineRemover.LAST_ROW_COLUMN_COUNT).equals("0"));
    }


}
