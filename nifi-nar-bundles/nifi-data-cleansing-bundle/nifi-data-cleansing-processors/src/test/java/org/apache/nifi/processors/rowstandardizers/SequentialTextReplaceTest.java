/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.nifi.processors.rowstandardizers;


import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;


public class SequentialTextReplaceTest {

    @Test
    public void TestValidReplace() {
        final String input = StringEscapeUtils.unescapeJava("IN,CA,20171018,IA,\"TEST\"\"\"\"\",IA0000637352,450564,DUMMY DATA,13503605");
        final TestRunner runner = TestRunners.newTestRunner(new SequentialTextReplace());
        final String mappingFile = Paths.get("src/test/resources/replace-text-mapping.txt").toFile().getAbsolutePath();
        runner.setProperty(SequentialTextReplace.MAPPING_FILE, mappingFile);

        runner.enqueue(input.getBytes());
        runner.run();

        runner.assertAllFlowFilesTransferred(SequentialTextReplace.REL_SUCCESS, 1);
        final MockFlowFile out = runner.getFlowFilesForRelationship(SequentialTextReplace.REL_SUCCESS).get(0);
        String outputString = new String(out.toByteArray());
        String expected = "IN,CA,20171018,IA,TEST\"\"\"\",IA0000637352,450564,DUMMY DATA,13503605";
        assertEquals(expected, outputString);
    }

    @Test
    public void TestValidReplaceWithNewLines() {
        final String input = StringEscapeUtils.unescapeJava("IN,CA,20171018,IA,\"TEST\"\"\"\"\",IA0000637352," +
                "\n450564,DUMMY DATA,13503605");
        final TestRunner runner = TestRunners.newTestRunner(new SequentialTextReplace());
        final String mappingFile = Paths.get("src/test/resources/replace-text-mapping.txt").toFile().getAbsolutePath();
        runner.setProperty(SequentialTextReplace.MAPPING_FILE, mappingFile);

        runner.enqueue(input.getBytes());
        runner.run();

        runner.assertAllFlowFilesTransferred(SequentialTextReplace.REL_SUCCESS, 1);
        final MockFlowFile out = runner.getFlowFilesForRelationship(SequentialTextReplace.REL_SUCCESS).get(0);
        String outputString = new String(out.toByteArray());
        String expected = "IN,CA,20171018,IA,TEST\"\"\"\",IA0000637352,\n450564,DUMMY DATA,13503605";
        assertEquals(expected, outputString);
    }

    @Test
    public void TestValidFirstAndLastColumnsDoubleQuotes() {
        final String input = StringEscapeUtils.unescapeJava("\"\"IN\"\",CA,20171018,IA,\"TEST\"\"\"\"\",IA0000637352,450564,DUMMY DATA,\"\"13503605\"\"");
        final TestRunner runner = TestRunners.newTestRunner(new SequentialTextReplace());
        final String mappingFile = Paths.get("src/test/resources/replace-text-mapping.txt").toFile().getAbsolutePath();
        runner.setProperty(SequentialTextReplace.MAPPING_FILE, mappingFile);

        runner.enqueue(input.getBytes());
        runner.run();

        runner.assertAllFlowFilesTransferred(SequentialTextReplace.REL_SUCCESS, 1);
        final MockFlowFile out = runner.getFlowFilesForRelationship(SequentialTextReplace.REL_SUCCESS).get(0);
        String outputString = new String(out.toByteArray());
        String expected = "\"IN\",CA,20171018,IA,TEST\"\"\"\",IA0000637352,450564,DUMMY DATA,\"13503605\"";
        assertEquals(expected, outputString);
    }


    @Ignore
    public void TestValidAcbs() throws IOException {
        final String input = FileUtils.readFileToString(Paths.get("src/test/resources/quoted-csv.txt").toFile());
        final TestRunner runner = TestRunners.newTestRunner(new SequentialTextReplace());
        final String mappingFile = Paths.get("src/test/resources/replace-text-mapping.txt").toFile().getAbsolutePath();
        runner.setProperty(SequentialTextReplace.MAPPING_FILE, mappingFile);
        runner.setProperty(SequentialTextReplace.DELIMITER, "\u0001");

        runner.enqueue(input.getBytes());
        runner.run();

        runner.assertAllFlowFilesTransferred(SequentialTextReplace.REL_SUCCESS, 1);
        final MockFlowFile out = runner.getFlowFilesForRelationship(SequentialTextReplace.REL_SUCCESS).get(0);
        String outputString = new String(out.toByteArray());
        String expected = "IN\u0001CA\u000120171016\u0001IF\u0001 \u0001 \u0001IF003272794\u0001IF003272794000446561600\u00010002125579\u000120171016\u00011536636   2017101607484100950\u0001NLN\u0001CL\u0001 \u0001300\u0001NEW DISBURSEMENT\u0001INR\u00013500000\u00010\u0001 \u000120171016\u00011536636   2017101607492700971\u0001118691953\u0001INR\u0001-3500000\u000123\u000100510191\u0001I04\u000120171016\u0001INR\u00013500000\u0001O\u0001 \u0001164\u0001ME TERM\n";
        assertEquals(expected, outputString);
    }


}
