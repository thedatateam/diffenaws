/*
 * Copyright 2017 Standard Chartered Bank
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.nifi.processors.rowstandardizers;

import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Common utilities shared across processors
 */
public class Utils {

    /**
     * @param value Unescaped delimiter char value
     * @return
     */
    public static char getDelimiter(String value) {
        if (isBlank(value)) throw new RuntimeException("Input Delimiter is Blank/Null : " + value);
        else if (value.startsWith("\\u")) return StringEscapeUtils.unescapeJava(value).charAt(0);
        else {
            switch (value.trim()) {
                case "\\u0001":
                    return 0x01;
                case "\\u0002":
                    return 0x02;
                case "\\u0003":
                    return 0x03;
                case "\\u0004":
                    return 0x04;
                default:
                    return value.charAt(0);
            }
        }
    }

    public static boolean isBlank(String str) {
        int strLen;
        if (str != null && (strLen = str.length()) != 0) {
            for(int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(str.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }
}
