/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.nifi.processors.rowstandardizers;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.StreamCallback;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.util.StopWatch;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.apache.nifi.processor.util.StandardValidators.NON_EMPTY_VALIDATOR;
import static org.apache.nifi.processors.rowstandardizers.EmbeddedNewLineRemover.*;

@EventDriven
@SideEffectFree
@SupportsBatching
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@Tags({"text formatting standardization cleansing"})
@CapabilityDescription("Removes any embedded newline characters from the content of the incoming flow file" +
        " that is delimited by the character provided in the delimiter field")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = EXPECTED_COLUMN_COUNT, description = "The expected number of columns in the incoming " +
        "flow file.")})
@WritesAttributes({@WritesAttribute(attribute = LAST_ROW_COLUMN_COUNT, description = "This count of columns that " +
        "are in the last row of the flowfile content"),
        @WritesAttribute(attribute = EXPECTED_COLUMN_COUNT, description = "This is count of columns that " +
                "were expected in all rows of the flowfile content"),
        @WritesAttribute(attribute = TOTAL_ROW_COUNT, description = "This is count of the total number of rows that " +
                "were processed from the flowfile content")})
public class EmbeddedNewLineRemover extends AbstractProcessor {

    static final PropertyDescriptor DELIMITER = new PropertyDescriptor.Builder()
            .name("csv-delimiter")
            .displayName("CSV delimiter")
            .description("Delimiter character for CSV records")
            .addValidator(NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .defaultValue(",")
            .build();

    static final PropertyDescriptor CHARSET = new PropertyDescriptor
            .Builder().name("Input Charset")
            .description("The input character set")
            .required(false)
            .defaultValue("UTF-8")
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("Success relationship")
            .build();


    static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Failure relationship")
            .build();

    static final String LAST_ROW_COLUMN_COUNT = "lastrow.column.count";
    static final String EXPECTED_COLUMN_COUNT = "expected.column.count";
    static final String TOTAL_ROW_COUNT = "total.row.count";


    private static List<PropertyDescriptor> DESCRIPTORS;
    private static final Set<Relationship> RELATIONSHIPS;

    static {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(DELIMITER);
        descriptors.add(CHARSET);
        DESCRIPTORS = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        RELATIONSHIPS = Collections.unmodifiableSet(relationships);
    }

    @Override
    protected void init(final ProcessorInitializationContext context) {

    }

    @Override
    public Set<Relationship> getRelationships() {
        return RELATIONSHIPS;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return DESCRIPTORS;
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {

        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        final StopWatch stopWatch = new StopWatch(true);
        final String INPUT_DELIMITER = String.valueOf(Utils.getDelimiter(context.getProperty(DELIMITER).evaluateAttributeExpressions(flowFile).getValue()));
        final int DELIMITER_COUNT = Integer.parseInt(flowFile.getAttribute(EXPECTED_COLUMN_COUNT)) - 1; //Adjust col count to delimiter count by reducing 1
        AtomicReference<Integer> lastRowColumnCount = new AtomicReference<>();
        AtomicReference<Long> totalRowCount = new AtomicReference<>();
        try {

            flowFile = session.write(flowFile, new StreamCallback() {
                @Override
                public void process(final InputStream rawIn, final OutputStream rawOut) throws IOException {

                    try (final PrintWriter out = new PrintWriter(new BufferedOutputStream(rawOut, 65536));
                         final BufferedReader br = new BufferedReader(new InputStreamReader(rawIn), 65536)) {

                        String currentLine;
                        int currentDelimiterCount = 0;
                        int previousDelimiterCount = 0;
                        long rowCount = 0;


                            while ((currentLine = br.readLine()) != null) {

                            // Current line delimiter count
                            currentDelimiterCount = StringUtils.countMatches(currentLine, INPUT_DELIMITER);

                            // if current line delimiter count > 0 and previous line delimiter count is equal to the number of delimiter
                            if (currentDelimiterCount != 0 && previousDelimiterCount == DELIMITER_COUNT) {
                                out.println();
                                rowCount++;
                                previousDelimiterCount = 0;
                            }

                            // if previous delimiter count + current delimiter count greater than the number of delimiter.
                            if (previousDelimiterCount + currentDelimiterCount > DELIMITER_COUNT) {
                                System.out.println("Number of Delimiter on record higher expected:" + DELIMITER_COUNT + " got :" + (previousDelimiterCount + currentDelimiterCount + 1));
                                lastRowColumnCount.set(currentDelimiterCount + previousDelimiterCount + 1); //Adjusting delimiter count to col count
                                totalRowCount.set(rowCount);
                                throw new ProcessException("Column Validation failed because columns not matching num columns:" + (DELIMITER_COUNT + 1) + ",current column:" + (previousDelimiterCount + currentDelimiterCount + 1));
                            }
                            if (currentDelimiterCount < DELIMITER_COUNT) {
                                out.print(currentLine.replace("\\r\\n", ""));
                                previousDelimiterCount = previousDelimiterCount + currentDelimiterCount;
                            } else if (currentDelimiterCount == DELIMITER_COUNT) {
                                out.print(currentLine.replace("\\r\\n", ""));
                                previousDelimiterCount = currentDelimiterCount;
                            } else {
                                getLogger().info("Number of Delimiter on record higher expected:" + DELIMITER_COUNT + " got :" + (previousDelimiterCount + currentDelimiterCount));
                                lastRowColumnCount.set(previousDelimiterCount + currentDelimiterCount + 1);
                                totalRowCount.set(rowCount);
                                throw new ProcessException("Column Validation failed because columns not matching num columns:" + (DELIMITER_COUNT + 1) + ",current column:" + (previousDelimiterCount + currentDelimiterCount + 1));
                            }
                        }

                        //Accounting for the last row
                        out.println();
                        rowCount++;

                        if (previousDelimiterCount < DELIMITER_COUNT) {
                            lastRowColumnCount.set(previousDelimiterCount + 1); //Adjusting delimiter count to col count
                            totalRowCount.set(rowCount);
                            throw new ProcessException("Column Validation failed because columns not matching num columns:" + (DELIMITER_COUNT + 1) + ",current column:" + (previousDelimiterCount + 1));
                        }
                        lastRowColumnCount.set(previousDelimiterCount + 1);
                        totalRowCount.set(rowCount);

                    }

                }
            });
            stopWatch.stop();
            flowFile = session.putAttribute(flowFile, EXPECTED_COLUMN_COUNT, Integer.toString(DELIMITER_COUNT + 1));
            flowFile = session.putAttribute(flowFile, LAST_ROW_COLUMN_COUNT, lastRowColumnCount.get().toString());
            flowFile = session.putAttribute(flowFile, TOTAL_ROW_COUNT, totalRowCount.get().toString());

            session.getProvenanceReporter().modifyContent(flowFile, stopWatch.getDuration(TimeUnit.MILLISECONDS));
            session.transfer(flowFile, REL_SUCCESS);
        } catch (final ProcessException e) {
            getLogger().error(e.toString());
            flowFile = session.putAttribute(flowFile, EXPECTED_COLUMN_COUNT, Integer.toString(DELIMITER_COUNT + 1));
            flowFile = session.putAttribute(flowFile, LAST_ROW_COLUMN_COUNT, lastRowColumnCount.get().toString());
            flowFile = session.putAttribute(flowFile, TOTAL_ROW_COUNT, totalRowCount.get().toString());
            session.transfer(flowFile, REL_FAILURE);
        }
    }

}
