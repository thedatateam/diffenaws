/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//Need to submit this as a Patch for ReplaceTextWithMapping
package org.apache.nifi.processors.rowstandardizers;

import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.EventDriven;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.InputRequirement.Requirement;
import org.apache.nifi.annotation.behavior.SideEffectFree;
import org.apache.nifi.annotation.behavior.SupportsBatching;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.AttributeValueDecorator;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.StreamCallback;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.processors.rowstandardizers.Utils;
import org.apache.nifi.stream.io.StreamUtils;
import org.apache.nifi.util.StopWatch;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

import static org.apache.nifi.processor.util.StandardValidators.NON_EMPTY_VALIDATOR;

@EventDriven
@SideEffectFree
@SupportsBatching
@InputRequirement(Requirement.INPUT_REQUIRED)
@Tags({"Text", "Regular Expression", "Update", "Change", "Replace", "Modify", "Regex", "Mapping"})
@CapabilityDescription("Updates the content of a FlowFile by evaluating a Regular Expression against it and replacing the section of the content that "
        + "matches the Regular Expression with some alternate value provided in a mapping file.")
public class SequentialTextReplace extends AbstractProcessor {

    public static final PropertyDescriptor DELIMITER = new PropertyDescriptor.Builder()
            .name("csv-delimiter")
            .displayName("CSV delimiter")
            .description("Delimiter character for CSV records")
            .addValidator(NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .defaultValue(",")
            .build();
    public static final PropertyDescriptor MAPPING_FILE = new PropertyDescriptor.Builder()
            .name("Mapping File")
            .description("The name of the file (including the full path) containing the Mappings.")
            .addValidator(StandardValidators.FILE_EXISTS_VALIDATOR)
            .required(true)
            .build();
    public static final PropertyDescriptor MAPPING_FILE_REFRESH_INTERVAL = new PropertyDescriptor.Builder()
            .name("Mapping File Refresh Interval")
            .description("The polling interval in seconds to check for updates to the mapping file. The default is 60s.")
            .addValidator(StandardValidators.TIME_PERIOD_VALIDATOR)
            .required(true)
            .defaultValue("60s")
            .build();
    public static final PropertyDescriptor CHARACTER_SET = new PropertyDescriptor.Builder()
            .name("Character Set")
            .description("The Character Set in which the file is encoded")
            .required(true)
            .addValidator(StandardValidators.CHARACTER_SET_VALIDATOR)
            .defaultValue("UTF-8")
            .build();
    public static final PropertyDescriptor MAX_BUFFER_SIZE = new PropertyDescriptor.Builder()
            .name("Maximum Buffer Size")
            .description("Specifies the maximum amount of data to buffer (per file) in order to apply the regular expressions. If a FlowFile is larger "
                    + "than this value, the FlowFile will be routed to 'failure'")
            .required(true)
            .addValidator(StandardValidators.DATA_SIZE_VALIDATOR)
            .defaultValue("1 MB")
            .build();

    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("FlowFiles that have been successfully updated are routed to this relationship, as well as FlowFiles whose content does not match the given Regular Expression")
            .build();
    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("FlowFiles that could not be updated are routed to this relationship")
            .build();


    private List<PropertyDescriptor> properties;
    private Set<Relationship> relationships;

    private final ReentrantLock processorLock = new ReentrantLock();
    private final AtomicLong lastModified = new AtomicLong(0L);
    final AtomicLong mappingTestTime = new AtomicLong(0);
    private final AtomicReference<ConfigurationState> configurationStateRef = new AtomicReference<>(new ConfigurationState(null));

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> properties = new ArrayList<>();
        properties.add(DELIMITER);
        properties.add(MAPPING_FILE);
        properties.add(MAPPING_FILE_REFRESH_INTERVAL);
        properties.add(CHARACTER_SET);
        properties.add(MAX_BUFFER_SIZE);
        this.properties = Collections.unmodifiableList(properties);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return properties;
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relationships;
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        updateMapping(context);
        final List<FlowFile> flowFiles = session.get(5);
        if (flowFiles.isEmpty()) {
            return;
        }

        final ComponentLog logger = getLogger();

        final int maxBufferSize = context.getProperty(MAX_BUFFER_SIZE).asDataSize(DataUnit.B).intValue();

        for (FlowFile flowFile : flowFiles) {
            if (flowFile.getSize() > maxBufferSize) {
                session.transfer(flowFile, REL_FAILURE);
                continue;
            }

            final StopWatch stopWatch = new StopWatch(true);

            flowFile = session.write(flowFile, new ReplaceTextCallback(context, flowFile, maxBufferSize));

            logger.info("Transferred {} to 'success'", new Object[]{flowFile});
            session.getProvenanceReporter().modifyContent(flowFile, stopWatch.getElapsed(TimeUnit.MILLISECONDS));
            session.transfer(flowFile, REL_SUCCESS);
        }
    }

    private void updateMapping(final ProcessContext context) {
        if (processorLock.tryLock()) {
            final ComponentLog logger = getLogger();
            try {
                // if not queried mapping file lastUpdate time in
                // mapppingRefreshPeriodSecs, do so.
                long currentTimeSecs = System.currentTimeMillis() / 1000;
                long mappingRefreshPeriodSecs = context.getProperty(MAPPING_FILE_REFRESH_INTERVAL).asTimePeriod(TimeUnit.SECONDS);

                boolean retry = (currentTimeSecs > (mappingTestTime.get() + mappingRefreshPeriodSecs));
                if (retry) {
                    mappingTestTime.set(System.currentTimeMillis() / 1000);
                    // see if the mapping file needs to be reloaded
                    final String fileName = context.getProperty(MAPPING_FILE).getValue();
                    final File file = new File(fileName);
                    if (file.exists() && file.isFile() && file.canRead()) {
                        if (file.lastModified() > lastModified.get()) {
                            lastModified.getAndSet(file.lastModified());
                            try (FileInputStream is = new FileInputStream(file)) {
                                logger.info("Reloading mapping file: {}", new Object[]{fileName});

                                final LinkedHashMap<String, String> mapping = loadMappingFile(is);
                                final ConfigurationState newState = new ConfigurationState(mapping);
                                configurationStateRef.set(newState);
                            } catch (IOException e) {
                                logger.error("Error reading mapping file: {}", new Object[]{e.getMessage()});
                            }
                        }
                    } else {
                        logger.error("Mapping file does not exist or is not readable: {}", new Object[]{fileName});
                    }
                }
            } catch (Exception e) {
                logger.error("Error loading mapping file: {}", new Object[]{e.getMessage()});
            } finally {
                processorLock.unlock();
            }
        }
    }

    protected LinkedHashMap<String, String> loadMappingFile(InputStream is) throws IOException {
        LinkedHashMap<String, String> mapping = new LinkedHashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line = null;
        while ((line = reader.readLine()) != null) {
            final String[] splits = StringUtils.split(line, "\t ", 2);
            if (splits.length == 1) {
                mapping.put(splits[0].trim(), ""); // support key with empty value
            } else if (splits.length == 2) {
                final String key = splits[0].trim();
                final String value = splits[1].trim();
                mapping.put(key, value);
            }
        }
        return mapping;
    }

    public static class ConfigurationState {

        final LinkedHashMap<String, String> mapping = new LinkedHashMap<>();

        public ConfigurationState(final LinkedHashMap<String, String> mapping) {
            if (mapping != null) {
                this.mapping.putAll(mapping);
            }
        }

        public LinkedHashMap<String, String> getMapping() {
            return new LinkedHashMap<>(mapping);
        }

        public boolean isConfigured() {
            return !mapping.isEmpty();
        }
    }

    private final class ReplaceTextCallback implements StreamCallback {

        private final Charset charset;
        private final byte[] buffer;
        private final FlowFile flowFile;
        private String delimiter;

        private final AttributeValueDecorator quotedAttributeDecorator = new AttributeValueDecorator() {
            @Override
            public String decorate(final String attributeValue) {
                return Pattern.quote(attributeValue);
            }
        };

        private ReplaceTextCallback(ProcessContext context, FlowFile flowFile, int maxBufferSize) {
            this.flowFile = flowFile;
            this.charset = Charset.forName(context.getProperty(CHARACTER_SET).getValue());
            this.buffer = new byte[maxBufferSize];
            this.delimiter = String.valueOf(Utils.getDelimiter(context.getProperty(DELIMITER).evaluateAttributeExpressions(flowFile).getValue()));
        }

        @Override
        public void process(final InputStream in, final OutputStream out) throws IOException {

            final LinkedHashMap<String, String> mapping = configurationStateRef.get().getMapping();

            StreamUtils.fillBuffer(in, buffer, false);

            final int flowFileSize = (int) flowFile.getSize();

            final String contentString = new String(buffer, 0, flowFileSize, charset);

            String finalString = contentString;
            for (Map.Entry<String, String> entry : mapping.entrySet()) {
                String searchPattern = entry.getKey().replaceAll("DELIM", delimiter);
                String replacementPattern = entry.getValue().replaceAll("DELIM", delimiter);
                finalString = finalString.replaceAll(searchPattern, replacementPattern);
            }

            out.write(finalString.getBytes(charset));
        }
    }
}
