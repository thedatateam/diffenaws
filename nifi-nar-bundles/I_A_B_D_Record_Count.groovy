import IOUtils
import java.nio.charset.StandardCharsets
import java.io.*

 
flowFile = session.get()
if(!flowFile)return

def notBRecordsCount=0
def BRecordsCount=0
def IRecordsCount=0
def DRecordsCount=0
def ARecordsCount=0
def errorOccurred=''
def br
def aftervalue = flowFile.getAttribute("table.aftervalue.indicator");
def beforevalue = flowFile.getAttribute("table.beforevalue.indicator");
def insertvalue = flowFile.getAttribute("table.insertvalue.indicator");
def deletevalue = flowFile.getAttribute("table.deletevalue.indicator");

session.read(flowFile, {inputStream ->

    try{
        br=new BufferedReader(new InputStreamReader(inputStream));
        def line
        
        while ((line = br.readLine()) != null) {
			
			def operationType = line.split('\u0001')[2]
			
            if (((beforevalue != null && operationType == beforevalue)) || (operationType == 'B')) {
				BRecordsCount++ 
			}
			else if (((insertvalue != null && operationType == insertvalue)) || (operationType == 'I')) {
				IRecordsCount++;
				notBRecordsCount++;
			}
			else if (((deletevalue != null && operationType == deletevalue)) || (operationType == 'D')) {
				DRecordsCount++;
				notBRecordsCount++;
			}
			else if (((aftervalue != null && operationType == aftervalue)) || (operationType == 'A')) {
				ARecordsCount++;
				notBRecordsCount++;
        }
    }
	}
    catch (Exception e){
       errorOccurred=e.getMessage()
    }
    finally{
        br.close()
    }

});

flowFile = session.putAttribute(flowFile, 'total.row.count', String.valueOf(notBRecordsCount))
flowFile = session.putAttribute(flowFile, 'b.row.count', String.valueOf(BRecordsCount))
flowFile = session.putAttribute(flowFile, 'i.row.count', String.valueOf(IRecordsCount))
flowFile = session.putAttribute(flowFile, 'a.row.count', String.valueOf(ARecordsCount))
flowFile = session.putAttribute(flowFile, 'd.row.count', String.valueOf(DRecordsCount))

if(errorOccurred!='') {
  session.transfer(flowFile, REL_FAILURE)
}
else {
  session.transfer(flowFile, REL_SUCCESS)
}