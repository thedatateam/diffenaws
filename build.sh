#!/usr/bin/env bash

# Pre-requisites
# mvn and sbt is setup for the script to work

# Logging & status alert decorator around statement execution
function running() {
    echo $@
    eval $@
    if [[ $? -ne 0 ]]; then
        echo "$@ failed with a non-zero exception; aborting build process..."
        exit 2
    fi
}

echo "checking if mvn or sbt are missing..."
if [[ -z "$(command -v mvn)" || -z "$(command -v sbt)" ]]; then
    echo "maven or sbt not installed; aborting"
    exit 1
fi

running "mvn clean install -f nifi-nar-bundles/pom.xml"

running "pushd .; cd batchIngestion; sbt assembly; popd"

running "rm -r bundle"

running "mkdir -p bundle/nifi-nars; mkdir -p bundle/lib; mkdir -p bundle/scripts; mkdir -p bundle/conf"

running "cp nifi-nar-bundles/distribution/target/distribution-1.0.0-bin/*.nar bundle/nifi-nars"

running "cp batchIngestion/target/scala-2.11/IngestionProcessing-assembly-1.0.jar bundle/lib"

running "cp scripts/*.sh bundle/scripts"

running "cp conf/* bundle/conf"

echo "Build Complete"
