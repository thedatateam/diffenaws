# SCHEMA EVOLUTION

This docuemnt explains how diffen structure changes in source end is cascaded to the Data Lake.

## PREREQUISITES

1. A data mapping file should be present, which can be used for mapping source data types to hive data types. This file has to be referred using the diffen property diffen.config.source.datatype.mapping.path.
2. diffen.config.schema.evolution.class.name should contiane the  transformation class name. `i.e. in.tdt.diffen.processors.transform.FlatFileConfig2TableConfigTransformer`
2. Schema file should be of the format mentioned in the schema xml section.
3. Schema file name should be of the format <>_<>_<>_tableName_DI.xml.

## SCHEMA XML FORMAT

To trigger schema evolution a schema xml file must be placed in the incoming folder. The structure of schema file should be in the below format.

Table type can take values `Transaction` and `Manster`.

    <TableMapping table_type = "Transaction">
        <SourceColumn columnName = "id" dataType = "VARCHAR" selected="true" nullable="true" length="200" Primarykey="true" scale="0"/>
        <SourceColumn columnName = "message" dataType = "VARCHAR" selected="true" nullable="true" length="200" Primarykey="false" scale="0"/>
        <SourceColumn columnName = "value" dataType = "VARCHAR" selected="true" nullable="true" length="200" Primarykey="false" scale="0"/>
    </TableMapping>

## FLOW

Once the schema changes are detected, a schema file mentioned in the above format should be available in the incoming folder. Based on the file extension the schema evolution path is chosen. This logic can be found in the NiFi processor `RouteOnAttribute`.

![SChema Evolution Flow](img/schema_evolution_flow.png)

Schema evolution implementation can be found in the processor `Schema Evolution`.
![SChema Evolution Processor Group](img/schema_evolution_processor_group.jpg)

## VALIDATION

Upon successful schema evolution, updated schema for the affected tables can be found in the snapshot layer and in `diffen_common_metadata.diffen_allt_table_columns`.

![SChema Evolution Processor Group](img/diffen_all_tab_columns.jpg)