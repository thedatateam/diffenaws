# RERUN
To rebuild a snapshot in the HDR layer for specific tables, HDR layer can be used.

## TRIGGER RERUN
To initiate a rerun, the file to be processed again should be placed in the `rerun` folder.
![rerun folder](img/rerun_folder.png)

## NOTE
For hourly intra-day snapshot scenario, only the latest snapshot will be re-processed for the rerun file's date.