# DIFFEN ISNTALLATION

This document gives in detail the steps for installing diffen on a cluster of Azure VMs.

## SCRIPT INSTALLATION

Download diffen initi script from the url below
`https://diffenartifacts.blob.core.windows.net/artifacts/diffenInit.sh`

Execute the init script with the parameters given below.
`sh diffenInit.sh diffen-datalake <storage_Account_Name> <mysql_hostname> <azure_file_share_name> <zookeeper_id> <resource_group_location> <mysql_password>`

### NOTE

For Nifi cluster installation, the same script has to be run across all three machines. *Zookeeper_id* has to be update as the machine.

## MANUAL INSTALLATION

Install java 1.8.
`yum install -y java-1.8.0-openjdk-devel`

Install jq.
`yum install -y epel-release
yum install -y jq`

Install azure-cli.
`rpm --import https://packages.microsoft.com/keys/microsoft.asc
sh -c 'echo -e "[azure-cli]\nname=Azure CLI\nbaseurl=https://packages.microsoft.com/yumrepos/azure-cli\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/azure-cli.repo'
yum -y install azure-cli`

Create and mount Azure File Share

    fileShareExists=az storage share exists --name $AZURE_FILESHARE_NAME --account-name $STORAGE_ACCOUNT_NAME --account-key $STORAGE_ACCOUNT_PASSWORD | jq .exists
    [[ $fileShareExists =~ 'false' ]] && az storage share create --name $AZURE_FILESHARE_NAME --account-name $STORAGE_ACCOUNT_NAME --account-key $STORAGE_ACCOUNT_PASSWORD

    mkdir /mnt/diffen-share

    if [ ! -d "/etc/smbcredentials" ]; then
        mkdir /etc/smbcredentials
    fi

    if [ ! -d "/etc/smbcredentials" ]; then
        mkdir /etc/smbcredentials
    fi

    if [ ! -f "/etc/smbcredentials/$STORAGE_ACCOUNT_NAME.cred" ]; then
        echo "username=$STORAGE_ACCOUNT_NAME" >> /etc/smbcredentials/${STORAGE_ACCOUNT_NAME}.cred
        echo "password=$STORAGE_ACCOUNT_PASSWORD" >> /etc/smbcredentials/${STORAGE_ACCOUNT_NAME}.cred
    fi

    chmod 600 /etc/smbcredentials/${STORAGE_ACCOUNT_NAME}.cred

    echo "//${STORAGE_ACCOUNT_NAME}.file.core.windows.net/${AZURE_FILESHARE_NAME} /mnt/diffen-share cifs nofail,vers=3.0,credentials=/etc/smbcredentials/${STORAGE_ACCOUNT_NAME}.cred,dir_mode=0777,file_mode=0777,serverino" >> /etc/fstab

    mount -t cifs //${STORAGE_ACCOUNT_NAME}.file.core.windows.net/${AZURE_FILESHARE_NAME} /mnt/diffen-share -o vers=3.0,username=${STORAGE_ACCOUNT_NAME},password=${STORAGE_ACCOUNT_PASSWORD},dir_mode=0777,file_mode=0777,serverino

Download Diffen related binaries from the link below and extract the files to /opt/diffen
<https://diffenartifacts.blob.core.windows.net/artifacts/spark.tar.gz>
<https://diffenartifacts.blob.core.windows.net/artifacts/hadoop.tar.gz>
<https://diffenartifacts.blob.core.windows.net/artifacts/hive.tar.gz>
<https://diffenartifacts.blob.core.windows.net/artifacts/nifi.tar.gz>
<https://diffenartifacts.blob.core.windows.net/artifacts/databricks.tar.gz>
<https://diffenartifacts.blob.core.windows.net/artifacts/diffen.tar.gz>

Download necessary config files and scripts.
<https://diffenartifacts.blob.core.windows.net/artifacts/diffenInit.sh>
<https://diffenartifacts.blob.core.windows.net/artifacts/hadoop.sh>
<https://diffenartifacts.blob.core.windows.net/artifacts/nifi.properties>
<https://diffenartifacts.blob.core.windows.net/artifacts/core-site.xml>
<https://diffenartifacts.blob.core.windows.net/artifacts/hive-site.xml>
<https://diffenartifacts.blob.core.windows.net/artifacts/hiveserver2.service>
<https://diffenartifacts.blob.core.windows.net/artifacts/hiveserver2_service.sh>

Update hadoop `/opt/diffen/hadoop/etc/hadoop/core-site.xml` with Azure storage account key and password.

Update `/opt/diffen/hive/conf/hive-site.xml` with the node IP and metastore connection details.

Add databricks required param in the `/opt/diffen/databricks/set_spark_params.sh` file.

Add diffen environment variables to the `/opt/diffen/diffen/1.0.0/scripts/env.profile` file.

Add zookeeper id ot the `/opt/diffen/nifi/state/zookeeper/myid` file.

Enter the zookeeper quorum details to the `/opt/diffen/nifi/conf/nifi.properties` file and `/opt/diffen/nifi/conf/zookeeper.properties` file.

Crete metastore tables using hive schema tool `/opt/diffen/hive/bin/schematool -dbType mysql -initSchemaTo 2.2.0`

Copy hiveserver2 service scripts

    cp /opt/deploy/hiveserver2_service.sh /usr/bin/hiveserver2_service.sh
    cp /opt/deploy/hiveserver2.service /lib/systemd/system/hiveserver2.service
    systemctl enable hiveserver2.service

Start nifi and hiveserver2 service.
`service nifi start
service hiveserver2 start`