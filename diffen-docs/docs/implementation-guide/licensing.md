# LICENSING

Diffen's licensing model is based on the number of sources that gets onboarded.
License is provided for onboarding 10, 20, 30, 40 and 50 sources.

## IMPLEMENTATION

For every increamental of 10 sources unique license key is provided.
This key has to be avaiable as an environment variable `DIFFEN_SOURCE` during the time of deployment.
By default all diffen related environment will be maintained in `env.profile` as part of diffen scripts.

## INCREASING NUMBER OF SUBSCRIPTIONS

If a customer wants to increase the number of sources that can be onboarded, then customer has to contact the support and replace the license key for the number of source that the customer wants.
