#SNAPSHOT
Once a set of validated data for any particular day is available in staging layer, the data is converted to ORC format and stored in DIFFEN layer for consumption.

## CONFIGURATION
Snapshot for a source can be done hourly, daily or monthly.
This congiuration is specified in the source_country_param.xml.

`<property>
    <name>diffen.config.snapshotDate.format</name>
    <value>yyyy-MM-dd-hh</value>
</property>`

##SNAPSHOT TRIGGER
Snapshot gets triggered whenever a eod/snapshot file is sent. This eod file will contain the eod/snapshot marker as column. All records in the staging layer upto the marker time will be considered for snapshot.
###SNAPSHOT FILE CONTENTS
The table below gives a sample of how a EOD/snapshot table looks like in storage layer.

| rowid                                             | filename                        | c_journaltime       | c_transactionid | c_operationtype | c_userid | eod_date            | marker_time         | vds        |
| ------------------------------------------------- | ------------------------------- | ------------------- | --------------- | --------------- | -------- | ------------------- | ------------------- | ---------- | 
| 575485510700_d210f023-6d0f-43b7-bc37-5267b80780ec | EOD_MARKER.D20190319.T010000.R1 | 2019-03-19 01:59:00 | 1               | A               | abc      | 2019-03-19 02:00:00 | 2019-03-19 02:00:00 | 2019-03-19 | 

##SNAPSHOT PROCESS
On receiving the snapshot/eod marker a spark job gets triggered which creates the snapshot tables. The eod table table is used a reference for generating snapshot.
###EOD TABLE ENTRY
| source | country | markertime          | previousbusinessdate | businessdate  |
|--------|---------|---------------------|----------------------|---------------|
| diffen | all     | 2019-03-19 02:00:00 | 2019-03-09           | 2019-03-19-02 |
|        |         |                     |                      |               |
|        |         |                     |                      |               |

##SNAPSHOT TABLE
Snapshot tables can be found under the schema `source_country_diffen_open`. 
###SAMPLE DATA
| rowid                                             | s_startdt     | s_starttime         | s_enddt    | s_endtime           | s_deleted_flag | c_journaltime       | c_transactionid | c_operationtype | c_userid | id | message | ods           |
|---------------------------------------------------|---------------|---------------------|------------|---------------------|----------------|---------------------|-----------------|-----------------|----------|----|---------|---------------|
| 575456418100_fc65c147-fae7-45da-a072-86de86701608 | 2019-03-19-02 | 2019-10-10 20:27:05 | 9999-12-31 | 9999-12-31 00:00:00 | 0              | 2019-03-19 01:59:00 | 1               | I               | abc      | 1  | 1rerun  | 2019-03-19-02 |
| 575456578900_7bd628ab-0357-4ed0-81f2-e1b5425e4886 | 2019-03-19-02 | 2019-10-10 20:27:05 | 9999-12-31 | 9999-12-31 00:00:00 | 0              | 2019-03-19 01:59:00 | 1               | I               | abc      | 2  | 2       | 2019-03-19-02 |
| 575456594300_b7316a12-fec1-43e4-af9c-ce7de3f6b5a6 | 2019-03-19-02 | 2019-10-10 20:27:05 | 9999-12-31 | 9999-12-31 00:00:00 | 0              | 2019-03-19 01:59:00 | 1               | I               | abc      | 3  | 3       | 2019-03-19-02 |

All closed records cab be found i tables under the schema  `source_country_diffen_non_open`.