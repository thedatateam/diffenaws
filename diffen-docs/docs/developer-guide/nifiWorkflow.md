# NIFI WORKFLOW

This document explains the Diffen Nifi workflow.

The steps that are involved as part of the nifi workflow are listed below.

## NORMAL FLOW

A normal run for a file that has to be ingested in data lake using diffen arrives in incoming folder and ends in archive location.

- Watch for new files in incoming folder.
![watch incoming data](img/watch_incoming_data.png)
- Fetch global properties from the `source_all_param.xml` file.
![fetch global parameters](img/fetch_global_parameters.png)
- Execute groovy script to fetch the date to which the file belongs to.
![fetch file data](img/fetch_file_date.png)
- Fetch table properties from the `source_tables_config.xml` file.
![retrieve table properties](img/retrieve_table_properties.png)
- Maintain a copy of the file as it is in hdfs landing directory.
![hdfs landing file](img/hdfs_landing.png)
- Prepare the incoming file for validation by cleansing extra new line characters, converting double to single quotes and removing backslashes.
![file cleansing](img/file_cleansing.png)
- Check if the record count and number of rows in the incoming file are the same.
![row count check](img/row_count_check.png)
- If the counts don't match then the files will be rejected.
![reject storage](img/reject_storage.png)
- If the counts match, the rowid, filename and partition name are tagged to all records and an entry is made to rowhistory table.
![prepend rowid](img/prepend_rowid.png)
![prepend rowdid pg](img/prepend_rowid_pg.png)
- Data type validation takes place for all records based on the schema available in the tables_config.xml.
![data type verification](img/data_type_verification.png)
- Any records that get rejected during the schema validation, will be pushed to the invalid types.
![invalid data](img/invalid_data.png)
- Check if the file incoming file is an eod file.
![eod file check](img/is_marker_file.png)
- If file is or not eod file, the file gets pushed into storage layer in avro format.
![load_verify_types](img/load_verify_types.png)
- A entry is made in the process metadata table for the file that got loaded into verify types.
![process metadata entry](img/process_metadata_entry.png)
- Finally, the incoming file gets moved to the archive folder.
![move to archive](img/save_to_archive.png)

## RERUN FLOW

To recompute a snapshot for a particular file, the rerun flow can be triggered.

- Rerun flow starts with a file watcher on the rerun folder.
![Rerun file watcher](img/rerun_file_watcher.png)
- It takes the regular flow until the processor where the file gets loaded to the storage layer.
![load verify types](img/load_verify_types.png)
- If the incoming file's base path is from rerun then it gets routed for rerun snapshot process.
![rerun routing](img/rerun_routing.png)
- Hive query is run to fetch the eod_marker for the file's date.
![rerun select eod marker](img/rerun_select_eod_marker.png)
- Trigger snapshot processing for the rerun table and eod maker.
![rerun spark submit](img/rerun_spark_submit.png)

## SNAPSHOT FLOW

HDR(History Data Reply) layer holds snapshot of all tables.
Snapshot process is a spark application which is gets triggered on an `eod file` arrival.

![snapshot processing](img/snapshot_processing.png)
