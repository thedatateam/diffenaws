# DEPLOYMENT

Diffen deployment is done using a spark application. To onboard a source Diffen Deployer application has to be invoked.

The responsibilities of the Deployment application are as below.

1. Create local diffen directories
2. Create diffen hdfs directories
3. Create hive schema and tables for source
4. Upload nifi template

## PREREQUISITE

- `<source>_<country>_param.xml` Global configuration xml.
- `<source>_<country>_tables_config.xml` Table schema configuration xml.
- `<source>_<country>_tables_rename.xml` Table rename xml.

## STEPS

Before onboarding a source the prerequisite services should be installed and running.

- Copy the `bundle` folder that got created to the machine where diffen will be deployed.
- Copy the xml file mentioned in the prerequisite to the `conf` folder.
- Trigger the diffen-deployer script with the necessary parameters. `diffen-deployer.sh --source <source> --country <country> --nifi-username <nifi-username> --nifi-authentication-enabled --nifi-url <nifi-api-url> --config-location <diffen_home>/conf --distributed-map-cache-port <port>`

## VERIFICATION

On successful onboarding nifi workflow will be uploaded successfully ready for processing.
![nifi workflow](img/nifi_workflow.png)