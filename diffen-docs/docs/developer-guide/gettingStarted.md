# GETTING STARTED

This document explains how a contributor can start contribute to diffen.

## REPO URL

Clone project from repo using the command below.

`git clone https://username_tdt@bitbucket.org/thedatateam/diffen.git`

## PREREQUISITE

- Hadoop 2.7.3
- Hive till 2.2
- hiveserver 2
- nifi 1.2.0
- spark 2.3.0

## BUILD

To trigger the build run the `build.bat` file from the root folder, which will create a bundle folder under which the required builds will be present.
![bundle folder structure](img/bundle_folder_structure.png)

## CODE STRUCTURE

![Diffen code file structure](img/code_structure.png)

### NIFI NAR BUNDLER

Nifi related code are present in the `nifi-nar-bundles` folder.

![Nifi nar bundle folder structure](img/nifi_nar_bundle.png)

All custom processors are available under the nifi-tdt-diffen-bundle folder.

### BATCH INGESTION

Code related to deployment and snapshot processing are available inside the `batchIngestion` folder.