# NIFI SPECIFICATION

This document explains about the specification for NiFi workflows in Diffen.

## PURPOSE

The pre-processing activities that are required for difffen before the snapshot is take are done through nifi.

## NIFI CUSTOM PROCESSORS

All nifi custom processors are available under the `nifi-tdt-diffen-bundle` directory.

![Nifi Processors](img/nifi_processors.png)