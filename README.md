# README #

This is the DIFFEN code-base which contains different modules for DIFFEN framework.

Please refer [Documentation](docs/diffenDocumentation.md) for details on DIFFEN framework.

### What is this repository for? ###

* Contains code artifacts related to DIFFEN ingestion framework
* Version 1.0.0

### Who do I talk to? ###

TBC

# Pre-requisites

| Software      |  Version          |
| ----- | ---------- |
| HDP   | 2.4        |
| HDF   | 3.0.1      |
| Spark | 2.3.0     |
| Java  | 1.8.0\_102 |
| Maven | 3.x        |
| SBT   | 0.13.x     |

# DIFFEN Build

1. **Download the code base from [DIFFEN Github](https://github.com/SCDIFFEN/DIFFEN/) Page**.

In the root folder, you will see, ```build.bat``` and ```build.sh```.
Based on the operating system you are, you can use the corresponding script to do the build.

The script will build the nifi-nars and batch-ingestion projects and create bundle folder on root.

The bundle can be zipped and used an artefact for deployment. The bundle will have following folder structure.
```

├── bundle
│   ├── conf
│   │   └── diffen-environment.properties
│   ├── lib
│   │   └── IngestionProcessing-assembly-1.0.jar
│   ├── nifi-nars
│   │   ├── nifi-data-cleansing-nar-1.0.0.nar
│   │   ├── nifi-distributed-cache-services-nar-1.2.0.nar
│   │   ├── nifi-hadoop-nar-1.2.0.nar
│   │   ├── nifi-record-serialization-services-nar-1.2.0.nar
│   │   ├── nifi-scb-edmhdpif-nar-1.0.0.nar
│   │   ├── nifi-standard-nar-1.2.0.nar
│   │   └── nifi-standard-services-api-nar-1.2.0.nar
│   └── scripts
│       ├── env.profile
│       ├── move.sh
│       ├── nifi-deployer.sh
│       ├── schema_evolution_trigger.sh
│       ├── diffen-deployer.sh
│       └── SparkSubmit.sh

```

# DIFFEN Deployment

**Step 1: Unpack DIFFEN Bundle**

Download bundle.zip on linux box, created by build process mentioned earlier, and unzip at desired location.

Sample Unzip Location: ```/home/diffenadmin/deployment/```

**Step 2: Update nifi.properties**

Add below line to nifi.properties.
```
nifi.variable.registry.properties=/home/diffenadmin/deployment/current/conf/diffen-environment.properties
```

**Step 3: Change permissions DEPLOYMENT/\<version\> directory**

```
chmod -R 755 /home/diffenadmin/deployment/1.0.0
```

**Step 4: Update current to point to new version**

```
cd /home/diffenadmin/deployment/
ln -s current 1.0.0
```

**Step 5: Update env.profile**

```
File: /home/diffenadmin/deployment/current/scripts/env.profile

export SPARK_HOME=
export DIFFEN_HOME=
export DIFFEN_VERSION=
export NIFI_HOME=
```

**Step 6:** **Nifi components deployment**

user-id: ```nifi```
```
/home/diffenadmin/deployment/current/scripts/nifi-deployer.sh
```

## Rollback Steps

**Step 1: Revert the current location to previous version**
```
cd /home/diffenadmin/deployment
ln -s current <previous-version>
```

**Step 2: Nifi Component roll back**

user-id: ```nifi```
```
/home/diffenadmin/deployment/current/scripts/nifi-deployer.sh
```

# Source - Country Deployment

**Step 1: Backup existing configurations.**
 ```
 mv /user/corebanking/DIFFEN/crbnk/in/config /user/corebanking/DIFFEN/crbnk/in/config.bkp.<date>
 ```

**Step 2: Clean nifi instance**

 Delete existing Nifi process group DIFFEN-crbnk-in. Before deleting stop the DIFFEN-crbnk-in under DIFFEN-crbnk process group -*Refer image below*

![](docs/media/ProcessGroup.png)

**Step 3: Configuration setup**

Copy following files in ```/home/diffenadmin/deployment/current/conf``` directory

```
1.  crbnk-in-param.xml
2.  crbnk-in-tables-config.xml
3.  crbnk-in-tables-rename.xml
```

**Step 4:RUN ```diffen-deployer.sh```**

The script will create the necessary local directories, HDFS directories and place the table-config xmls in the local and the HDFS locations. The script will also instantiate the template in nifi and start the necessary controller services.

```
/home/diffenadmin/deployment/current/scripts/diffen-deployer.sh --source crbnk --country in \
--nifi-username <nifi-username> --nifi-authentication-enabled  --nifi-url <nifi-api-url> \
--config-location /home/diffenadmin/deployment/current/conf --distributed-map-cache-port <port>
```

**Step 5 Access Provisions**

For the below directories, please provide 775 access with ```nifi``` as the owner,

```
/mnt/diffen-share/deployment/diffen/all/incoming
/mnt/diffen-share/deployment/diffen/all/rerun
/mnt/diffen-share/deployment/diffen/all/archival
/mnt/diffen-share/deployment/diffen/all/reject
/mnt/diffen-share/deployment/diffen/all/error
/mnt/diffen-share/deployment/diffen/all/appl
/mnt/diffen-share/deployment/diffen/all/config
```

