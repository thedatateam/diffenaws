# TDT: Ingestion Framework

## Standard Processing Patterns

1) Master  
2) Delta  
3) Events  

## Features

1) Data Quality Checks - Data type and Constraint validation  
2) Snapshot Creation at a given point of time
3) Container Level Validation  
    - File Rowcount validation  
    - File Name format validation (`<table_name>.D<business_date>.T<time>.R<row_count>.<file_format>`)  
4) Deduplicate Records  
5) Lineage, Audit, Operation Metadata  
6) Column Data Transformations
7) Late and Early arrivals, (Requires a time column)  

## Supported Data formats

1) IBM CDC  
2) Event Sourced Data (CDC generalized)  
3) Extracted Data (JDBC)

## Stages in framework 

1) Pre Processing  
    - Container Level Validation  
2) Staging Layer  
    - Data Quality Check  
    - Constraints Check  
3) Standardization Layer  
    - Deduplication  
    - Diffen Layer (Daily Snapshot Creation)  
    

---

# Bootstrap Framework 

1) Environment Setup
2) Framework Setup
3) On-Board Source  


### Environment Setup

1) Cluster creation  
2) Nifi/Stream Sets Application setup  
3) Framework deployment  
    - Copy the framework code in new file version  

### Framework Setup

1) Source level configs - Schemas, Key columns, table type (delta/txn) table etc  
2) Framework level configs - Input/Output file formats, cluster configs, paths for deployment etc  


### On-Boarding Source

1) Creates Storage Layer and Snapshot layer hive table based on source configurations  
2) Lineage, Audit, Operations Metadata table creations  
3) Creating necessary file system directories based on framework and source configs  
4) Creates Nifi Template based on framework config  
 
---
 
# Demo

### Scenarios

1) New Source

    - Environment setup
    - Create Source level configs
    - On-Board Source
    
2) Initial Load

    - Load files to Storage layer
    - Create Snapshot layer
    
3) Daily Load

    - EOD load
    
        - File with data (valid data) - fact_loan.D20181030.T023000.R150
        - File Row count mismatch - lea_agreemnet_dtl.D20181030.T023000.R100
        - txn table - lea_nbfc_pmnt_dtl.D20181029.T023000.R49998
    
    - Intraday load
        
        - File with duplicates - lea_repaysch_dtl.D20181030.T*.R100
        - Invalid Types, Warn Types - txntemp_vfdt_vw_oth1.D20181029.T023000.R100