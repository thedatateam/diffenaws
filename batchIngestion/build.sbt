name := "IngestionProcessingV2"

version := "1.0"

scalaVersion := "2.11.8"

val sparkVersion = "2.4.3"

val hadoopVersion = "2.10.0"

headerLicense := Some(HeaderLicense.ALv2("2019", "The Data Team"))

test in assembly := {}

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion % Compile exclude("org.scalatest", "scalatest_2.11"),
  "org.apache.spark" %% "spark-sql" % sparkVersion % Compile,
  "org.apache.spark" %% "spark-hive" % sparkVersion,
  "org.apache.hadoop" % "hadoop-client" % hadoopVersion % Compile excludeAll ExclusionRule(organization = "javax.servlet"),
  "org.apache.hadoop" % "hadoop-hdfs" % hadoopVersion % Compile excludeAll ExclusionRule(organization = "javax.servlet"),
  "org.rogach" %% "scallop" % "3.1.1",
  "com.amazonaws" % "aws-java-sdk" % "1.7.4",
  "org.apache.hadoop" % "hadoop-aws" % hadoopVersion,
  "mysql" % "mysql-connector-java" % "8.0.11",
  "org.scala-lang" % "scala-reflect" % "2.11.8",
  "com.squareup.okhttp" % "okhttp" % "2.7.5",
  "com.typesafe" % "config" % "1.3.3",
  "org.apache.spark" %% "spark-avro" % sparkVersion,
  "com.holdenkarau" %% "spark-testing-base" % "2.2.0_0.8.0" % "test",
  "org.scalacheck" %% "scalacheck" % "1.12.6" % "test" withSources() withJavadoc(),
  "com.fortysevendeg" %% "scalacheck-datetime" % "0.2.0" %"test" exclude("org.scalacheck", "scalacheck") withSources() withJavadoc()
)

resolvers ++= Seq(
  "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
  "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "Maven Central" at "https://repo1.maven.org/maven2/",
  Resolver.sonatypeRepo("releases")
)

val currentTime = System.currentTimeMillis()

//assemblyJarName in assembly := s"${name.value}-${version.value}-$currentTime.jar"

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

//skipping slf4j jars during assembly to avoid clash during azure deployment
assemblyExcludedJars in assembly := {
  val cp = (fullClasspath in assembly).value
  cp filter { f =>
    f.data.getName.contains("slf4j")
  }
}

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case _ => MergeStrategy.first
}

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-target:jvm-1.7", "-Ywarn-unused-import")
scalacOptions in (Compile, doc) ++= Seq("-unchecked", "-deprecation", "-diagrams", "-implicits")

fork in Test := true
javaOptions ++= Seq("-Xms2g", "-Xmx3g", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")
parallelExecution in Test := true
