import java.io.File._
import java.util.{Date, Properties}

import DiffenJobGlue.getSecret
import com.amazonaws.services.glue.GlueContext
import com.amazonaws.services.glue.util.GlueArgParser
import com.amazonaws.services.secretsmanager.AWSSecretsManager
import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.DiffenConstants._
import com.tdt.diffen.utils.SchemaUtils.{AllTables, ConfigHistory}
import com.tdt.diffen.utils.XmlParser.{AllTabColumn, TableConfig}
import com.tdt.diffen.utils._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, FileUtil, Path}
import org.apache.log4j.Logger
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.joda.time.{DateTime, Days}
import org.joda.time.format.DateTimeFormatter
import com.amazonaws.services.secretsmanager._
import com.amazonaws.services.secretsmanager.model._
import org.json.JSONObject

import scala.collection.mutable.ListBuffer
import scala.util.Try

/**
  * Entry point of the Schema Evolution process. The primary function of this class is to compare the old and the new schema
  * and retrofit data, if needed, for a specific window.
  * Handles Addition and Deletion of columns in the beginning, middle and end. Does not support renaming of columns.
  *
  */
object SchemaEvolutionV2Glue {

  val logger: Logger = Logger.getLogger(SchemaEvolutionV2Glue.getClass)

  def main(args: Array[String]): Unit = {

    logger.info(s"Start time of the Job ${DateTime.now()}")
    logger.info("Spark Driver arguments - " + args.mkString(" | "))

    val secretName: String = "rdsPasswordMysql5"
    val region: String = "ap-south-1"
    val rdsEndPoint: String = "cebu-mysql5.cr6rwvusygkg.ap-south-1.rds.amazonaws.com"

    var startMarker = ""
    var endMarker = ""
    var tableNames = List[String]()
    var generateBackwardCompatibleTableConfig = false

    if (args.contains(s"--startMarker")){
      startMarker = GlueArgParser.getResolvedOptions(args, Array("startMarker"))("startMarker")
    }
    if (args.contains(s"--endMarker")){
      endMarker = GlueArgParser.getResolvedOptions(args, Array("endMarker"))("endMarker")
    }
    if (args.contains(s"--tableNames")){
      tableNames = GlueArgParser.getResolvedOptions(args, Array("tableNames")).values.toList
    }
    if (args.contains(s"--generateBackwardCompatibleTableConfig")){
      generateBackwardCompatibleTableConfig = GlueArgParser.getResolvedOptions(args,
        Array("generateBackwardCompatibleTableConfig"))("generateBackwardCompatibleTableConfig").toBoolean
    }

    val argsMap: Map[String, String] = GlueArgParser.getResolvedOptions(args,
      Array("paramConfigPath", "newTableConfigPath"))

    val paramConfigPath = argsMap.get("paramConfigPath").get.trim
    val newTableConfigPath = argsMap.get("newTableConfigPath").get.trim

    val extraSparkConfMap = Map (
      "spark.sql.catalogImplementation" -> "hive",
      "spark.app.name" -> "Schema Evolution",
      "spark.sql.sources.partitionOverwriteMode" -> "DYNAMIC",
      "spark.hadoop.javax.jdo.option.ConnectionURL" -> s"jdbc:mysql://${rdsEndPoint}:3306/metastore3",
      "spark.hadoop.javax.jdo.option.ConnectionUserName" -> "admin",
      "spark.hadoop.javax.jdo.option.ConnectionPassword" -> getSecret(secretName, region, "javax.jdo.option.ConnectionPassword"),
      "spark.hadoop.javax.jdo.option.ConnectionDriverName" -> "com.mysql.jdbc.Driver"
    )

    val sparkConf = new SparkConf()
    sparkConf.setAll(extraSparkConfMap)
    val sc: SparkContext = new SparkContext(sparkConf)

    val properties = new Properties()
    extraSparkConfMap.foreach{
      case (x, y) => properties.setProperty(x, y)
    }

    val glueContext: GlueContext = new GlueContext(sc)
    glueContext.setConf(properties)
    val spark = glueContext.getSparkSession

    val fs: FileSystem = new Path(paramConfigPath).getFileSystem(spark.sparkContext.hadoopConfiguration)
    FileSystemUtils.setFileSystem(fs)

    val paramConf = DiffenUtils.getParamConf(fs, paramConfigPath)
    val diffenParams: DiffenParams = DiffenParams(paramConf)

    val newTableConfigs = getTableConfigs(fs, newTableConfigPath, tableNames)
    val oldTableConfigs = getTableConfigs(fs, diffenParams.tableConfigXml, tableNames).map(x => {(x.name, x)}).toMap

    val processedTableConfigs = processUpdatedTableConfigs(newTableConfigs, oldTableConfigs, diffenParams)

    val evolutionTableConfigs = processedTableConfigs.filter(_.isSchemaEvolutionRequired)
    updateSchema(evolutionTableConfigs, getPartitions(startMarker, endMarker, diffenParams.getSnapshotFormat), newTableConfigs, oldTableConfigs, fs, paramConf, diffenParams)(spark)

    if(generateBackwardCompatibleTableConfig)
      constructNewMonolithicTableConfig(fs, diffenParams.tableConfigXml, newTableConfigPath, tableNames)
    else
      constructNewIndividualTableConfigs(fs, diffenParams.tableConfigXml, newTableConfigPath, evolutionTableConfigs.map(_.tableConfig.name))
  }

  /**
    * Get the value for the secretName passed using the key.
    */

  def getSecret(secretName: String, region: String, key: String) = {
    val client: AWSSecretsManager = AWSSecretsManagerClientBuilder.standard().withRegion(region).build();
    var secret = ""
    var decodedBinarySecret = ""
    val getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName)
    var getSecretValueResult: GetSecretValueResult  = null;

    try {
      getSecretValueResult = client.getSecretValue(getSecretValueRequest)
    } catch {
      case e: InternalServiceErrorException => println("InternalServiceErrorException")
      case e: InvalidParameterException => println("InvalidParameterException")
      case e: InvalidRequestException => println("InvalidRequestException")
      case e: InternalServiceErrorException => println("InternalServiceErrorException")
    }

    if (getSecretValueResult.getSecretString() != null) {
      secret = getSecretValueResult.getSecretString();
    }
    val jsonObj = new JSONObject(secret)
    val pass = jsonObj.getString(key)
    pass
  }

  private def getTableConfigs(fs: FileSystem, tableConfigPath: String, tableNames: List[String]) = {
    XmlParser.parseXmlConfig(tableConfigPath, fs).filter(x=> tableNames.isEmpty || tableNames.contains(x))
  }

  def constructNewMonolithicTableConfig(fs: FileSystem, tableConfigPath: String, newTableConfigPath: String, tableNames: List[String]) = {
    logger.info(s"Generating table configs in location $tableConfigPath")
    val oldTableConfigProperties = XmlParser.getConfs(fs, tableConfigPath)
    val newTableConfigProperties = XmlParser.getConfs(fs, newTableConfigPath).filter(x=> tableNames.isEmpty || tableNames.contains(x._1.replace(PrefixConfigDiffenTable, "").split('.')(0)))
    val updatedTableConfigs = oldTableConfigProperties ++ newTableConfigProperties

    val newTableConfigContent = "<configuration>\n" +
      updatedTableConfigs.toSeq.sortBy(_._1).map( prop =>
        "<property>\n" +
          s"\t<name>${prop._1}</name>\n" +
          s"\t<value>${prop._2}</value>\n" +
          "</property>\n").mkString("") +
      "</configuration>"
    fs.rename(new Path(tableConfigPath), new Path(tableConfigPath + "_bkp_" + new Date().getTime))
    logger.info(s"Generating table config: $tableConfigPath.\n Content: $newTableConfigContent")
    Ops.createHDFSFile(fs, tableConfigPath, newTableConfigContent)
  }

  def constructNewIndividualTableConfigs(fs: FileSystem, tableConfigPath:String, newTableConfigPath: String, tableNames: List[String])  = {
    val updatedTableConfigs = XmlParser.getConfs(fs, newTableConfigPath)
      .map(x=> (x._1.replace(PrefixConfigDiffenTable, "").split('.')(0), x._1, x._2)).groupBy(_._1)
      .filter(x=> tableNames.isEmpty || tableNames.contains(x._1))

    logger.info(s"Generating table configs in location $tableConfigPath for table names : ${updatedTableConfigs.map(_._1)}")

    updatedTableConfigs.map( prop => {
      val newTableConfigContent = "<configuration>\n" + prop._2.toSeq.sortBy(_._2).map(attribute =>
        "<property>\n" +
          s"\t<name>${attribute._2}</name>\n" +
          s"\t<value>${attribute._3}</value>\n" +
          "</property>\n").mkString("\n") +
        "</configuration>"
      //Replace old config file with new one
      val tableConfigFileName = tableConfigPath + separator + prop._1 + "_tables_config.xml"
      logger.info(s"Generating table config: $tableConfigFileName.\n Content: $newTableConfigContent")
      fs.rename(new Path(tableConfigFileName), new Path(tableConfigFileName + "_bkp_" + new Date().getTime))
      Ops.createHDFSFile(fs, tableConfigFileName, newTableConfigContent)
    })
  }

  def getInScopePartitions(spark: SparkSession, database: String, tableName: String, partitionColumnName: String, allPartitions: List[String], isRenameRequired: Boolean = false): List[String] = {
    val existingPartitions = Try(spark.sql(s"show partitions $database.$tableName").collect().map(r => r.getString(0).replace(s"$partitionColumnName=", "")).toList).getOrElse(List())
    val partitionsInScope = existingPartitions.filter(x=> allPartitions.isEmpty || allPartitions.contains(x))
    if(!isRenameRequired) {
      partitionsInScope
    } else {
      partitionsInScope.sortWith(_ > _).slice(0, 4)
    }
  }

  /**
    * Modifies the Hive table structure and retrofits the data across Diffen open, NonOpen and storage for all the modified tables
    *
    * @param modifiedTables     List of [[TableConfig]] for all modified tables
    * @param allPartitions      List of affected partitions
    * @param diffenParams          Environment parameters in the form of [[DiffenParams]]
    * @param newTableConfigs    List of new [[TableConfig]] generated from NiFi
    * @param oldTableConfigsMap List of old [[TableConfig]] that was used before the Schema evolution
    */
  private def updateSchema(modifiedTables: List[SchemaEvolutionTableConfig], allPartitions: List[String], newTableConfigs: List[TableConfig], oldTableConfigsMap: Map[String, TableConfig], fs: FileSystem, paramConf: Configuration, diffenParams: DiffenParams)(spark: SparkSession) = {

    val isCdcSource = diffenParams.isCdcSource
    val hdfsBaseDir = diffenParams.hdfsBaseDir
    val hdfsTmpDir = diffenParams.hdfsTmpDir

    val allHiveQueries = ListBuffer[String]()
    modifiedTables.foreach {
      seTableConfig =>
        val hiveQueries = ListBuffer[String]()

        val tableConfig = seTableConfig.tableConfig
        val tableName = tableConfig.name

        logger.info (s"Schema evolution in progress for : $seTableConfig")

        val diffenOpenSchema = diffenParams.diffenOpenSchema
        val diffenNonOpenSchema = diffenParams.diffenNonOpenSchema
        val verifyTypeSchema = diffenParams.verifyTypesSchema

        val diffenOpenPartitionColumn = diffenParams.diffenOpenPartitionColumn
        val diffenNonOpenPartitionColumn = diffenParams.diffenNonOpenPartitionColumn
        val verifyTypesPartitionColumn = diffenParams.verifyTypesPartitionColumn

        val inScopeDiffenOpenPartitions = getInScopePartitions(spark, diffenOpenSchema, tableName, diffenOpenPartitionColumn, allPartitions, seTableConfig.isRenameRequired)
        val inScopeNonDiffenOpenPartitions = getInScopePartitions(spark, diffenNonOpenSchema, tableName, diffenNonOpenPartitionColumn, allPartitions, seTableConfig.isRenameRequired)
        val inScopeVerifyTypePartitions = getInScopePartitions(spark, verifyTypeSchema, tableName, verifyTypesPartitionColumn, allPartitions)

        if(seTableConfig.isDataRetrofitRequired) {

          //Retrofit data for Diffen Open
          evolveSchemaForData(spark, tableName, inScopeDiffenOpenPartitions, hdfsBaseDir, hdfsTmpDir,
            DiffenUtils.fetchDiffenStructure(oldTableConfigsMap(tableName), diffenOpenPartitionColumn, isCdcSource),
            DiffenUtils.fetchDiffenStructure(tableConfig, diffenOpenPartitionColumn, isCdcSource),
            diffenOpenSchema, diffenOpenPartitionColumn, OrcFormat)

          //Retrofit data for Diffen Non Open
          evolveSchemaForData(spark, tableName, inScopeNonDiffenOpenPartitions, hdfsBaseDir, hdfsTmpDir,
            DiffenUtils.fetchDiffenStructure(oldTableConfigsMap(tableName), diffenNonOpenPartitionColumn, isCdcSource),
            DiffenUtils.fetchDiffenStructure(tableConfig, diffenNonOpenPartitionColumn, isCdcSource),
            diffenNonOpenSchema, diffenNonOpenPartitionColumn, OrcFormat)

          //Retrofit data for Storage
          evolveSchemaForData(spark, tableName, inScopeVerifyTypePartitions, hdfsBaseDir, hdfsTmpDir,
            DiffenUtils.fetchVerifyStructure(oldTableConfigsMap(tableName), verifyTypesPartitionColumn, isCdcSource),
            DiffenUtils.fetchVerifyStructure(tableConfig, verifyTypesPartitionColumn, isCdcSource),
            verifyTypeSchema, verifyTypesPartitionColumn, AvroFormat)

          /*
           * Scenario 3: Delete Column from table (true, true, true)
           * Scenario 6: Datatype change for column in table (true, true, true)
           */
          if(seTableConfig.isRenameRequired) {
            hiveQueries ++= moveTable(fs, spark, hdfsBaseDir, diffenOpenSchema, tableName, tableName+"_"+new Date().getTime, diffenOpenPartitionColumn)
            hiveQueries ++= moveTable(fs, spark, hdfsBaseDir, diffenNonOpenSchema, tableName, tableName+"_"+new Date().getTime, diffenNonOpenPartitionColumn)
          }
        }

        hiveQueries ++= SchemaUtils.getBusinessTablesSchema(tableConfig, diffenParams)

        hiveQueries ++= inScopeDiffenOpenPartitions.map(p=>createHivePartition(diffenOpenSchema, tableName, s"$diffenOpenPartitionColumn ='$p'"))
        hiveQueries ++= inScopeNonDiffenOpenPartitions.map(p=>createHivePartition(diffenNonOpenSchema, tableName, s"$diffenNonOpenPartitionColumn ='$p'"))
        hiveQueries ++= inScopeVerifyTypePartitions.map(p=>createHivePartition(verifyTypeSchema, tableName, s"$verifyTypesPartitionColumn ='$p'"))

        allHiveQueries ++= hiveQueries

        hiveQueries.foreach(sql=> {
          logger.info(sql)
          spark.sql(sql)
        })

        if(seTableConfig.isDataRetrofitRequired) {
          moveHDFSFiles(fs, new Path(hdfsTmpDir + separator + diffenOpenSchema + separator + tableName), new Path(hdfsBaseDir + separator + diffenOpenSchema + separator + tableName))
          moveHDFSFiles(fs, new Path(hdfsTmpDir + separator + diffenNonOpenSchema + separator + tableName), new Path(hdfsBaseDir + separator + diffenNonOpenSchema + separator + tableName))
          moveHDFSFiles(fs, new Path(hdfsTmpDir + separator + verifyTypeSchema + separator + tableName), new Path(hdfsBaseDir + separator + verifyTypeSchema + separator + tableName))
        }

        logger.info (s"Schema evolution compeleted for : $tableName")
    }

    populateCommonMetadata(modifiedTables.map(_.tableConfig), newTableConfigs, diffenParams, spark)
    Ops.createStatusFile(fs, paramConf, diffenParams, "global", s"SCHEMA_EVOLUTION_HIVE_QUERIES_${modifiedTables.map(_.tableConfig.name).mkString("_")}", allHiveQueries.mkString(";\n")+";")
  }

  /** Updates the metadata tables with the updated version of the schema - [[AllTables]] [[AllTabColumn]] and [[ConfigHistory]] */
  private def populateCommonMetadata(modifiedTables: List[TableConfig], newTableConfigs: List[TableConfig], diffenParams: DiffenParams, spark: SparkSession): Unit = {

    val df1 = spark.read.format("orc").load(s"${diffenParams.hdfsBaseDir}${diffenParams.metadataSchema}/diffen_all_tables/${diffenParams.source}_${diffenParams.country}_diffen_all_tables").cache
    val tableNameWdVersion: Map[String, Int] = df1.groupBy("tableName").max("version").collect.map(row => row.getAs[String](0) -> row.getAs[Int](1)).toMap

    val configs = ListBuffer[ConfigHistory]()
    val allTables = ListBuffer[AllTables]()
    val allTabColumns = ListBuffer[AllTabColumn]()

    newTableConfigs.foreach { tableConfig =>

      val tableName: String = tableConfig.name.replace(s"${diffenParams.source}_${diffenParams.country}_", "")
      val latestVersion = tableNameWdVersion.getOrElse(tableName, 1) + 1
      val currentTime = nowAsString()
      val allTabColumn = XmlParser.getAllTabColumns(currentTime, diffenParams.source, diffenParams.country, tableConfig, latestVersion)
      val diffenAllTables = List(AllTables(diffenParams.country, tableName, diffenParams.source, tableConfig.sourceType, "0p", "", "", "", "", latestVersion, latestVersion,
        "Y", currentTime, EndOfTimes, currentTime, currentTime, "Y"))
      val configHistory = List(ConfigHistory("", tableName, diffenParams.sourcingType.toString, allTabColumn.map(_.columnName).mkString(","), tableConfig.colSchema, currentTime))
      configs ++= configHistory

      if (modifiedTables.contains(tableConfig)) {
        allTables ++= diffenAllTables
        allTabColumns ++= allTabColumn
      }
    }

    spark.createDataFrame(allTables).write.mode("append").format("orc").save(s"${diffenParams.hdfsBaseDir}${diffenParams.metadataSchema}/diffen_all_tables/${diffenParams.source}_${diffenParams.country}_diffen_all_tables")
    spark.createDataFrame(allTabColumns).write.mode("append").format("orc").save(s"${diffenParams.hdfsBaseDir}${diffenParams.metadataSchema}/diffen_all_tab_columns/${diffenParams.source}_${diffenParams.country}_diffen_all_tab_columns")
    spark.createDataFrame(configs).write.mode("append").format("orc").save(s"${diffenParams.hdfsBaseDir}${diffenParams.opsSchema}/${diffenParams.source}_${diffenParams.country}_config_history/${diffenParams.source}_${diffenParams.country}_config_history")
  }

  /** Loads tha table data as a [[DataFrame]], applies the latest schema on top of it and returns the updated one **/
  private def evolveSchemaForData(spark: SparkSession, tableName: String, allPartitions: List[String], hdfsBaseDir: String, hdfsTmpDir: String,
                                  sourceTableSchema: StructType, targetTableSchema: StructType, schemaName: String, partitionColumnName: String, format: String) = {
    val sourceDataFrame = getSourceDataFrame(spark, (hdfsBaseDir + separator + schemaName + separator + tableName), partitionColumnName, allPartitions, format, sourceTableSchema)

    retrofitData(sourceDataFrame, targetTableSchema).write.format(format).partitionBy(partitionColumnName).save(hdfsTmpDir + separator + schemaName + separator + tableName)
  }

  private def getSourceDataFrame(spark: SparkSession, path: String, partitionColumnName: String, allPartitions: List[String], format: String, sourceTableSchema: StructType): DataFrame = {
    val sourceDataFrame = Try(spark.read.format(format).schema(sourceTableSchema).load(path))
      .getOrElse(spark.createDataFrame(spark.sparkContext.emptyRDD[Row], sourceTableSchema))
    if (!allPartitions.isEmpty) {
      sourceDataFrame.filter(col(partitionColumnName).isin(allPartitions: _*))
    } else {
      sourceDataFrame
    }
  }

  /** Creates a new DataFrame with the updated schema **/
  def retrofitData(df: DataFrame, newTableSchema: StructType): DataFrame = {

    logger.info (s"Source Dataframe Structure: ${df.schema}")
    logger.info (s"Target Dataframe Structure: $newTableSchema")

    val sourceDFFields = df.schema.fields.map(_.name)
    val (availableColumns, newColumns) = newTableSchema.fields.partition(x => sourceDFFields.contains(x.name))
    var targetDF = df.select(availableColumns.map(x => col(x.name)): _ *)
    newColumns.foreach(newCol => targetDF = targetDF.withColumn(newCol.name, lit(null)))

    targetDF.select(newTableSchema.fields.map(x => col(x.name).cast(x.dataType)): _ *)
  }

  /** Identifies the tables whose schema has changed
    *
    * Possible Scenarios: isSchemaEvolutionRequired: Boolean, isRenameRequired: Boolean, isDataRetrofitRequired: Boolean
    *
    * Scenario 1: Deletion of Table
    *   Do Nothing
    *
    * Scenario 2: Addition of Table (true, false, false)
    *   Create new table in Hive
    *
    * Scenario 3: Delete Column from table (true, true, true)
    *   Rename old table to date version and create new table with new schema
    *
    * Scenario 4: Add column to table (true, false, true)
    *   Recreate table with additional column and retrofit historical data
    *
    * Scenario 5: Reorder of columns in table (true, false, true)
    *   Recreate table with new ordering and retrofit historical data
    *
    * Scenario 6: Datatype change for column in table (true, true, true)
    *   Rename old table to date version and create new table with new schema
    *
    * Scenario 7: Length/precision change for column in table (true, false, false)
    *   Recreate table with new schema but no retrofit of historical data
    *
    * Scenario 8: No change in the table (false, false, false)
    *   Do nothing
    *
    **/

  private def processUpdatedTableConfigs(inScopeTableConfigs: List[TableConfig], allTableConfigsMap: Map[String, TableConfig], diffenParams: DiffenParams): List[SchemaEvolutionTableConfig] = {

    val inScopeTables = inScopeTableConfigs.map {
      tableConfig =>

        if(!allTableConfigsMap.contains(tableConfig.name)) { //Scenario 2
          SchemaEvolutionTableConfig(tableConfig, true, false, false)
        } else { // across columns level check

          val currentTime = nowAsString()

          val newColumnList = XmlParser.getAllTabColumns(currentTime, diffenParams.source, diffenParams.country, tableConfig)
          val newColumnListMap = newColumnList.map(x => {(x.columnName, x)}).toMap

          val oldColumnList = XmlParser.getAllTabColumns(currentTime, diffenParams.source, diffenParams.country, allTableConfigsMap(tableConfig.name))
          val oldColumnListMap = oldColumnList.map(x => {(x.columnName, x)}).toMap

          if(oldColumnList.exists(col=> !newColumnListMap.contains(col.columnName))) { //Scenario 3
            SchemaEvolutionTableConfig(tableConfig, true, true, true)
          } else if (newColumnList.exists(col=> !oldColumnListMap.contains(col.columnName))) {//Scenario 4
            SchemaEvolutionTableConfig(tableConfig, true, false, true)
          }else if(!oldColumnList.map(_.columnName).equals(newColumnList.map(_.columnName))) {//Scenario 5
            SchemaEvolutionTableConfig(tableConfig, true, false, true)
          } else { // per column attribute level check
            if(newColumnList.exists(col=> col.dataType != (oldColumnListMap(col.columnName).dataType))) { //Scenario 6
              SchemaEvolutionTableConfig(tableConfig, true, true, true)
            } else if(newColumnList.exists(col=> col != oldColumnListMap(col.columnName))) { //Scenario 7
              SchemaEvolutionTableConfig(tableConfig, true, false, false)
            } else { //Scenario 8
              SchemaEvolutionTableConfig(tableConfig, false, false, false)
            }
          }
        }
    }

    inScopeTables.foreach(x=> logger.info(s"${x.tableConfig.name}|${x.isSchemaEvolutionRequired}|${x.isRenameRequired}|${x.isDataRetrofitRequired}"))
    inScopeTables
  }

  /** Given a start and end date, identifies the partitions to be retrofitted */
  private def getPartitions(startMarker: String, endMarker: String, snapshotDateFormat: DateTimeFormatter): List[String] = {
    if(startMarker.isEmpty())
      return List()

    val startDay = snapshotDateFormat.parseDateTime(startMarker)
    val endDay = snapshotDateFormat.parseDateTime(endMarker)

    (for (f <- 0 to Days.daysBetween(startDay, endDay).getDays) yield startDay.plusDays(f)).map(x => {snapshotDateFormat.print(x)}).toList
  }

  def createHivePartition(database: String, tableName: String, partitionName: String) = s"alter table ${database}.${tableName} add if not exists partition(${partitionName})"

  def moveHDFSFiles(fs: FileSystem, sourcePath: Path, targetPath: Path) = {
    val config = new Configuration()
    fs.mkdirs(targetPath)
    fs.listStatus(sourcePath).foreach(x => {
      logger.info("Moving file: " + x.getPath)
      fs.delete(new Path(targetPath, x.getPath.getName), true)
      FileUtil.copy(fs, x.getPath, fs, targetPath, false, config)
      logger.info("Files moved  : " +x.getPath)
    })
  }
  case class SchemaEvolutionTableConfig(tableConfig: TableConfig, isSchemaEvolutionRequired: Boolean, isRenameRequired: Boolean, isDataRetrofitRequired: Boolean)

  def moveTable(fs:FileSystem, spark: SparkSession, basePath:String, database: String, sourceTableName: String, targetTableName: String, partitionColumnName: String) = {

    logger.info(s"Moving table $database.$sourceTableName to $database.$targetTableName")

    val sourcePath = basePath + separator + database + separator + sourceTableName
    val targetPath = basePath + separator + database + separator + targetTableName

    val existingPartitions = Try(spark.sql(s"show partitions $database.$sourceTableName").collect().map(r => r.getString(0).replace(s"${partitionColumnName}=", "")).toList).getOrElse(List())
    val hiveQueries =
      existingPartitions.map(p=> s"alter table $database.$sourceTableName drop if exists partition ($partitionColumnName = '$p')") ++
        List(s"alter table $database.$sourceTableName rename to $database.$targetTableName",
          s"alter table $database.$targetTableName set location '${fs.getUri}$targetPath'") ++
        existingPartitions.map(p=>createHivePartition(database, targetTableName, s"$partitionColumnName = '$p'"))

    fs.delete(new Path(targetPath), true)
    fs.rename(new Path(sourcePath), new Path(targetPath))

    hiveQueries
  }
}