import java.util.Properties
import com.amazonaws.services.glue.GlueContext
import com.amazonaws.services.glue.util.GlueArgParser
import com.tdt.diffen.models.OpsModels._
import com.tdt.diffen.processor._
import com.tdt.diffen.utils.FileSystemUtils
import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.DiffenUtils.{getBusinessDayPartition, getParamConf}
import com.tdt.diffen.utils.XmlParser.parseXmlConfig
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.catalyst.expressions.{GenericRowWithSchema, UnsafeRow}
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.joda.time.DateTime
import scala.collection.mutable.WrappedArray
import com.amazonaws.SDKGlobalConfiguration
import com.amazonaws.AmazonClientException
import com.amazonaws.AmazonServiceException
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.AmazonWebServiceRequest
import com.amazonaws.services.secretsmanager._
import com.amazonaws.services.secretsmanager.model._
import org.json.JSONObject


/**
  * Entry point for the Source Image construction process
  */
object DiffenJobGlue {

  val logger: Logger = Logger.getLogger(DiffenJobGlue.getClass)

  def main(args: Array[String]) {

    logger.info(s"Start time of the Job ${DateTime.now()}")
    logger.info("Spark Driver arguments - " + args.mkString(" | "))
    try {
      val secretName: String = "rdsPasswordMySql5"
      val region: String = "ap-southeast-1"
      val rdsEndPoint: String = "edw-metastore-mysql5-dev.cgdfjmprzr1k.ap-southeast-1.rds.amazonaws.com"

      val extraSparkConfMap = Map(
        "spark.sql.catalogImplementation" -> "hive",
        "spark.app.name" -> "Diffen",
        "spark.sql.sources.partitionOverwriteMode" -> "DYNAMIC",
        "spark.hadoop.javax.jdo.option.ConnectionURL" -> s"jdbc:mysql://${rdsEndPoint}:3306/metastore",
        "spark.hadoop.javax.jdo.option.ConnectionUserName" -> "admin",
        "spark.hadoop.javax.jdo.option.ConnectionPassword" -> getSecret(secretName, region, "javax.jdo.option.ConnectionPassword"),
        "spark.hadoop.javax.jdo.option.ConnectionDriverName" -> "com.mysql.jdbc.Driver",
        "spark.serializer" -> "org.apache.spark.serializer.KryoSerializer",
        "mapreduce.fileoutputcommitter.marksuccessfuljobs" -> "false",
      )

      val sparkConf = new SparkConf()
        .registerKryoClasses(
          Array(
            classOf[WrappedArray.ofRef[_]],
            classOf[RowCount],
            classOf[Array[RowCount]],
            classOf[DiffenParams],
            classOf[Array[Row]],
            Class.forName("scala.reflect.ClassTag$$anon$1"), //for SPARK-6497
            Class.forName("java.lang.Class"),
            Class.forName("org.apache.spark.sql.types.StringType$"),
            Class.forName("scala.collection.immutable.Map$EmptyMap$"),
            classOf[Array[InternalRow]],
            classOf[Array[StructField]],
            classOf[Array[Object]],
            classOf[Array[String]],
            classOf[StructField],
            classOf[Metadata],
            classOf[StringType],
            classOf[DecimalType],
            classOf[TimestampType],
            classOf[DateType],
            classOf[UnsafeRow],
            classOf[GenericRowWithSchema],
            classOf[StructType],
            classOf[OpsEvent],
            classOf[Array[OpsEvent]],
            classOf[ProcessMetadataEvent],
            classOf[Array[ProcessMetadataEvent]],
            Class.forName("org.apache.spark.sql.execution.joins.UnsafeHashedRelation"),
            Class.forName("org.apache.spark.sql.execution.joins.LongHashedRelation"),
            Class.forName("org.apache.spark.sql.execution.joins.LongToUnsafeRowMap"),
            Class.forName("org.apache.spark.sql.execution.columnar.CachedBatch"),
            classOf[org.apache.spark.sql.catalyst.expressions.GenericInternalRow],
            classOf[org.apache.spark.unsafe.types.UTF8String],
            Class.forName("[[B")
          )
        )

      sparkConf.setAll(extraSparkConfMap)
      val sc: SparkContext = new SparkContext(sparkConf)

      val properties = new Properties()
      extraSparkConfMap.foreach{
        case (x, y) => properties.setProperty(x, y)
      }

      val glueContext: GlueContext = new GlueContext(sc)
      glueContext.setConf(properties)
      val sparkSession = glueContext.getSparkSession

      sparkSession.conf.getAll.foreach(println)
      sparkSession.sparkContext.setLocalProperty("spark.scheduler.pool", "production")


      val argsMap: Map[String, String] = GlueArgParser.getResolvedOptions(
        args,
        Array("eodMarker", "businessDate", "paramXmlHdfsPath", "runType", "rerunTable")
      )

      val eodMarker = argsMap.get("eodMarker").get.trim
      val businessDate = argsMap.get("businessDate").get.trim
      val paramXmlHdfsPath = argsMap.get("paramXmlHdfsPath").get.trim
      val runType = argsMap.get("runType").get.trim
      val rerunTable = argsMap.get("rerunTable").get.trim

      System.setProperty(SDKGlobalConfiguration.ENABLE_S3_SIGV4_SYSTEM_PROPERTY, "true")

      val fs: FileSystem = new Path(paramXmlHdfsPath).getFileSystem(sparkSession.sparkContext.hadoopConfiguration)
      FileSystemUtils.setFileSystem(fs)

      val (todaysPartition, paramPath, diffenParams, tableConfigs, rerunTables, paramConf) = parseParameters(fs, eodMarker, businessDate, paramXmlHdfsPath, runType, rerunTable)

      sparkSession.sparkContext.setCheckpointDir(diffenParams.hdfsTmpDir + "/checkpoint")

      BootstrapProcessor.businessDateProcess(todaysPartition, diffenParams, tableConfigs, rerunTables, paramConf)(sparkSession)
      logger.info("businessDateProcess End")
      //StandardizedLayer.createStandardizeddLayer(todaysPartition,diffenParams)(sparkSession)
      sparkSession.stop()
    }
    catch {
      case c: Exception => c.printStackTrace()
    }
  }


  /**
    * Get the value for the secretName passed using the key.
    */

  def getSecret(secretName: String, region: String, key: String) = {
    val client: AWSSecretsManager = AWSSecretsManagerClientBuilder.standard().withRegion(region).build();
    var secret = ""
    var decodedBinarySecret = ""
    val getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName)
    var getSecretValueResult: GetSecretValueResult  = null;

    try {
      getSecretValueResult = client.getSecretValue(getSecretValueRequest)
    } catch {
      case e: InternalServiceErrorException => println("InternalServiceErrorException")
      case e: InvalidParameterException => println("InvalidParameterException")
      case e: InvalidRequestException => println("InvalidRequestException")
      case e: InternalServiceErrorException => println("InternalServiceErrorException")
    }

    if (getSecretValueResult.getSecretString() != null) {
      secret = getSecretValueResult.getSecretString();
    }
    val jsonObj = new JSONObject(secret)
    val pass = jsonObj.getString(key)
    pass
  }

  /**
    * Parses parameters and submits the Diffen Job
    */
  def parseParameters(fs: FileSystem, eodMarker: String, submitTime: String, paramPath: String, runType: String, rerunTableList: String) = {

    val paramConf: Configuration = getParamConf(fs, paramPath)
    val todaysPartition: String = getBusinessDayPartition(submitTime, paramConf.get("diffen.config.timestampcolformat"), paramConf.get("diffen.config.snapshotDate.format", "yyyy-MM-dd")) match {
      case Some (part) => part
      case None => throw new scala.Exception(" Business date and todays partition format were incorrectly specified!")
    }
    val diffenParams: DiffenParams = DiffenParams(paramConf, todaysPartition, todaysPartition, runType, eodMarker)
    val tableConfigsFromParamFile = parseXmlConfig(paramPath, fs)
    val tableConfigs = parseXmlConfig(diffenParams.tableConfigXml, fs) ++ tableConfigsFromParamFile
    val rerunTables: List[String] = RerunProcessor.getRerunTableList(rerunTableList, fs, tableConfigs)
    val rerunFlag = rerunTableList.nonEmpty

    val tableDictionary = if (rerunTables.isEmpty) tableConfigs else tableConfigs.filter(x => rerunTables.contains(x.name))

    (todaysPartition, paramPath, diffenParams.copy(isRerun = rerunFlag), tableDictionary, rerunTables, paramConf)

  }

}
