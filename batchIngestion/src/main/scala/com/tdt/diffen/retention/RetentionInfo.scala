package com.tdt.diffen.retention

case class RetentionInfo(
    schemaName: String,
    tableNames: List[String],
    partitionKey: String
) {
  require(!schemaName.isEmpty, "Schema name cannot be empty")
  require(partitionKey.toLowerCase().matches("[a-z]{3}"),
    "Partition key should match the regex '[a-z]{3}'")
  require(tableNames.nonEmpty, "Table names cannot be empty")
}
