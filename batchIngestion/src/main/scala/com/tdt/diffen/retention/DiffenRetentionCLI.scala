package com.tdt.diffen.retention

import com.tdt.diffen.retention.RetentionConstants._
import org.rogach.scallop.ScallopConf

/**
  * CLI class fore fetching retention parameters.
  * @param arguments application arguments.
  */
class DiffenRetentionCLI(arguments: Seq[String])
    extends ScallopConf(arguments) {

  val source = opt[String](
    required = true,
    descr = "Source for which deployment is being done",
    argName = "source"
  )
  val country = opt[String](
    required = true,
    descr = "Country for which deployment is being done",
    argName = "country"
  )
  val configLocation = opt[String](
    required = true,
    descr = "Location on local file system where config files are available",
    argName = "configLocation"
  )
  val layer = opt[String](
    descr = s"Layer on which retention will be applied. " +
      s"Allowed values are <${LANDING_LAYER}|${VERIFY_TYPES_LAYER}" +
      s"|${INVALIDS_LAYER}|${DIFFEN_OPEN_LAYER}|${DIFFEN_NONOPEN_LAYER}>",
    argName = "layer",
    validate = x =>
      List(
        LANDING_LAYER,
        VERIFY_TYPES_LAYER,
        INVALIDS_LAYER,
        DIFFEN_OPEN_LAYER,
        DIFFEN_NONOPEN_LAYER
      ).contains(x)
  )
  val startDate = opt[String](
    required = false,
    descr = "Start date of retention. Format yyyy-MM-dd",
    argName = "startDate"
  )
  val retentionDays = opt[Int](
    required = true,
    descr = "Number of days worth of data to retain.",
    argName = "retentionDays",
    validate = x => x > 0
  )
  val partitionFormat = opt[String](
    required = true,
    descr = "Date format of partition.",
    argName = "partitionFormat"
  )
  val tableType = opt[String](
    required = true,
    descr = "Takes delta or txn value.",
    argName = "tableType",
    validate = x => List("delta", "txn").contains(x)
  )

  verify()

  def getSource = source.toOption.get.trim

  def getCountry = country.toOption.get.trim

  def getConfigLocation = configLocation.toOption.get.trim

  def getStartDate = startDate.toOption

  def getRetentionDays = retentionDays.toOption.get

  def getPartitionFormat = partitionFormat.toOption.get

  def getTableType = tableType.toOption.get
}
