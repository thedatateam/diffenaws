package com.tdt.diffen.retention

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.retention.RetentionConstants._
import com.tdt.diffen.retention.exception.UnknownDiffenLayer
import com.tdt.diffen.utils.XmlParser.TableConfig

abstract class DiffenRetention(
    layer: String,
    startDate: LocalDate,
    retentionDays: Int,
    partitionFormat: DateTimeFormatter,
    tableType: Option[String],
    diffenParams: DiffenParams,
    tableConfigs: List[TableConfig]
) {
  protected val minPartition = startDate.atStartOfDay()
    .minusDays(retentionDays)
    .format(partitionFormat)
  protected val retentionInfo: RetentionInfo = {

    layer match {
      case LANDING_LAYER =>
        RetentionInfo(
          diffenParams.verifyTypesSchema,
          List(
            s"${diffenParams.source}_${diffenParams.country}_${LANDING_TABLE_NAME_SUFFIX}"
          ),
          STORAGE_PARTITION_COLUMN
        )

      case VERIFY_TYPES_LAYER =>
        assert(
          tableType.nonEmpty,
          s"Table type can't be empty for ${VERIFY_TYPES_LAYER} retention."
        )
        RetentionInfo(
          diffenParams.verifyTypesSchema,
          tableConfigs
            .filter(_.sourceType.equalsIgnoreCase(tableType.get))
            .map(_.name),
          diffenParams.verifyTypesPartitionColumn
        )

      case INVALIDS_LAYER =>
        RetentionInfo(
          diffenParams.verifyTypesSchema,
          List(
            s"${diffenParams.source}_${diffenParams.country}_${INVALID_TABLE_NAME_SUFFIX}"
          ),
          STORAGE_PARTITION_COLUMN
        )

      case DIFFEN_OPEN_LAYER =>
        assert(
          tableType.nonEmpty,
          s"Table type can't be empty for ${DIFFEN_OPEN_LAYER} retention."
        )
        RetentionInfo(
          diffenParams.diffenOpenSchema,
          tableConfigs
            .filter(_.sourceType.equalsIgnoreCase(tableType.get))
            .map(_.name),
          diffenParams.diffenOpenPartitionColumn
        )

      case DIFFEN_NONOPEN_LAYER =>
        assert(
          tableType.nonEmpty,
          s"Table type can't be empty for ${DIFFEN_NONOPEN_LAYER} retention."
        )
        RetentionInfo(
          diffenParams.diffenNonOpenSchema,
          tableConfigs
            .filter(_.sourceType.equalsIgnoreCase(tableType.get))
            .map(_.name),
          diffenParams.diffenNonOpenPartitionColumn
        )

      case layer =>
        throw new UnknownDiffenLayer(
          s"Layer ${layer} is not a valid diffen layer."
        )
    }
  }

  def getDropHivePartitionQueries: Seq[String] = {
    val evictPartitions = this.getPartitions
    val tables = this.retentionInfo.tableNames
    val schema = this.retentionInfo.schemaName
    val partitionKey = this.retentionInfo.partitionKey
    for {
      partition <- evictPartitions
      table <- tables
    } yield s"ALTER TABLE ${schema}.${table} DROP IF EXISTS PARTITION (${partitionKey} = '${partition}')"
  }

  def getHDFSDropFolders(): Seq[String] = {
    val evictPartitions = this.getPartitions
    val tables = this.retentionInfo.tableNames
    val schema = this.retentionInfo.schemaName
    val partitionKey = this.retentionInfo.partitionKey
    for {
      partition <- evictPartitions
      table <- tables
    } yield s"${this.diffenParams.hdfsBaseDir}/${schema}/${table}/${partitionKey}=${partition}" }

  def getPartitions: Seq[String]

  def dropHivePartitions(): Unit

  def dropHDFSFolder(): Unit

}
