package com.tdt.diffen.retention

object RetentionConstants {
  val STORAGE_PARTITION_COLUMN = "vds"
  val INVALID_TABLE_NAME_SUFFIX = "invalidtypes"
  val LANDING_TABLE_NAME_SUFFIX = "landing"
  val LANDING_LAYER = "landing"
  val VERIFY_TYPES_LAYER = "verify_types"
  val INVALIDS_LAYER = "invalids"
  val DIFFEN_OPEN_LAYER = "diffen_open"
  val DIFFEN_NONOPEN_LAYER = "diffen_non_open"
}
