package com.tdt.diffen.retention

import java.io.FileInputStream
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.XmlParser
import com.tdt.diffen.utils.XmlParser.TableConfig
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

class SparkDiffenRetention(
    layer: String,
    startDate: LocalDate,
    retentionDays: Int,
    partitionFormat: DateTimeFormatter,
    tableType: Option[String],
    diffenParams: DiffenParams,
    tableConfigs: List[TableConfig],
    spark: SparkSession
) extends DiffenRetention(
      layer,
      startDate,
      retentionDays,
      partitionFormat,
      tableType,
      diffenParams,
      tableConfigs
    ) {

  val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)

  override def getPartitions: Seq[String] = {
    val path = new Path(
      s"${diffenParams.hdfsBaseDir}/" +
        s"${retentionInfo.schemaName}/" +
        s"${retentionInfo.tableNames.head}/" +
        s"${retentionInfo.partitionKey}=*"
    )
    val pathList = this.fs.globStatus(path)
    pathList
      .map(path => path.getPath.getName.split("=")(1))
      .filter(partition => partition < this.minPartition)
  }

  override def dropHivePartitions(): Unit = {
    val hiveQueries = super.getDropHivePartitionQueries
    hiveQueries.foreach(spark.sql(_).collect())
  }

  override def dropHDFSFolder(): Unit = {
    val hdfsFolders = super.getHDFSDropFolders()
    hdfsFolders.foreach(filePath => fs.delete(new Path(filePath), true))
  }
}

object SparkDiffenRetention extends App {

  val logger = LoggerFactory.getLogger(this.getClass)

  //fetch file name from params
  val cliParams =
    if (args.isEmpty) new DiffenRetentionCLI(Array("--help"))
    else new DiffenRetentionCLI(args)

  //get spark session to run hive queries
  val sparkConf = new SparkConf()
  sparkConf.setIfMissing("spark.master", "local[1]")
  sparkConf.setAppName(
    s"Diffen-evictor-${cliParams.getSource}-${cliParams.getSource}"
  )
  implicit val spark: SparkSession = SparkSession
    .builder()
    .config(sparkConf)
    .getOrCreate()

  //fetch config from param xml file
  val paramFileName = s"${cliParams.getConfigLocation}/" +
    s"${cliParams.getSource}_${cliParams.getCountry}_param.xml"
  val diffenConfig = new Configuration()
  logger.info(s"Fetching diffen param from config file ${paramFileName}")
  diffenConfig.addResource(new FileInputStream(paramFileName))
  val diffenParams = DiffenParams(diffenConfig)

  //fetch fs from spark config
  val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)

  //fetch diffen table configs
  val tableDictionary =
    XmlParser.parseXmlConfig(diffenParams.tableConfigXml, fs)

  val partitionFormatter =
    DateTimeFormatter.ofPattern(cliParams.getPartitionFormat)
  //get retention object
  val layer = cliParams.layer.toOption.get
  val retention = new SparkDiffenRetention(
    layer,
    if (cliParams.startDate.isEmpty) LocalDate.now()
    else LocalDate.parse(cliParams.getStartDate.get),
    cliParams.getRetentionDays,
    partitionFormatter,
    cliParams.tableType.toOption,
    diffenParams,
    tableDictionary,
    spark
  )

  if (!layer.equalsIgnoreCase(RetentionConstants.LANDING_LAYER)) {
    retention.dropHivePartitions()
  }
  retention.dropHDFSFolder()

}
