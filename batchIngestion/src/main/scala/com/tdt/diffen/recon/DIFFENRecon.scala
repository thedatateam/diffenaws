/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.recon

import java.sql.Timestamp
import java.util.Date

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.models.cli.DIFFENReconCli
import com.tdt.diffen.utils.{FileSystemUtils, Ops, DiffenUtils}
import com.tdt.diffen.utils.{Ops, DiffenUtils}
import com.typesafe.config.ConfigFactory
import org.apache.hadoop.fs.FileSystem
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.{SaveMode, SparkSession}

import scala.collection.GenSeq
import scala.collection.JavaConversions._
import scala.collection.parallel.ForkJoinTaskSupport

object DIFFENRecon {

  val logger: Logger = Logger.getLogger(DIFFENRecon.getClass)

  /**
    * This program will go through the recon files sent by the source and generate corresponding output for recon comparision.
    *
    * @param args Input arguments to the program. Please refer DIFFENReconCli for different params expected.
    */

  def main(args: Array[String]): Unit = {

    val cliArguments = if (!args.isEmpty) args else Array("--help")

    val cliParams = new DIFFENReconCli(cliArguments)
    println(s"Parameters passed to DIFFEN Recon: $cliParams")

    val fs: FileSystem = FileSystemUtils.fetchFileSystem
    val configuration = DiffenUtils.getParamConf(fs, s"${cliParams.getParamConfigLocation}/${cliParams.getSource}_${cliParams.getCountry}_param.xml")
    val diffenParams: DiffenParams = DiffenParams(configuration, cliParams.getBusinessDate, cliParams.getBusinessDate, "", "")

    try {
      val sparkConf = new SparkConf()
        .set("mapreduce.fileoutputcommitter.algorithm.version", "2")
        .set("mapreduce.fileoutputcommitter.marksuccessfuljobs", "false")

      implicit val spark = SparkSession
        .builder()
        .config(sparkConf)
        .appName(s"DIFFEN-Recon-${cliParams.getSource}-${cliParams.getCountry}-${cliParams.getBusinessDate}")
        .enableHiveSupport()
//        .master("local[*]")
        .getOrCreate()
      val reconConfig =
        if (diffenParams.parallelExecution) {
          val parConfig = getReconConfig(cliParams.getSource, cliParams.getCountry, cliParams.getBusinessDate).par
          val parThreads = 3 * spark.conf.get("spark.driver.cores", "1").toInt //oversubscribe based on driver threads
          parConfig.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(parThreads))
          parConfig
        } else {
          getReconConfig(cliParams.getSource, cliParams.getCountry, cliParams.getBusinessDate)
        }

      logger.info(s"Recon Process started for ${cliParams.getSource} - ${cliParams.getCountry} - ${cliParams.getBusinessDate}")
      val reconResults = reconConfig.map(_.reconResults)
      writeReconTableOutput(diffenParams, reconResults)

      if (!reconResults.filter(!_.reconMatch).isEmpty) {
        val content = reconResults.filter(!_.reconMatch).map(x => x.toString).mkString("\n")
        Ops.createStatusFile(fs, configuration, diffenParams, "global", "_SOURCE_RECON_FAILURE", content)
      }
    } catch {
      case e: Exception =>
        logger.error("Process DIFFEN Source Recon encountered error: ", e)
        Ops.createStatusFile(fs, configuration, diffenParams, "global", "_SOURCE_RECON_FAILURE", e.getStackTrace.mkString("\n"))
        throw e
    }
  }

  private def getReconConfig(source: String, country: String, businessDate: String) = {

    val config = ConfigFactory.load(s"${source}_${country}_recon.conf")

    config.getConfigList("config.tables").flatMap(tableConfig => {

      val tableName = tableConfig.getString("tableName")
      val placeholderMap: Map[String, String] = getPlaceholderMap(source, country, businessDate, tableName)

      tableConfig.getConfigList("reconQueries").map(queries => {

        val identifier = replacePlaceHolders(queries.getString("identifier"), placeholderMap)
        val sourceQuery = Query(replacePlaceHolders(queries.getString("sourceQuery"), placeholderMap), true)
        val diffenQuery = Query(replacePlaceHolders(queries.getString("diffenQuery"), placeholderMap), false)

        ReconConfig(source, country, tableName, businessDate, identifier, sourceQuery, diffenQuery)
      })
    })
  }

  private def getPlaceholderMap(source: String, country: String, businessDate: String, tableName: String) = {
    Map(
      "source" -> source,
      "country" -> country,
      "businessDate" -> businessDate,
      "tableName" -> tableName
    )
  }

  private def replacePlaceHolders(line: String, placeholderMap: Map[String, String]) = {
    var updatedLine = line
    placeholderMap.foreach(x => updatedLine = updatedLine.replaceAll(s"##${x._1}##", x._2))
    updatedLine
  }

  private def writeReconTableOutput(diffenParams: DiffenParams, reconRecords: GenSeq[ReconResults])(implicit spark: SparkSession): Unit = {

    logger.info(s"Write the records to Recon Output Table: ${diffenParams.reconOutputTableName}")
    logger.info("Recon Output: " + reconRecords.mkString("\n"))

    val partitionQuoted = s"""business_date='${diffenParams.businessDate}'"""

    import spark.sqlContext.implicits._
    reconRecords.toList.toDS()
      .coalesce(1)
      .write
      .format("orc")
      .partitionBy("business_date")
      .mode(SaveMode.Append)
      .save(s"${diffenParams.hdfsBaseDir}/${diffenParams.opsSchema}/${diffenParams.reconOutputTableName}")

    DiffenUtils.createHivePartition(diffenParams.opsSchema, diffenParams.reconOutputTableName, partitionQuoted)
  }

  case class ReconConfig(source: String, country: String, tableName: String, businessDate: String, queryIdentifier: String, sourceQuery: Query, diffenQuery: Query) {
    def reconResults(implicit spark: SparkSession) = {
      val sourceValue = sourceQuery.value
      val diffenValue = diffenQuery.value
      ReconResults(source, country, tableName, queryIdentifier, sourceQuery.query, sourceValue, diffenQuery.query, diffenValue, sourceValue == diffenValue, new Timestamp(new Date().getTime), businessDate)
    }
  }

  case class Query(query: String, isSourceQuery: Boolean) {
    def value(implicit spark: SparkSession) = {
      logger.info(s"Executing query : $query")
      spark.sql(query).collect().map(r => r.get(0)).headOption.getOrElse("0").toString.toDouble
    }
  }

  case class ReconResults(source: String, country: String, tableName: String, identifier: String, sourceQuery: String, sourceValue: Double, diffenQuery: String, computedValue: Double, reconMatch: Boolean, insertTime: Timestamp, business_date: String)

}