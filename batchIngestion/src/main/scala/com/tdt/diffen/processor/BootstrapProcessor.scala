/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor

import java.util.UUID
import java.util.concurrent.Executors

import com.tdt.diffen.models.OpsModels._
import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.DiffenConstants._
import com.tdt.diffen.utils.Enums.{RunType, TableType}
import com.tdt.diffen.utils.PartitionUtils.getNextBusinessDatePartition
import com.tdt.diffen.utils.DiffenUtils._
import com.tdt.diffen.utils.XmlParser._
import com.tdt.diffen.utils._
import org.apache.commons.lang3.StringUtils
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j.Logger
import org.apache.spark.sql._
import org.apache.spark.sql.functions.{date_format, _}
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime
//import org.apache.avro._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/**
  * This is the core workhorse of the Diffen application. While the [[Diffen]] sets the configuration,
  * the building of the [[TableConfig]] and the [[DiffenParams]] is done within this class. Once that's done, Diffen process for
  * each table is driven out of this class.
  */
object BootstrapProcessor {
  val logger: Logger = Logger.getLogger(BootstrapProcessor.getClass)

  /**
    * Prepares the configuration before invoking the processDiffen
    *
    */
  def businessDateProcess(todaysPartition: String, diffenParams: DiffenParams, tableConfigs: List[TableConfig],
                          rerunTables: List[String], paramConf: Configuration)(implicit spark: SparkSession): Unit = {

    val fs: FileSystem = FileSystemUtils.fetchFileSystem
    logger.info("businessDateProcess Starting")

    try {
      processDiffen(diffenParams, fs, tableConfigs, paramConf)
      Ops.createStatusFile(fs, paramConf, diffenParams, "global", "_COMPLETED_PROCESSING_BATCH")
    }
    catch {
      case e: Exception =>
        logger.error("Process DIFFEN encountered error: ", e)
        Ops.createStatusFile(fs, paramConf, diffenParams, "global", "_ERROR_PROCESSING_BATCH", e.getStackTrace.mkString("\n"))
    } finally {
      //cascaded rerun only if any of the tables are delta tables
      RerunProcessor.cascadedReruns(diffenParams.runType, todaysPartition, rerunTables, paramConf, diffenParams, tableConfigs)
      logger.info("Cascade Rerun Exit")
    }
  }

  /**
    * Derives the runtype for a table
    * For File based systems - If there's a runtype configured at a table level, that gets picked up before falling back
    * to the parameter passed for submitting this Diffen job
    *
    * For CDC systems - The logic is dependent on the presence of "NOT-SET" in the userid column. If there's one, then
    * it means that the data is a full dump, else incremental.
    *
    * @param vTypesOfToday
    * @param tableConfig
    * @param diffenParams
    * @return
    */
  def getRunType(vTypesOfToday: Dataset[Row], tableConfig: TableConfig, diffenParams: DiffenParams) = {
    if (diffenParams.isBatchSource) {
      //Use the table config's custom runtype, else fall back to the global runtype passed as params
      tableConfig.runType match {
        case Some(lRunType) => RunType.fromString(lRunType).get
        case None if diffenParams.fullDump => RunType.FULLDUMP
        case _ => RunType.INCREMENTAL
      }
    }
    else { //CDC source
      if (vTypesOfToday.select(col(CDCUserIdColName)).filter(col(CDCUserIdColName).contains(NotSetUserId)).count() > 0)
        RunType.FULLDUMP
      else
        RunType.INCREMENTAL
    }
  }

  /**
    * The root method for Source Image processing
    *
    * @param diffenParams       Global Parameters
    * @param fs              Hadoop File system
    * @param tableDictionary Table configs
    * @param paramConf       Hadoop Configuration
    * @param spark           SparkSession
    */
  def processDiffen(diffenParams: DiffenParams, fs: FileSystem, tableDictionary: List[TableConfig], paramConf: Configuration)
                (implicit spark: SparkSession) = {

    val jobUuid = UUID.randomUUID().toString
    val tableCounter = spark.sparkContext.longAccumulator("TableCounter")

    val parThreads = 3 * spark.conf.get("spark.driver.cores", "1").toInt
    implicit val executionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(parThreads))

    val tableConfigs = tableDictionary.filterNot(_.name.isEmpty)

    val previousTableLevelEodMarkers = Ops.parseEodMarkerForTable(spark, diffenParams, paramConf, getPreviousExistingDiffenPartition(diffenParams, diffenParams.businessDate, tableDictionary.head.name, 10))
    val currentTableLevelEodMarkers = Ops.parseEodMarkerForTable(spark, diffenParams, paramConf, diffenParams.businessDate)

    val auditOutputs = tableConfigs.par.map { tableConfig =>

      val start = System.currentTimeMillis

      val processMetadataEvents = ArrayBuffer[OpsEvent]()

      val rawTableName = tableConfig.name.split('_').drop(2).mkString("_")
      val todaysEodMarker = currentTableLevelEodMarkers.filter(x => StringUtils.equalsIgnoreCase(rawTableName, x.tableName)).map(_.marker).headOption
      val updatedDiffenParams = diffenParams.copy(hdfsTmpDir = diffenParams.hdfsTmpDir + UUID.randomUUID().toString.substring(3), eodMarker = todaysEodMarker.getOrElse(diffenParams.eodMarker))
      val businessDateContext = getInputFiles(updatedDiffenParams, fs, tableConfig)
      val tableType = TableType.fromString(tableConfig.sourceType).get

      val auditOutput = Try {
        fs.mkdirs(new Path(diffenParams.hdfsTmpDir))
        fs.deleteOnExit(new Path(diffenParams.hdfsTmpDir))

        logger.info("Fetching Verify Types for " + tableConfig.name)
        processMetadataEvents += OpsEvent(DateTime.now(), s"Fetching Verify Types for ${tableConfig.name}")

        val previousEodMarkerStr = if (diffenParams.isTableLevelEod)
          previousTableLevelEodMarkers.filter(x => rawTableName.equals(x.tableName)).map(_.marker).headOption.getOrElse("1970-01-01 00:00:00")
        else
          Ops.getEODMarkerFor(fs, paramConf, diffenParams, businessDateContext.previousBusinessDate).getOrElse("1970-01-01 00:00:00")

        logger.info(s"Current EOD Marker time for Table ${tableConfig.name}  : ${updatedDiffenParams.eodMarker}")
        logger.info(s"Previous EOD Marker time for Table ${tableConfig.name} : $previousEodMarkerStr")
        val tomorrow: String = getNextBusinessDatePartition(diffenParams.businessDate, diffenParams.snapshotFormatString, "days", 1)

        logger.info("Reading vTypes of yesterday " + tableConfig.name)
        logger.info(s"Fetching verify types yesterday for table ${tableConfig.name} for date ${businessDateContext.previousBusinessDate}")
        val vTypesOfYesterday = getVerifyTypes(fs, diffenParams, tableConfig, businessDateContext.previousBusinessDate)

        logger.info("Reading vTypes of tomorrow " + tableConfig.name)
        val vTypesOfTomorrow = getVerifyTypes(fs, diffenParams, tableConfig, tomorrow)
        logger.info(s"Fetching verify types tomorrow for table ${tomorrow}")
        logger.info(s"Fetching verify types yesterday for table ${tableConfig.name} for date ${tomorrow}")


        logger.info("Reading vTypes of today " + tableConfig.name)
        logger.info(s"Fetching verify types yesterday for table ${tableConfig.name} for date ${diffenParams.businessDate}")
        val vTypesOfToday = getVerifyTypes(fs, diffenParams, tableConfig,
          diffenParams.getSnapshotFormat.parseDateTime(diffenParams.businessDate).toString(PartitionFormat))

        logger.info("Consolidating verify types " + tableConfig.name)
        val verifyTypes = vTypesOfToday.union(vTypesOfYesterday).union(vTypesOfTomorrow)

        val effectiveVerifyTypesNoX = verifyTypes
          .filter(date_format(col(diffenParams.timestampCol), diffenParams.timestampColFormat)
            .gt(date_format(lit(previousEodMarkerStr), diffenParams.timestampColFormat)))
          .filter(date_format(col(diffenParams.timestampCol), diffenParams.timestampColFormat)
            .leq(date_format(lit(updatedDiffenParams.eodMarker), diffenParams.timestampColFormat)))
          .drop(StartDate)
          .withColumn(StartDate, lit(diffenParams.businessDate)) //reassign correct (today as) start date for the effective data

        val effectiveVerifyTypes = transformExceptionalTypes(effectiveVerifyTypesNoX, diffenParams, tableConfig).persist(StorageLevel.MEMORY_AND_DISK)

        val runType = getRunType(effectiveVerifyTypes, tableConfig, diffenParams)

        logger.info("Reading Previous DIFFEN " + tableConfig.name)

        val diffenStructType = fetchDiffenStructure(tableConfig, diffenParams.diffenOpenPartitionColumn, diffenParams.isCdcSource)

        val previousDiffen =
          if (fs.exists(new Path(businessDateContext.diffenOpenPrevPartitionPath)) && tableType == TableType.DELTA) {
            //val parts = calculatePartitions(businessDateContext.diffenOpenPrevPartitionPath)
            spark.read.format(OrcFormat).schema(diffenStructType).load(businessDateContext.diffenOpenPrevPartitionPath)//.repartition(parts)
          } else {
            spark.createDataFrame(spark.sparkContext.emptyRDD[Row], diffenStructType)
          }

        logger.info("Fetching column configs " + tableConfig.name)

        val primaryKeyCols = XmlParser.fetchKeyColsFromTable(tableConfig)
        val tableColNames = XmlParser.fetchColNamesFromTable(tableConfig)

        logger.info("Processing Diffen for " + tableConfig.name)
        processMetadataEvents += OpsEvent(DateTime.now(), s"Processing Diffen for ${tableConfig.name}")

        val vTypesAuditCols = XmlParser.getColumnConfigs(diffenParams.vTypesAuditColSchema, "\\^").map(_.name)
        val diffenAuditCols = XmlParser.getColumnConfigs(diffenParams.diffenAuditColSchema, "\\^").map(_.name)
        val systemAuditCols = XmlParser.getColumnConfigs(diffenParams.systemAuditColSchema, "\\^").map(_.name)


        val diffenOutput@DiffenOutput(diffenOpen, diffenNonOpen, vTypesDuplicates, prevDiffenVsVtypesDuplicates, _) =
          if (diffenParams.isCdcSource || (diffenParams.isRecordTypeBatch && StringUtils.isNotBlank(tableConfig.operationTypeCol))) {

            val recordTypeMeta = getRecordTypeMetaInfo(diffenParams, tableConfig, vTypesAuditCols, diffenAuditCols, systemAuditCols)
            val recordTypeBasedProcessor = new RecordTypeBasedProcessor
            recordTypeBasedProcessor.processDiffen(tableConfig.name, tableColNames, primaryKeyCols, effectiveVerifyTypes,
              previousDiffen, recordTypeMeta, tableType, runType, diffenParams.businessDt, diffenParams.getSnapshotFormat)
          }
          else {

            val metaInfo = NonRecordTypeMetaInfo(
              timestampColName = diffenParams.timestampCol,
              timestampColFormat = diffenParams.timestampColFormat,
              vTypesAuditCols, diffenAuditCols, systemAuditCols
            )
            val nonRecordTypeBasedProcessor = new NonRecordTypeBasedProcessor
            nonRecordTypeBasedProcessor.processDiffen(tableConfig.name, tableColNames, primaryKeyCols, effectiveVerifyTypes,
              previousDiffen, metaInfo, tableType, runType, diffenParams.businessDt, diffenParams.getSnapshotFormat)
          }

        //DIFFEN OPEN
        val diffenOpenFut = Future {
          logger.info("Writing DiffenOpen " + tableConfig.name)
          processMetadataEvents += OpsEvent(DateTime.now(), s"Writing DiffenOpen ${tableConfig.name}")
            DiffenUtils.persistAsHiveTable(diffenOpen, diffenParams.getDiffenOpenPath(tableConfig.name), diffenParams.diffenOpenSchema, tableConfig.name, diffenParams.diffenOpenPartitionColumn, diffenParams.businessDate)

        }

        //DIFFEN NON-OPEN
        val diffenNonOpenFut = Future {
          logger.info("Writing DiffenNonOpen " + tableConfig.name)
          processMetadataEvents += OpsEvent(DateTime.now(), s"Writing DiffenNonOpen ${tableConfig.name}")

            DiffenUtils.persistAsHiveTable(diffenNonOpen, diffenParams.getDiffenNonOpenPath(tableConfig.name), diffenParams.diffenNonOpenSchema, tableConfig.name, diffenParams.diffenNonOpenPartitionColumn, diffenParams.businessDate)

        }

        //VTypes DUPLICATES
        val duplicatesFut = Future {
          logger.info("Writing Duplicates for " + tableConfig.name)
          processMetadataEvents += OpsEvent(DateTime.now(), s"Writing Duplicates for ${tableConfig.name}")

            Ops.writeDuplicates(diffenParams, vTypesDuplicates, tableConfig.name, "STORAGE")

        }

        //PrevDIFFEN Duplicates - Full Dump
        val diffenDuplicatesFut = Future {
          prevDiffenVsVtypesDuplicates match {
            case Some(df) =>
              logger.info("Writing Previous DIFFEN vs VTypes Duplicates for " + tableConfig.name)
              processMetadataEvents += OpsEvent(DateTime.now(), s"Writing PreviousDiffen vs Verify Types Duplicates for ${tableConfig.name}")
              Ops.writeDuplicates(diffenParams, df, tableConfig.name, "DIFFEN")
            case None =>
          }
        }

        Await.result(Future.sequence(Seq(diffenOpenFut, diffenNonOpenFut, duplicatesFut, diffenDuplicatesFut)), Duration.Inf)

        spark.synchronized {
          tableCounter.add(1)
          logger.info(s"Table Counter : ${tableCounter.value}")
        }

        //ROWCOUNTS
        val earlyCount = getEarlyCount(verifyTypes, updatedDiffenParams.timestampColFormat, updatedDiffenParams.timestampCol, updatedDiffenParams.eodMarker)
        val tableRowCount = Ops.getRowCount(updatedDiffenParams, tableConfig.name, tableConfig.sourceType, effectiveVerifyTypes, diffenOutput, earlyCount, jobUuid, runType.toString, diffenParams.isRerun.toString)
        tableRowCount.foreach(rowCount => logger.info("Table:" + tableConfig.name + " RowCount Record:" + rowCount))

        val elapsed = (System.currentTimeMillis - start) / 1000L
        processMetadataEvents += OpsEvent(DateTime.now(), s"Time taken to process Diffen for ${tableConfig.name} is $elapsed seconds")
        logger.info(s"Time taken to process Diffen for ${tableConfig.name} is $elapsed seconds")

        diffenOpen.unpersist()
        diffenNonOpen.unpersist()
        vTypesDuplicates.unpersist()

        AuditOutput(Option(tableRowCount), Ops.convertToProcessMetadataEvents(processMetadataEvents.toList, diffenParams, tableConfig.name, SuccessStatus, runType.toString, DiffenModule), businessDateContext.previousBusinessDate)
      }

      auditOutput match {
        case Success(res) =>
          logger.info("Successfully Processed DIFFEN for " + tableConfig.name)
          res
        case Failure(ex) =>
          processMetadataEvents += OpsEvent(DateTime.now(), s"ERROR processing DIFFEN for ${tableConfig.name}")
          logger.error(s" Exception processing ${tableConfig.name} for ${diffenParams.toString} " + ex.getMessage, ex)
          Ops.createStatusFile(fs, paramConf, diffenParams, "tables", "_ERROR_PROCESSING_TABLE_" + tableConfig.name, ex.getStackTrace.mkString("\n"))
          //FIXME While refactoring this entire function. This is a long and ugly function
          AuditOutput(None, Ops.convertToProcessMetadataEvents(processMetadataEvents.toList, diffenParams, tableConfig.name, FailureStatus, "", DiffenModule), businessDateContext.previousBusinessDate)
      }
    }

    import spark.sqlContext.implicits._
    val auditOutputsL = auditOutputs.toList
    val rowCountsDf = auditOutputsL.flatMap(_.rowCountsOpt).flatten.toDF()

    logger.info("Writing RowCounts and process metadata for all tables")
    Ops.writeProcessMetadataEvents(auditOutputsL.flatMap(_.opsEvents), diffenParams)
    Ops.writeRowCounts(diffenParams, rowCountsDf)

    logger.info("Writing EOD marker to be used for tomorrow")
    //FIXME There must be a better way to get the previous business date other than this
    val eodRecord = EODTableRecord(diffenParams.source, diffenParams.country, diffenParams.eodMarker, auditOutputs.map(_.businessDate).find(StringUtils.isNotEmpty).getOrElse("BOOTSTRAP"))
    Ops.writeEodTableAndMarker(diffenParams, eodRecord)

  }

  def getEarlyCount(vTypes: DataFrame, timestampColFormat: String, timestampCol: String, eodMarker: String): Long = {
    val effectiveVerifyTypes = vTypes.select(col("filename"), date_format(col(timestampCol), timestampColFormat).gt(date_format(lit(eodMarker), timestampColFormat)).alias("eod_marker_check"))
    val groupedVTypes = effectiveVerifyTypes.groupBy("filename", "eod_marker_check").count()
    val fileNameExtendingAcrossEods = groupedVTypes.groupBy("filename").count().filter(col("count") > 1).select("filename")
    groupedVTypes.join(fileNameExtendingAcrossEods, "filename").filter(col("eod_marker_check")).collect().map(row => row.getLong(2)).headOption.getOrElse(0l)
  }

  /**
    * Reads Verified Types data as a dataframe normalizing it with the structure of DIFFEN for Union-ing
    *
    * @param fs
    * @param diffenParams
    * @param tableConfig
    * @param partitionDate
    * @param spark SparkSession
    * @return
    */
  def getVerifyTypes(fs: FileSystem, diffenParams: DiffenParams, tableConfig: TableConfig, partitionDate: String)(implicit spark: SparkSession) = {
    val partitionPath = diffenParams.getVerifyTypesPartitionPath(tableConfig.name, partitionDate)
    val vTypesStructType = fetchVerifyStructure(tableConfig, diffenParams.verifyTypesPartitionColumn, diffenParams.isCdcSource)

    val dataFrame = if (fs.exists(new Path(partitionPath))) {
      //val parts = calculatePartitions(partitionPath)
      spark.read.format("avro").load(partitionPath).drop("vds")//.repartition(parts)
    }
    else {
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], vTypesStructType).drop("vds")
    }

    //Add Diffen audit columns to enable Unions across Previous Diffen and VTypes
    dataFrame
      .withColumn(StartDate, lit(partitionDate))
      .withColumn(StartTime, lit(DiffenConstants.nowAsString()))
      .withColumn(EndDate, lit(DiffenConstants.EndOfDays))
      .withColumn(EndTime, lit(DiffenConstants.EndOfTimes))
      .withColumn(DeleteFlag, lit("0"))
  }

  def getRecordTypeMetaInfo(diffenParams: DiffenParams, tableConfig: TableConfig, vTypesAuditCols: List[String], diffenAuditCols: List[String], systemAuditCols: List[String]) = {

    diffenParams match {
      case params if diffenParams.isCdcSource =>
        RecordTypeMetaInfo(
          timestampColName = diffenParams.timestampCol,
          timestampColFormat = diffenParams.timestampColFormat,
          operationTypeColName = "c_operationtype",
          insertType = Option(tableConfig.insertValue).getOrElse("I"),
          beforeType = Option(tableConfig.beforeValue).getOrElse("B"),
          afterType = Option(tableConfig.afterValue).getOrElse("A"),
          deleteType = Option(tableConfig.deleteValue).getOrElse("D"),
          vTypesAuditCols, diffenAuditCols, systemAuditCols)
      case params if diffenParams.isRecordTypeBatch =>
        RecordTypeMetaInfo(
          timestampColName = diffenParams.timestampCol,
          timestampColFormat = diffenParams.timestampColFormat,
          operationTypeColName = tableConfig.operationTypeCol,
          insertType = tableConfig.insertValue,
          beforeType = tableConfig.beforeValue,
          afterType = tableConfig.afterValue,
          deleteType = tableConfig.deleteValue,
          vTypesAuditCols, diffenAuditCols, systemAuditCols)
      //Possible MatchError for Incorrect config
    }
  }
}
