/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.Enums.RunType.RunType
import com.tdt.diffen.utils.XmlParser.TableConfig
import org.apache.commons.lang3.StringUtils
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession

/**
  * Reruns for a table or a set of tables for a previous business date are driven out of this class.
  */
object RerunProcessor {
  val logger: Logger = Logger.getLogger(RerunProcessor.getClass)

  /** Given a comma separated list of table names, this would validate them for correctness */
  def getRerunTableList(rerunTableList: String, fs: FileSystem, tableConfigs: List[TableConfig]): scala.List[String] = rerunTableList match {
    case x if StringUtils.isEmpty(x) => List()
    case x if StringUtils.equalsIgnoreCase(x, "ALL") => tableConfigs.map(_.name)
    case x if x.contains(",") || tableConfigs.map(_.name).contains(x) =>
      lazy val tableNames = tableConfigs.map(_.name)
      logger.info(" Rerun for tables " + x)
      val tables = x.split(',').map(_.trim)
      lazy val rerunTables = tables.filter(x => tableNames.contains(x))
      tables.filterNot(x => tableNames.contains(x)).foreach(wrongTable => logger.warn(s"$wrongTable is not a proper table name in the configured list!"))
      require(rerunTables.length > 0, s"$x did not contain a single configured table name, please check rerun inputs")
      rerunTables.toList
    case _ => throw new IllegalStateException(s"Rerun table list $rerunTableList is incorrect")
  }

  /**
    * If a job is submitted for any day before the current business day, then a cascading runs of Diffen would be achieved
    * using this class.
    */
  def cascadedReruns(runType: RunType, businessDate: String,
                     rerunTables: List[String],
                     paramConf: Configuration,
                     diffenParams: DiffenParams,
                     tableDictionary: List[TableConfig])(implicit spark: SparkSession): Unit = {
    rerunTables match {
      case List() => logger.info("Normal run ...")
      case tables if rerunTables.exists(tableDictionary.filter(_.isDeltaSource).map(_.name).contains)  =>
        //fetch next business date
        import spark.implicits._
        val nextBusinessDateTuple = spark.sql(s"select businessdate, markertime from ${diffenParams.opsSchema}.${diffenParams.eodTableName} where businessdate > '${businessDate}' order by businessdate")
                                            .map(r=> (r.getString(0), r.getString(1))).collect().headOption

        if (nextBusinessDateTuple.isDefined) {

          val (nextBusinessDate, nextBusinessDateMarker) = nextBusinessDateTuple.get
          logger.info(" Preparing to cascade reprocessing towards the next business date " + nextBusinessDate)

          val diffenParamsC: DiffenParams = diffenParams.copy(eodMarker = nextBusinessDateMarker, batchPartition = nextBusinessDate, businessDate = nextBusinessDate, isRerun = true)

          val rerunTables = tables.filter(tableDictionary.filter(_.isDeltaSource).map(_.name).contains)
          BootstrapProcessor.businessDateProcess(nextBusinessDate, diffenParamsC, tableDictionary, rerunTables, paramConf)

        } else {
          logger.info(" Need not be cascaded ... as there are not further business dates from " + businessDate)
        }
      case tables if !rerunTables.exists(tableDictionary.filter(_.isDeltaSource).map(_.name).contains) =>
        logger.info("Transaction table re-run. Ignoring...")
    }
  }
}
