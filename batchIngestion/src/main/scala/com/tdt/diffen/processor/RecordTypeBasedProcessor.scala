/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor

import java.text.SimpleDateFormat

import com.tdt.diffen.processor.DiffenBaseProcessor._
import com.tdt.diffen.utils.Enums.RunType.RunType
import com.tdt.diffen.utils.Enums.TableType.TableType
import com.tdt.diffen.utils.Enums.{RunType, TableType}
import com.tdt.diffen.utils.DiffenConstants._
import com.tdt.diffen.utils.DiffenUDFs.getDeleteFlagUdf
import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormatter

/**
  * Holds implementation methods for handling CDC and RecordType/OperationType based Flatfile processing
  * The primary functions are :
  * 1. processDiffenForIncremental
  * 2. processDiffenForFullDump
  */
class RecordTypeBasedProcessor extends DiffenBaseProcessor {

  @transient val logger: Logger = Logger.getLogger(classOf[RecordTypeBasedProcessor])
  /**
    * Entry point for processing Source Image for CDC and RecordType based filesets
    */
  def processDiffen(tableName: String, dataCols: List[String], primaryKeyCols: List[String], verifyTypesDf: DataFrame, previousDiffenDf: DataFrame,
                 recordTypeMeta: RecordTypeMetaInfo, tableType: TableType, runType: RunType, businessDate: DateTime, snapshotDateFormat: DateTimeFormatter)(implicit spark: SparkSession): DiffenOutput = {

    runType match {
      case RunType.INCREMENTAL => processDiffenForIncremental(tableName, dataCols, primaryKeyCols, verifyTypesDf, previousDiffenDf, recordTypeMeta, tableType, businessDate, snapshotDateFormat)(spark)
      case RunType.FULLDUMP => processDiffenForFullDump(tableName, dataCols, primaryKeyCols, verifyTypesDf, previousDiffenDf, recordTypeMeta, tableType, businessDate, snapshotDateFormat)(spark)
    }
  }

  /**
    * Function that handles data that is a delta of the previous day.  This new data is
    * 1. Filtered for duplicates &
    * 2. Analyzed for primary key changes
    *
    * All transitions of data for a primary key which is deleted will also move into DIFFEN-NON-OPEN
    * Only the latest record will get into DIFFEN-OPEN
    */
  private def processDiffenForIncremental(tableName: String, dataCols: List[String], primaryKeyCols: List[String], verifyTypesDfR: DataFrame, previousDiffenDfR: DataFrame,
                                       recordTypeMeta: RecordTypeMetaInfo, tableType: TableType, businessDate: DateTime, snapshotDateFormat: DateTimeFormatter)(implicit spark: SparkSession): DiffenOutput = {

    import recordTypeMeta._

    val diffenApplicableVTypesAuditCols = vTypesAuditCols.diff(List("vds", "filename"))
    val auditColList = diffenApplicableVTypesAuditCols ++ diffenAuditCols ++ systemAuditCols
    val orderColList = auditColList ++ dataCols

    val verifyTypesDf = verifyTypesDfR.orderedBy(orderColList).withColumn(RowSrc, lit(VerifyTypes))
    val previousDiffenDf = previousDiffenDfR.orderedBy(orderColList)

    //If there's no data today, just carry over the previousDiffen as DiffenOpen
    if (verifyTypesDf.head(1).isEmpty) {

      val emptyDiffenRdd = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], previousDiffenDf.schema)
      val emptyDuplicateRdd = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], StructType(Seq(StructField("rowid", StringType), StructField("duplicate_rowid", StringType))))

      val diffenOpen = tableType match {
        case TableType.DELTA => previousDiffenDf
        case TableType.TXN => emptyDiffenRdd
      }

      DiffenOutput(
        diffenOpen,
        emptyDiffenRdd,
        emptyDuplicateRdd,
        None,
        None)
    }
    else {
      val primarySortKey = recordTypeMeta.timestampColName

      val dateParser: (String => Long) = { sDate =>
        val formatter = new SimpleDateFormat(recordTypeMeta.timestampColFormat)
        formatter.parse(sDate).getTime
      }
      val dateParserUDF = udf(dateParser)

      val getRecordsOfOperationType = filterRecordsByColumnValue(recordTypeMeta.operationTypeColName) _ //either the operationtype column of CDC that contains I,A,B,D as values or D/X for Delimited

      logger.info ("Finding duplicates " + tableName)
      val (deDupVTypesDf, vTypeDups1) = findDuplicates(verifyTypesDf, orderColList, auditColList, ignorableColsForDeDuplication, orderColList, pickLatest = false)
      logger.info ("Union VTypes and Previous DIFFEN " + tableName)

      val vTypeDups = vTypeDups1.drop(RowSrc)

      val allDataDF = tableType match {
        case TableType.DELTA => previousDiffenDf.union(deDupVTypesDf)
        case TableType.TXN => deDupVTypesDf
      }

      val (diffenOpen, diffenNonOpen, bRecordsOpt, vTypeDupsXBRecords) = tableType match {
        case TableType.TXN if primaryKeyCols.isEmpty =>

          val diffenOpen = allDataDF.filter(col(operationTypeColName).isin(afterType, insertType, deleteType))
          val diffenNonOpen = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], diffenOpen.schema)
          (diffenOpen, diffenNonOpen, None, vTypeDups)

        case _ =>
          val allDataWithTSNumber = allDataDF.withColumn(SortKeyAsNumberColName, dateParserUDF(col(primarySortKey)))
          //bRecords - Will be used to filter out from Open
          val bRecords = getRecordsOfOperationType(verifyTypesDf, beforeType)
          val bRecordRowIds = bRecords.select(col(RowId))

          logger.info ("Getting latest Data by Sort key " + tableName)
          val sortKeyLatestDf =
            getLatestDataDf(allDataWithTSNumber, primaryKeyCols, SortKeyAsNumberColName)
              .drop(col(SortKeyAsNumberColName))
              .orderedBy(orderColList)

          logger.info ("Getting latest Data by RowId " + tableName)
          val latestDataDF = getLatestDataDf(sortKeyLatestDf.withColumn(RowIdAsNumber, rowIdUDF(col(RowId))), primaryKeyCols, RowIdAsNumber)
            .drop(col(RowIdAsNumber))
            .persist(StorageLevel.MEMORY_AND_DISK_SER)

          logger.info ("Diffen Open begin "  + tableName)
          val diffenOpen = latestDataDF.filter(col(operationTypeColName).isin(afterType, insertType))

          logger.info ("Diffen Open done. PKV Validation " + tableName)
          //PKV Validation
          val (updatedAllDataDF, pkRecordsChangedToD) = updateLatestBeforeToDeleteRecords(allDataDF, latestDataDF, recordTypeMeta, orderColList)

          logger.info ("Excludable Row ids " + tableName)
          val nonOpenExcludableRowIds = bRecordRowIds
            .join(pkRecordsChangedToD, List(RowId), "leftanti")
            .union(vTypeDups.select(RowId))

          logger.info ("Diffen Non-Open begin " + tableName)
          //DIFFEN_OPEN
          val diffenNonOpen = updatedAllDataDF
            .join(diffenOpen.select(col(RowId))
              .union(nonOpenExcludableRowIds), List(RowId), "leftanti")

          logger.info ("Diffen Non-Open done " + tableName)
          val actualBRecordsAfterPkValidation = bRecords.join (pkRecordsChangedToD.select(col(RowId)), List(RowId), "leftanti")
          val vTypeDupsExcludingBRecords = vTypeDups.join(bRecordRowIds.select(col(RowId)), List(RowId), "leftanti")

          (diffenOpen, diffenNonOpen, Some(actualBRecordsAfterPkValidation), vTypeDupsExcludingBRecords)
      }


      val diffenOutputWithAuditFields = DiffenOutput(diffenOpen, diffenNonOpen, vTypeDupsXBRecords, None, bRecordsOpt)
        .withAuditFields(businessDate, snapshotDateFormat)

      DiffenOutput(
        diffenOutputWithAuditFields.diffenOpen.orderedBy(orderColList),
        diffenOutputWithAuditFields.diffenNonOpen.orderedBy(orderColList),
        diffenOutputWithAuditFields.vTypesDuplicates,
        None,
        bRecordsOpt.map(_.orderedBy(orderColList))
      )
    }

  }


  /**
    * If for a primary key, the last record is a 'B', then it means that the primary key for that record has changed.
    * Utility function that converts such a record from B to D.  This record will eventually move into DIFFEN-NON-OPEN
    */
  private def updateLatestBeforeToDeleteRecords(allDataDF: DataFrame, latestDataDF: DataFrame, recordTypeMeta: RecordTypeMetaInfo, orderColList: List[String]) = {
    import recordTypeMeta._
    val primaryKeyChangedRecords = latestDataDF.filter(col(operationTypeColName) === beforeType) //last record is a B
    val primaryKeyChangedRecordsConvertedToD =
      primaryKeyChangedRecords
        .drop(operationTypeColName)
        .withColumn(operationTypeColName, lit(deleteType))
        .orderedBy(orderColList)

    val updatedAllDataWithD = allDataDF
      .join(primaryKeyChangedRecordsConvertedToD.select(col(RowId)), List(RowId), "leftanti")
      .union(primaryKeyChangedRecordsConvertedToD)
      .withColumn("del_flag_upd", getDeleteFlagUdf(col(recordTypeMeta.operationTypeColName), lit(recordTypeMeta.deleteType)))
      .drop(DeleteFlag)
      .withColumnRenamed("del_flag_upd", DeleteFlag)

    (updatedAllDataWithD, primaryKeyChangedRecordsConvertedToD)
  }


  /**
    * For cases when the source system sends the entire snapshot of a table as a "full dump", this function loads into the DIFFEN-OPEN and
    * DIFFEN-NON-OPEN.
    *
    * About 90% of the records would be duplicates of the existing DIFFEN-OPEN. These "duplicates" are extracted and logged. The original record
    * with the earlier "start_date" and "start_time" are retained to maintain history
    */
  private def processDiffenForFullDump(tableName: String, dataCols: List[String], primaryKeyCols: List[String], verifyTypesDfR: DataFrame, previousDiffenDfR: DataFrame, metaInfo: RecordTypeMetaInfo,
                                    tableType: TableType, businessDate: DateTime, snapshotDateFormat: DateTimeFormatter)(implicit spark: SparkSession): DiffenOutput = {

    import metaInfo._

    val auditColList = vTypesAuditCols.diff(List("vds", "filename")) ++ diffenAuditCols ++ systemAuditCols
    val orderColList = auditColList ++ dataCols
    val groupByCols = if (primaryKeyCols.isEmpty) dataCols else primaryKeyCols

    val verifyTypesDf = verifyTypesDfR.orderedBy(orderColList)
    val previousDiffenDf = previousDiffenDfR.orderedBy(orderColList)

    if (verifyTypesDf.head(1).isEmpty) {

      val emptyDiffenRdd = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], previousDiffenDf.schema)
      val emptyDuplicateRdd = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], StructType(Seq(StructField("rowid", StringType), StructField("duplicate_rowid", StringType))))

      val diffenOpen = tableType match {
        case TableType.DELTA => previousDiffenDf
        case TableType.TXN => emptyDiffenRdd
      }

      DiffenOutput(
        diffenOpen,
        emptyDiffenRdd,
        emptyDuplicateRdd,
        None,
        None)
    }
    else {

      val dateParser: (String => Long) = { sDate =>
        val formatter = new SimpleDateFormat(metaInfo.timestampColFormat)
        formatter.parse(sDate).getTime
      }

      val dateParserUDF = udf(dateParser)

      val getRecordsOfOperationType = filterRecordsByColumnValue(metaInfo.operationTypeColName) _
      val bRecordRowIds = getRecordsOfOperationType(verifyTypesDf, beforeType).select(col(RowId))
      val verifyTypesWithoutB = verifyTypesDf.join(bRecordRowIds, List(RowId), "leftanti").persist(StorageLevel.MEMORY_AND_DISK_SER)

      val (deDupVTypesDf, vTypeDups) = findDuplicates(verifyTypesWithoutB, groupByCols, auditColList, ignorableColsForDeDuplication, orderColList, pickLatest = true)

      val allDataDF = tableType match {
        case TableType.DELTA => previousDiffenDf.union(deDupVTypesDf)
        case TableType.TXN => deDupVTypesDf
      }

      val deletedPkRowIds = previousDiffenDf
        .withColumn(AllColString, concat_ws(";", groupByCols.map(col): _*))
        .select(col(RowId), col(AllColString))
        .join(verifyTypesDf.withColumn(AllColString, concat_ws(";", groupByCols.map(col): _*)), List(AllColString), "leftanti")
        .select(RowId)

      val (deDuplicates, diffenVsVTypeDups) = findDuplicates(allDataDF, dataCols, auditColList, ignorableColsForDeDuplication, orderColList, pickLatest = false)

      val allDataWithTSNumber = deDuplicates.withColumn(SortKeyAsNumberColName, dateParserUDF(col(metaInfo.timestampColName)))

      //DIFFEN_OPEN
      val sortKeyLatestDf = getLatestDataDf(allDataWithTSNumber, groupByCols, SortKeyAsNumberColName)
        .drop(col(SortKeyAsNumberColName))
        .orderedBy(orderColList)

      val diffenOpen =
        getLatestDataDf(sortKeyLatestDf.withColumn(RowIdAsNumber, rowIdUDF(col(RowId))), groupByCols, RowIdAsNumber).drop(col(RowIdAsNumber))
          .join(deletedPkRowIds, List(RowId), "leftanti")

      //DIFFEN_NONOPEN - except diffen open and duplicates
      val diffenNonOpen = allDataDF.join(
        diffenOpen.select(col(RowId))
          .union(vTypeDups.select(col(RowId)))
          .union(diffenVsVTypeDups.select(col(RowId)))
        , List(RowId), "leftanti")

      val diffenOutputWithAuditFields = DiffenOutput(diffenOpen, diffenNonOpen, vTypeDups, Option(diffenVsVTypeDups), None)
        .withAuditFields(businessDate,snapshotDateFormat)

      DiffenOutput(
        diffenOutputWithAuditFields.diffenOpen.orderedBy(orderColList),
        diffenOutputWithAuditFields.diffenNonOpen.orderedBy(orderColList),
        diffenOutputWithAuditFields.vTypesDuplicates,
        diffenOutputWithAuditFields.prevDiffenVsVtypesDuplicates,
        Some(bRecordRowIds)
      )
    }
  }
}
