/*
* Copyright 2019 The Data Team
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.tdt.diffen

import java.sql.Timestamp
import java.text.SimpleDateFormat

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.XmlParser.TableConfig
import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, _}
import org.apache.spark.sql.types.DecimalType

package object processor {

  /**
    * NiFi 1.2 doesn't support Avro's Logical types. Decimal, Date and Timestamp values are stored as string in Storage
    * and recovered as original datatypes in Diffen.
    *
    * @param diffenParams
    * @param tableConfig
    * @return
    */

  def transformExceptionalTypes(df: DataFrame, diffenParams: DiffenParams, tableConfig: TableConfig): DataFrame = {

    val decimalColConfigs = tableConfig.getColumnConfig().filter(colConfig => StringUtils.startsWithIgnoreCase(colConfig.typee, "DECIMAL"))
    val dateColConfigs = tableConfig.getColumnConfig().filter(colConfig => colConfig.typee.equalsIgnoreCase("DATE"))
    val timestampColConfigs = tableConfig.getColumnConfig().filter(colConfig => colConfig.typee.equalsIgnoreCase("TIMESTAMP"))

    val decColumns = decimalColConfigs.map { colConfig =>
      val (scale, precision) = getScaleAndPrecision(colConfig.typee)
      col(colConfig.name).cast(DecimalType(scale, precision))
    }

    val nonDecCols = df.columns.filterNot(c => decimalColConfigs.map(_.name.toLowerCase).contains(c.toLowerCase))

    val decDf = df.select(nonDecCols.map(col) ++ decColumns: _*)

    val dateDf = dateColConfigs.foldLeft(decDf) { (accDf, colConfig) =>
      val targetName = s"${colConfig.name}_diffencasted"
      accDf.withColumn(targetName, to_date(col(colConfig.name), diffenParams.dateColFormat)).drop(colConfig.name).withColumnRenamed(targetName, colConfig.name)
    }

    val timeDf = timestampColConfigs.foldLeft(dateDf) { (accDf, colConfig) =>
      val targetName = s"${colConfig.name}_diffencasted"
      accDf.withColumn(targetName, ConversionUtils.parseTS(col(colConfig.name), lit(diffenParams.timestampColFormat)))
        .drop(colConfig.name).withColumnRenamed(targetName, colConfig.name)
    }

    timeDf
  }

  private def getScaleAndPrecision(dataType: String) = {
    if (dataType.contains(",")) {
      val precisionScaleArray = StringUtils.substringBetween(dataType, "(", ")").split(",")
      (precisionScaleArray(0).toInt, precisionScaleArray(1).toInt)
    }
    else {
      val precisionStr = StringUtils.substringBetween(dataType, "(", ")")
      (precisionStr.toInt, 0)
    }
  }

}

object ConversionUtils extends Serializable {

  val parseTS = udf {
    (s: String, format: String) => {
      assert(format != null && !format.isEmpty, "Time format missing!")
      val dateFormat = new SimpleDateFormat(format)
      if(s == null || s.isEmpty) {
        null
      } else {
        new Timestamp(dateFormat.parse(s).getTime)
      }
    }
  }
}
