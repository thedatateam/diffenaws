/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor

import java.text.SimpleDateFormat

import com.tdt.diffen.utils.Enums.RunType.RunType
import com.tdt.diffen.utils.Enums.TableType.TableType
import com.tdt.diffen.utils.Enums.{RunType, TableType}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormatter

/**
  * Holds implementation methods for handling Non-CDC Flatfile processing
  * The primary functions are :
  * 1. processDiffenForIncremental
  * 2. processDiffenForFullDump
  */
class NonRecordTypeBasedProcessor extends DiffenBaseProcessor {

  import DiffenBaseProcessor._

  /**
    * Entry point for processing Source Image for Non-CDC and Non-RecordType based filesets
    */
  def processDiffen(tableName: String, dataCols: List[String], primaryKeyCols: List[String], verifyTypesDf: DataFrame, previousDiffenDf: DataFrame, metaInfo: NonRecordTypeMetaInfo,
                 tableType: TableType, runType: RunType, businessDate: DateTime, snapshotDateFormat: DateTimeFormatter)(implicit spark: SparkSession): DiffenOutput = {
    runType match {
      case RunType.INCREMENTAL => processDiffenForIncremental(tableName, dataCols, primaryKeyCols, verifyTypesDf, previousDiffenDf, metaInfo, tableType, businessDate, snapshotDateFormat)
      case RunType.FULLDUMP => processDiffenForFullDump(tableName, dataCols, primaryKeyCols, verifyTypesDf, previousDiffenDf, metaInfo, tableType, businessDate, snapshotDateFormat)
    }
  }

  /**
    * Function that handles data that is a delta of the previous day.  This new data is
    * 1. Filtered for duplicates &
    * 2. Analyzed for primary key changes
    *
    * All intermediary transitions of data for a primary key is extracted out as history data (DIFFEN-NON-OPEN).
    * All transitions of data for a primary key which is deleted will also move into DIFFEN-NON-OPEN
    * B-Records (for CDC) is ignored but the count logged.
    * Only the latest record will get into DIFFEN-OPEN
    */
  private def processDiffenForIncremental(tableName: String, dataCols: List[String], primaryKeyCols: List[String], verifyTypesDfR: DataFrame, previousDiffenDfR: DataFrame, metaInfo: NonRecordTypeMetaInfo,
                                       tableType: TableType, businessDate: DateTime, snapshotDateFormat: DateTimeFormatter)(implicit spark: SparkSession): DiffenOutput = {

    import metaInfo._

    val auditColList = vTypesAuditCols.diff(List("vds", "filename")) ++ diffenAuditCols ++ systemAuditCols
    val orderColList = auditColList ++ dataCols
    val groupByCols = if (primaryKeyCols.isEmpty) dataCols else primaryKeyCols

    val verifyTypesDf = verifyTypesDfR.orderedBy(orderColList).withColumn(RowSrc, lit(VerifyTypes))
    val previousDiffenDf = previousDiffenDfR.orderedBy(orderColList).withColumn(RowSrc, lit(PreviousDiffen))

    if (verifyTypesDf.head(1).isEmpty) {

      val emptyDiffenRdd = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], previousDiffenDf.schema)
      val emptyDuplicateRdd = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], StructType(Seq(StructField("rowid", StringType), StructField("duplicate_rowid", StringType))))

      val diffenOpen = tableType match {
        case TableType.DELTA => previousDiffenDf
        case TableType.TXN => emptyDiffenRdd
      }

      DiffenOutput(
        diffenOpen,
        emptyDiffenRdd,
        emptyDuplicateRdd,
        None,
        None)
    }
    else {
      val dateParser: (String => Long) = { sDate =>
        val formatter = new SimpleDateFormat(metaInfo.timestampColFormat)
        formatter.parse(sDate).getTime
      }

      val dateParserUDF = udf(dateParser)

      // val (deDupVTypesDf, vTypeDupsInt) = findDuplicates(verifyTypesDf, groupByCols, auditColList,ignorableColsForDeDuplication, orderColList, pickLatest = true)

      //For verify types duplicates across Flat files, two rows for a primary key are considered duplicates
      val allDataDF = tableType match {
        case TableType.DELTA =>
          previousDiffenDf.union(verifyTypesDf)
        case TableType.TXN =>
          verifyTypesDf
      }

      val (deDuplicates, dupsDf1) = findDuplicates(allDataDF, dataCols, auditColList, ignorableColsForDeDuplication, orderColList, pickLatest = false)

      val dupsDf = dupsDf1.checkpoint()
      val vTypeDups = dupsDf.filter(col(RowSrc) === VerifyTypes).drop(RowSrc).persist()
      val diffenVsVTypeDups = dupsDf.filter(col(RowSrc) === PreviousDiffen).drop(RowSrc).persist()

      val allDataWithTSNumber = deDuplicates
        .withColumn(SortKeyAsNumberColName, dateParserUDF(col(metaInfo.timestampColName)))
        .withColumn(RowIdAsNumber, rowIdUDF(col(RowId)))
        .checkpoint()


      val (diffenOpen1, diffenNonOpen1) = getDiffenDf(allDataWithTSNumber, groupByCols, SortKeyAsNumberColName, RowIdAsNumber)

      val diffenOpen = diffenOpen1.drop(SortKeyAsNumberColName, RowIdAsNumber)
      val diffenNonOpen = diffenNonOpen1.drop(SortKeyAsNumberColName, RowIdAsNumber)

      val diffenOutputWithAuditFields = DiffenOutput(diffenOpen, diffenNonOpen, vTypeDups, Option(diffenVsVTypeDups), None).withAuditFields(businessDate, snapshotDateFormat)

      DiffenOutput(
        diffenOutputWithAuditFields.diffenOpen.orderedBy(orderColList).checkpoint(),
        diffenOutputWithAuditFields.diffenNonOpen.orderedBy(orderColList).checkpoint(),
        diffenOutputWithAuditFields.vTypesDuplicates,
        diffenOutputWithAuditFields.prevDiffenVsVtypesDuplicates,
        None
      )
    }
  }


  /**
    * For cases when the source system sends the entire snapshot of a table as a "full dump", this function loads into the DIFFEN-OPEN and
    * DIFFEN-NON-OPEN.
    *
    * About 90% of the records would be duplicates of the existing DIFFEN-OPEN. These "duplicates" are extracted and logged. The original record
    * with the earlier "start_date" and "start_time" are retained to maintain history
    */
  private def processDiffenForFullDump(tableName: String, dataCols: List[String], primaryKeyCols: List[String], verifyTypesDfR: DataFrame, previousDiffenDfR: DataFrame,
                                    metaInfo: NonRecordTypeMetaInfo, tableType: TableType, businessDate: DateTime, snapshotDateFormat: DateTimeFormatter)(implicit spark: SparkSession): DiffenOutput = {

    import metaInfo._

    val auditColList = vTypesAuditCols.diff(List("vds", "filename")) ++ diffenAuditCols ++ systemAuditCols
    val orderColList = auditColList ++ dataCols
    val groupByCols = if (primaryKeyCols.isEmpty) dataCols else primaryKeyCols

    val verifyTypesDf = verifyTypesDfR.orderedBy(orderColList)
    val previousDiffenDf = previousDiffenDfR.orderedBy(orderColList)

    if (verifyTypesDf.head(1).isEmpty) {

      val emptyDiffenRdd = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], previousDiffenDf.schema)
      val emptyDuplicateRdd = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], StructType(Seq(StructField("rowid", StringType), StructField("duplicate_rowid", StringType))))

      val diffenOpen = tableType match {
        case TableType.DELTA => previousDiffenDf
        case TableType.TXN => emptyDiffenRdd
      }

      DiffenOutput(
        diffenOpen,
        emptyDiffenRdd,
        emptyDuplicateRdd,
        None,
        None)
    }
    else {

      val dateParser: (String => Long) = { sDate =>
        val formatter = new SimpleDateFormat(metaInfo.timestampColFormat)
        formatter.parse(sDate).getTime
      }

      val dateParserUDF = udf(dateParser)

      //For verify types duplicates across Flat files, two rows for a primary key are considered duplicates
      val (deDupVTypesDf, vTypeDups) = findDuplicates(verifyTypesDf, groupByCols, auditColList, ignorableColsForDeDuplication, orderColList, pickLatest = true)

      val allDataDF = tableType match {
        case TableType.DELTA => previousDiffenDf.union(deDupVTypesDf)
        case TableType.TXN => deDupVTypesDf
      }

      val deletedPkRowIds = previousDiffenDf
        .withColumn(AllColString, concat_ws(";", groupByCols.map(col): _*))
        .select(col(RowId), col(AllColString))
        .join(verifyTypesDf.withColumn(AllColString, concat_ws(";", groupByCols.map(col): _*)), List(AllColString), "leftanti")
        .select(RowId)

      val (deDuplicates, diffenVsVTypeDups) = findDuplicates(allDataDF, dataCols, auditColList, ignorableColsForDeDuplication, orderColList, pickLatest = false)

      val allDataWithTSNumber = deDuplicates.withColumn(SortKeyAsNumberColName, dateParserUDF(col(metaInfo.timestampColName)))

      //DIFFEN_OPEN
      val sortKeyLatestDf = getLatestDataDf(allDataWithTSNumber, groupByCols, SortKeyAsNumberColName)
        .drop(col(SortKeyAsNumberColName))
        .orderedBy(orderColList)

      val diffenOpen =
        getLatestDataDf(sortKeyLatestDf.withColumn(RowIdAsNumber, rowIdUDF(col(RowId))), groupByCols, RowIdAsNumber).drop(col(RowIdAsNumber))
        .join(deletedPkRowIds, List(RowId), "leftanti")

      //DIFFEN_NONOPEN - except diffen open and duplicates
      val diffenNonOpen = allDataDF.join(
        diffenOpen.select(col(RowId))
          .union(vTypeDups.select(col(RowId)))
          .union(diffenVsVTypeDups.select(col(RowId)))
        , List(RowId), "leftanti")

      val diffenOutputWithAuditFields = DiffenOutput(diffenOpen, diffenNonOpen, vTypeDups, Option(diffenVsVTypeDups), None).withAuditFields(businessDate, snapshotDateFormat)

      DiffenOutput(
        diffenOutputWithAuditFields.diffenOpen.orderedBy(orderColList),
        diffenOutputWithAuditFields.diffenNonOpen.orderedBy(orderColList),
        diffenOutputWithAuditFields.vTypesDuplicates,
        diffenOutputWithAuditFields.prevDiffenVsVtypesDuplicates,
        None
      )
    }
  }
}