/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor

import com.tdt.diffen.models.OpsModels.{ProcessMetadataEvent, RowCount}
import com.tdt.diffen.utils.DiffenConstants
import com.tdt.diffen.utils.DiffenConstants._
import com.tdt.diffen.utils.DiffenUDFs._
import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormatter

/**
  * Base Processor for both RecordType and Non-RecordType processors.
  */
trait DiffenBaseProcessor {

  import DiffenBaseProcessor._

  lazy val ignorableColsForDeDuplication = List("rowId", "filename", "vds", DeleteFlag)

  val rowIdToNumber: (String => Long) = { rowId =>
    StringUtils.substringBefore(rowId, "_").toLong
  }
  val rowIdUDF = udf(rowIdToNumber)

  /**
    * Given a DataFrame fetches the latest for a PrimaryKey (or a group of columns)
    */
  // TODO: Migrate to new getDiffenDf function
  protected def getLatestDataDf(allDataDF: DataFrame, groupByCols: List[String], numberSortKey: String)(implicit spark: SparkSession): DataFrame = {

    val allDataDFInt = allDataDF.withColumn(AllColString, concat_ws(";", groupByCols.map(col): _*))
    val latestCalculationDF = allDataDFInt.select(AllColString, numberSortKey)

    val latestRecordByPK = latestCalculationDF
      .groupBy(AllColString)
      .agg(max(numberSortKey))
      .withColumnRenamed(s"max($numberSortKey)", numberSortKey)

    //val latestRecordByPKCp = spark.createDataFrame(latestRecordByPK.rdd, latestRecordByPK.schema)

    allDataDFInt
      .join(latestRecordByPK, List(AllColString, numberSortKey))
      .drop(AllColString)

  }


  /**
    * Given a DataFrame fetches the latest for a PrimaryKey (or a group of columns)
    */
  protected def getDiffenDf(allDataDF: DataFrame, groupByCols: List[String], numberSortKey: String,
                         rowIdAsNumber: String)(implicit spark: SparkSession): (DataFrame, DataFrame) = {

    val latestCalculationDF = allDataDF.withColumn(AllColString, hash(groupByCols.map(col): _*))
    val windowSpec = Window.partitionBy(AllColString).orderBy(desc(numberSortKey), desc(rowIdAsNumber))

    val maxRowIdAsNumber = maxColumnName(rowIdAsNumber)

    val latestRecordByPK = latestCalculationDF
      .withColumn(maxRowIdAsNumber, first(col(rowIdAsNumber)).over(windowSpec))
      .persist(StorageLevel.MEMORY_AND_DISK)

    val diffenOpen = latestRecordByPK
      .filter(col(rowIdAsNumber) === col(maxRowIdAsNumber))
      .drop(maxRowIdAsNumber)


    val diffenNonOpen = latestRecordByPK
      .filter(col(rowIdAsNumber) =!= col(maxRowIdAsNumber))
      .drop(maxRowIdAsNumber)

    (diffenOpen.drop(AllColString), diffenNonOpen.drop(AllColString))
  }



  /**
    * Given a Dataframe and the columns to be considered for de-duplication, this function returns two Dataframes
    * 1. Duplicates (along with the reference to the retained record)
    * 2. De-duplicates
    *
    * There's also an option to retain either the latest or the oldest record while de-deduplicating.
    */
  def findDuplicates(inputDf: DataFrame, tableColumns: List[String], auditColList: List[String],
                               ignorableCols: List[String], orderColList: List[String], pickLatest: Boolean)
                              (implicit spark: SparkSession): (DataFrame, DataFrame) = {

    val dataColsWithPk = tableColumns.diff(ignorableCols)
    val dataDF = inputDf
      .withColumn(RowIdAsNumber, rowIdUDF(col(RowId)))
      .withColumn(AllColString, sha2(concat(dataColsWithPk.map(x => coalesce(col(x).cast(StringType), lit("")) ): _*), 512))


    val windowSpec = Window
      .partitionBy(col(AllColString))
      .orderBy(col(RowIdAsNumber))
      .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val processedDf =
      if (pickLatest) {
        dataDF
          .withColumn(DeDupRowId, last(col(RowId)).over(windowSpec))
          .withColumn(DeDupRowSrc, last(col(RowSrc)).over(windowSpec))
          .persist(StorageLevel.MEMORY_AND_DISK)
      } else {
        dataDF
          .withColumn(DeDupRowId, first(col(RowId)).over(windowSpec))
          .withColumn(DeDupRowSrc, first(col(RowSrc)).over(windowSpec))
          .persist(StorageLevel.MEMORY_AND_DISK)
      }

    val deDuplicates =
      processedDf
        .filter(col(RowId) === col(DeDupRowId))
        .drop(DeDupRowId, RowIdAsNumber, RowSrc, DeDupRowSrc)

    val exactDuplicates =
      processedDf
        .filter(col(RowId) =!= col(DeDupRowId))
        .drop(RowSrc)
        .drop(dataColsWithPk ++ auditColList.toSet.diff(Set(RowId)): _*)
        .withColumnRenamed(DeDupRowSrc, RowSrc)

    (deDuplicates.drop(AllColString), exactDuplicates.drop(AllColString))
  }

  implicit class DataframeOps(df: DataFrame) {
    def orderedBy(colList: List[String]): DataFrame = {
      df.select(colList.map(col): _*)
    }
  }

  protected def filterRecordsByColumnValue(recordTypeColName: String)(verifyTypesDf: DataFrame, recordType: String): DataFrame =
    verifyTypesDf.filter(col(recordTypeColName).isNotNull && col(recordTypeColName).eqNullSafe(recordType)) //Need to use it for rowcounts

  implicit class DiffenOutputOps(diffenOutput: DiffenOutput) {

    /**
      * Enriches a Dataframe with SCD related audit columns
      */
    def withAuditFields(businessDt: DateTime, snapshotDateFormat: DateTimeFormatter): DiffenOutput = {

      val businessDateStr = snapshotDateFormat.print(businessDt)
      val yesterday = snapshotDateFormat.print(businessDt.minusDays(1))

      val diffenOpen = diffenOutput.diffenOpen
        .withColumn("start_date_upd", retainIfNotNullOrSetUdf(col(StartDate), lit(businessDateStr)))
        .withColumn("start_time_upd", retainIfNotNullOrSetUdf(col(StartTime), lit(DiffenConstants.nowAsString())))
        .withColumn("end_date_upd", lit(DiffenConstants.EndOfDays))
        .withColumn("end_time_upd", lit(DiffenConstants.EndOfTimes))
        .drop(StartDate, StartTime, EndDate, EndTime)
        .withColumnRenamed("start_date_upd", StartDate)
        .withColumnRenamed("start_time_upd", StartTime)
        .withColumnRenamed("end_date_upd", EndDate)
        .withColumnRenamed("end_time_upd", EndTime)

      val diffenNonOpen = diffenOutput.diffenNonOpen
        .withColumn("start_date_upd", retainIfNotNullOrSetUdf(col(StartDate), lit(businessDateStr)))
        .withColumn("start_time_upd", retainIfNotNullOrSetUdf(col(StartTime), lit(DiffenConstants.nowAsString())))
        .withColumn("end_date_upd", setIfPresentOrFallbackToUdf(col(StartDate), lit(yesterday), lit(businessDateStr))) //If today's data, set today. Else, yesterday
        .withColumn("end_time_upd", lit(DiffenConstants.nowAsString()))
        .drop(StartDate, StartTime, EndDate, EndTime)
        .withColumnRenamed("start_date_upd", StartDate)
        .withColumnRenamed("start_time_upd", StartTime)
        .withColumnRenamed("end_date_upd", EndDate)
        .withColumnRenamed("end_time_upd", EndTime)

      diffenOutput.copy(diffenOpen = diffenOpen, diffenNonOpen = diffenNonOpen)
    }

//    def withOds(businessDt: DateTime): DiffenOutput = {
//
//      val businessDateStr = DiffenConstants.SnapshotDateFormat.print(businessDt)
//
//      val diffenOpen = diffenOutput.diffenOpen.withColumn("ods", lit(businessDateStr))
//      val diffenNonOpen = diffenOutput.diffenNonOpen.withColumn("nds", lit(businessDateStr))
//      val bRecordsOpt = diffenOutput.bRecords.map(_.withColumn("ods", lit(businessDateStr)))
//      val vTypesDuplicates = diffenOutput.vTypesDuplicates.withColumn("dds", lit(businessDateStr))
//      val prevDiffenVsVtypesDuplicatesOpt = diffenOutput.prevDiffenVsVtypesDuplicates.map(_.withColumn("dds", lit(businessDateStr)))
//
//      diffenOutput.copy(diffenOpen, diffenNonOpen, vTypesDuplicates, prevDiffenVsVtypesDuplicatesOpt, bRecordsOpt)
//    }
  }

}

object DiffenBaseProcessor {

  val SortKeyAsNumberColName = "sortKeyAsNumberColName"
  val RowIdAsNumber = "rowIdAsNumber"
  val RowId = "rowId"
  val AllColString = "ALL_COL_STRING"
  val DeDupRowId = "DUPLICATE_ROWID"
  val RowSrc = "s_row_src"
  val DeDupRowSrc = "s_dedup_row_src"
  val PreviousDiffen = "previousDiffen"
  val VerifyTypes = "verifyTypes"

  def maxColumnName(colName: String): String = {
    s"max($colName)"
  }
}

/**
  * A sub-set of the Global config that is closely tied with Diffen Processing.
  */
sealed trait DiffenMetaInfo {
  val timestampColName: String
  val timestampColFormat: String
  val vTypesAuditCols: List[String]
  val diffenAuditCols: List[String]
  val systemAuditCols: List[String]
}

case class NonRecordTypeMetaInfo(timestampColName: String, timestampColFormat: String, vTypesAuditCols: List[String], diffenAuditCols: List[String], systemAuditCols: List[String]) extends DiffenMetaInfo

case class RecordTypeMetaInfo(timestampColName: String, timestampColFormat: String, operationTypeColName: String, insertType: String, beforeType: String, afterType: String, deleteType: String,
                              vTypesAuditCols: List[String], diffenAuditCols: List[String], systemAuditCols: List[String]) extends DiffenMetaInfo

/**
  * Holder of the result of the DIFFEN processing
  * 1. DIFFEN-OPEN
  * 2. DIFFEN-NON OPEN
  * 3. Duplicates while looking at Verified types
  * 4. Duplicates that were a difference between Previous DIFFEN and today's Verify types (applicable only for Full Dump)
  * 5. B-Records (Applicable only for CDC/other Record Type based systems which has a 'Before' image of the data)
  */
case class DiffenOutput(diffenOpen: DataFrame, diffenNonOpen: DataFrame, vTypesDuplicates: DataFrame, prevDiffenVsVtypesDuplicates: Option[DataFrame] = None, bRecords: Option[DataFrame] = None)

case class AuditOutput(rowCountsOpt: Option[List[RowCount]], opsEvents: List[ProcessMetadataEvent], businessDate: String)
