/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor

import com.tdt.diffen.models.OpsModels._
import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.FileSystemUtils
import com.tdt.diffen.utils.DiffenUtils.{getBusinessDayPartition, getParamConf}
import com.tdt.diffen.utils.XmlParser.parseXmlConfig
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.catalyst.expressions.{GenericRowWithSchema, UnsafeRow}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}
import org.joda.time.DateTime

import scala.collection.mutable.WrappedArray
import scala.util.{Failure, Success, Try}

/**
  * Entry point for the Source Image construction process
  */
object Diffen {

  val logger: Logger = Logger.getLogger(Diffen.getClass)

  def main(args: Array[String]) {
    val nArgs = 5
    if (args.length != nArgs) {
      logger.error("Usage : Diffen <eod Marker> <business Day> <parameter xml hdfs path> <fulldump|incremental> <comma separated reruntable list")
      sys.error("Usage : Diffen <eod Marker> <business Day> <parameter xml hdfs path> <fulldump|incremental> <comma separated reruntable list")
      System.exit(1)
    } else {
      logger.info(s"Start time of the Job ${DateTime.now()}")
      logger.info("Spark Driver arguments - " + args.mkString(" | "))

      val sparkConf = new SparkConf()
        .registerKryoClasses(
          Array(
            classOf[WrappedArray.ofRef[_]],
            classOf[RowCount],
            classOf[Array[RowCount]],
            classOf[DiffenParams],
            classOf[Array[Row]],
            //Class.forName("com.databricks.spark.avro.DefaultSource$SerializableConfiguration"),
            Class.forName("scala.reflect.ClassTag$$anon$1"), //for SPARK-6497
            Class.forName("java.lang.Class"),
            Class.forName("org.apache.spark.sql.types.StringType$"),
            Class.forName("scala.collection.immutable.Map$EmptyMap$"),
            classOf[Array[InternalRow]],
            classOf[Array[StructField]],
            classOf[Array[Object]],
            classOf[Array[String]],
            classOf[StructField],
            classOf[Metadata],
            classOf[StringType],
            classOf[DecimalType],
            classOf[TimestampType],
            classOf[DateType],
            classOf[UnsafeRow],
            classOf[GenericRowWithSchema],
            classOf[StructType],
            classOf[OpsEvent],
            classOf[Array[OpsEvent]],
            classOf[ProcessMetadataEvent],
            classOf[Array[ProcessMetadataEvent]],
            Class.forName("org.apache.spark.sql.execution.joins.UnsafeHashedRelation"),
            Class.forName("org.apache.spark.sql.execution.joins.LongHashedRelation"),
            Class.forName("org.apache.spark.sql.execution.joins.LongToUnsafeRowMap"),
            Class.forName("org.apache.spark.sql.execution.columnar.CachedBatch"),
            //Class.forName("org.apache.spark.sql.execution.datasources.FileFormatWriter$WriteTaskResult"),
            classOf[org.apache.spark.sql.catalyst.expressions.GenericInternalRow],
            classOf[org.apache.spark.unsafe.types.UTF8String],
            Class.forName("[[B")
          )
        )
        .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
        //.set("spark.kryo.registrationRequired", "true")
        .set("mapreduce.fileoutputcommitter.algorithm.version", "2")
        .set("mapreduce.fileoutputcommitter.marksuccessfuljobs", "false")

      implicit val sparkSession = SparkSession
        .builder()
        .config(sparkConf)
        .appName("DiffenProcess_0.1")
        .enableHiveSupport()
//        .master("local[*]")
        .getOrCreate()

      logger.info("Executing testing query.")
      Try(sparkSession.sql("show databases")) match {
        case Success(_) => logger.info("Successfully executed tested query.")
        case Failure(exception) =>
          logger.error("Failed to execute test query.", exception)
          System.exit(1)
      }

      sparkSession.sparkContext.setLocalProperty("spark.scheduler.pool", "production")

      val Array(eodMarker, submitTime, paramXmlHdfsPath, runType, rerunTableList) = args

      if (!paramXmlHdfsPath.startsWith("s3") || !paramXmlHdfsPath.startsWith("hdfs") || !paramXmlHdfsPath.startsWith("file") ){
        val fs: FileSystem = new Path(paramXmlHdfsPath).getFileSystem(sparkSession.sparkContext.hadoopConfiguration)
        FileSystemUtils.setFileSystem(fs)

        val (todaysPartition, paramPath, diffenParams, tableConfigs, rerunTables, paramConf) = parseParameters(fs, eodMarker, submitTime, paramXmlHdfsPath,  runType, rerunTableList)

        sparkSession.sparkContext.setCheckpointDir(diffenParams.hdfsTmpDir + "/checkpoint")

        BootstrapProcessor.businessDateProcess(todaysPartition, diffenParams, tableConfigs, rerunTables, paramConf)
        logger.info("businessDateProcess End")
        //StandardizedLayer.createStandardizeddLayer(todaysPartition,diffenParams)(sparkSession)
        sparkSession.stop()
        //System.exit(0)
    }
      else{
        logger.error("Invalid file system protocol. Correct schemes are s3a://, hdfs://, file://")
        sys.error("Invalid file system protocol. Correct schemes are s3a://, hdfs://, file://")
        System.exit(1)
      }
    }

    /**
      * Parses parameters and submits the Diffen Job
      */
    def parseParameters(fs: FileSystem, eodMarker: String, submitTime: String, paramPath: String, runType: String, rerunTableList: String) = {

      //val Array(eodMarker, submitTime, paramPath, runType, rerunTableList) = args

      //val fs: FileSystem = FileSystemUtils.fetchFileSystem
      val paramConf: Configuration = getParamConf(fs, paramPath)
      val todaysPartition: String = getBusinessDayPartition(submitTime, paramConf.get("diffen.config.timestampcolformat"), paramConf.get("diffen.config.snapshotDate.format", "yyyy-MM-dd")) match {
        case Some (part) => part
        case None => throw new scala.Exception(" Business date and todays partition format were incorrectly specified!")
      }
      val diffenParams: DiffenParams = DiffenParams(paramConf, todaysPartition, todaysPartition, runType, eodMarker)
      val tableConfigsFromParamFile = parseXmlConfig(paramPath, fs)
      val tableConfigs = parseXmlConfig(diffenParams.tableConfigXml, fs) ++ tableConfigsFromParamFile
      val rerunTables: List[String] = RerunProcessor.getRerunTableList(rerunTableList, fs, tableConfigs)
      val rerunFlag = rerunTableList.nonEmpty

      val tableDictionary = if (rerunTables.isEmpty) tableConfigs else tableConfigs.filter(x => rerunTables.contains(x.name))

      (todaysPartition, paramPath, diffenParams.copy(isRerun = rerunFlag), tableDictionary, rerunTables, paramConf)

    }
  }

}
