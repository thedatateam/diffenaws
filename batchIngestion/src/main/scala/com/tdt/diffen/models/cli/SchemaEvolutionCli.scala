/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.models.cli

import org.rogach.scallop.ScallopConf

import scala.util.Try

class SchemaEvolutionCli(arguments: Seq[String]) extends ScallopConf(arguments) {

  val newTableConfigPath = opt[String](required = true, descr = "hdfs directory where new table config files are available", argName = "newTableConfigPath")
  val paramConfigPath = opt[String](required = true, descr = "Location on hdfs where param config files are available", argName = "paramConfigPath")
  val startMarker = opt[String](descr = "Business date from which schema evolution is being done", argName = "startMarker")
  val endMarker = opt[String](descr = "Business date till which schema evolution is being done", argName = "endMarker")
  val tableNames = opt[List[String]](descr = "Table names for which schema evolution to be done", argName = "tableNames")
  val generateBackwardCompatibleTableConfig = toggle("generate-backward-compatible-table-config", default = Option(false), descrYes = "Set this parameter for generating singleTableConfig", descrNo = "By default it will be generating individual table config file per table")


  override def toString = "SchemaEvolutionCli(" +
    s"\n\tnewTableConfigPath=$getNewTableConfigPath" +
    s"\n\tparamConfigPath=$getParamConfigPath" +
    s"\n\ttableNames=$getTableNames" +
    s"\n\tgenerateBackwardCompatibleTableConfig=$isGenerateBackwardCompatibleTableConfig" +
    Try {s"\n\tstartMarker=$getStartMarker"}.getOrElse("") +
    Try {s"\n\tendMarker=$getEndMarker"}.getOrElse("") +
    s"\n)"

  def getNewTableConfigPath = newTableConfigPath.toOption.get.trim
  def getParamConfigPath = paramConfigPath.toOption.get.trim
  def getStartMarker = startMarker.toOption.get.trim
  def getEndMarker = endMarker.toOption.get.trim
  def getTableNames = tableNames.toOption.getOrElse(List())
  def isGenerateBackwardCompatibleTableConfig = generateBackwardCompatibleTableConfig.toOption.get

  verify()
}