/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.models

import java.util.UUID

import com.tdt.diffen.utils.Enums.RunType.RunType
import com.tdt.diffen.utils.Enums.SourceType.SourceType
import com.tdt.diffen.utils.Enums.{RunType, SourceType}
import com.tdt.diffen.utils.DiffenConstants._
import org.apache.hadoop.conf.Configuration
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.util.{Failure, Success, Try}

//FIXME Source this from pureconfig
/** Scala binding for param.xml **/
case class DiffenParams(source: String, country: String, hdfsBaseDir: String,
                     diffenOpenSchema: String, diffenNonOpenSchema: String, verifyTypesSchema: String, opsSchema: String,
                     diffenOpenPartitionColumn: String, diffenNonOpenPartitionColumn: String, verifyTypesPartitionColumn: String,
                     hdfsTmpDir: String, tableConfigXml: String, reconTableName: String, reconTableNameColumn: String, reconCountColumn: String,
                     runType: RunType, businessDate: String, batchPartition: String,
                     eodMarker: String, sourcingType: SourceType,
                     eodTableName: String, reconOutputTableName: String, metadataSchema: String, recordTypeBatch: String,
                     dateColFormat: String,
                     timestampCol: String, timestampColFormat: String,
                     vTypesAuditColSchema: String, diffenAuditColSchema: String, systemAuditColSchema: String,
                     standardizedlayerSchema: String ="",
                     parallelExecution: Boolean = true, isTableLevelEod: Boolean = false,
                     reconSourceQuery: String = "", isRerun: Boolean = false, snapshotFormatString: String = "yyyy_MM_dd") {

  def getDiffenOpenPartitionPath(tableName: String, partition: String): String = {
    getDiffenOpenPath(tableName) + diffenOpenPartitionColumn + "=" + partition + "/"
  }


  def getDiffenOpenPath(tableName: String): String = {
    hdfsBaseDir + diffenOpenSchema + "/" + tableName + "/"
  }

  def getDiffenOpenTmpPath(tableName: String): String = {
    hdfsTmpDir + "/" + diffenOpenSchema + "/" + tableName + "/"
  }

  def getDiffenNonOpenPartitionPath(tableName: String, partition: String): String = {
    getDiffenNonOpenPath(tableName) + diffenNonOpenPartitionColumn + "=" + partition + "/"
  }

  def getDiffenNonOpenPath(tableName: String): String = {
    hdfsBaseDir + diffenNonOpenSchema + "/" + tableName + "/"
  }

  def getDiffenNonOpenTmpPath(tableName: String): String = {
    hdfsTmpDir + "/" + diffenNonOpenSchema + "/" + tableName + "/"
  }

  def getVerifyTypesPartitionPath(tableName: String, partition: String): String = {
    getVerifyTypesPath(tableName) + verifyTypesPartitionColumn + "=" + partition + "/"
  }

  def getVerifyTypesPath(tableName: String): String = {
    hdfsBaseDir + verifyTypesSchema + "/" + tableName + "/"
  }

  def getVerifyTypesTmpPath(tableName: String): String = {
    hdfsTmpDir + "/" + verifyTypesSchema + "/" + tableName + "/"
  }

  def isCdcSource: Boolean = {
    sourcingType == SourceType.CDC
  }

  def isRecordTypeBatch: Boolean = {
    isBatchSource && recordTypeBatch.equalsIgnoreCase("Y")
  }

  def isBatchSource: Boolean = {
    (sourcingType == SourceType.BATCH_FIXEDWIDTH) || (sourcingType == SourceType.BATCH_DELIMITED)
  }

  def fullDump = if (runType == RunType.FULLDUMP) true else false

  def businessDt: DateTime = {
    Try(TimestampFormat.parseDateTime(businessDate)) match {
      case Success(bd) => bd
      case Failure(throwable) =>
        //Allow it to blow up if both formats doesn't work
        getSnapshotFormat.parseDateTime(businessDate)
    }
  }

  def getSnapshotFormat = DateTimeFormat.forPattern(snapshotFormatString)
}

/** Factory for DiffenParams **/
object DiffenParams {
  def apply(paramConf: Configuration): DiffenParams = {
    apply(paramConf, "", "", "", "")
  }

  def apply(paramConf: Configuration, partition: String, businessDate: String, runType: String, eodMarker: String): DiffenParams = {

    val country: String = paramConf.get("diffen.config.country")
    val source: String = paramConf.get("diffen.config.source")
    val hdfsDataParentDir = paramConf.get("diffen.config.hdfs.data.parent.dir") + "/"
    val diffenOpenSchema = paramConf.get("diffen.config.diffen.open.schema")
    val standardizedlayerSchema = paramConf.get("diffen.config.standardized.layer.schema","")
    val diffenNonOpenSchema = paramConf.get("diffen.config.diffen.nonopen.schema")
    val verifyTypesSchema = paramConf.get("diffen.config.storage.schema")
    val opsSchema: String = paramConf.get("diffen.config.ops.schema")

    val diffenOpenPartitionName = paramConf.get("diffen.config.diffen.open.partition.name", "ods")
    val diffenNonOpenPartitionName = paramConf.get("diffen.config.diffen.nonopen.partition.name", "nds")
    val verifyTypesPartitionName = paramConf.get("diffen.config.storage.partition.name", "vds")

    val dateColFormat: String = paramConf.get("diffen.config.datecolformat")
    val timestampCol: String = paramConf.get("diffen.config.timestampcol")
    val timestampColFormat: String = paramConf.get("diffen.config.timestampcolformat")

    val hdfsTmpDir = paramConf.get("diffen.config.hdfs.tmpdir", "/tmp") + "/" + UUID.randomUUID.toString
    val tableConfigXml = paramConf.get("diffen.config.hdfs.tableconfig.xml.path")

    val reconTable = paramConf.get("diffen.config.recon.tablename", "")
    val reconTableNameCol = paramConf.get("diffen.config.recon.tablename.colname", "")
    val reconTableCountCol = paramConf.get("diffen.config.recon.tablecount.colname", "")

    val recordTypeBatch = paramConf.get("diffen.config.batch.recordtype", "N").trim

    val vTypesAuditColSchema = paramConf.get("diffen.config.vtypes.auditcolschema", "")
    val diffenAuditColSchema = paramConf.get("diffen.config.diffen.auditcolschema", "")
    val systemAuditColSchema = paramConf.get("diffen.config.system.auditcolschema", "")

    val runTypeEnum = RunType.fromString(runType).getOrElse(RunType.INCREMENTAL)

    val sourcingType = SourceType.fromString(paramConf.get("diffen.config.sourcing.type")).getOrElse(SourceType.CDC)

    val eodTableName = paramConf.get("diffen.config.eod.table.name", "eod_table")
    val parallelExecution = paramConf.get("diffen.config.parallel.execution", "true").toBoolean

    val isTableLevelEod = paramConf.get("diffen.config.eod.marker.strategy", "global_eod").equals(TableLevelEod)

    val reconSourceQuery = paramConf.get("diffen.config.recon.source.query", "")

    val reconOutputTableName = paramConf.get("diffen.config.recon.output.tablename", "recon_output")
    val metadataTableName = paramConf.get("diffen.config.commonmetadata.schema", s"${source}_common_metadata")
    val snapshotString: String = paramConf.get("diffen.config.snapshotDate.format", "yyyy-MM-dd")
    new DiffenParams(source, country, hdfsDataParentDir, diffenOpenSchema, diffenNonOpenSchema, verifyTypesSchema, opsSchema,
      diffenOpenPartitionName, diffenNonOpenPartitionName, verifyTypesPartitionName,
      hdfsTmpDir, tableConfigXml, reconTable, reconTableNameCol, reconTableCountCol, runTypeEnum, businessDate, partition, eodMarker,
        sourcingType, eodTableName, reconOutputTableName, metadataTableName, recordTypeBatch,
      dateColFormat,
      timestampCol, timestampColFormat,
      vTypesAuditColSchema, diffenAuditColSchema, systemAuditColSchema,standardizedlayerSchema, parallelExecution, isTableLevelEod, reconSourceQuery, snapshotFormatString = snapshotString
    )
  }
}