package com.tdt.diffen.utils

import java.io.{File, FileInputStream, PrintWriter}

import com.tdt.diffen.utils.DiffenConstants._
import org.apache.hadoop.conf.Configuration
import org.apache.log4j.Logger

import scala.collection.JavaConverters._

object TableConfigSplitter {

  val logger: Logger = Logger.getLogger(TableConfigSplitter.getClass)

  def main(args: Array[String]): Unit = {

    val nArgs = 2
    if (args.length != nArgs) {
      logger.error("Usage : TableConfigSplitter <source path> <target path>")
      sys.error("Usage : TableConfigSplitter <source path> <target path>")
      System.exit(1)
    }

    val sourcePath = args(0)
    val targetFolder = args(1)

    logger.info("Input files location : " + sourcePath)
    logger.info("Output files folder : " + targetFolder)

    new File(targetFolder).mkdirs()
    val newTableConfigContent = constructTableConfigs(sourcePath)
    logger.info("Total number of distinct tables found : " + newTableConfigContent.size)

    newTableConfigContent.foreach(x=> {
      logger.info(s"Creating file : ${x._1}_tables_config.xml" )
      val pw = new PrintWriter(new File(targetFolder, s"${x._1}_tables_config.xml"))
      pw.write(x._2)
      pw.close
    })
  }

  def constructTableConfigs(sourcePath: String) = {
    val updatedTableConfigs = getConfs(sourcePath)
    updatedTableConfigs.map( prop =>
      (prop._1, "<configuration>\n" + prop._2.toSeq.sortBy(_._2).map(attribute =>
            "<property>\n" +
              s"\t<name>${attribute._2}</name>\n" +
              s"\t<value>${attribute._3}</value>\n" +
              "</property>\n").mkString("\n") +
          "</configuration>"))
  }

  def getConfs(path: String) = {
    val sourcePath = new File(path)
    val patternR = (PrefixConfigDiffenTable).r

    val keyValuePairs = if(sourcePath.isDirectory) {
      sourcePath.listFiles().filter(_.getName.endsWith("xml")).map(t=> {
        logger.info(s"Processing file : ${t.getAbsoluteFile}")
        val paramConf: Configuration = new Configuration()
        paramConf.addResource(new FileInputStream(t.getAbsolutePath))
        paramConf.getValByRegex(patternR.toString()).asScala
      }).flatten[(String, String)].toMap
    } else {
      logger.info(s"Processing file : ${path}")
      val paramConf = new Configuration()
      paramConf.addResource(new FileInputStream(path))
      paramConf.getValByRegex(patternR.toString()).asScala.toMap
    }
    keyValuePairs.map(x=> (x._1.split('.')(2), x._1, x._2)).groupBy(_._1)
  }
}
