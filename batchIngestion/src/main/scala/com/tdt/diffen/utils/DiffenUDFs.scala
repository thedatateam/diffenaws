package com.tdt.diffen.utils

import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.functions.udf

/** Collection of User Defined functions that are used with the Spark [[org.apache.spark.sql.DataFrame]] */
object DiffenUDFs {

  /** Returns a default value if the original column value is null */
  val retainIfNotNullOrSet: ((String, String) => String) = { (actualValue, fallValue) =>
    if (StringUtils.isBlank(actualValue)) fallValue
    else actualValue
  }

  val retainIfNotNullOrSetUdf = udf(retainIfNotNullOrSet)

  /** Updates a column value with a new value if the column value is NOT null */
  val setIfPresentOrFallbackTo: ((String, String, String) => String) = { (actualValue, ifPresentValue, ifAbsentValue) =>
    if (StringUtils.isNotBlank(actualValue)) ifPresentValue
    else ifAbsentValue
  }

  val setIfPresentOrFallbackToUdf = udf(setIfPresentOrFallbackTo)

  /** The delete_flag must be 1 (or set to True) if a row is a 'D' record. Else, the delete_flag must be 0. */
  val getDeleteFlag: ((String, String) => String) = { (currentValue, deleteValue) =>
    if (StringUtils.equalsIgnoreCase(currentValue, deleteValue)) "1"
    else "0"
  }

  val getDeleteFlagUdf = udf(getDeleteFlag)
}
