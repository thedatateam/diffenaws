package com.tdt.diffen.utils

import java.sql.Timestamp

import java.sql.Timestamp

import com.tdt.diffen.models.OpsModels._
import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.processor.DiffenOutput
import com.tdt.diffen.utils.DiffenConstants._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.functions.{col, lit}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.joda.time.format.DateTimeFormat

import scala.util.Try


/** A composition of utility and persistence functions for operation tables **/
object Ops {

  /**
    * This function is used to write
    *   1. Duplicates in Verify Types in case of Incremental.
    *   2. Previous DIFFEN vs VerifyTypes in case of FullDump.
    *
    * @param diffenParams  instance of [[DiffenParams]]
    * @param spark      Spark Session
    * @param duplicates Dataframe that wraps the duplicate row ids
    * @param tableName  Table that corresponds to these duplicate entries
    */
  def writeDuplicates(diffenParams: DiffenParams, duplicates: DataFrame, tableName: String, stepName: String)
                     (implicit spark: SparkSession): Unit = {
    import diffenParams._

    if (duplicates.head(1).nonEmpty) {
      val duplicatesTableName = s"${source}_${country}_duplicates"
      val duplicatesDf = duplicates.select("ROWID", "DUPLICATE_ROWID").withColumn("STEPNAME", lit(stepName)).withColumn("TABLENAME", lit(tableName)).withColumn("asof", lit(DiffenConstants.nowAsString()))
      val targetPath = hdfsBaseDir + diffenParams.opsSchema + "/" + duplicatesTableName
      spark.synchronized { //Duplicates of all tables get into this
        DiffenUtils.persistAsHiveTable(duplicatesDf, targetPath, diffenParams.opsSchema, duplicatesTableName, "dds", diffenParams.businessDate, SaveMode.Append)
      }
    }
  }

  /**
    * Persists the [[com.tdt.diffen.models.OpsModels.RowCount]] [[DataFrame]] to the appropriate HDFS location and creates a
    * Hive partition over it
    *
    * @param diffenParams   instance of [[DiffenParams]] for this job
    * @param rowCountsDf [[DataFrame]] of [[com.tdt.diffen.models.OpsModels.RowCount]] objects
    * @param spark       Spark Session
    */
  def writeRowCounts(diffenParams: DiffenParams, rowCountsDf: DataFrame)(implicit spark: SparkSession): Unit = {
    val rowCountTableName: String = diffenParams.source + "_" + diffenParams.country + "_rowcounts"
    val processMetadataTableLocation = s"${diffenParams.hdfsBaseDir}${diffenParams.opsSchema}/$rowCountTableName"
    val partition = s"""rcds=${diffenParams.businessDate}"""
    val partitionQuoted = s"""rcds='${diffenParams.businessDate}'"""
    rowCountsDf
      .write
      .format(OrcFormat)
      .mode(SaveMode.Append)
      .save(s"$processMetadataTableLocation/$partition")

    DiffenUtils.createHivePartition(diffenParams.opsSchema, rowCountTableName, partitionQuoted)
  }


  /**
    * Given the result of the Diffen process [[DiffenOutput]] and verifyTyples generates a [[Seq]] of loggable [[RowCount]] instances
    */
  def getRowCount(diffenParams: DiffenParams, tableName: String, sourceType: String, verifyTypes: DataFrame, diffenOutput: DiffenOutput, earlyCount: Long, jobId: String, runType: String, isRerun: String)(implicit spark: SparkSession): List[RowCount] = {
    val nowAsString: String = DiffenConstants.nowAsString()

    val stepName = if (sourceType.equals("delta")) "DIFFEN OPEN" else "DIFFEN TXN"

    val generalRowCounts = List(
      RowCount(diffenParams.diffenOpenSchema, "DiffenOpen", "Diffen Open", getOrcFileCount(diffenParams.getDiffenOpenPartitionPath(tableName, diffenParams.businessDate)),
        tableName, nowAsString, stepName, jobId, runType, isRerun),
      RowCount(diffenParams.diffenNonOpenSchema, "DiffenNonOpen", "Diffen Non Open", diffenOutput.diffenNonOpen.count(), tableName, nowAsString, "DIFFEN NONOPEN", jobId, runType, isRerun),

      RowCount(diffenParams.verifyTypesSchema, "BRecordsDiffen", "Storage", diffenOutput.bRecords.map(_.count()).getOrElse(0), tableName, nowAsString, "BRecordsDiffen", jobId, runType, isRerun),
      RowCount(diffenParams.opsSchema, "StorageDuplicates", "Storage", diffenOutput.vTypesDuplicates.count(), tableName, nowAsString, "Duplicates", jobId, runType, isRerun),
      RowCount(diffenParams.opsSchema, "DiffenDuplicates", "Storage", diffenOutput.prevDiffenVsVtypesDuplicates.map(_.count()).getOrElse(0), tableName, nowAsString, "FullDumpDuplicates", jobId, runType, isRerun),
      RowCount(diffenParams.verifyTypesSchema, "VerifyTypesDiffen", "Storage", verifyTypes.count(), tableName, nowAsString, "VerifyTypesDiffen", jobId, runType, isRerun),
      RowCount(diffenParams.verifyTypesSchema, "EarlyArrivalDiffen", "Storage", earlyCount, tableName, nowAsString, "EarlyArrivalDiffen", jobId, runType, isRerun)
    )

    val specializedRowCounts: List[RowCount] =
      if (diffenParams.isCdcSource) {
        //All DFs are already cached. So, count is a trivial operation
        val diffenOpenInsertForTodayCount = diffenOutput.diffenOpen.filter(col("s_startdt").equalTo(diffenParams.businessDate)).filter(col("c_operationtype").equalTo(lit("I"))).count()
        val diffenNonOpenInsertForTodayCount = diffenOutput.diffenNonOpen.filter(col("s_startdt").equalTo(diffenParams.businessDate)).filter(col("c_operationtype").equalTo(lit("I"))).count()

        val diffenOpenUpdateForTodayCount = diffenOutput.diffenOpen.filter(col("s_startdt").equalTo(diffenParams.businessDate)).filter(col("c_operationtype").equalTo(lit("A"))).count()
        val diffenNonOpenUpdateForTodayCount = diffenOutput.diffenNonOpen.filter(col("s_startdt").equalTo(diffenParams.businessDate)).filter(col("c_operationtype").equalTo(lit("A"))).count()

        val diffenDeleteCount = diffenOutput.diffenNonOpen.filter(col(DeleteFlag).equalTo("1")).count()

        List(
          //Differentiate between Insert/Update and Delete
          RowCount(diffenParams.diffenOpenSchema, "DiffenInserts", "Diffen", (diffenOpenInsertForTodayCount + diffenNonOpenInsertForTodayCount), tableName, nowAsString, "Diffen-Inserts", jobId, runType, isRerun),
          RowCount(diffenParams.diffenOpenSchema, "DiffenUpdates", "Diffen", (diffenOpenUpdateForTodayCount + diffenNonOpenUpdateForTodayCount), tableName, nowAsString, "Diffen-Updates", jobId, runType, isRerun),
          RowCount(diffenParams.diffenOpenSchema, "DiffenDeletes", "Diffen", diffenDeleteCount, tableName, nowAsString, "Diffen-Deletes", jobId, runType, isRerun)
        )
      }
      else {
        List()
      }

    generalRowCounts ++ specializedRowCounts
  }

  /**
    * Persists the [[com.tdt.diffen.models.OpsModels.ProcessMetadataEvent]] [[DataFrame]] to the appropriate HDFS location and creates a
    * Hive partition over it
    *
    * @param events    [[DataFrame]] of [[ProcessMetadataEvent]] objects
    * @param diffenParams instance of [[DiffenParams]] for this job
    * @param spark     Spark Session
    */
  def writeProcessMetadataEvents(events: List[ProcessMetadataEvent], diffenParams: DiffenParams)(implicit spark: SparkSession): Unit = {

    val processMetadataTableLocation = s"${diffenParams.hdfsBaseDir}${diffenParams.opsSchema}/${diffenParams.source}_process_metadata"
    val partition = s"""partition_date=${diffenParams.businessDate}"""
    val partitionQuoted = s"""partition_date='${diffenParams.businessDate}'"""

    import spark.sqlContext.implicits._
    spark.sparkContext.parallelize(events)
      .toDF()
      .write
      .format(OrcFormat)
      .mode(SaveMode.Append)
      .save(s"$processMetadataTableLocation/$partition")

    DiffenUtils.createHivePartition(diffenParams.opsSchema, s"${diffenParams.source}_process_metadata", partitionQuoted)
  }

  /** Converts the simplified version of Operational Events ([[com.tdt.diffen.models.OpsModels.OpsEvent]] to the full version ([[com.tdt.diffen.models.OpsModels.ProcessMetadataEvent]] */
  def convertToProcessMetadataEvents(opsEvents: List[OpsEvent], diffenParams: DiffenParams, tableName: String, status:String, runType: String, module: String): List[ProcessMetadataEvent] = {

    opsEvents.map { opsEvent =>
      if (opsEvent.event.startsWith("ERROR"))
        ProcessMetadataEvent(new Timestamp(opsEvent.date.getMillis), "processing", tableName, "FAILURE", opsEvent.event,
          TimestampFormat.print(opsEvent.date), "processing", "", runType, diffenParams.businessDate, "", diffenParams.source,
          diffenParams.country, "Diffen Processing")
      else
        ProcessMetadataEvent(new Timestamp(opsEvent.date.getMillis), "processing", tableName, "SUCCESS", opsEvent.event,
          TimestampFormat.print(opsEvent.date), "processing", "", runType, diffenParams.businessDate, "", diffenParams.source,
          diffenParams.country, "Diffen Processing")
    }
  }

  /**
    * Persists the EOD Record for today to the appropriate HDFS location and creates a
    * Hive partition over it
    *
    * @param diffenParams instance of [[DiffenParams]] for this job
    * @param eodRecord case class that wraps all the EOD information
    * @param spark     Spark Session
    */
  def writeEodTableAndMarker(diffenParams: DiffenParams, eodRecord: EODTableRecord)(implicit spark: SparkSession): Unit = {
    val eodTableRecord = List(eodRecord)
    val tableName = diffenParams.eodTableName

    val eodTableLocation = s"${diffenParams.hdfsBaseDir}/${diffenParams.opsSchema}/$tableName"
    val partition = s"""businessdate=${diffenParams.businessDate}"""
    val partitionQuoted = s"""businessdate='${diffenParams.businessDate}'"""
    spark.createDataFrame(eodTableRecord)
      .write
      .format(OrcFormat)
      .mode(SaveMode.Overwrite)
      .save(s"$eodTableLocation/$partition")

    DiffenUtils.createHivePartition(diffenParams.opsSchema, tableName, partitionQuoted)
  }


  /**
    * If the EOD marker strategy is table-level, then this function will retrieve all the EOD markers for the tables in this source application.
    *
    * @param spark                Instance of [[SparkSession]]
    * @param diffenParams            Instance of [[DiffenParams]]
    * @param paramConf            Unparsed Hadoop [[Configuration]]
    * @param previousBusinessDate Previous business date, in order to lookup the markers
    * @return List of marker times
    */
  def parseEodMarkerForTable(spark: SparkSession, diffenParams: DiffenParams, paramConf: Configuration, previousBusinessDate: String): List[TableLevelEodMarkers] = {
    val markerTable = paramConf.get("diffen.config.eod.marker.table.name")
    val eodTablePartitionPath = diffenParams.getVerifyTypesPartitionPath(markerTable, previousBusinessDate)
    val colName = paramConf.get("diffen.config.eod.marker.column.name")
    val tableColName = paramConf.get("diffen.config.eod.marker.column.table.name")

    if (diffenParams.isTableLevelEod && FileSystemUtils.fetchFileSystem.exists(new Path(eodTablePartitionPath))) {
      val markerDetails = spark.read.format("avro").load(eodTablePartitionPath)
        .select(colName, tableColName).rdd
        .map { row =>
          val markerTime = Try(formatter(diffenParams.timestampColFormat).parseDateTime(row.getString(0)))
          val marker = markerTime.map(DateTimeFormat.forPattern(diffenParams.timestampColFormat).print).get
          val tableName = row.getString(1)
          (tableName, marker)
        }.groupByKey()
        .mapValues(x => x.max)
        .collect()
      //FIXME Converting Date to string again.
      markerDetails.map { x => TableLevelEodMarkers(x._1, x._2.toString) }.toList
    }
    else List.empty
  }

  /** Gets the Previous business day's global EOD time */
  def getEODMarkerFor(fs: FileSystem, paramConf: Configuration, diffenParams: DiffenParams, businessDate: String)(implicit spark: SparkSession): Option[String] = {
    val tableName = paramConf.get("edmhdpif.config.eod.table.name", "eod_table")

    val previousEodRecordPath = s"${diffenParams.hdfsBaseDir}/${diffenParams.opsSchema}/$tableName/businessdate=$businessDate"

    val pathExists: PartialFunction[String, Option[String]] = {
      case path if fs.exists(new Path(path)) =>
        val prevEodMarkerFrame = spark.read.format(OrcFormat).load(path).select("markertime")
        if (prevEodMarkerFrame.count() == 1) {
          Some(prevEodMarkerFrame.collect()(0).get(0).toString)
        }
        else None
    }

    val pathDoesNotExist: PartialFunction[String, Option[String]] = {
      case _ => None
    }

    pathExists.orElse(pathDoesNotExist)(previousEodRecordPath)
  }

  /**
    * Create a status file
    *
    * @param fs        HDFS FS
    * @param paramConf parameter configuration
    * @param diffenParams diffen params
    * @param scope     scope : global/table
    * @param marker    name of status marker file
    * @param content   Optional file content
    */
  def createStatusFile(fs: FileSystem, paramConf: Configuration, diffenParams: DiffenParams, scope: String, marker: String, content: String = ""): Unit = {
    createHDFSFile(fs, paramConf.get("diffen.config.statusfile.location", "target/") + "/" + s"$scope/${diffenParams.businessDate}" + marker, content)
  }

  /**
    * Create a file on HDFS
    *
    * @param fs      HDFS FS
    * @param path    Path on HDFS
    * @param content Optional file content
    */
  def createHDFSFile(fs: FileSystem, path: String, content: String = ""): Unit = {
    fs.mkdirs(new Path(path).getParent)
    val outputStream = fs.create(new Path(path), true)
    outputStream.write(content.getBytes("UTF-8"))
    outputStream.close()
  }


  def getOrcFileCount(partitionPath: String)(implicit spark: SparkSession): Long = {
    spark.read.format("orc").load(partitionPath).count()
  }
}
