package com.tdt.diffen.utils

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

object PartitionUtils {

  /**
    * Given a start partition and a time range with a limit to go back to, returns all the previous partitions
    *
    * @param partition      Start partition
    * @param format         Format of the partition - Generally it is yyyy-MM-dd
    * @param unit           Unit of time - minutes, hours, days or months
    * @param value          Quantity of time that is to added/subtracted
    * @param iterationLimit The maximum number of times that this operation has to be applied (aka) the maximum time that we would want to go back or look forward
    * @return [[List]] of partitions
    */
  def allPartitions(partition: String, format: String, unit: String, value: Int, iterationLimit: Int): List[String] = {
    if (iterationLimit <= 0) Nil
    else {
      getPreviousBusinessDatePartition(partition, format, unit, value) ::
        allPartitions(getPreviousBusinessDatePartition(partition, format, unit, value), format, unit, value, iterationLimit - 1)
    }
  }

  /** Given a partition returns the next partition based on the frequency in which the partitions are created **/
  def getPreviousBusinessDatePartition(basePartition: String, partitionFormat: String, unit: String, value: Int): String = {
//    val parsedDate: DateTime = SnapshotDateFormat parseDateTime basePartition
    val dateFormat = DateTimeFormat.forPattern(partitionFormat)
    val parsedDate = dateFormat parseDateTime basePartition
    dateFormat print minusDateTime(unit, value, parsedDate)
  }

  /** Given a partition returns the next partition based on the frequency in which the partitions are created **/
  def getNextBusinessDatePartition(basePartition: String, partitionFormat: String, unit: String, value: Int): String = {
//    val parsedDate: DateTime = SnapshotDateFormat parseDateTime basePartition
    val dateFormat = DateTimeFormat.forPattern(partitionFormat)
    val parsedDate = dateFormat parseDateTime basePartition
    dateFormat print addDateTime(unit, value, parsedDate)
  }

  /**
    * Performs subtraction operation on [[DateTime]] given a unit and the value
    *
    * @param unit     Unit of deduction - minutes, hours, days or months
    * @param value    Actual quantity of time that has to be subtracted
    * @param baseDate [[DateTime]] on which the operation has to be performed
    * @return Immutable instance of the modified [[DateTime]]
    */
  private def minusDateTime(unit: String, value: Int, baseDate: DateTime): DateTime = unit match {
    case "minutes" => baseDate.minusMinutes(value)
    case "hours" => baseDate.minusHours(value)
    case "days" => baseDate.minusDays(value)
    case "months" => baseDate.minusMonths(value)
    case _ => throw new IllegalArgumentException(s"value $unit was expected to be in minutes or hours or days or months")
  }

  /**
    * Performs addition operation on [[DateTime]] given a unit and the value
    *
    * @param unit     Unit of deduction - minutes, hours, days or months
    * @param value    Actual quantity of time that has to be subtracted
    * @param baseDate [[DateTime]] on which the operation has to be performed
    * @return Immutable instance of the modified [[DateTime]]
    */
  private def addDateTime(unit: String, value: Int, baseDate: DateTime): DateTime = unit match {
    case "minutes" => baseDate.plusMinutes(value)
    case "hours" => baseDate.plusHours(value)
    case "days" => baseDate.plusDays(value)
    case "months" => baseDate.plusMonths(value)
    case _ => throw new IllegalArgumentException(s"value $unit was expected to be in minutes or hours or days or months")
  }
}
