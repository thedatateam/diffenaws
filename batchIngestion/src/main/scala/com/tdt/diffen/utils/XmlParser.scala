package com.tdt.diffen.utils

import com.tdt.diffen.utils.DiffenConstants._
import org.apache.commons.lang3.StringUtils
import org.apache.hadoop.fs.{FileSystem, Path}

import scala.collection.JavaConverters._
import scala.util.Try

object XmlParser {

  /** Given a table config, returns a collection of [[ColumnConfig]] that represents the columns in that table. */
  def fetchColumnConfig(tableConfig: TableConfig): List[ColumnConfig] = getColumnConfigs(tableConfig.colSchema, "\\^")

  /** Given the column configs string for a table, returns a collection of [[ColumnConfig]] that represents the columns in that table. */
  def getColumnConfigs(colsSchemas: String, delimiter: String): List[ColumnConfig] = {
    if (StringUtils.isBlank(colsSchemas)) List()
    else {
      colsSchemas.split(delimiter).foldRight(Nil: List[ColumnConfig]) {
        (colSchema: String, list: List[ColumnConfig]) =>
          getColumnConfigFromColSchema(colSchema) :: list
      }
    }
  }

  /** Given a single config string for a single column, returns the [[ColumnConfig]] scala binding */
  private def getColumnConfigFromColSchema(colSchema: String): ColumnConfig = {
    val splitValues: Array[String] = colSchema.split(" ")
    val width = if (splitValues.length > 2 && splitValues(2).equals("WIDTH")) splitValues(3) else null
    val nullable: Boolean = {
      if (null != width) if (splitValues.length > 4 && splitValues(4).equals("NOT")) false else true
      else if (splitValues.length > 2 && splitValues(2).equals("NOT")) false else true
    }
    val comment = if (splitValues(splitValues.length - 2).equals("COMMENT")) splitValues(splitValues.length - 1) else null
    ColumnConfig(splitValues(0), splitValues(1), nullable, width, comment)
  }

  /** Returns the list of primary key columns for a table */
  def fetchKeyColsFromTable(tableConfig: TableConfig): List[String] = {
    if (StringUtils.isNotBlank(tableConfig.keyCols)) {
      tableConfig.keyCols.split(",").map(_.trim).toList
    }
    else List()
  }

  /** Returns the list of columns in that table */
  def fetchColNamesFromTable(tableConfig: TableConfig): List[String] = {
    fetchColumnConfig(tableConfig).map(_.name)
  }

  def getConfs(fs: FileSystem, path: String): Map[String, String] = {
    val hdfsPath = new Path(path)
    val patternR = (PrefixConfigDiffenTable).r
    if(fs.isDirectory(hdfsPath)) {
      fs.listStatus(hdfsPath).filter(_.getPath.getName.endsWith("xml")).map(t=> {
        DiffenUtils.getParamConf(fs, t.getPath.toString).getValByRegex(patternR.toString()).asScala
      }).flatten[(String, String)].toMap
    }
    else {
      DiffenUtils.getParamConf(fs, path).getValByRegex(patternR.toString()).asScala.toMap
    }
  }

  /**
    * Parses Table config xml and constructs the corresponding [[TableConfig]] scala binding
    */
  def parseXmlConfig(path: String, fs: FileSystem): List[TableConfig] = {

    val patternR = (DiffenConstants.PrefixConfigDiffenTable + "(.*)" + DiffenConstants.SuffixConfigColSchema).r
    val parseTableName: String => String = {
      case patternR(name) => name
    }

    val tables = getConfs(fs, path)
    val tableNames: Map[String, String] = tables.filterKeys(_ matches(DiffenConstants.PrefixConfigDiffenTable + "(.*)" + DiffenConstants.SuffixConfigColSchema))
    tableNames.keys.foldRight(Nil: List[TableConfig]) {
      (key: String, tableList: List[TableConfig]) =>
        val tableName: String = parseTableName(key)
        val sourceType: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigSourceType, null)
        val keyCols: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigKeyCols, null)
        val colSchema: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigColSchema, null)
        val reconQuery: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigReconQuery, null)
        val deleteValue: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigDeleteValue, null)
        val deleteField: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigDeleteFieldName, "")

        val operationTypeCol: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigOperationTypeCol, null)
        val insertValue: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigInsertValue, null)
        val beforeValue: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigBeforeValue, null)
        val afterValue: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigAfterValue, null)
        val runType: Option[String] = tables.get(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigRunType)
        val reconFileName: String = tables.getOrElse(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.ReconFileRename, "")

        val deleteIndex: String = if (deleteField.nonEmpty) {
          (colSchema.split("\\^").map(_.split(" ")(0)).indexOf(deleteField) + 1).toString
        }
        else {
          (Try(tables.get(DiffenConstants.PrefixConfigDiffenTable + tableName + DiffenConstants.SuffixConfigDeleteIndex).get.toInt).getOrElse(-2) + 1).toString
        }
        TableConfig(tableName, sourceType, keyCols, colSchema, reconQuery, deleteIndex, deleteValue, operationTypeCol, insertValue, beforeValue, afterValue, runType, reconFileName) :: tableList
    }
  }

  /** Constructs a List of [[AllTabColumn]] for a source-country given a [[TableConfig]] */
  def getAllTabColumns(currentTime: String, source: String, country: String, tableConfig: TableConfig, version: Int = 1): List[AllTabColumn] = {
    var keyColumns: List[String] = List()
    if (null != tableConfig.keyCols) {
      keyColumns = tableConfig.keyCols.split(",").toList
    }
    val columnConfig = fetchColumnConfig(tableConfig)
    columnConfig.zipWithIndex.map { case (column, index) =>
      val nullString = if (column.nullable) "Y" else "N"
      val isKeyCol = if (keyColumns.contains(column.name)) "Y" else "N"
      if (column.typee.contains("(")) {
        val dataType = column.typee.substring(0, column.typee.indexOf("("))
        val dataLength = column.typee.substring(column.typee.indexOf("(") + 1, column.typee.indexOf(")")).split(",")(0).toInt
        AllTabColumn(tableConfig.name.replace(source + "_" + country + "_", ""), column.name, country, source, dataType,
          dataLength, dataLength, isKeyCol,
          nullString, index, version, version, column.width,
          "1", currentTime, EndOfTimes, currentTime, currentTime, "")
      }
      else {
        AllTabColumn(tableConfig.name.replace(source + "_" + country + "_", ""), column.name, country, source, column.typee,
          0, 0, isKeyCol,
          nullString, index, version, version, column.width,
          "1", currentTime, EndOfTimes, currentTime, currentTime, "")
      }
    }
  }

  /** Represents an entry of versioned TableConfig in a flat format */
  case class AllTabColumn(tableName: String, columnName: String, country: String, source: String, dataType: String, dataLength: Int,
                          precision: Int, isPrimaryKey: String, nullable: String, order: Int, currentVersion: Int, version: Int, width: String,
                          valid: String, validFrom: String, validTo: String, creationDate: String, lastUpdateDate: String, Comments: String)

  //FIXME Re-arrange fields inside this case class
  /** Scala binding for the Table config XML */
  case class TableConfig(private val _name: String, sourceType: String, keyCols: String, colSchema: String, reconQuery: String, deleteIndex: String, deleteValue: String,
                         operationTypeCol: String, insertValue: String, beforeValue: String, afterValue: String, runType: Option[String], reconTableRename: String ="") {

    val name = _name.toLowerCase

    def isDeltaSource: Boolean = {
      sourceType.equals("delta")
    }

    def isTxnSource: Boolean = {
      sourceType.equals("txn")
    }

    def getColumnConfigForIndex(index: Int): ColumnConfig = {
      fetchColumnConfig(this)(index)
    }

    def getColumnConfig(): List[ColumnConfig] = {
      fetchColumnConfig(this)
    }
  }

  /** Scala binding for all the attributes of a column */
  case class ColumnConfig(name: String, typee: String, nullable: Boolean, width: String, comment: String)


}
