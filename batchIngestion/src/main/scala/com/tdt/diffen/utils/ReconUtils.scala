package com.tdt.diffen.utils

import java.math
import java.sql.Timestamp
import java.util.Date

import com.tdt.diffen.models.OpsModels.EdmRecon
import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.DiffenConstants.OrcFormat
import com.tdt.diffen.utils.XmlParser.TableConfig
import org.antlr.stringtemplate.StringTemplate
import org.antlr.stringtemplate.language.DefaultTemplateLexer
import org.apache.commons.lang3.StringUtils
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import scala.collection.JavaConverters.mapAsJavaMapConverter
import scala.util.{Failure, Success, Try}

/** Runs reconciliation between the source reconciliation data and the data that DIFFEN stored and processed */
object ReconUtils {
  val logger: Logger = Logger.getLogger(ReconUtils.getClass)

  case class ReconValue(tableName: String, count: BigDecimal, checksums: Map[String, String] = Map())

  /**
    * Runs the reconciliation task for each of the tables.
    * Upon comparison, either a Success or a Failure status file is written.
    *
    * @param diffenParams       Instance of [[DiffenParams]]
    * @param tableDictionary List of [[TableConfig]]
    * @param fs              Handle to the HDFS
    * @param paramConf       Unparsed Hadoop configuration
    * @param spark           Spark session
    */
  def recon(diffenParams: DiffenParams, tableDictionary: List[TableConfig], fs: FileSystem, paramConf: Configuration)(implicit spark: SparkSession): Unit = {

    val tableNameColumn = diffenParams.reconTableNameColumn
    val reconCountColumns = diffenParams.reconCountColumn.split(",")
    val reconTableValues: Map[String, ReconValue] = getReconTableValues(diffenParams).select (tableNameColumn, reconCountColumns:_*).rdd.zipWithIndex().map{ case (x, index) =>
      val tableName = {x.getString(0).toLowerCase}
      tableName -> ReconValue(x.getString(0).toLowerCase, checkTypeAndParseDecimal(x.get(1)))
    }.collect().toMap

    val reconTableConfig = tableDictionary.filterNot(_.name.isEmpty).filterNot(_.name.equalsIgnoreCase(diffenParams.reconTableName)).map(config => setDefaultReconQueryWhenEmpty(diffenParams, config))

    if (reconTableConfig.nonEmpty) {
      val reconResult = reconTableConfig.map {
        tableConfig =>
         val tableName = tableConfig.name
          val lookupTableName =
            if (StringUtils.isNotEmpty(tableConfig.reconTableRename)) tableConfig.reconTableRename.toLowerCase
            else tableName.toLowerCase.replaceFirst(s"${diffenParams.source}_${diffenParams.country}_", "")

          Try {
            val context = Map(
              "business_date" -> diffenParams.businessDate,
              "eod_maker" -> diffenParams.eodMarker,
              "country" -> diffenParams.country,
              "source" -> diffenParams.source,
              "type_of_source" -> diffenParams.sourcingType.toString, //Yikes
              "table" -> tableName,
              "keys" -> tableConfig.keyCols,
              "columns" -> tableConfig.colSchema,
              "type" -> tableConfig.sourceType
            )
            val diffenReconQuery = tableConfig.reconQuery.split(";", -1).map(fillInPlaceholders(_, context)).filter(StringUtils.isNotEmpty)
            val diffenReconResult = Try(spark.sql(diffenReconQuery.last))
            val srcRecon = reconTableValues.getOrElse(lookupTableName, {
              logger.warn(s" Recon misconfigured , there was no recon count found for $tableName while ${diffenParams.reconTableNameColumn} was used to mark table name columns")
              ReconValue(tableName, new math.BigDecimal(0))
            })

            reconCountColumns.indices.map {
              column =>
                val diffenValue = diffenReconResult.map(_.take(1).map(x => x.getLong(column)).head).toOption.getOrElse {
                  logger.warn(s"Diffen Count could not be found for $tableName using ${diffenReconQuery.last} and $column , hence count assumed to be zero")
                  0L
                }
                val diffenValueBig: BigDecimal = BigDecimal.apply(diffenValue)
                val compare: Int = srcRecon.count.compare(diffenValueBig.bigDecimal)
                logger.info(tableName + " -> " + diffenValue + " : " + srcRecon + " : compare " + compare)
                prepareReconRecord(diffenParams, tableConfig, srcRecon, column, diffenValueBig, compare)
            }

          } match {
            case Success(recon) =>
              Ops.createStatusFile(fs, paramConf, diffenParams, "tables", "_COMPLETED_RECON_TABLE_" + tableName)
              Some(recon)
            case Failure(ex) =>
              logger.error(ex.getMessage + " was encountered trying to reconcile ")
              Ops.createStatusFile(fs, paramConf, diffenParams, "tables", "_ERROR_RECON_TABLE_" + tableName, ex.getStackTrace.mkString("\n"))
              None
          }
      }
      val reconList = reconResult.filter(_.isDefined).flatMap(_.get).toList
      if (reconList.nonEmpty) {
        writeReconTableOutput(diffenParams, reconList)
      }
    }
  }


  /**
    * Check to see if recon sourcing query was configured rather  than
    * recon table name configuration. Returns the one that's configured as a [[DataFrame]].
    *
    * @param spark     Spark session
    * @param diffenParams Diffen Params
    * @return
    */
  private def getReconTableValues(diffenParams: DiffenParams)(implicit spark: SparkSession): DataFrame = {
    diffenParams match {
      case `diffenParams` if StringUtils.isEmpty(diffenParams.reconSourceQuery) =>
        spark.read.format(OrcFormat).load(diffenParams.getDiffenOpenPartitionPath(diffenParams.reconTableName, diffenParams.businessDate))
      case _ =>
        val context = Map(
          "business_date" -> diffenParams.businessDate,
          "eod_maker" -> diffenParams.eodMarker,
          "country" -> diffenParams.country,
          "source" -> diffenParams.source,
          "type_of_source" -> diffenParams.sourcingType.toString
        )
        spark.sql(diffenParams.
          reconSourceQuery.
          trim.split(";", -1)
          .map(fillInPlaceholders(_, context))
          .filter(StringUtils.isNotEmpty).last)
    }

  }

  /**
    * Sets the default reconciliation query if there's no configured query.
    * Default query checks the rowcount of the table
    *
    * @param diffenParams   instance of DiffenParams
    * @param tableConfig instance of TableConfig
    * @return updated TableConfig
    */
  def setDefaultReconQueryWhenEmpty(diffenParams: DiffenParams, tableConfig: XmlParser.TableConfig): XmlParser.TableConfig = {
    if (StringUtils.isEmpty(tableConfig.reconQuery)) {
      tableConfig.copy(reconQuery = s"select count(*) from ${diffenParams.diffenOpenSchema}.${tableConfig.name} where ods='${diffenParams.businessDate}'")
    }
    else {
      tableConfig
    }
  }

  /** Constructs [[EdmRecon]] output record by comparing the source and the target recon values */
  private def prepareReconRecord(diffenParams: DiffenParams, tableConfig: TableConfig, srcRecon: ReconValue,
                                 column: Int, diffenReconValue: BigDecimal, compare: Int): EdmRecon = {

    import diffenParams._
    implicit val stringToDecimal: String => BigDecimal = new java.math.BigDecimal(_)
      EdmRecon(source, country, sourcingType.toString,
        tableConfig.name, srcRecon.count.longValue().toString, diffenReconValue.longValue().toString,
        column.toString, srcRecon.count.longValue().toFloat,
        diffenReconValue.longValue().toFloat, getCountStatus(srcRecon.count.longValue().toDouble, diffenReconValue.toDouble),
        getCountStatus(srcRecon.count.longValue().toDouble, diffenReconValue.toDouble), new Timestamp(new Date().getTime), batchPartition)
  }

  private def getCountStatus(first: Double, second: Double) = if (first == second) "Success" else "Failed"

  /**
    * Persists the [[EdmRecon]] [[DataFrame]] to the appropriate HDFS location and creates a
    * Hive partition over it
    *
    * @param diffenParams    instance of [[DiffenParams]] for this job
    * @param reconRecords [[DataFrame]] of [[EdmRecon]] objects
    * @param spark        Spark Session
    */
  private def writeReconTableOutput(diffenParams: DiffenParams, reconRecords: List[EdmRecon])(implicit spark: SparkSession): Unit = {

    val reconTableLocation = s"${diffenParams.hdfsBaseDir}${diffenParams.opsSchema}/${diffenParams.reconOutputTableName}"
    val partitionQuoted = s"""batch_date='${diffenParams.businessDate}'"""

    import spark.sqlContext.implicits._
    spark.sparkContext.parallelize(reconRecords, 1)
      .toDF()
      .write
      .format(OrcFormat)
      .partitionBy("batch_date")
      .mode(SaveMode.Append)
      .save(s"$reconTableLocation")

    DiffenUtils.createHivePartition(diffenParams.opsSchema, diffenParams.reconOutputTableName, partitionQuoted)
  }

  /**
    * This fills in the query with the corresponding template
    *
    * @param input
    * @param context
    * @return
    */
  def fillInPlaceholders(input: String, context: Map[String, String]): String = {
    val template = new StringTemplate(input, classOf[DefaultTemplateLexer])
    template.setAttributes(context.asJava)
    Try(template.toString()) match {
      case Success(x) => x
      case Failure(x) => logger.warn("Template resolution failed for recon query " + input + " with " + x.getLocalizedMessage); x.printStackTrace(); input
    }
  }

  /** Returns the Recon value as BigDecimal **/
  private def checkTypeAndParseDecimal(input: Any): java.math.BigDecimal = {
    input match {
      case decimal: java.math.BigDecimal =>
        decimal
      case _ =>
        logger.info(s"${input.toString} was encountered as count value")
        new math.BigDecimal(input.toString)
    }
  }
}
