package com.tdt.diffen.utils

import java.util.Date

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.DiffenConstants._
import com.tdt.diffen.utils.XmlParser.{TableConfig, _}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.SQLContext

import scala.collection.mutable.ListBuffer

/**
  * Collection of functions that create Hive tables for VerifyTypes, Diffen Open, Diffen Non Open and operational tables.
  */
object SchemaUtils {

  /** Constructs schema and creates the Hive tables for all the table definitions in the table config.xml */
  def getBusinessTablesSchema(tableConfig: TableConfig, diffenParams: DiffenParams): List[String] = {
    val tableName: String = tableConfig.name
    val diffenOpenHdfsLocation: String = s"""${diffenParams.hdfsBaseDir}${diffenParams.diffenOpenSchema}/$tableName"""
    val diffenNonOpenHdfsLocation: String = s"""${diffenParams.hdfsBaseDir}${diffenParams.diffenNonOpenSchema}/$tableName"""
    val verifyTypesHdfsLocation: String = s"""${diffenParams.hdfsBaseDir}${diffenParams.verifyTypesSchema}/$tableName"""
    val hiveColSchemaOrc: String = createHiveSchema(tableConfig)
    val hiveColSchemaAvro: String = createHiveSchema(tableConfig, isAvro = true)

    val diffenOpenTable = getDiffenTableSchema(diffenParams.diffenOpenSchema, tableName, hiveColSchemaOrc,
      diffenParams.diffenOpenPartitionColumn, diffenOpenHdfsLocation, diffenParams.isCdcSource)

    val diffenNonOpenTable = getDiffenTableSchema(diffenParams.diffenNonOpenSchema, tableName, hiveColSchemaOrc,
      diffenParams.diffenNonOpenPartitionColumn, diffenNonOpenHdfsLocation, diffenParams.isCdcSource)

    val verifyTypesTable = getVerifyTypesTableSchema(diffenParams.verifyTypesSchema, tableName, hiveColSchemaAvro,
      diffenParams.verifyTypesPartitionColumn, verifyTypesHdfsLocation, diffenParams.isCdcSource)

    diffenOpenTable ++ diffenNonOpenTable ++ verifyTypesTable
  }

  /** Constructs the Hive table definition for an Diffen Open/NonOpen table **/
  def getDiffenTableSchema(database: String, tableName: String, hiveColSchema: String, partitionColumn: String, hdfsLocation: String, cdcSource: Boolean): List[String] = {
    val cdcColumn = if (cdcSource) cdcColSchema else ""
    List(
      s"create schema if not exists $database",
      s"drop table if exists $database.$tableName",
      s"""
         |
         | create external table if not exists $database.$tableName(
         | ROWID string
         |, s_startdt string, s_starttime string, s_enddt string, s_endtime string, s_deleted_flag string
         | $cdcColumn$hiveColSchema
         |)
         | partitioned by($partitionColumn string)
         | stored as ORC
         | location '$hdfsLocation'""".stripMargin)
  }


  /** Constructs the Hive table definition for a Verify type/Storage table **/
  def getVerifyTypesTableSchema(database: String, tableName: String, hiveColSchema: String, partitionColumn: String,
                                hdfsLocation: String, cdcSource: Boolean): List[String] = {
    val cdcColumn = if (cdcSource) {
      cdcColSchema
    } else {
      ""
    }

    List(
      s"create schema if not exists $database",
      s"drop table if exists $database.$tableName",
      s"""create external table if not exists $database.$tableName(
         |   ROWID string, FILENAME string
         |   $cdcColumn$hiveColSchema
         |   )
         |partitioned by ($partitionColumn string)
         |stored as AVRO
         |location '$hdfsLocation'""".stripMargin)
  }

  /** Given a table config, constructs the column definition part of the Hive create table DDL **/
  def createHiveSchema(tableConfig: TableConfig, isAvro: Boolean = false): String = {
    val columns: Array[String] = tableConfig.colSchema.split("\\^")
    columns.map {
      col =>
        val strings = col.split(" ")
        val drop = strings.exists(_.equalsIgnoreCase("DROP"))

        drop match {
          case true => ""
          case false if !isAvro => "," + strings(0) + " " + toOrcType(strings(1))
          case false if isAvro => "," + strings(0) + " " + toAvroType(strings(1))
        }

    }.foldRight("") { (x, y) => x + y }
  }

  /** Constructs schema and creates the following Operational tables
    * 1. Warn Type Table
    *
    * 2. All Country Table
    *
    * 3. All Tab Columns
    *
    * 4. All Tables
    *
    * 5. XForm Failure
    *
    * 6. Data Dictionary
    *
    * 7. Row History Table
    *
    * 8. Row Counts Table
    *
    * 9. Recon Output Table
    *
    * 10. Edm Recon Table
    *
    * 11. Ops Data Table
    *
    * 12. Eod Table
    *
    * 13. Invalid Types Table
    *
    * 14. Config Table
    *
    * 15. Process Metadata Table
    *
    * 16. Duplicates Table
    */
  def getOpsSchema(diffenParams: DiffenParams): List[String] = {

    val rowHistoryTable = getRowHistoryTable(diffenParams.source, diffenParams.country, diffenParams.opsSchema, diffenParams.hdfsBaseDir)
    val rowCountsTable = getRowCountsTable(diffenParams.source, diffenParams.country, diffenParams.opsSchema, diffenParams.hdfsBaseDir)
    val reconOutputTable = getReconOutputTable(diffenParams.source, diffenParams.country, diffenParams.opsSchema, diffenParams.reconOutputTableName, diffenParams.hdfsBaseDir)
    val integratedReconTable = createIntegratedReconTable(diffenParams.opsSchema,diffenParams.source,diffenParams.country,diffenParams.hdfsBaseDir)
    val integratedReconHistoryView = createIntegratedReconHistoryView(diffenParams.opsSchema,diffenParams.source,diffenParams.country)
    val integratedReconLatestView = createIntegratedReconLatestView(diffenParams.opsSchema,diffenParams.source,diffenParams.country)
    val fileCountTable = createFileCountTable(diffenParams.opsSchema,diffenParams.source,diffenParams.country,diffenParams.hdfsBaseDir)
    val opsDataTable = getOpsDataTable(diffenParams.source, diffenParams.country, diffenParams.opsSchema, diffenParams.hdfsBaseDir)
    val eodTable = createEodTable(diffenParams.opsSchema, diffenParams.eodTableName, diffenParams.hdfsBaseDir + diffenParams.opsSchema + "/" + diffenParams.eodTableName)
    val invalidTypesTable = getInvalidTypesTable(diffenParams.source, diffenParams.country, diffenParams.verifyTypesSchema, diffenParams.hdfsBaseDir, diffenParams.verifyTypesPartitionColumn)
    val warnTypeTable = getWarnTypesTable(diffenParams.source, diffenParams.country, diffenParams.verifyTypesSchema, diffenParams.hdfsBaseDir, diffenParams.verifyTypesPartitionColumn)
    val xFormFailure = getXformFailureTable(diffenParams.source, diffenParams.country, diffenParams.verifyTypesSchema, diffenParams.hdfsBaseDir, diffenParams.verifyTypesPartitionColumn)
    val dataDictionary = createDataDictionary(diffenParams.opsSchema, diffenParams.hdfsBaseDir + diffenParams.opsSchema + "/"
      + s"${diffenParams.source}_${diffenParams.country}_data_dictionary", s"${diffenParams.source}_${diffenParams.country}_data_dictionary")
    val configTable = createConfigHistoryTableSchema(diffenParams.opsSchema, diffenParams.source, diffenParams.country, diffenParams.hdfsBaseDir)
    val processMetadataTable = createProcessMetaData(diffenParams.opsSchema, diffenParams.source, diffenParams.hdfsBaseDir)

    val duplicatesTable = createDuplicatesTables(diffenParams.opsSchema, diffenParams.source, diffenParams.country, diffenParams.hdfsBaseDir)
    val allCountryTable = createAllCountryTable(diffenParams.metadataSchema, diffenParams.hdfsBaseDir)
    val allTabColumns = createAllTabColumns(diffenParams.metadataSchema, diffenParams.hdfsBaseDir)
    val allTables = createAllTables(diffenParams.metadataSchema, diffenParams.hdfsBaseDir)

    val fileMetadataInfoTable = getFileMetadataInfoTable(diffenParams.source, diffenParams.country, diffenParams.verifyTypesSchema, diffenParams.hdfsBaseDir, diffenParams.verifyTypesPartitionColumn)

    warnTypeTable ++ allCountryTable ++ allTabColumns ++ allTables ++ xFormFailure ++ dataDictionary ++ rowHistoryTable ++ rowCountsTable ++ reconOutputTable ++
      opsDataTable ++ eodTable ++ invalidTypesTable ++ configTable ++ processMetadataTable ++ duplicatesTable ++ fileMetadataInfoTable ++ integratedReconTable ++ fileCountTable ++
      integratedReconHistoryView ++ integratedReconLatestView
  }

  /** Constructs the Hive table definition for config history **/
  private def createConfigHistoryTableSchema(opsSchemaName: String, source: String, country: String, hdfsLocation: String): List[String] = {
    List(
      s"create schema if not exists $opsSchemaName",
      s"""create external table if not exists $opsSchemaName.${source}_${country}_config_history(
         |filename string COMMENT 'source config file path',
         |tablename string COMMENT 'source table',
         |sourcetype string COMMENT 'delta or transaction',
         |keycols string COMMENT 'source table key columns - comma-separated',
         |schema string COMMENT 'schema as a string',
         |asof string COMMENT 'date timestamp when the config extraction was executed')
         |stored as ORC
         |location '$hdfsLocation$opsSchemaName/${source}_${country}_config_history'""".stripMargin)
  }

  /** Constructs the Hive table definition for process metadata **/
  private def createProcessMetaData(opsSchemaName: String, source: String, hdfsLocation: String): List[String] = {
    List(
      s"create schema if not exists $opsSchemaName",
      s""" CREATE EXTERNAL TABLE if not exists $opsSchemaName.${source}_process_metadata (
         |  meta_time timestamp,
         |  partition_name string,
         |  table_name string,
         |  status string,
         |  description string,
         |  execution_time string,
         |  action_name string,
         |  file_name string,
         |  run_type string,
         |  batch_date string,
         |  json_attribute string,
         |  source_name string,
         |  country_name string,
         |  module_name string
         |)PARTITIONED BY (partition_date string )
         |  STORED AS ORC
         |  LOCATION  '$hdfsLocation/$opsSchemaName/${source}_process_metadata'""".stripMargin)
  }

  private def createDuplicatesTables(opsSchemaName: String, source: String, country: String, hdfsLocation: String): List[String] = {
    List(
      s"create schema if not exists $opsSchemaName",
      s"""CREATE EXTERNAL TABLE if not exists $opsSchemaName.${source}_${country}_duplicates(
         |   ROWID string,
         |   DUPLICATE_ROWID string,
         |   STEPNAME string,
         |   TABLENAME string,
         |   asof string
         |)
         |partitioned by (dds string)
         |STORED as ORC
         |location '$hdfsLocation$opsSchemaName/${source}_${country}_duplicates'""".stripMargin)
  }

  /** Constructs the Hive table definition for integrated recon table **/
  private def createIntegratedReconTable(opsSchemaName: String, source: String, country: String, hdfsLocation: String): List[String] = {
    List(
      s"create schema if not exists $opsSchemaName",
      s"""CREATE EXTERNAL TABLE if not exists $opsSchemaName.${source}_${country}_integrated_recon(
         | source_name string,
         | country_name string,
         | table_name string,
         | partition_name string,
         | file_type string,
         | data_pattern string,
         | run_type string,
         | frequency string,
         | prd_nas_count string,
         | prd_source_count string,
         | file_rcds_count string,
         | files_processed string,
         | new_record_count string,
         | updated_record_count string,
         | deleted_record_count string,
         | prd_storage_count string,
         | prd_invalid_types string,
         | prev_diffenopen_count string,
         | diffen_open string,
         | diffen_nonopen string,
         | duplicate_rcds_count string,
         | diffen_layer string,
         | storage_day_cnt_after_eod string,
         | prev_earlyarrival_diffen string,
         | storage_eod_count string,
         | new_rcds_diffen string,
         | update_rcds_diffen string,
         | delete_rcds_diffen string,
         | storage_day_cnt_before_eod string,
         | nas_Processed_Diff string,
         | file_storage_diff string,
         | storage_diffen_diff string,
         | source_diffen_diff string,
         | status_edmp string,
         | status_source string,
         | overall_recon_report string,
         | source_checksum string,
         | snapshot_checksum string,
         | checksum_field string,
         | checksum_status string,
         | insert_time string,
         | brecordfile string,
         | brecordsdiffen string)
         | PARTITIONED BY (partition_date string)
         | STORED as ORC
         | location '$hdfsLocation$opsSchemaName/${source}_${country}_integrated_recon'""".stripMargin)
  }


  /** Creates History Integrated Recon View **/
  def createIntegratedReconHistoryView(opsSchemaName: String, source: String, country: String):  List[String] ={
    List(
      //s"drop view if exists ${opsSchemaName}.${viewName}",
      s""" CREATE VIEW IF NOT EXISTS ${opsSchemaName}.${source}_${country}_integrated_recon_history_view AS SELECT
         | source_name,
         | country_name ,
         | table_name ,
         | partition_date ,
         | file_type ,
         | data_pattern ,
         | run_type ,
         | frequency ,
         | prd_nas_count as nas_files_count,
         | files_processed ,
         | nas_processed_diff,
         | file_rcds_count ,
         | prd_storage_count as storage_count,
         | prd_invalid_types as invalid_types_count,
         | new_record_count,
         | updated_record_count,
         | deleted_record_count ,
         | brecordfile as brecord_count,
         | storage_day_cnt_before_eod,
         | storage_day_cnt_after_eod,
         | prev_earlyarrival_diffen,
         | file_storage_diff ,
         | prev_diffenopen_count ,
         | diffen_open,
         | diffen_nonopen,
         | duplicate_rcds_count,
         | brecordsdiffen as brecords_diffen,
         | diffen_layer,
         | storage_eod_count,
         | new_rcds_diffen,
         | update_rcds_diffen,
         | delete_rcds_diffen,
         | storage_diffen_diff ,
         | prd_source_count as source_recon_count,
         | checksum_field ,
         | source_checksum ,
         | snapshot_checksum as diffen_checksum,
         | checksum_status,
         | source_diffen_diff,
         | status_source ,
         | status_edmp,
         | overall_recon_report,
         | from_unixtime(unix_timestamp(insert_time, "dd-MMM-yyyy HH:mm:ss")) as insert_time
         | FROM ${opsSchemaName}.${source}_${country}_integrated_recon """.stripMargin)
  }

  /** Creates Latest Integrated Recon View **/
  def createIntegratedReconLatestView(opsSchemaName: String, source: String, country: String):  List[String] ={
    List(
      //s"drop view if exists ${opsSchemaName}.${viewName}",
      s""" CREATE VIEW IF NOT EXISTS ${opsSchemaName}.${source}_${country}_integrated_recon_latest_view AS SELECT
         | source_name,
         | country_name ,
         | table_name ,
         | partition_date ,
         | file_type ,
         | data_pattern ,
         | run_type ,
         | frequency ,
         | prd_nas_count as nas_files_count,
         | files_processed ,
         | nas_processed_diff,
         | file_rcds_count ,
         | prd_storage_count as storage_count,
         | prd_invalid_types as invalid_types_count,
         | new_record_count,
         | updated_record_count,
         | deleted_record_count ,
         | brecordfile as brecord_count,
         | storage_day_cnt_before_eod,
         | storage_day_cnt_after_eod,
         | prev_earlyarrival_diffen,
         | file_storage_diff ,
         | prev_diffenopen_count ,
         | diffen_open,
         | diffen_nonopen,
         | duplicate_rcds_count,
         | brecordsdiffen as brecords_diffen,
         | diffen_layer,
         | storage_eod_count,
         | new_rcds_diffen,
         | update_rcds_diffen,
         | delete_rcds_diffen,
         | storage_diffen_diff ,
         | prd_source_count as source_recon_count,
         | checksum_field ,
         | source_checksum ,
         | snapshot_checksum as diffen_checksum,
         | checksum_status,
         | source_diffen_diff,
         | status_source ,
         | status_edmp,
         | overall_recon_report,
         | insert_time
         | FROM (
         | SELECT
         | source_name ,
         | country_name ,
         | table_name ,
         | partition_date ,
         | file_type ,
         | data_pattern ,
         | run_type ,
         | frequency ,
         | prd_nas_count ,
         | prd_source_count ,
         | file_rcds_count ,
         | files_processed ,
         | new_record_count ,
         | updated_record_count ,
         | deleted_record_count ,
         | prd_storage_count ,
         | prd_invalid_types ,
         | prev_diffenopen_count ,
         | diffen_open,
         | diffen_nonopen,
         | duplicate_rcds_count,
         | diffen_layer,
         | storage_day_cnt_after_eod,
         | prev_earlyarrival_diffen,
         | storage_eod_count,
         | new_rcds_diffen,
         | update_rcds_diffen,
         | delete_rcds_diffen,
         | storage_day_cnt_before_eod,
         | nas_processed_diff,
         | file_storage_diff ,
         | storage_diffen_diff ,
         | source_diffen_diff ,
         | status_edmp ,
         | status_source ,
         | overall_recon_report ,
         | source_checksum ,
         | snapshot_checksum,
         | checksum_field ,
         | checksum_status ,
         | from_unixtime(unix_timestamp(insert_time, "dd-MMM-yyyy HH:mm:ss")) as insert_time,
         | brecordfile,
         | brecordsdiffen,
         | row_number() over (partition by table_name,partition_date order by insert_time desc) as latest_row
         | FROM ${opsSchemaName}.${source}_${country}_integrated_recon
         | )T
         |WHERE latest_row = 1 """.stripMargin)
  }


  private def createFileCountTable(opsSchemaName: String, source: String, country: String, hdfsLocation: String): List[String] = {
    List(
      s"create schema if not exists $opsSchemaName",
      s"""CREATE EXTERNAL TABLE if not exists $opsSchemaName.${source}_${country}_file_count(
         | source_name string,
         | country_name string,
         | table_name string,
         | incoming_count int,
         | archive_count int,
         | reject_count int,
         | data_pattern_ind string,
         | insert_time timestamp)
         | PARTITIONED BY (partition_date string)
         | STORED as ORC
         | location '$hdfsLocation$opsSchemaName/${source}_${country}_file_count'""".stripMargin)
  }

  /** Constructs the Hive table definition for all tables **/
  private def createAllTables(commonMetadataSchema: String, hdfsRootDir: String): List[String] = {
    List(
      s"create schema if not exists $commonMetadataSchema",
      s""" create external table if not exists $commonMetadataSchema.diffen_all_tables(
         |    country_name string ,table_name string, source string ,
         |    source_type string , threshold_val string, user string,
         |    server_id string,
         |    port string,
         |    other_info string,
         |    current_version int,
         |    version int,
         |    valid string,
         |    valid_from string,
         |    valid_to string,
         |    creation_date string,
         |    last_update_date string,
         |    new_line_char string
         |)
         |stored as ORC
         |location '$hdfsRootDir$commonMetadataSchema/difen_all_tables'""".stripMargin)
  }

  /** Constructs the Hive table definition for All table columns **/
  private def createAllTabColumns(commonMetadataSchema: String, hdfsRootDir: String): List[String] = {
    List(
      s"create schema if not exists $commonMetadataSchema",
      s"""create external table if not exists $commonMetadataSchema.diffen_all_tab_columns(
         |    table_name string ,
         |    column_name string,
         |    country_name string ,
         |    source string ,
         |    data_type string,
         |    data_length int,
         |    data_precision int,
         |    primary_column_indicator string,
         |    nullable string,
         |    column_order int,
         |    current_version int,
         |    version int,
         |    width string,
         |    valid string,
         |    valid_from string,
         |    valid_to string,
         |    creation_date string,
         |    last_update_date string,
         |    comment string
         |)
         |stored as ORC
         |location '$hdfsRootDir$commonMetadataSchema/diffen_all_tab_columns'""".stripMargin)
  }

  /** Constructs the Hive table definition for all country  **/
  private def createAllCountryTable(commonMetadataSchema: String, hdfsLocation: String): List[String] = {
    List(
      s"create schema if not exists $commonMetadataSchema",
      s"""create external table if not exists $commonMetadataSchema.diffen_all_countries(
         |    source string ,
         |    country_name string,
         |    data_path string ,
         |    version string ,
         |    source_group string,
         |    threshold_val string,
         |    cut_off_hour int,
         |    retention_period_hdfsfile int,
         |    retention_period_edge_node int,
         |    retention_period_ops int,
         |    retention_period_backup int,
         |    retention_period_eod_data_file string,
         |    time_zone_value string
         |)
         |stored as ORC
         |location '$hdfsLocation$commonMetadataSchema/diffen_all_countries'""".stripMargin)

  }

  /** Constructs the Hive table definition for data dictionary **/
  private def createDataDictionary(opsSchema: String, hdfsLocation: String, name: String): List[String] = {
    List(
      s"create schema if not exists $opsSchema",
      s"""
         |
         | create external table  if not exists $opsSchema.$name(
         | tablename string,
         | colname string,
         | coltype string,
         | colnull string,
         | commentstr string,
         | asof string
         |)
         | stored as ORC
         | location '$hdfsLocation'""".stripMargin)

  }

  /** Constructs the Hive table definition for EOD table **/
  private def createEodTable(database: String, tableName: String, hdfsLocation: String): List[String] = {
    List(
      s"create schema if not exists $database",
      s"""
         | create external table if not exists $database.$tableName(
         | source string, country string, markerTime string, previousBusinessDate string
         |)
         | partitioned by(businessdate string)
         | stored as ORC
         | location '$hdfsLocation'""".stripMargin)
  }

  /** Constructs the Hive table definition for row counts **/
  private def getRowCountsTable(source: String, country: String, opsSchemaName: String, hdfsRootDir: String): List[String] = {
    val rowCountsTable: String = s"${source}_${country}_rowcounts"
    List(
      s"create schema if not exists $opsSchemaName",
      s"""create external table   if not exists $opsSchemaName.$rowCountsTable(
         | schemaname string COMMENT 'schema (or database) ',
         | tablename string COMMENT 'physical table that is the source of rowcount computation',
         | filename string COMMENT 'source file name',
         | rowcount bigint ,
         | functionaltable string COMMENT 'logical table whose data is embedded in the physical table and that forms the granularity of rowcounts',
         | asof string COMMENT 'date timestamp at which the row count entry was made',
         | step_name string COMMENT 'processing step that populated this row',
         | attempt_id string COMMENT 'job attempt that executed this step',
         | runtype string COMMENT 'fulldump/incremental indicator',
         | isRerun string COMMENT 'run/re-run indicator')
         | partitioned by(rcds string)
         | stored as ORC
         | location '$hdfsRootDir$opsSchemaName/$rowCountsTable'""".stripMargin)
  }

  /** Constructs the Hive table definition for Ops metadata **/
  private def getOpsDataTable(source: String, country: String, opsSchemaName: String, hdfsRootDir: String): List[String] = {
    val opsTableName: String = s"""${source}_${country}_ops_data"""
    List(
      s"create schema if not exists $opsSchemaName",
      s"""
         |  create external table if not exists $opsSchemaName.$opsTableName(
         |   event_time string COMMENT 'time at which an event occurred',
         |   event_message string COMMENT 'message describing a given event'
         |   )
         |partitioned by (event_date string)
         |stored as ORC
         |location '$hdfsRootDir$opsSchemaName/$opsTableName'""".stripMargin)

  }

  /** Constructs the Hive table definition for Row history table **/
  private def getRowHistoryTable(source: String, country: String, opsSchemaName: String, hdfsRootDir: String): List[String] = {
    val rowHistoryTable: String = s"""${source}_${country}_rowhistory"""
    List(
      s"create schema if not exists $opsSchemaName",
      s"""
         |CREATE EXTERNAL TABLE if not exists $opsSchemaName.$rowHistoryTable(
         |   ROWID string,
         |   FILENAME string
         |)
         | partitioned by (vds string)
         |STORED as AVRO
         |location '$hdfsRootDir$opsSchemaName/$rowHistoryTable'""".stripMargin)
  }

  /** Constructs the Hive table definition for Invalid types **/
  private def getInvalidTypesTable(source: String, country: String, storageSchemaName: String, hdfsRootDir: String, partitionColumn: String): List[String] = {

    val invalidTypesTable: String = s"""${source}_${country}_invalidtypes"""
    List(
      s"create schema if not exists $storageSchemaName",
      s"""
         |create external table if not exists $storageSchemaName.$invalidTypesTable(
         |      source string,country string,tablename string,rowid string,data string,errormsg string, ts string
         |  )
         |  partitioned by ($partitionColumn string)
         |  stored as AVRO
         |  location '$hdfsRootDir$storageSchemaName/$invalidTypesTable'""".stripMargin)
  }

  /** Constructs the Hive table definition for Warn types **/
  private def getWarnTypesTable(source: String, country: String, storageSchemaName: String, hdfsRootDir: String, partitionColumn: String): List[String] = {

    val warnTypesTable: String = s"${source}_${country}_warntypes"
    List(
      s"create schema if not exists $storageSchemaName",
      s"""create external table if not exists $storageSchemaName.$warnTypesTable(
         |      source string,country string,tablename string,rowid string,data string,errormsg string, ts string
         |  )
         |  partitioned by ($partitionColumn string)
         |  stored as AVRO
         |  location '$hdfsRootDir$storageSchemaName/$warnTypesTable'""".stripMargin)

  }

  /** Constructs the Hive table definition for transformation failure table **/
  private def getXformFailureTable(source: String, country: String, storageSchemaName: String, hdfsRootDir: String, partitionColumn: String): List[String] = {

    val xformFailureTable: String = s"${source}_${country}_xform_failure"
    List(
      s"create schema if not exists $storageSchemaName",
      s"""create external table if not exists $storageSchemaName.$xformFailureTable(
         |      source string,country string,tablename string,rowid string,data string,errormsg string, partitioncolumn string, ts string
         |  )
         |  partitioned by ($partitionColumn string)
         |  stored as AVRO
         |  location '$hdfsRootDir$storageSchemaName/$xformFailureTable'""".stripMargin)

  }

  /** Constructs the Hive table definition for Recon output table **/
  private def getReconOutputTable(source: String, country: String, opsSchemaName: String, reconOutputTableName: String, hdfsRootDir: String): List[String] = {
    List(
      s"create schema if not exists $opsSchemaName",
      s"""
         create external table if not exists $opsSchemaName.$reconOutputTableName(
         |   source_name string,
         |   country_name string,
         |   table_name string,
         |   identifier string,
         |   source_query string,
         |   source_value double,
         |   target_query string,
         |   target_value double,
         |   recon_status boolean,
         |   insert_time timestamp
         |   )
         |partitioned by (business_date string)
         |stored as ORC
         |location '$hdfsRootDir$opsSchemaName/$reconOutputTableName'""".stripMargin)
  }

  /** Constructs the Hive table definition for File Metadata Info **/
  private def getFileMetadataInfoTable(source: String, country: String, storageSchemaName: String, hdfsRootDir: String, partitionColumn: String): List[String] = {

    val fileMetadataInfoTable: String = s"""${source}_${country}_file_metadata_info"""
    List(
      s"create schema if not exists $storageSchemaName",
      s"""
         |create external table if not exists $storageSchemaName.$fileMetadataInfoTable(
         |      source string,country string,tablename string,filename string,file_date string,header map<string,string>, trailer map<string,string>, asof timestamp
         |  )
         |  partitioned by ($partitionColumn string)
         |  stored as ORC
         |  location '$hdfsRootDir$storageSchemaName/$fileMetadataInfoTable'""".stripMargin)
  }

  /** Constructs data for the following metadata tables and stores them. This is called for the first installation and for subsequent SchemaEvolution
    *
    * 1. DataDictionary
    *
    * 2. Config History
    *
    * 3. All tables
    *
    * 4. All Table columns
    *
    * 5. All country
    *
    */
  def populateCommonTables(tableConfigs: Seq[TableConfig], sqlContext: SQLContext, diffenParams: DiffenParams, fetchFileSystem: FileSystem): Unit = {
    val dictionary = ListBuffer[DataDictionary]()
    val configs = ListBuffer[ConfigHistory]()
    val allTables = ListBuffer[AllTables]()
    val allTabColumns = ListBuffer[AllTabColumn]()
    val allCountry = List(AllCountry(diffenParams.source, diffenParams.country, "NULL", "1", "NULL", "NULL", 0, 0, 0, 0, 0, "", ""))
    val currentTime = DiffenConstants.nowAsString()

    tableConfigs.foreach {
      tableConfig =>
        val colConfig = fetchColumnConfig(tableConfig)
        val columns = colConfig.map(_.name)
        val dataDictionary = colConfig.map(eachCol => DataDictionary(tableConfig.name, eachCol.name, eachCol.typee, eachCol.nullable.toString.capitalize, "", new Date().toString))
        val allTabColumn = getAllTabColumns(currentTime, diffenParams.source, diffenParams.country, tableConfig)

        val configHistory = List(ConfigHistory("", tableConfig.name.replace(s"${diffenParams.source}_${diffenParams.country}_", ""), diffenParams.sourcingType.toString,
          columns.mkString(","), tableConfig.colSchema, currentTime))

        val diffenAllTables = List(AllTables(diffenParams.country, tableConfig.name.replace(s"${diffenParams.source}_${diffenParams.country}_", ""), diffenParams.source,
          tableConfig.sourceType, "0p", "", "", "", "", 1, 1, "Y", currentTime, "2099-12-31 00:00:00", currentTime, currentTime, "Y"))

        dictionary ++= dataDictionary
        configs ++= configHistory
        allTables ++= diffenAllTables
        allTabColumns ++= allTabColumn
    }

    fetchFileSystem.delete(new Path(s"${diffenParams.hdfsBaseDir}${diffenParams.opsSchema}/${diffenParams.source}_${diffenParams.country}_data_dictionary/${diffenParams.source}_${diffenParams.country}_data_dictionary"), true)
    fetchFileSystem.delete(new Path(s"${diffenParams.hdfsBaseDir}${diffenParams.metadataSchema}/diffen_all_tables/${diffenParams.source}_${diffenParams.country}_diffen_all_tables"), true)
    fetchFileSystem.delete(new Path(s"${diffenParams.hdfsBaseDir}${diffenParams.metadataSchema}/diffen_all_countries/${diffenParams.source}_${diffenParams.country}_diffen_all_countries"), true)
    fetchFileSystem.delete(new Path(s"${diffenParams.hdfsBaseDir}${diffenParams.opsSchema}/${diffenParams.source}_${diffenParams.country}_config_history/${diffenParams.source}_${diffenParams.country}_config_history"), true)
    fetchFileSystem.delete(new Path(s"${diffenParams.hdfsBaseDir}${diffenParams.metadataSchema}/diffen_all_tab_columns/${diffenParams.source}_${diffenParams.country}_diffen_all_tab_columns"), true)
    sqlContext.createDataFrame(allCountry).write.format(OrcFormat).save(s"${diffenParams.hdfsBaseDir}${diffenParams.metadataSchema}/diffen_all_countries/${diffenParams.source}_${diffenParams.country}_diffen_all_countries")
    sqlContext.createDataFrame(dictionary).write.format(OrcFormat).save(s"${diffenParams.hdfsBaseDir}${diffenParams.opsSchema}/${diffenParams.source}_${diffenParams.country}_data_dictionary/${diffenParams.source}_${diffenParams.country}_data_dictionary")
    sqlContext.createDataFrame(allTables).write.format(OrcFormat).save(s"${diffenParams.hdfsBaseDir}${diffenParams.metadataSchema}/diffen_all_tables/${diffenParams.source}_${diffenParams.country}_diffen_all_tables")
    sqlContext.createDataFrame(configs).write.format(OrcFormat).save(s"${diffenParams.hdfsBaseDir}${diffenParams.opsSchema}/${diffenParams.source}_${diffenParams.country}_config_history/${diffenParams.source}_${diffenParams.country}_config_history")
    sqlContext.createDataFrame(allTabColumns).write.format(OrcFormat).save(s"${diffenParams.hdfsBaseDir}${diffenParams.metadataSchema}/diffen_all_tab_columns/${diffenParams.source}_${diffenParams.country}_diffen_all_tab_columns")

  }

  private val commonMappings: PartialFunction[String, String] = {
    case "BINARY" => "binary"
    case "BOOLEAN" | "SHORT" => "boolean"
    case "INT" | "SMALLINT" => "int"
    case "LONG" | "BIGINT" => "long"
    case "FLOAT" => "float"
    case typee@x if List("DOUBLE").exists(typee.contains) => "double"
  }

  private val commonMappingsOrc: PartialFunction[String, String] = {
    case "BINARY" => "binary"
    case "BOOLEAN" | "SHORT" => "boolean"
    case "INT" | "SMALLINT" => "int"
    case "LONG" | "BIGINT" => "bigint"
    case "FLOAT" => "float"
    case typee@x if List("DOUBLE").exists(typee.contains) => "double"
  }

  private val stringMappings: PartialFunction[String, String] = {
    case typee@x if List("CHAR", "VARCHAR", "STRING").exists(typee.contains) => "string"
  }

  private val logicalTypeMappingsAvro: PartialFunction[String, String] = {
    case typee@x if List("DECIMAL", "DATE", "TIMESTAMP").exists(typee.contains) => "string"
  }

  private val logicalTypeMappingsOrc: PartialFunction[String, String] = {
    case typee@x if List("DECIMAL", "DATE", "TIMESTAMP").exists(typee.contains) => typee
  }

  private val invalidTypes: PartialFunction[String, String] = {
    case typee => throw new IllegalArgumentException(s"Invalid type: $typee")
  }

  /** Mapping of table config datatypes to Avro datatypes */
  private val toAvroType: String => String = {
    commonMappings orElse stringMappings orElse logicalTypeMappingsAvro orElse invalidTypes
  }

  private val toOrcType: String => String = {
    commonMappingsOrc orElse stringMappings orElse logicalTypeMappingsOrc orElse invalidTypes
  }

  /** CDC Column schema in the same format as the column schema in the Table config xml. **/
  //FIXME Source this from the systemColSchema
  private val cdcColSchema = ",c_journaltime STRING,c_transactionid string,c_operationtype string,c_userid string"


  /** Object representation of all tables stored for a country stored in the AllTables table */
  case class AllTables(country: String, tableName: String, source: String, sourceType: String, threshold: String, user: String, serverId: String, port: String, info: String, currentVersion: Int
                       , version: Int, valid: String, validFrom: String, validTo: String, createDate: String, lastUpdateDate: String, newLineChar: String)

  /** Object representation of the table config stored in Config history table */
  case class ConfigHistory(fileName: String, tableName: String, sourceType: String, keyColumns: String, schema: String, asOf: String)

  /** Object representation of an entry of source and country made into the AllCountry table */
  private case class AllCountry(source: String, country: String, dataPath: String, version: String, sourceGroup: String, thresholdVal: String, cutOffHour: Int,
                                retPeriodHdfsfile: Int, retPeriodEdgeNode: Int, retPeriodOps: Int, retPeriodBackup: Int, retPeriodEodDataFile: String, timeZoneValue: String)

  /** Object representation table metadata instance stored in the data dictionary table */
  private case class DataDictionary(tablename: String, colname: String, coltype: String, colnull: String, commentstr: String, asof: String)

}
