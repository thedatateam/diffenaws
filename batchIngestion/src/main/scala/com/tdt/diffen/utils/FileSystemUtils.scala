package com.tdt.diffen.utils

import org.apache.hadoop.fs.FileSystem

object FileSystemUtils {
  /** Provides a singleton instance of Hadoop File system **/
  var fetchFileSystem: FileSystem = _
  def getFileSystem: FileSystem = fetchFileSystem
  def setFileSystem(fs: FileSystem): Unit = this.fetchFileSystem = fs
  //val fetchFileSystem: FileSystem = FileSystem.get(SparkContext.getOrCreate().hadoopConfiguration)
}
