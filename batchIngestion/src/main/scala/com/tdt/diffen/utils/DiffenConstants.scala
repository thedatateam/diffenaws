package com.tdt.diffen.utils

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}

/** Consolidation of all the common constants and minor utility functions **/

object DiffenConstants {

  val PartitionFormatString = "yyyy-MM-dd"
  val TableLevelEod = "table_level_eod"
  val PartitionFormat = DateTimeFormat.forPattern(PartitionFormatString)

  val TimestampFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  // Table Config Constants
  val PrefixConfigDiffenTable = "diffen.table."
  val SuffixConfigColSchema = ".col_schema"
  val SuffixConfigKeyCols = ".keycols"
  val SuffixConfigSourceType = ".sourcetype"
  val SuffixConfigReconQuery = ".query"
  val SuffixConfigDeleteValue = ".deletevalue"
  val SuffixConfigDeleteIndex = ".deleteindex"
  val SuffixConfigDeleteFieldName = ".deletefield"
  val SuffixConfigOperationTypeCol = ".operationtypecol"
  val SuffixConfigInsertValue = ".insertvalue"
  val SuffixConfigBeforeValue = ".beforevalue"
  val SuffixConfigAfterValue = ".aftervalue"
  val SuffixConfigRunType = ".runtype"
  val ReconFileRename = ".reconfilerename"
  val EndOfDays = "9999-12-31"
  val EndOfTimes = "9999-12-31 00:00:00"
  val DeleteFlag = "s_deleted_flag"
  val StartDate = "s_startdt"
  val StartTime = "s_starttime"
  val EndDate = "s_enddt"
  val EndTime = "s_endtime"
  val AvroFormat = "com.databricks.spark.avro"
  val OrcFormat = "orc"
  val CDCUserIdColName = "c_userid"
  val NotSetUserId = "NOT SET"
  val DiffenModule = "DIFFEN"
  val SuccessStatus = "SUCCESS"
  val FailureStatus = "FAILURE"

  //FIXME Find a better home for the following two functions
  /** Given a pattern, returns a DateTimeFormatter corresponding to that format. **/
  lazy val formatter: String => DateTimeFormatter = { str: String => DateTimeFormat forPattern str }

  /** Returns the current time as a String */
  def nowAsString(): String = TimestampFormat.print(DateTime.now())

}
