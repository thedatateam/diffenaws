/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.deployment

import java.io.{File, _}
import java.nio.file.attribute.PosixFilePermission
import java.nio.file.{Files, Paths}
import java.util.concurrent.TimeUnit
import java.util.regex.Matcher

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.models.cli.DIFFENDeployerCli
import com.tdt.diffen.utils.{SchemaUtils, XmlParser}
import io.swagger.client.ApiClient
import io.swagger.client.api._
import io.swagger.client.auth.{Authentication, OAuth}
import io.swagger.client.model._
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.StringUtils
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

import scala.collection.JavaConversions._
import scala.collection.parallel.ForkJoinTaskSupport
import scala.io.Source
import scala.util.Random

/**
  * This program will be used to deploy for a particular source - country system.
  *
  * It will create :
  *
  * 1. Folder structure on NAS/Local file system for given source - country
  *
  * 2. Folder structure on HDFS for given source - country
  *
  * 3. Tables in Hive based on table configs for source - country
  *
  * 4. DIFFEN Nifi template instantiation for source - country
  *
  */
object DIFFENClusterDeployer {

  /**
    * Entry point to the Deployer. Please refer [[DIFFENDeployerCli]] for different params expected.
    *
    * @param args Input arguments to the program.
    */
  def main(args: Array[String]): Unit = {
    val cliArguments = if (!args.isEmpty) args else Array("--help")

    val cliParams = new DIFFENDeployerCli(cliArguments)
    println(s"Parameters passed to DIFFEN Deployer: $cliParams")

    val console = System.console
    //    if (console == null) {
    //      System.err.println("No console available.")
    //      System.exit(-1)
    //    }

    val envMap = System.getenv()

    if(! envMap.containsKey("PLATFORM")) {
      System.err.println("Environment variable PLATFORM is missing.")
      System.exit(-1)
    }

    val platform = envMap.get("PLATFORM")

    def getPassword(envVar: String, cliVar: Option[String], console: Console, inputMessage: String): String = {
      if (StringUtils.isNotBlank(envVar)) envVar
      else cliVar.getOrElse(new String(console.readPassword(inputMessage)))
    }

    val nifiPassword =
      if (cliParams.isNifiAuthenticationEnabled)
        getPassword(System.getenv("NIFI_PASSWORD"), cliParams.nifiPassword.toOption, console, "Please enter Nifi Password: ")
      else null

    val hivePassword = getPassword(System.getenv("HIVE_PASSWORD"), cliParams.hivePassword.toOption, console, "Please enter Hive Password for Controller Service: ")
    val hiveDRPassword = getPassword(System.getenv("HIVE_DR_PASSWORD"), cliParams.drHivePassword.toOption, console, "Please enter Hive DR Password for Controller Service: ")

    val deployerConfiguration = new Configuration()
    deployerConfiguration.addResource(new FileInputStream(s"${cliParams.getConfigLocation}/${cliParams.getSource}_${cliParams.getCountry}_param.xml"))

    val fs = FileSystem.get(deployerConfiguration)

    val sourceCount = getSourceCount(deployerConfiguration.get("diffen.config.source.nfs.location"))

    val maxSourceHashCode = System.getenv("DIFFEN_SOURCE")

    if(maxSourceHashCode == null || maxSourceHashCode.isEmpty) {
      System.err.println("Unable to find environment variable DIFFEN_SOURCE")
      System.exit(-1)
    }

    val maxCount  = getMaxSourceCount(maxSourceHashCode)

    if(maxCount < 0) {
      System.err.println("Invalid env variable value DIFFEN_SOURCE")
      System.exit(-1)
    }

    if(sourceCount > maxCount) {
      System.err.println(s"Number of sources deployed is more than $maxCount.")
      System.exit(-1)
    }

    if (!cliParams.isTemplateOnlyDeployment) {
      //Create NAS mount directory
      prepareNASDirectory(deployerConfiguration, cliParams.getConfigLocation, cliParams.getSource, cliParams.getCountry, platform)

      //Create HDFSDirectories
      prepareHDFSDirectories(cliParams.getSource, cliParams.getCountry, cliParams.getConfigLocation, deployerConfiguration, fs)

      //Create DIFFEN Open, Non Open, Storage and Ops tables
      createHiveTables(deployerConfiguration, fs, cliParams.getSource, cliParams.getCountry)
    }

    //Upload Nifi Template
    uploadNifiTemplate(cliParams, deployerConfiguration, nifiPassword, hivePassword, hiveDRPassword)
  }

  /**
    * Get number of source deployed based on the folders under nasParentPath
    *
    * @param nasParentPath nas folder under which deployment will be done
    * @return number of sources
    */
  def getSourceCount(nasParentPath: String): Int = {

    val nasParentFile = new File(nasParentPath)
    if(nasParentFile.exists()) {
      nasParentFile
        .listFiles
        .filter(_.isDirectory)
        .flatMap(_.listFiles())
        .count(_.isDirectory)
    } else 0
  }

  /**
    * Returns the maximum number for sources that can be on-boarded.
    * The hashcode represents the number of sources that can be on-boraded for that use.
    * These hashcode are pre-computed and are served to customers.
    *
    * @param hashcode hashcode value for the current customer.
    * @return maximum number of sources that can be on-boarded.
    */
  def getMaxSourceCount(hashcode: String): Int = {
    val hashCodeMap = Map(
      "1302552720" -> 10,
      "1302552751" -> 20,
      "1302552782" -> 30,
      "1302552813" -> 40,
      "1302552844" -> 50
    )

    val hashCodeCheck = hashCodeMap.isDefinedAt(hashcode)
    if(hashCodeCheck) {
      hashCodeMap(hashcode)
    } else {
      -1
    }
  }

  /** Creates the required NAS mounts/local folders
    * Includes
    * 1. Input file incoming folder
    * 2. Archival folder
    * 3. Rerun incoming folder
    * 4. Reject folder
    * 5. Error folder
    * 6. Application files folder (scripts, config files)
    *
    * @param primConfig Hadoop configuration
    */
  private def prepareNASDirectory(primConfig: Configuration, inputConfigLocation: String, source: String, country: String, platformType: String) = {

    val nasParentPath = primConfig.get("diffen.config.source.nfs.location")
    val incomingLocation = s"$nasParentPath/$source/$country/incoming"
    val archivalLocation = s"$nasParentPath/$source/$country/archival"
    val reRunLocation = s"$nasParentPath/$source/$country/rerun"
    val rejectLocation = s"$nasParentPath/$source/$country/reject"
    val errorLocation = s"$nasParentPath/$source/$country/error"
    val applLocation = s"$nasParentPath/$source/$country/appl"
    val configLocation = s"$nasParentPath/$source/$country/configs"





    /*
    Create NAS Directories
    */
    new File(s"$incomingLocation").mkdirs()
    new File(s"$archivalLocation").mkdirs()
    new File(s"$reRunLocation").mkdirs()
    new File(s"$rejectLocation").mkdirs()
    new File(s"$errorLocation").mkdirs()
    new File(s"$applLocation").mkdirs()
    FileUtils.deleteDirectory(new File(configLocation))
    new File(s"$configLocation").mkdirs()

    val perms = java.util.EnumSet.noneOf(classOf[PosixFilePermission])
    if(platformType != "AZURE") {
      /*
        Use 775 permissions to all the NAS directories
      */
      //add owner permissions
      perms.add(PosixFilePermission.OWNER_READ)
      perms.add(PosixFilePermission.OWNER_WRITE)
      perms.add(PosixFilePermission.OWNER_EXECUTE)
      //add group permissions
      perms.add(PosixFilePermission.GROUP_READ)
      perms.add(PosixFilePermission.GROUP_WRITE)
      perms.add(PosixFilePermission.GROUP_EXECUTE)
      //add others permissions
      perms.add(PosixFilePermission.OTHERS_READ)
      perms.add(PosixFilePermission.OTHERS_EXECUTE)

      /*
      Set Directory Permissions
      */
      Files.setPosixFilePermissions(Paths.get(s"$nasParentPath/$source"), perms)
      Files.setPosixFilePermissions(Paths.get(s"$nasParentPath/$source/$country"), perms)
      Files.setPosixFilePermissions(Paths.get(incomingLocation), perms)
      Files.setPosixFilePermissions(Paths.get(archivalLocation), perms)
      Files.setPosixFilePermissions(Paths.get(reRunLocation), perms)
      Files.setPosixFilePermissions(Paths.get(rejectLocation), perms)
      Files.setPosixFilePermissions(Paths.get(errorLocation), perms)
      Files.setPosixFilePermissions(Paths.get(applLocation), perms)
      Files.setPosixFilePermissions(Paths.get(configLocation), perms)
    }



    new File(inputConfigLocation).listFiles().filter(_.getName.endsWith(".xml")).foreach({f=>
      if(platformType == "AZURE") {

      }
      FileUtils.copyFile(new File(f.getAbsolutePath),
        new File(configLocation + File.separator + f.getName))
      if(platformType != "AZURE")
        Files.setPosixFilePermissions(Paths.get(s"$configLocation/${f.getName}"), perms)
    })

    new File(inputConfigLocation + "/../scripts").listFiles().foreach({f=>
      FileUtils.copyFile(new File(f.getAbsolutePath),
        new File(applLocation + File.separator + f.getName))
      if(platformType != "AZURE")
        Files.setPosixFilePermissions(Paths.get(s"$applLocation/${f.getName}"), perms)
    })

    println("Successfully created NAS Directories locations")
  }

  /** Creates HDFS directories
    * Includes
    * 1. HDFS data parent directory - root of storage and diffen hdfs directories
    * 2. Verify types/Storage folder
    * 3. Diffen Open folder
    * 4. Diffen NonOpen folder
    * 5. Ops folder
    * 6. Config location in HDFS
    *
    * @param source
    * @param country
    * @param configLocation
    * @param primConfig
    * @param fs
    */
  private def prepareHDFSDirectories(source: String, country: String, configLocation: String, primConfig: Configuration, fs: FileSystem) = {

    val parentPath = primConfig.get("diffen.config.hdfs.data.parent.dir")
    val verifyTypesSchema = primConfig.get("diffen.config.storage.schema")
    val diffenOpenSchema = primConfig.get("diffen.config.diffen.open.schema")
    val diffenNonOpenSchema = primConfig.get("diffen.config.diffen.nonopen.schema")
    val opsSchemaName = primConfig.get("diffen.config.ops.schema")
    val hdfsConfigDir = primConfig.get("diffen.config.hdfs.config.dir")

    fs.mkdirs(new Path(s"$parentPath/$verifyTypesSchema"))
    fs.mkdirs(new Path(s"$parentPath/$diffenOpenSchema"))
    fs.mkdirs(new Path(s"$parentPath/$diffenNonOpenSchema"))
    fs.mkdirs(new Path(s"$parentPath/$opsSchemaName"))
    fs.mkdirs(new Path(s"$hdfsConfigDir"))
    println("Successfully created HDFS Directories locations")
    fs.delete(new Path(s"${hdfsConfigDir}/${source}_${country}_*.xml"), false)

    new File(configLocation).listFiles().filter(_.getName.endsWith("xml")).foreach(x=>
      fs.copyFromLocalFile(new Path(x.getAbsolutePath), new Path(s"${hdfsConfigDir}/${x.getName}"))
    )
    println(s"Successfully uploaded configs to HDFS location: ${hdfsConfigDir}")
  }

  /** Invokes the [[SchemaUtils]] for creation of Hive tables after parsing the param and table configs
    *
    * @param primConfig HDFS configuration
    * @param fs         HDFS filesystem
    * @param source     Source application name of this deployment
    * @param country    Country/Region of this deployment
    */
  private def createHiveTables(primConfig: Configuration, fs: FileSystem, source: String, country: String) = {

    val sparkConf = new SparkConf()

    val spark = SparkSession
      .builder()
      .config(sparkConf)
      .appName(s"DIFFENDeployer-${source}-${country}")
      .enableHiveSupport()
      .getOrCreate()

    val diffenParams = DiffenParams(primConfig)
    val tableDictionary = XmlParser.parseXmlConfig(diffenParams.tableConfigXml, fs)

    //Build OPS Tables
    queryExecutor(SchemaUtils.getOpsSchema(diffenParams), spark)

    val tableConfigs =
      if (diffenParams.parallelExecution) {
        val parTableDictionary = tableDictionary.filterNot(_.name.isEmpty).par
        val parThreads = 3 * spark.conf.get("spark.driver.cores", "1").toInt //oversubscribe based on driver threads
        parTableDictionary.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(parThreads))
        parTableDictionary
      } else {
        tableDictionary.filterNot(_.name.isEmpty)
      }

    //Build Storage, Open and Non Open Tables
    tableConfigs.foreach { config =>
      queryExecutor(SchemaUtils.getBusinessTablesSchema(config, diffenParams), spark)
    }

    //Build Data directory
    SchemaUtils.populateCommonTables(tableConfigs.toList, spark.sqlContext, diffenParams, fs)
  }

  /** Executes a list of queries, given a SQLContext **/
  private def queryExecutor(items: List[String], spark: SparkSession) = {
    items.filter(x => StringUtils.isNotEmpty(x.trim))
      .foreach { query =>
        println(s"Executing query :$query")
        spark.sql(query)
      }
  }

  /**
    * Upload template to Nifi after replacing all the placeholders.
    * The process will also instantiate the template and initialize and enable all the controller services associated to that Nifi Process Group.
    *
    * @param cliParams
    * @param deployerConfiguration
    * @param nifiPassword
    * @param hivePassword
    * @param hiveDRPassword
    */
  private def uploadNifiTemplate(cliParams: DIFFENDeployerCli, deployerConfiguration: Configuration, nifiPassword: String, hivePassword: String, hiveDRPassword: String) = {

    val rootProcessorGroupId = "root"

    val client = new ApiClient(Map[String, Authentication]("oAuth" -> new OAuth)).setBasePath(cliParams.getNifiUrl)
    io.swagger.client.Configuration.setDefaultApiClient(client)
    client.getHttpClient.setReadTimeout(0, TimeUnit.MILLISECONDS)
    client.getHttpClient.setConnectTimeout(0, TimeUnit.MILLISECONDS)

    val flowApi = new FlowApi()
    val templatesApi = new TemplatesApi()
    val processGroupsApi = new ProcessgroupsApi()
    val controllerServicesApi = new ControllerservicesApi()

    createTemplate(cliParams, createPropertiesResolutionMap(cliParams, deployerConfiguration, hivePassword, hiveDRPassword))
    println(s"Replaced placeholders in the nifi template file and created final file: ${getTemplateFileName(cliParams)}")

    if (cliParams.isNifiAuthenticationEnabled) {
      client.setAccessToken(new AccessApi().createAccessToken(cliParams.getNifiUsername, nifiPassword))
      println("Successfully Logged in to nifi and set acccess token")
    }

    val processGroupEntity = new ProcessGroupEntity().revision(new RevisionDTO().version(0l))
      .component(new ProcessGroupDTO().name(s"DIFFEN-${cliParams.getSource}-${cliParams.getCountry}"))
    val pgDetails = flowApi.getFlow(rootProcessorGroupId).getProcessGroupFlow.getFlow.getProcessGroups.map(o => (o.getComponent.getName, o.getComponent.getId)).toMap
    val sourceGroupId = if (pgDetails.contains((s"DIFFEN-${cliParams.getSource}-${cliParams.getCountry}")))
      pgDetails(s"DIFFEN-${cliParams.getSource}-${cliParams.getCountry}") else processGroupsApi.createProcessGroup(rootProcessorGroupId, processGroupEntity).getId
    processGroupsApi.uploadTemplate(rootProcessorGroupId, new File(cliParams.getConfigLocation, getTemplateFileName(cliParams)))
    println(s"Uploaded Template XML to the Nifi Process Group : DIFFEN-${cliParams.getSource}-${cliParams.getCountry} with process group id : ${sourceGroupId}")

    flowApi.getTemplates().getTemplates().filter(_.getTemplate.getName == s"DIFFEN-${cliParams.getSource}-${cliParams.getCountry}").foreach { availableTemplate =>

      val inputTemplate = availableTemplate.getTemplate()
      val body = new InstantiateTemplateRequestEntity()
        .templateId(inputTemplate.getId())
        .originX(new Random().nextInt(100).toDouble)
        .originY(new Random().nextInt(100).toDouble)

      val result = processGroupsApi.instantiateTemplate(sourceGroupId, body)

      val processGroupId = sourceGroupId
      println(s"Successfully Instantiated the Process Group : ${inputTemplate.getName} with process group id : ${processGroupId}")

      val distributedMapCacheServer = new ControllerServiceEntity()
        .revision(new RevisionDTO().version(0l))
        .component(new ControllerServiceDTO().`type`("org.apache.nifi.distributed.cache.server.map.DistributedMapCacheServer").properties(Map("Port" -> cliParams.getDistributedMapCachePort)))

      //processGroupsApi.createControllerService(processGroupId, distributedMapCacheServer)

      flowApi.getControllerServicesFromGroup(processGroupId).getControllerServices.filter(processGroupId == _.getComponent.getParentGroupId)
        .foreach { controllerService =>
          val controllerServiceName = controllerService.getComponent.getName
          println(s"Enabling Controller Service : ${controllerServiceName}")
          controllerService.getComponent.setState(ControllerServiceDTO.StateEnum.ENABLED)
          if ("HiveConnectionPool" == controllerServiceName) {
            controllerService.getComponent.setProperties(Map("hive-db-password" -> hivePassword))
          } else if ("DRHiveConnectionPool" == controllerServiceName) {
            controllerService.getComponent.setProperties(Map("hive-db-password" -> hiveDRPassword))
          }

          if("DRHiveConnectionPool" != controllerServiceName) {
            controllerServicesApi.updateControllerService(controllerService.getId, controllerService)
          }
        }
      templatesApi.removeTemplate(inputTemplate.getId())
    }

    FileUtils.deleteQuietly(new File(cliParams.getConfigLocation, getTemplateFileName(cliParams)))
    println(s"Deployment for ${cliParams.getSource} - ${cliParams.getCountry} completed")
  }

  /**
    * Merge different data structures to create a placeholder map used for nifi template placeholder replacements.
    *
    * @param cliParams
    * @param deployerConfiguration
    * @param hivePassword
    * @param hiveDRPassword
    * @return
    */
  private def createPropertiesResolutionMap(cliParams: DIFFENDeployerCli, deployerConfiguration: Configuration, hivePassword: String, hiveDRPassword: String): Map[String, String] = {
    deployerConfiguration.getValByRegex("diffen.config.")
      .map(entry => (entry._1.replace("diffen.config.", ""), entry._2)).toMap ++
      cliParams.getPropertyResolutionMap ++ Map(
      "source" -> cliParams.getSource,
      "country" -> cliParams.getCountry,
      "template.name" -> (s"DIFFEN-${cliParams.getSource}-${cliParams.getCountry}"),
      "distributed.map.cache.port" -> cliParams.getDistributedMapCachePort,
      "hive.password" -> hivePassword,
      "hive.dr.password" -> hiveDRPassword
    )
  }

  /**
    * Update Nifi Template xml which has placeholders with proper filled in values.
    * Format for placeholder: ##<placeholder>##
    *
    * @param cliParams      input params passed to program to create intermitent file
    * @param placeholderMap placeholderMap used for template placeholders replacement
    */
  private def createTemplate(cliParams: DIFFENDeployerCli, placeholderMap: Map[String, String]) = {

    val writer = new PrintWriter(new File(cliParams.getConfigLocation, getTemplateFileName(cliParams)))
    Source.fromFile(new File(cliParams.getConfigLocation, "DIFFEN-Template.xml")).getLines().foreach { line =>
      var updatedLine = line
      //placeholderMap.foreach(x => updatedLine = updatedLine.replaceAll(s"##${x._1}##", x._2))
      placeholderMap.foreach(x => updatedLine = updatedLine.replaceAll(s"##${x._1}##", Matcher.quoteReplacement(x._2)))
      writer.println(updatedLine)
    }
    writer.flush()
    writer.close()
  }

  private def getTemplateFileName(cliParams: DIFFENDeployerCli) = {
    s"DIFFEN-Template-${cliParams.getSource}-${cliParams.getCountry}.xml"
  }
}