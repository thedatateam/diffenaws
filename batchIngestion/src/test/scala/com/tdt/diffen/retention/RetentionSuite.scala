package com.tdt.diffen.retention

import java.io.FileInputStream

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.XmlParser
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfterEach, FlatSpec}

class RetentionSuite extends FlatSpec with BeforeAndAfterEach {

  val source = "diffen"
  val country = "all"
  val configLocation = "src/test/resources"
  val diffenConfig: Configuration = new Configuration()
  val paramFileName = s"${configLocation}/${source}_${country}_param.xml"
  diffenConfig.addResource(new FileInputStream(paramFileName))
  val diffenParams: DiffenParams = DiffenParams(diffenConfig)
  val fs = FileSystem.get(diffenConfig)
  val tableDictionary: Seq[XmlParser.TableConfig] =
    XmlParser.parseXmlConfig(diffenParams.tableConfigXml, fs)
  val landingDirectory =
    s"${diffenParams.hdfsBaseDir}/" +
      s"${diffenParams.verifyTypesSchema}/" +
      s"${diffenParams.source}_${diffenParams.country}_landing"

  implicit val spark: SparkSession = SparkSession
    .builder()
    .appName("eviction_test")
    .master("local[*]")
    .enableHiveSupport()
    .getOrCreate()
  val retentionDays = 1

  behavior of "spark diffen retention on daily snapshot"
  val dailyPartitionFormat = "yyyy_MM_dd"
  val startDate = "2020-04-16"
  //partitions to be added
  val dailyPartitions = Array("2020_04_14", "2020_04_15", "2020_04_16")
  val dailyExpectedPartitions = Array("2020_04_15", "2020_04_16")
  val hourlyRetentionDays = 1

  it should "evict old partitions in landing layer" in {
    RetentionFixture.init(dailyPartitions.toList, diffenParams, tableDictionary)
    assertLayerWithHivePatitions(
      RetentionConstants.LANDING_LAYER,
      diffenParams.verifyTypesSchema,
      RetentionConstants.LANDING_TABLE_NAME_SUFFIX,
      startDate,
      retentionDays,
      dailyPartitionFormat,
      dailyPartitions,
      dailyExpectedPartitions
    )
  }

  it should "evict old partitions in invalids layer" in {
    RetentionFixture.init(dailyPartitions.toList, diffenParams, tableDictionary)
    assertLayerWithHivePatitions(
      RetentionConstants.INVALIDS_LAYER,
      diffenParams.verifyTypesSchema,
      RetentionConstants.INVALID_TABLE_NAME_SUFFIX,
      startDate,
      retentionDays,
      dailyPartitionFormat,
      dailyPartitions,
      dailyExpectedPartitions
    )
  }

  it should "evict old partitions in verify layer" in {
    RetentionFixture.init(dailyPartitions.toList, diffenParams, tableDictionary)
    assertLayerWithHivePatitions(
      RetentionConstants.VERIFY_TYPES_LAYER,
      diffenParams.verifyTypesSchema,
      "delta",
      startDate,
      retentionDays,
      dailyPartitionFormat,
      dailyPartitions,
      dailyExpectedPartitions
    )
  }

  it should "evict old partitions in open layer" in {
    RetentionFixture.init(dailyPartitions.toList, diffenParams, tableDictionary)
    assertLayerWithHivePatitions(
      RetentionConstants.DIFFEN_OPEN_LAYER,
      diffenParams.diffenOpenSchema,
      "delta",
      startDate,
      retentionDays,
      dailyPartitionFormat,
      dailyPartitions,
      dailyExpectedPartitions
    )
  }

  it should "evict old partitions in non-open layer" in {
    RetentionFixture.init(dailyPartitions.toList, diffenParams, tableDictionary)
    assertLayerWithHivePatitions(
      RetentionConstants.DIFFEN_NONOPEN_LAYER,
      diffenParams.diffenNonOpenSchema,
      "delta",
      startDate,
      retentionDays,
      dailyPartitionFormat,
      dailyPartitions,
      dailyExpectedPartitions
    )
  }

  behavior of "spark diffen retention on hourly snapshot"
  val hourlyPartitionFormat = "yyyy_MM_dd_HH"
  val hourlySartDate = "2020-04-16"
  //partitions to be added
  val hourlyPartitions =
    Array(
      "2020_04_13_00",
      "2020_04_14_00",
      "2020_04_14_02",
      "2020_04_14_04",
      "2020_04_14_23",
      "2020_04_15_00",
      "2020_04_16_00"
    )
  val hourlyExpectedPartitions = Array("2020_04_15_00", "2020_04_16_00")

  def assertLayerWithHivePatitions(
      layer: String,
      schema: String,
      checkTable: String,
      startDate: String,
      retentionDays: Int,
      partitionFormat: String,
      initPartitions: Array[String],
      expectedPartitions: Array[String]
  ) = {

    val inputArguments = Array(
      "--source",
      diffenParams.source,
      "--country",
      diffenParams.country,
      "--layer",
      layer,
      "--config-location",
      configLocation,
      "--retention-days",
      retentionDays.toString,
      "--start-date",
      startDate,
      "--partition-format",
      partitionFormat,
      "--table-type",
      "delta"
    )

    SparkDiffenRetention.main(inputArguments)
    val schemaPath = s"${diffenParams.hdfsBaseDir}/${schema}/"
    val tableName =
      s"${diffenParams.source}_${diffenParams.country}_${checkTable}"
    val tablePath = new Path(s"${schemaPath}/${tableName}/*")
    val deltaActualPartitions = fs
      .globStatus(tablePath)
      .map(_.getPath.getName.split("=")(1))

    assert(deltaActualPartitions === expectedPartitions)
    if (!layer.equalsIgnoreCase(RetentionConstants.LANDING_LAYER)) {
      val hivePartitions = RetentionFixture.getHivePartitions(schema, tableName)
      assert(hivePartitions === expectedPartitions)
    }
  }

  it should "evict old partitions in open layer" in {
    RetentionFixture.init(
      hourlyPartitions.toList,
      diffenParams,
      tableDictionary
    )
    assertLayerWithHivePatitions(
      RetentionConstants.DIFFEN_OPEN_LAYER,
      diffenParams.diffenOpenSchema,
      "delta",
      hourlySartDate,
      hourlyRetentionDays,
      hourlyPartitionFormat,
      hourlyPartitions,
      hourlyExpectedPartitions
    )
  }

  it should "evict old partitions in non-open layer" in {
    RetentionFixture.init(
      hourlyPartitions.toList,
      diffenParams,
      tableDictionary
    )
    assertLayerWithHivePatitions(
      RetentionConstants.DIFFEN_NONOPEN_LAYER,
      diffenParams.diffenNonOpenSchema,
      "delta",
      hourlySartDate,
      hourlyRetentionDays,
      hourlyPartitionFormat,
      hourlyPartitions,
      hourlyExpectedPartitions
    )
  }

  override protected def afterEach(): Unit = {

    val databases = List(
      "diffen_all_diffen_nonopen",
      "diffen_all_diffen_open",
      "diffen_all_storage"
    )

    databases
      .foreach(database =>
        spark.sql(s"drop database ${database} cascade").collect()
      )

    fs.delete(new Path("src/test/resources/hdata"), true)
    fs.close()
    super.afterEach()
  }
}
