package com.tdt.diffen.retention

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.utils.XmlParser.TableConfig
import com.tdt.diffen.utils.{SchemaUtils, XmlParser}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

object RetentionFixture {

  val logger = LoggerFactory.getLogger(this.getClass)

  def getSchemaAndPartitionColumns(diffenParams: DiffenParams) = {
    List(
      (diffenParams.diffenOpenSchema, diffenParams.diffenOpenPartitionColumn),
      (diffenParams.diffenNonOpenSchema, diffenParams.diffenNonOpenPartitionColumn),
      (diffenParams.verifyTypesSchema, diffenParams.verifyTypesPartitionColumn)
    )
  }

  def createHiveTables(tableDictionary: Seq[TableConfig],
                       diffenParams: DiffenParams,
                       partitions: Seq[String])
                      (implicit spark: SparkSession): Unit = {
    tableDictionary.foreach(
      tableConfig => {
        SchemaUtils.getBusinessTablesSchema(tableConfig, diffenParams)
          .foreach(ddl => {
            spark.sql(ddl)
          })

        for{
          partition <- partitions
          (schema, partColumn) <- getSchemaAndPartitionColumns(diffenParams)
        } {
          val dropPartSql =s"ALTER TABLE ${schema}.${tableConfig.name} " +
            s"DROP IF EXISTS PARTITION (${partColumn}='${partition}')"
          val addPartSql =s"ALTER TABLE ${schema}.${tableConfig.name} " +
            s"ADD PARTITION (${partColumn}='${partition}')"
          logger.debug(s"Executing add partition query ${addPartSql}")
          spark.sql(dropPartSql)
          spark.sql(addPartSql)
        }
      }
    )

    //create invalid types table
    SchemaUtils.getOpsSchema(diffenParams).foreach(spark.sql)
    val invalidsTable = s"${diffenParams.verifyTypesSchema}" +
      s".${diffenParams.source}_${diffenParams.country}_${RetentionConstants.INVALID_TABLE_NAME_SUFFIX}"
    partitions.foreach(
      partition =>{
        spark.sql(s"ALTER TABLE ${invalidsTable} DROP IF EXISTS PARTITION (vds='${partition}')")
        spark.sql(s"ALTER TABLE ${invalidsTable} ADD IF NOT EXISTS PARTITION (vds='${partition}')")
      })
  }

  def createDirectories(directoryList: List[String])
                       (implicit spark: SparkSession) = {
    val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
    directoryList.foreach(path => fs.mkdirs(new Path(path)))
  }

  def getHivePartitions(
      schemaName: String,
      tableName: String)(implicit spark: SparkSession) = {
    spark
      .sql(
        s"show partitions ${schemaName}.${tableName}"
      )
      .collect()
      .map(_.getString(0).split("=")(1))
  }

  def init(partitions: List[String],
           diffenParams: DiffenParams,
           tableDictionary: Seq[XmlParser.TableConfig])(implicit spark: SparkSession) = {
    val landingDirectory =
      s"${diffenParams.hdfsBaseDir}/" +
        s"${diffenParams.verifyTypesSchema}/" +
        s"${diffenParams.source}_${diffenParams.country}_landing"
    val landingPartitionKey = RetentionConstants.STORAGE_PARTITION_COLUMN
    val landingPartitionDirs = partitions
      .map(partition => s"$landingDirectory/$landingPartitionKey=$partition")

    RetentionFixture.createHiveTables(tableDictionary, diffenParams, partitions)
    RetentionFixture.createDirectories(landingPartitionDirs.toList)
  }
}
