/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.prop

import java.util.UUID

import com.tdt.diffen.processor._
import com.tdt.diffen.processor.fixtures.{DelimitedTableRow, DiffenSharedSparkContext, NonCDCTestFixtures}
import com.tdt.diffen.utils.Enums.{RunType, TableType}
import org.joda.time.DateTime
import org.scalacheck.Prop._
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
import com.tdt.diffen.utils.DiffenConstants._
import org.joda.time.format.DateTimeFormat

/** Tests the Core logic for Non-Record Type/Non-CDC based master/delta tables for Incremental/Daily loads */
class NonRecordTypeMasterIncrementalTest extends FunSuite with DiffenSharedSparkContext with Checkers with NonCDCTestFixtures {

  val dataCols = List("drReason", "drReasonE", "drReasonC", "drKind", "drDrrKind")
  val diffenAuditCols = List(StartDate, StartTime, EndDate, EndTime, DeleteFlag)
  val vTypesAuditCols = List[String]("rowId", "vds", "filename")
  val metaInfo = NonRecordTypeMetaInfo(timestampColName = StartTime, timestampColFormat = "yyyy-MM-dd HH:mm:ss", vTypesAuditCols, diffenAuditCols, List[String]())

  val januaryFirst = new DateTime(2017, 1, 1, 10, 0)
  val december31st = new DateTime(2016, 12, 31, 10, 0)

  test("NonCDC Master Incremental Test") {

    spark.sparkContext.setLogLevel("ERROR")

    val todaysVerifyTypesListGen = DelimitedDataGenerators.createGeneratorWithoutRecordType(januaryFirst)

    val previousDiffenListGen = DelimitedDataGenerators.createGeneratorWithoutRecordType(december31st)
    check {
      forAllNoShrink(todaysVerifyTypesListGen, previousDiffenListGen) { (verifyTypesList, previousDiffenList) =>
        getAllAssertions(verifyTypesList, previousDiffenList)
      }
    }
  }

  /** Encapsulates all the assertions for this testcase
    *
    * @param verifyTypesList List of DelimitedTableRowWithRecordType that composes Verify types
    * @param previousDiffenList List of DelimitedTableRowWithRecordType that composes the Previous DIFFEN
    * @return Consolidated assertion of all properties
    */
  def getAllAssertions(verifyTypesList: List[DelimitedTableRow], previousDiffenList: List[DelimitedTableRow]) = {

    implicit val sqlContext = spark.sqlContext

    val previousDiffenSingleInstancePerKey = previousDiffenList
      .groupBy(_.drReason) //have only one instance per primarykey
      .map(_._2.head)
      .toList

    val selfIntroducedDuplicates = if (verifyTypesList.nonEmpty) List(verifyTypesList.last.copy(rowId = s"${System.nanoTime()}_${UUID.randomUUID().toString}")) else List()

    val verifyTypesConsolidated = verifyTypesList ++ selfIntroducedDuplicates
    //logDatasetForDebugging(verifyTypesConsolidated, previousDiffenSingleInstancePerKey)

    import sqlContext.implicits._

    val todaysVerifyTypesDF = spark.sparkContext.parallelize(verifyTypesConsolidated).toDF
    val previousDiffenDF = spark.sparkContext.parallelize(previousDiffenSingleInstancePerKey).toDF.drop("vds") //.withColumn("ods", lit(dec31stStr))

    println("###################################### TODAY'S VERIFY TYPES ######################################")
    todaysVerifyTypesDF.show(100, false)
    println("######################################     PREVIOUS DIFFEN     ######################################")
    previousDiffenDF.show(false)

    //TODO Duplicated across Testcase and assertions
    val januaryFirst = new DateTime(2017, 1, 1, 10, 0)
    val tableName = "DUMMY_TABLENAME"

    val processor = new NonRecordTypeBasedProcessor

    //Ref : Note that runType - fulldump/incremental doesnt make sense for transactional assertions
    val DiffenOutput(diffenOpen, diffenNonOpen, vTypesDuplicates, previousDiffenVsVTypes, bRecordsOpt) = processor.processDiffen(tableName, dataCols, List("drReason"), todaysVerifyTypesDF,
      previousDiffenDF, metaInfo, TableType.DELTA, RunType.INCREMENTAL, januaryFirst, DateTimeFormat.forPattern("yyyy_MM_dd_HH"))(spark)

    println("######################################       DIFFEN OPEN       ######################################")
    diffenOpen.show(false)
    println("######################################     DIFFEN NON OPEN     ######################################")
    diffenNonOpen.show(false)
    println("######################################       DUPLICATES       ######################################")
    vTypesDuplicates.show(false)

    val diffenOpenRowIds = diffenOpen.select("rowId").collect().map(row => row.get(0).toString).toSet
    val diffenNonOpenRowIds = diffenNonOpen.select("rowId").collect().map(row => row.get(0).toString).toSet

    val duplicatesRowIds = vTypesDuplicates.select("rowId").collect().map(row => row.get(0).toString).toSet
    val previousDiffenVsVTypesRowIds = previousDiffenVsVTypes.map(df => df.select("rowId").collect().map(row => row.get(0).toString)).getOrElse(Array()).toSet

    //val updatedVerifyTypesSet = verifyTypesConsolidated.filterNot(_.recordType == "B").toSet
    val updatedVerifyTypesSet = verifyTypesConsolidated.toSet

    //val latestPerKeyInVTypes = verifyTypesConsolidated.groupBy(_.drReason).map(_._2.last).toSet
    val latestPerKeyInVTypes = verifyTypesConsolidated
      .groupBy(row => (row.drReason, row.drReasonE, row.drReasonC, row.drKind, row.drDrrKind))
      .map { case (_, listRow) => listRow.last }
      .groupBy(_.drReason)
      .mapValues(groupList => groupList.maxBy(delimRow => delimRow.rowId))
      .values.toSet


    val oldUpdatesDuringTheDay = updatedVerifyTypesSet.diff(latestPerKeyInVTypes)
    //Has to go into DIFFEN NON_OPEN -  Updates for I records (+ Old As for the day)
    val prevDiffenOpenProductsInVTypes = previousDiffenSingleInstancePerKey.filter(row => updatedVerifyTypesSet.map(_.drReason).contains(row.drReason)).toSet
    //Has to stay in DIFFEN OPEN - Previous Diffen open for which no update has been received (+ Latest update on Verify types)
    val prevDiffenOpenProductsNotInVTypes = previousDiffenSingleInstancePerKey.filterNot(row => updatedVerifyTypesSet.map(_.drReason).contains(row.drReason)).toSet

    //DIFFEN OPEN = latestInVerifyTypes + PrevDIFFEN data which is not updated through verify types - D Records
    val diffenOpenCountCheck = diffenOpen.count() === (latestPerKeyInVTypes.size + prevDiffenOpenProductsNotInVTypes.size)

    //DIFFEN NON_OPEN = prevDIFFEN Data which is updated through today's VTypes + Changes during the day in Vtypes + D records - bRecords
    val diffenNonOpenCountCheck = diffenNonOpen.count() === prevDiffenOpenProductsInVTypes.size + oldUpdatesDuringTheDay.size - duplicatesRowIds.size - previousDiffenVsVTypesRowIds.size


    //Actual record checks
    val diffenOpenRowIdCheck = diffenOpenRowIds ===
      (prevDiffenOpenProductsNotInVTypes.map(_.rowId) ++ latestPerKeyInVTypes.map(_.rowId))

    val diffenNonOpenRowIdCheck = diffenNonOpenRowIds === (prevDiffenOpenProductsInVTypes.map(_.rowId) ++ oldUpdatesDuringTheDay.map(_.rowId)).diff(duplicatesRowIds ++ previousDiffenVsVTypesRowIds)

    println(s"diffenOpenCountCheck: $diffenOpenCountCheck   -> ${diffenOpen.count()} === ${latestPerKeyInVTypes.size} + ${prevDiffenOpenProductsNotInVTypes.size}")
    println(s"diffenNonOpenCountCheck: $diffenNonOpenCountCheck   -> ${diffenNonOpen.count()} === ${prevDiffenOpenProductsInVTypes.size} + ${oldUpdatesDuringTheDay.size} - ${duplicatesRowIds.size} - ${previousDiffenVsVTypesRowIds.size}")
    println(s"diffenOpenRowIdCheck: $diffenOpenRowIdCheck")
    println(s"diffenNonOpenRowIdCheck: $diffenNonOpenRowIdCheck")

    diffenOpenCountCheck && diffenNonOpenCountCheck && diffenOpenRowIdCheck && diffenNonOpenRowIdCheck
  }
}