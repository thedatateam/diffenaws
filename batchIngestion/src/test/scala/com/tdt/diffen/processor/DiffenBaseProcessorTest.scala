package com.tdt.diffen.processor

import com.tdt.diffen.utils.DiffenConstants.{DeleteFlag, EndDate, EndTime, StartDate, StartTime}
import org.apache.spark.SparkConf
import org.apache.spark.sql.types.{BooleanType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.lit
import org.scalatest.FunSuite

class DiffenBaseProcessorTest extends FunSuite {

  val sparkConf = new SparkConf()
  sparkConf.setMaster("local[*]")
  sparkConf.setAppName("duplicates_test")

  implicit val spark = SparkSession.builder().config(sparkConf).getOrCreate()

  test("testFindDuplicates") {
    val processor =  new RecordTypeBasedProcessor()

    val columnList = List(
      ("rowId", StringType),
      ("filename", StringType),
      ("c_journaltime", StringType),
      ("c_transactionid", StringType),
      ("c_operationtype", StringType),
      ("c_userid", StringType),
      ("agreementid", StringType),
      ("current_npa", StringType),
      ("new_npa", StringType),
      ("npa_run_date", StringType),
      ("branchid", StringType),
      ("productid", StringType),
      ("proposalid", StringType),
      ("schemeid", StringType),
      ("currencyid", StringType),
      ("movementid", StringType),
      ("repossess_date", StringType),
      ("repossess_flag", BooleanType),
      ("valuation_date", StringType),
      ("valuation_amount", StringType),
      ("release_date", StringType),
      ("release_flag", StringType),
      ("sale_amount", StringType),
      ("sale_date", StringType),
      ("sale_flag", StringType),
      ("makerid", StringType),
      ("makedate", StringType),
      ("authid", StringType),
      ("authdate", StringType),
      ("status", StringType),
      ("reposession_charges", StringType),
      ("parking_charges", StringType),
      ("other_charges", StringType),
      ("mprofit", StringType),
      ("mloss", StringType),
      ("lastupdated_new", StringType),
      ("vat_on_sale", StringType),
      ("depreciated_asset", StringType),
      ("repo_authorized", StringType),
      ("vds", StringType)
    )

    val schema = StructType(columnList.map(x => StructField(x._1, x._2)))

    val vTypesAuditCols = List[String]("rowId", "vds", "filename")
    val diffenAuditCols = List(StartDate, StartTime, EndDate, EndTime, DeleteFlag)
    val systemAuditCols = List()

    val diffenApplicableVTypesAuditCols = vTypesAuditCols.diff(List("vds", "filename"))


    val inputDF: DataFrame = spark.read.format("csv")
      .schema(schema)
      .load("src/test/resources/dup.csv")
      .withColumn("s_row_src", lit("verifyTypes"))
    val tableColumns: List[String] = columnList.map(_._1)
    val auditColList = diffenApplicableVTypesAuditCols ++ diffenAuditCols ++ systemAuditCols
    val ignorableColumns: List[String] = List("rowId", "filename", "vds", DeleteFlag)
    val orderColList: List[String] = List()
    val pickLatest = true

    val _@(dedup, dup) = processor.findDuplicates(inputDF, tableColumns, auditColList, ignorableColumns, orderColList, pickLatest)
    assert(dedup.count() == 3)
    assert(dup.count() == 1)
    val dupRow = dup.collect().head
    val rowId = dupRow.getAs[String]("rowId")
    val dupRowId = dupRow.getAs[String]("DUPLICATE_ROWID")
    assert(rowId.equals("68234123262873_112bef12-9494-46e9-8c96-aeca971f4ffc"))
    assert(dupRowId.equals("68234123262875_112bef12-9494-46e9-8c96-aeca972f4ffc"))
  }

}
