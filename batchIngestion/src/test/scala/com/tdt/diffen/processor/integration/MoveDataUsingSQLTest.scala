package com.tdt.diffen.processor.integration


import com.tdt.diffen.processor.fixtures.DiffenSharedSparkContext
import org.scalatest.{FlatSpec, Matchers}
import org.apache.spark.sql.functions._

case class Data(id: String, score: String, p: String, t: String)

class MoveDataUsingSQLTest extends FlatSpec with Matchers with DiffenSharedSparkContext {


  def test(): Unit = {



   //spark.sql("drop table if exists")
      //spark.sql("CREATE EXTERNAL TABLE test.bfl_all_txntemp_vfdt_vw_oth1 (  `rowid` string,    `s_startdt` string,    `s_starttime` string,    `s_enddt` string,    `s_endtime` string,    `s_deleted_flag` string,    `caseid` int,    `bpid` int,    `txnadviceid` int,    `adviceamt` double,    `advicedate` string,    `txnid` int,    `txnadjustedamt` double,    `due` double,    `chargedesc` string,    `chargeid` int,    `status` string,    `chargecodeid` int,    `agreementno` string,    `instl_num` int,    `cust_id` int,    `loan_status` string)  PARTITIONED BY (    `ods` string)  ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'  LOCATION    '/data/bfl/hdata/bfl_all_diffen_open/bfl_all_txntemp_vfdt_vw_oth1'")
      // spark.sql("CREATE EXTERNAL TABLE test.bfl_all_lea_agreemnet_dtl (   `rowid` string,   `s_startdt` string,   `s_starttime` string,   `s_enddt` string,   `s_endtime` string,   `s_deleted_flag` string,   `proposalid` int,   `agreementid` int,   `agreementtype` string,   `agreementno` string,   `applid` int,   `assetcost` double,   `amtfin` double,   `intrate` double,   `effrate` double,   `markup` double,   `inttype` string,   `tenure` int,   `freq` string,   `numinstl` int,   `instaltype` string,   `instlmode` string,   `advanceinstl` int,   `pretaxirr` double,   `posttaxirr` int,   `sanctiondate` string,   `authid` string,   `stateid` int,   `catgid` int,   `taxrate` string,   `bouncechequefee` string,   `brokeragerate` string,   `mgmtfeerate` string,   `rvrate` string,   `sdrate` string,   `overduerate` string,   `agreementdate` string,   `commencedate` string,   `closuredate` string,   `rescheduledate` string,   `status` string,   `spotintrate` string,   `schemeid` int,   `marginmoney` double,   `proposalcrdate` string,   `proposalcrby` string,   `agreementcrdate` string,   `agreementcrby` string,   `proposalmoddate` string,   `proposalmodby` string,   `creditauthdate` string,   `creditauthby` string,   `prddocauthdate` string,   `prddocauthby` string,   `poddocauthdate` string,   `poddocauthby` string,   `creditauthreqd` string,   `prdauthreqd` string,   `podauthreqd` string,   `lesseeid` int,   `action` string,   `actiondate` string,   `print_flag` string,   `dueday` int,   `creditauthreqd_s` string,   `creditauthdate_s` string,   `creditauthby_s` string,   `creditauthreqd_o` string,   `creditauthdate_o` string,   `creditauthby_o` string,   `amtothers` int,   `rebate` string,   `penalty` string,   `accrual_flag` string,   `currencyid` string,   `prinintcal` string,   `deannualize` string,   `branchid` int,   `appdate` string,   `emi` double,   `resvalue` string,   `accessamt` string,   `prepaypenalty` double,   `marginrate` double,   `finalsource` string,   `finalsourceid` string,   `firstsource` string,   `firstsourceid` string,   `promotionscheme` string,   `loan_in_name` string,   `supplierid` int,   `dperyr` string,   `disbursaldate` string,   `forecloselockln` int,   `productflag` string,   `pdcflag` string,   `decimals` int,   `rs_confirm` string,   `graceperiod` int,   `lea_last_acr_date` string,   `lea_non_acr_flag` string,   `lea_acr_int_amt` double,   `batchnumber` int,   `accounting_monthly_irr` string,   `lea_monthly_irr` string,   `margin_money_2` string,   `notepad` string,   `vat_prin_rate` int,   `vat_int_rate` int,   `crscore` string,   `age` string,   `maturity_age` string,   `npa_stageid` string,   `bill_flag` string,   `last_lpi_date` string,   `npa_date` string,   `dpd` int,   `dpd_string` string,   `datelastupdt` string,   `disbursedamount` double,   `financecharge` double,   `benefitamount` int,   `disbursalto` string,   `mktgid` int,   `brokerid` int,   `refagreementid` string,   `advdisbintrate` int,   `vat_instl_rate` int,   `vat_input` string,   `dpd_string_stampdate` string,   `bucket` int,   `currseqno` string,   `grace_interest_charge` string,   `grace_adjust_schedule` string,   `sanctionid` string,   `autclos_flag` string,   `manual_npa_movement_flag` string,   `disb_type` string,   `no_disbursal` int,   `pemi_rate` string,   `inttaxrate` int,   `tdsrate` int,   `lpirate` int,   `floatingflg` string,   `plrtype` string,   `taxapplicable` string,   `balance_transfer` string,   `relational_discount` string,   `other_discount` string,   `co_appl_req` string,   `guarantor_req` string,   `sec_deposit_req` string,   `foreclose_allowed` string,   `foreclose_penalty` string,   `foreclose_lock_period` string,   `atmcode` string,   `rate` int,   `feerate` int,   `costoffunds` int,   `loss` int,   `expenses` int,   `customeryield` int,   `pretaxroi` int,   `roi` int,   `tax` int,   `tax_percentage` int,   `securitycover` int,   `securitytaken` string,   `cross_collateral` string,   `billno` string,   `relationship_disc` string,   `professional_disc` string,   `prepay_allowed` string,   `prepay_penalty` string,   `prepay_lock_period` string,   `foreclosure_penalty` string,   `dssno` int,   `maturitydate` string,   `del_monthly_string` string,   `maturityamt` double,   `tenuretype` string,   `lpitype` string,   `actual_months` string,   `actual_days` string,   `match_found` string,   `dedupe_result` string,   `employerid` int,   `repayment_effective_date` string,   `outstandingprinprecent` string,   `min_penalamount` string,   `emp_flag` string,   `plrrate` double,   `insurcoid` string,   `insur_productid` string,   `insurance_flag` string,   `insured_till` string,   `deposit_account` string,   `spread2` int,   `floating_frequency` int,   `last_float_spread_date` string,   `ltv_multiplier` int,   `disbursement_direct_credit` string,   `stat_acc_frequency` string,   `system` string,   `amtfin_regular` double,   `amtfin_bonus` int,   `freq_bonus` string,   `instlmode_bonus` string,   `advanceinstl_bonus` int,   `disbursaldate_bonus` string,   `int_roundoff_para` string,   `int_roundtill` int,   `instlamt_roundoff_para` string,   `instlamt_roundtill` int,   `prin_base_unit` int,   `fixed_term` string,   `intstart_date_bonus` string,   `instaltype_bonus` string,   `ins_bill_flag` string,   `special_spread` double,   `emi_bonus` string,   `next_float_prime_date` string,   `next_float_spread_date` string,   `refinance_receiptid` string,   `step_rate` int,   `age_past_agr` string,   `invoice_flag` string,   `rc_flag` string,   `servicetaxrate` int,   `dme_id` int,   `groupid` string,   `promotionid` int,   `fileno` string,   `app_formno` string,   `hfi_flag` string,   `hfi` string,   `employer_addressid` int,   `empcode` int,   `spreadid` string,   `initial_effrate` int,   `intstart_date_regular` string,   `tranching_flag` string,   `tranching_type` string,   `compound_freq` string,   `int_rebate_flag` string,   `actualirr` double,   `disb_status` string,   `gross_ltv` double,   `net_ltv` double,   `amt_requested` double,   `tenure_requested` int,   `rate_emi_flag` string,   `asset_marked` string,   `disbursalamount` double,   `org_schemeid` string,   `lsi_agreement_no` int,   `chrg_flg` string,   `disb_flag` string,   `hl_file_no` string,   `old_agreementno` string,   `pemi_billing` string) PARTITIONED BY (   `ods` string) ROW FORMAT SERDE   'org.apache.hadoop.hive.ql.io.orc.OrcSerde' STORED AS INPUTFORMAT   'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' OUTPUTFORMAT   'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat' LOCATION   '/data/bfl/hdata/bfl_all_diffen_open/bfl_all_lea_agreemnet_dtl'")
    //spark.sql("CREATE EXTERNAL TABLE test.bfl_all_fact_loan(    `rowid` string,    `s_startdt` string,    `s_starttime` string,    `s_enddt` string,    `s_endtime` string,    `s_deleted_flag` string,    `appl_id` int,    `emi` decimal(16,2),    `customer_id` bigint,    `installment_overdue` decimal(18,0),    `amount_outstanding` decimal(18,0),    `emi_paid` decimal(18,2),    `principal_overdue` decimal(18,0),    `principal_outstanding` decimal(18,0),    `interest_overdue` decimal(18,0),    `interest_outstanding` decimal(18,0),    `bucket` decimal(18,0),    `penalty_paid` decimal(19,5),    `penalty_due` decimal(19,5),    `bcc_paid` decimal(19,5),    `bcc_due` decimal(19,5),    `other_paid` decimal(19,5),    `other_due` decimal(19,5),    `principle_amount_paid` decimal(20,2),    `interest_paid` decimal(18,0),    `balance_tenure` decimal(10,0),    `last_emi_date` int,    `next_emi_date` int)  PARTITIONED BY (    `ods` string)  ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'  LOCATION    '/data/bfl/hdata/bfl_all_diffen_open/bfl_all_fact_loan'")
    //spark.sql("  CREATE EXTERNAL TABLE test.bfl_all_lea_nbfc_pmnt_dtl(    `rowid` string,    `s_startdt` string,    `s_starttime` string,    `s_enddt` string,    `s_endtime` string,    `s_deleted_flag` string,    `pmntid` int,    `pmnttype` string,    `pmntmode` string,    `moduleid` string,    `chequeid` int,    `txnadviceid` int,    `allocatedamt` double,    `pmntdate` string,    `bptype` string,    `bpid` int,    `status` string,    `bouncedate` string,    `reasonid` int,    `depositdate` string,    `datelastupdt` string,    `del_flag` string,    `bou_txn_date` string,    `princomp_recd` double,    `intcomp_recd` double,    `excessint_recd` int)  PARTITIONED BY (    `ods` string)  ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'  LOCATION    '/data/bfl/hdata/bfl_all_diffen_open/bfl_all_lea_nbfc_pmnt_dtl'")
/*
    spark.sql("drop table test.bfl_all_lea_repaysch_dtl ")
    spark.sql("  CREATE EXTERNAL TABLE test.bfl_all_lea_repaysch_dtl(  `rowid` string,    `s_startdt` string,    `s_starttime` string,    `s_enddt` string,    `s_endtime` string,    `s_deleted_flag` string,    `propinstlid` int,    `proposalid` int,    `agreementid` int,    `instlnum` int,    `duedate` double,    `instlamt` double,    `taxamt` int,    `recdamt` double,    `odamt` int,    `oddate` string,    `princomp` double,    `intcomp` double,    `advflag` string,    `sdadjamt` int,    `billflage` string,    `arrears` string,    `othersamt` int,    `floatint` string,    `effrate` double,    `balprin` double,    `prepayamt` string,    `exintcomp` int,    `billeddate` string,    `vat_prin` int,    `vat_int` int,    `datelastupdt` string,    `princomp_recd` double,    `intcomp_recd` double,    `vat_instl` int,    `vat_instl_recd` string,    `vat_prin_recd` string,    `vat_int_recd` string,    `vat_processid` string,    `exintcomp_recd` int,    `exint_payable` string,    `exint_income` string,    `npastageid` string,    `cons_billflag` string,    `tds` int,    `ler` int,    `acc_depr` int,    `servicetax` int,    `servicetax_recd` string,    `days` int,    `download_flag` string,    `batchid` string,    `emi_flag` string,    `bptype` string,    `billno` string,    `bpid` string,    `payment_place_id` string,    `billno_user` string,    `outward_batchid` int,    `emiadviceid` int,    `lpi_calculation` string,    `dropline_amt` int,    `dropline_limit` int,    `utilisation_limit` double,    `available_limit` double,    `instlnum_new` int,    `recievedate` string,    `act_available_limit` double,    `act_utilisation_limit` double,    `recdate` string,    `dpddays` int,    `emi_holiday` string)  PARTITIONED BY (    `ods` string)  ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'  LOCATION  '/data/bfl/hdata/bfl_all_diffen_open/bfl_all_lea_repaysch_dtl'")

    spark.sql("drop table if exists test.lea_nbfc_cheque_dtl")
    spark.sql("create table  test.lea_nbfc_cheque_dtl (chequeid string, chequetype string, mc_status string, status string," +
      "reference string) ")
    spark.sql("DROP table if exists test.JB_TXN_TBL_HTS")
    spark.sql("CREATE TABLE test.JB_TXN_TBL_HTS AS  " +
      "SELECT AGREEMENTID   FROM test.bfl_all_lea_agreemnet_dtl  WHERE  STATUS = 'A' AND productflag = 'Y'")

    spark.sql("DROP TABLE if exists test.JB_TXN_TBL_GL_UGL")
    spark.sql("CREATE TABLE test.JB_TXN_TBL_GL_UGL ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat' AS  SELECT AGREEMENTID  FROM test.bfl_all_lea_agreemnet_dtl   WHERE STATUS = 'A'")

    spark.sql("DROP TABLE if exists test.jb_txn_tbl_recon")
    spark.sql("create table test.jb_txn_tbl_recon ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat' as  SELECT AGREEMENTID FROM test.JB_TXN_TBL_HTS  UNION  SELECT AGREEMENTID FROM test.JB_TXN_TBL_GL_UGL")
    
    spark.sql("drop table if exists test.PP_REPAY_DTL_RECON")
    spark.sql("CREATE TABLE test.PP_REPAY_DTL_RECON ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat' AS SELECT   " +
      "b.agreementid,   " +
      " b.lesseeid  as customerid,      " +
      "sum(  CASE     when b.bill_flag in ('Y') then     CASE     when signum(5-10) in ('1','0')   then      A.instlamt -  CASE  when recdamt is null then 0 else 0      end     end      else 0   end) as INSTALLMENT_OVERDUE,  " +
      "SUM(  a.instlamt -     CASE     when recdamt is null then 0    end) as Amount_outstanding, " +
      " CASE    when  SUM(a.recdamt)  is null then 0  end   as emi_paid,  " +
      "SUM( CASE     when b.bill_flag in ('Y') then      CASE     when signum(5-10) in ('1','0')   then      A.princomp -  CASE  when princomp_recd is null then 0 else 0      end     end      else 0   end) as principal_overdue, " +
      " (CASE    when SUM(    CASE  when advflag is not null then a.princomp      when advflag is null then 0   end    ) is null then 0  end   -   " + "CASE    when SUM(    CASE  when advflag is not null then a.princomp_recd      when advflag is null then 0   end    ) is null then 0  end)   as principal_outstanding"
      + "  " + "," +
      "SUM( CASE     when b.bill_flag in ('Y') then      CASE     when signum(5-10) in ('1','0')   then      A.intcomp +      CASE  when exintcomp is null then 0 else 0      end     -    CASE  when intcomp_recd is null then 0 else intcomp_recd    end     end      else 0   end) as interest_overdue, " +
      " SUM(CASE     when a.exintcomp is not null  then a.intcomp + a.exintcomp else 0 end)   -  SUM(CASE     when a.INTCOMP_RECD is not null  then a.INTCOMP_RECD  else 0 end)   as INTEREST_OUTSTANDING ,"
      + "max(b.EMI) as EMI,    "
      + "max(b.Bucket)  as BUCKET, " +
      "SUM(if(A.PRINCOMP_RECD is not null,A.PRINCOMP_RECD,0))  as PRINCIPAL_PAID, " +
      "SUM(if(A.INTCOMP_RECD is not null,A.INTCOMP_RECD,0))  as INTEREST_PAID "  +
      "FROM test.bfl_all_lea_repaysch_dtl a" + //", test.bfl_all_lea_agreemnet_dtl b, test.bfl_all_fact_loan i " +
      " join test.bfl_all_lea_agreemnet_dtl b on  a.agreementid = b.agreementid  " +
      "join test.bfl_all_fact_loan i on b.agreementid = i.appl_id " +
       "where a.ods = '2016-01-01' and b.ods='2016-01-01' and i.ods='2016-01-01' " + "GROUP BY b.agreementid,b.lesseeid,amtfin     " 
   )

    //spark.sql("describe table test.PP_REPAY_DTL_RECON4 ").show

    spark.sql("drop table if exists test.PP_ADVICE_DTL_RECON ").show
    spark.sql("CREATE TABLE test.PP_ADVICE_DTL_RECON ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat' AS  SELECT a.caseid AS AGREEMENTID,  CASE   when  SUM(if(A.chargecodeid = 7,A.txnadjustedamt,0)) is not null then SUM(if(A.chargecodeid = 7,A.txnadjustedamt,0))  else 0  end  as penalpaid, " +
    " SUM(if(A.chargecodeid = 7,a.adviceamt - a.txnadjustedamt,0)) as penaldue ," +
    "SUM(if(A.chargecodeid = 8,a.txnadjustedamt,0)) as bccpaid,"+
    "SUM(if(A.chargecodeid = 8,a.adviceamt - a.txnadjustedamt,0)) as bccdue, "+
      " SUM(if(A.chargecodeid in (7,8,9,37,12,58),0, a.txnadjustedamt)) as otherpaid," +
      " SUM(if(A.chargecodeid in (7,8,9,37,12,58),0, a.adviceamt - a.txnadjustedamt)) as otherdue," +
     "'30-JUL-2017' as BUSINESSDATE," +
      //" --change   SYSDATE PROCESSDATE,"+
      "'U' as PROCESSED_FLAG" +
      "  FROM test.bfl_all_txntemp_vfdt_vw_oth1 a " +
      "join test.jb_txn_tbl_recon I on a.caseid = i.agreementid " +
      "where a.status ='A'  " +
      "AND a.chargecodeid NOT IN (9, 500037)" +
      "GROUP BY a.caseid"
    )
    spark.sql("describe table test.PP_ADVICE_DTL_RECON ").show
  
    spark.sql("drop table if exists test.PP_DM_LOAN_DUE_DETAILS_RECON")
    spark.sql("CREATE TABLE test.PP_DM_LOAN_DUE_DETAILS_RECON  ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat' AS  SELECT RE.AGREEMENTID,      RE.CUSTOMERID,      RE.INSTALLMENT_OVERDUE,      RE.AMOUNT_OUTSTANDING,      RE.EMI_PAID,      RE.PRINCIPAL_OVERDUE,      RE.PRINCIPAL_OUTSTANDING,      RE.INTEREST_OVERDUE,      RE.INTEREST_OUTSTANDING,      RE.EMI,      RE.BUCKET,      AD.PENALPAID,      AD.PENALDUE,      AD.BCCPAID,      AD.BCCDUE,      AD.OTHERPAID,      AD.OTHERDUE,      RE.PRINCIPAL_PAID,      RE.INTEREST_PAID      FROM test.PP_REPAY_DTL_RECON4 RE     right outer join    test.PP_ADVICE_DTL_RECON AD     on RE.AGREEMENTID = AD.AGREEMENTID")
    spark.sql("describe table test.PP_DM_LOAN_DUE_DETAILS_RECON").show()


    spark.sql("DROP TABLE if exists test.PP_BALANCE_TENURE_RECON")
    spark.sql("CREATE TABLE test.PP_BALANCE_TENURE_RECON ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'  AS  select D.AGREEMENTID,count(1) as BALANCE_TENURE  FROM test.bfl_all_lea_repaysch_dtl RE, test.PP_DM_LOAN_DUE_DETAILS_RECON  D  WHERE   RE.PROPOSALID = d.agreementid  AND RE.BILLFLAGE is null  AND RE.ADVFLAG is null  GROUP BY D.AGREEMENTID")
    spark.sql("describe TABLE  test.PP_BALANCE_TENURE_RECON").show()

*/

    spark.sql("DROP TABLE if exists test.PP_LAST_EMI_RECON")
    spark.sql("SELECT   d.AGREEMENTID, MAX(b.pmntdate) as LAST_EMI_DATE  FROM   test.lea_nbfc_cheque_dtl a " +
      "  full join test.bfl_all_lea_nbfc_pmnt_dtl b on " +
      "a.chequeid = b.chequeid  " +
      "full join  "+
      "test.PP_DM_LOAN_DUE_DETAILS_RECON D  on " +
      "a.reference = cast(d.agreementid as string)" +
      "WHERE    chequetype = 'R'  AND mc_status = 'A'  " +
      "and a.status NOT IN ('B','X') " +
      "GROUP BY d.agreementid")
   /* spark.sql("DROP TABLE if exists test.PP_NEXT_EMI_DATE_RECON")
    spark.sql("CREATE TABLE test.PP_NEXT_EMI_DATE_RECON   AS  SELECT d.agreementid,TO_CHAR(MIN(lrd.duedate), 'DD-MON-RRRR') as NEXT_EMI_DATE    FROM test.bfl_all_lea_repaysch_dtl lrd    full left join   test.PP_DM_LOAN_DUE_DETAILS_RECON D on            lrd.agreementid = d.agreementid           WHERE  lrd.advflag = 'N'                  GROUP BY d.agreementid;")
*/
/*
    spark.sql("DROP table if exists test.JB_TXN_TBL_HTS")
    spark.sql("CREATE TABLE test.JB_TXN_TBL_HTS   ROW FORMAT SERDE    'org.apache.hadoop.hive.ql.io.orc.OrcSerde'  STORED AS INPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'  OUTPUTFORMAT    'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat' AS  " +
      "SELECT AGREEMENTID,ods   FROM test.bfl_all_lea_agreemnet_dtl  WHERE  STATUS = 'A' AND productflag = 'Y'   and ods ='2018-10-28'  ")

*/

  }


  "run" should "work" in {
    test()
  }


}
