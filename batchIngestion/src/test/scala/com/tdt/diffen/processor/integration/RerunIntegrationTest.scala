/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.integration

import java.util.UUID

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.processor.BootstrapProcessor
import com.tdt.diffen.processor.fixtures.{IntegrationTestDataHelpers, DiffenSharedSparkContext}
import com.tdt.diffen.utils.Enums.SourceType
import com.tdt.diffen.utils.{FileSystemUtils, SchemaUtils, DiffenUtils}
import com.tdt.diffen.utils.XmlParser.TableConfig
import com.tdt.diffen.utils._
import org.apache.commons.lang3.StringUtils
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.scalatest.{FlatSpec, Matchers}

/** Tests the Delta source type behavior for CDC/Record Type based tables */
class RerunIntegrationTest extends FlatSpec with Matchers with DiffenSharedSparkContext {

  def prepareEodTableData(diffenParams: DiffenParams)(implicit spark: SparkSession) : Unit = {
    spark.sql(s"insert into ${diffenParams.opsSchema}.eod_table partition (businessdate='2017-01-02') values ('${diffenParams.source}', '${diffenParams.country}', '2017-01-03 00:00:00', '2017-01-02 00:00:00')")
    spark.sql(s"insert into ${diffenParams.opsSchema}.eod_table partition (businessdate='2017-01-03') values ('${diffenParams.source}', '${diffenParams.country}', '2017-01-04 00:00:00', '2017-01-03 00:00:00')")
  }

  "RerunIntegrationTest" should "perform automated cascaded runs" in {
    val id = StringUtils.substringAfterLast(UUID.randomUUID().toString, "-")
    val base = "target/batchIngestion/" + id

    val tableName = "gps_all_deltacdcdatadiffentest"

    val eodMarker = "2017-01-02 00:00:00"
    val businessDay = "2017-01-01 00:00:00"

    val partition: String = DiffenUtils.getBusinessDayPartition(businessDay, "yyyy-MM-dd HH:mm:ss", "yyyy-mm-dd").get

    val vTypesAuditColSchema = "rowId STRING NOT NULL ^filename STRING NOT NULL ^vds STRING NOT NULL"
    val diffenAuditColSchema = "s_startdt STRING NOT NULL ^s_starttime STRING NOT NULL ^s_enddt STRING NOT NULL ^s_endtime STRING NOT NULL ^s_deleted_flag STRING NOT NULL"
    val systemAuditColSchema = "c_journaltime STRING NOT NULL ^c_transactionid STRING NOT NULL ^c_operationtype STRING NOT NULL ^c_userid STRING NOT NULL"

    val diffenParams: DiffenParams = IntegrationTestDataHelpers.createDiffenParams(id, partition, partition, eodMarker, SourceType.CDC, base, vTypesAuditColSchema, diffenAuditColSchema, systemAuditColSchema,
      "c_journaltimestamp", "yyyy-MM-dd HH:mm:ss:SSSSSS", tableName.split("_")(2), reconTableName = "RECON_TABLE")
      .copy(recordTypeBatch = "Y", timestampCol = "c_journaltime", timestampColFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS")

    IntegrationTestDataHelpers.prepareSampleCDCData(diffenParams, spark, base, tableName)
    val tableDictionary: List[TableConfig] = IntegrationTestDataHelpers.cdcTableConfig(tableName)
      .map { x =>
        import x._
        TableConfig(name, sourceType, keyCols.toLowerCase, colSchema,
          s"select count(*) from ${diffenParams.diffenOpenSchema}.$tableName", deleteIndex, deleteValue, "c_operationtype", "I", "B", "A", Some("incremental"))
      }

    val fs: FileSystem = FileSystemUtils.fetchFileSystem
    SchemaUtils.getOpsSchema(diffenParams).filterNot(_.startsWith("drop")).foreach(spark.sql)
    SchemaUtils.getBusinessTablesSchema(tableDictionary.head, diffenParams).filterNot(_.startsWith("drop")).foreach(spark.sql)
    prepareEodTableData(diffenParams)(spark)
    val pConf = new Configuration()

    //BootstrapProcessor.processDiffen(diffenParams, fs, tableDictionary, new Configuration())(spark)
    val paramConf = new Configuration()
    val rerunTables = tableDictionary.map(_.name)
    BootstrapProcessor.businessDateProcess("2017-01-01", diffenParams, tableDictionary, rerunTables, paramConf)(spark)

    val sqlDiffenOpen1: DataFrame = spark.sql("select count(*) from " + diffenParams.diffenOpenSchema + "." + tableName + " where ods='2017-01-01'")
    assert(sqlDiffenOpen1.take(1).map(_.getLong(0)).head == 4, "Open count mis-match")

    val sqlDiffenNonOpen1: DataFrame = spark.sql(s"select count(*) from ${diffenParams.diffenNonOpenSchema}." + tableName + " where nds='2017-01-01'")
    assert(sqlDiffenNonOpen1.take(1).map(_.getLong(0)).head == 6, " Non open count mis-match")

    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-01' and step_name='DIFFEN OPEN'").take(1).map(_.getLong(0)).head == 4)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-01' and step_name='DIFFEN NONOPEN'").take(1).map(_.getLong(0)).head == 6)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-01' and step_name='BRecordsDiffen'").take(1).map(_.getLong(0)).head == 3)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-01' and step_name='Duplicates'").take(1).map(_.getLong(0)).head == 1)
    assert(spark.sql(s"select markertime, previousbusinessdate from ${diffenParams.opsSchema}.eod_table where businessdate='2017-01-01'").take(1).map(_.getString(0)).head == "2017-01-02 00:00:00")

    //Day 1 - Cascaded verification
    val sqlDiffenOpen2: DataFrame = spark.sql(s"select count(*) from ${diffenParams.diffenOpenSchema}." + tableName + " where ods='2017-01-02'")
    assert(sqlDiffenOpen2.take(1).map(_.getLong(0)).head == 8)

    val sqlDiffenNonOpen2: DataFrame = spark.sql(s"select count(*) from ${diffenParams.diffenNonOpenSchema}." + tableName + " where nds='2017-01-02'")
    assert(sqlDiffenNonOpen2.take(1).map(_.getLong(0)).head == 4)

    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-02' and step_name='DIFFEN OPEN'").take(1).map(_.getLong(0)).head == 8)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-02' and step_name='DIFFEN NONOPEN'").take(1).map(_.getLong(0)).head == 4)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-02' and step_name='BRecordsDiffen'").take(1).map(_.getLong(0)).head == 3)
    assert(spark.sql(s"select markertime, previousbusinessdate from ${diffenParams.opsSchema}.eod_table where businessdate='2017-01-02'").take(1).map(_.getString(0)).head == "2017-01-03 00:00:00")

    //Assert previous day's data again to ensure that we don't overwrite the partition somehow
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-01' and step_name='DIFFEN OPEN'").take(1).map(_.getLong(0)).head == 4)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-01' and step_name='DIFFEN NONOPEN'").take(1).map(_.getLong(0)).head == 6)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-01' and step_name='BRecordsDiffen'").take(1).map(_.getLong(0)).head == 3)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-01' and step_name='Duplicates'").take(1).map(_.getLong(0)).head == 1)
    assert(spark.sql(s"select markertime, previousbusinessdate from ${diffenParams.opsSchema}.eod_table where businessdate='2017-01-01'").take(1).map(_.getString(0)).head == "2017-01-02 00:00:00")

    //Day 2 - Cascaded verification
    val sqlDiffenOpen3: DataFrame = spark.sql(s"select count(*) from ${diffenParams.diffenOpenSchema}." + tableName + " where ods='2017-01-03'")
    assert(sqlDiffenOpen3.take(1).map(_.getLong(0)).head == 9)

    val sqlDiffenNonOpen3: DataFrame = spark.sql(s"select count(*) from ${diffenParams.diffenNonOpenSchema}." + tableName + " where nds='2017-01-03'")
    assert(sqlDiffenNonOpen3.take(1).map(_.getLong(0)).head == 1)

    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-03' and step_name='DIFFEN OPEN'").take(1).map(_.getLong(0)).head == 9)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-03' and step_name='DIFFEN NONOPEN'").take(1).map(_.getLong(0)).head == 1)
    assert(spark.sql(s"select rowcount from ${diffenParams.opsSchema}.gps_all_rowcounts where rcds='2017-01-03' and step_name='BRecordsDiffen'").take(1).map(_.getLong(0)).head == 0)
    assert(spark.sql(s"select markertime, previousbusinessdate from ${diffenParams.opsSchema}.eod_table where businessdate='2017-01-03'").take(1).map(_.getString(0)).head == "2017-01-04 00:00:00")

  }

}
