/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.integration

import java.util.UUID

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.processor.BootstrapProcessor
import com.tdt.diffen.processor.fixtures.{IntegrationTestDataHelpers, DiffenSharedSparkContext}
import com.tdt.diffen.utils.Enums.SourceType
import com.tdt.diffen.utils.{FileSystemUtils, SchemaUtils, DiffenUtils}
import com.tdt.diffen.utils.XmlParser.TableConfig
import com.tdt.diffen.utils.{SchemaUtils, DiffenUtils}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.sql.DataFrame
import org.scalatest.{FlatSpec, Matchers}
import com.tdt.diffen.utils.DiffenConstants._

/** Tests the Transaction source type behavior for NonRecord Type/Flat file based system tables */
class TransactionNonCdcIntegrationTest extends FlatSpec with Matchers with DiffenSharedSparkContext {

  "TxnNonCdcDataDiffenTest" should "createDiffenOpen_NonOpen" in {
      val id = UUID.randomUUID().toString.substring(1, 5)
      val base = "target/batchIngestion/" + id
      val tableName = "gps_all_txnnoncdcdatadiffentest"
      IntegrationTestDataHelpers.prepareSampleNonCDCData(id, spark, base, tableName)
      val tableDictionary: List[TableConfig] = IntegrationTestDataHelpers.nonCdcTableConfigTxn(tableName).map(x => TableConfig(x.name, x.sourceType, x.keyCols, x.colSchema, x.reconQuery, x.deleteIndex, x.deleteValue, "", "", "", "", None))
      val eodMarker = "2017-01-01"
      val businessDay = "2017-01-01"

      val partition: String = DiffenUtils.getBusinessDayPartition(businessDay, "yyyy-MM-dd", "yyyy-mm-dd").get

      val vTypesAuditColSchema = "rowId STRING NOT NULL ^filename STRING NOT NULL ^vds STRING NOT NULL"
      val diffenAuditColSchema = "s_startdt STRING NOT NULL ^s_starttime STRING NOT NULL ^s_enddt STRING NOT NULL ^s_endtime STRING NOT NULL ^s_deleted_flag STRING NOT NULL"
      val systemAuditColSchema = ""

      val diffenParams: DiffenParams = IntegrationTestDataHelpers.createDiffenParams(id, partition, partition, eodMarker, SourceType.BATCH_DELIMITED, base, vTypesAuditColSchema, diffenAuditColSchema, systemAuditColSchema, StartDate, "yyyy-MM-dd", tableName.split("_")(2))
      val fs: FileSystem = FileSystemUtils.fetchFileSystem
      SchemaUtils.getOpsSchema(diffenParams).filterNot(_.startsWith("drop")).foreach(spark.sql)
      SchemaUtils.getBusinessTablesSchema(tableDictionary.head, diffenParams).filterNot(_.startsWith("drop")).foreach(spark.sql)

      BootstrapProcessor.processDiffen(diffenParams, fs, tableDictionary, new Configuration())(spark)
      val sqlDiffenOpen1: DataFrame = spark.sql("select count(*) from " + diffenParams.diffenOpenSchema + "." + tableName + " where ods='2017-01-01'")
      assert(sqlDiffenOpen1.take(1).map(_.getLong(0)).head == 4)
      assert(spark.sql(s"select * from ${diffenParams.opsSchema}.gps_all_rowcounts where  rcds='2017-01-01' and step_name='Duplicates'").take(1).map(_.getLong(3)).head == 5)

      val eodMarker2 = "2017-01-02"
      val businessDay2 = "2017-01-02"

      val partition2: String = DiffenUtils.getBusinessDayPartition(businessDay2, "yyyy-MM-dd", "yyyy-mm-dd").get

      val diffenParams2: DiffenParams = IntegrationTestDataHelpers.createDiffenParams(id, partition2, partition2, eodMarker2, SourceType.BATCH_DELIMITED, base, vTypesAuditColSchema, diffenAuditColSchema, systemAuditColSchema, StartDate, "yyyy-MM-dd", tableName.split("_")(2))

      BootstrapProcessor.processDiffen(diffenParams2, fs, tableDictionary, new Configuration())(spark)
      val sqlDiffenOpen2: DataFrame = spark.sql("select count(*) from " + diffenParams.diffenOpenSchema + "." + tableName + " where ods='2017-01-02'")
      assert(sqlDiffenOpen2.take(1).map(_.getLong(0)).head == 5)
      assert(spark.sql(s"select * from ${diffenParams.opsSchema}.gps_all_rowcounts where  rcds='2017-01-02' and step_name='Duplicates'").take(1).map(_.getLong(3)).head == 4)

      //Assert previous days as well
      val sqlDiffenOpenPrev: DataFrame = spark.sql("select count(*) from " + diffenParams.diffenOpenSchema + "." + tableName + " where ods='2017-01-01'")
      assert(sqlDiffenOpenPrev.take(1).map(_.getLong(0)).head == 4)
      assert(spark.sql(s"select * from ${diffenParams.opsSchema}.gps_all_rowcounts where  rcds='2017-01-01' and step_name='Duplicates'").take(1).map(_.getLong(3)).head == 5)
  }


}