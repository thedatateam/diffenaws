/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.prop

import java.util.UUID

import com.tdt.diffen.processor._
import com.tdt.diffen.processor.fixtures.{DelimitedTableRow, DiffenSharedSparkContext, NonCDCTestFixtures}
import com.tdt.diffen.utils.Enums.{RunType, TableType}
import org.joda.time.DateTime
import org.scalacheck.Prop._
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
import com.tdt.diffen.utils.DiffenConstants._
import org.joda.time.format.DateTimeFormat

/** Tests the Core logic for Non-Record Type/Non-CDC based master/delta tables for Full dump/snapshot loads */
class NonRecordTypeMasterFullDumpTest extends FunSuite with DiffenSharedSparkContext with Checkers with NonCDCTestFixtures {

  val dataCols = List("drReason", "drReasonE", "drReasonC", "drKind", "drDrrKind")
  val diffenAuditCols = List(StartDate, StartTime, EndDate, EndTime, DeleteFlag)
  val vTypesAuditCols = List[String]("rowId", "vds", "filename")
  val metaInfo = NonRecordTypeMetaInfo(timestampColName = StartTime, timestampColFormat = "yyyy-MM-dd HH:mm:ss", vTypesAuditCols, diffenAuditCols, List[String]())
  val januaryFirst = new DateTime(2017, 1, 1, 10, 0)
  val december31st = new DateTime(2016, 12, 31, 10, 0)

  test("NonCDC Master FullDump Test") {

    spark.sparkContext.setLogLevel("ERROR")

    val todaysVerifyTypesListGen = DelimitedDataGenerators.createGeneratorWithoutRecordType(januaryFirst)

    val previousDiffenListGen = DelimitedDataGenerators.createGeneratorWithoutRecordType(december31st)
    check {
      forAll(todaysVerifyTypesListGen, previousDiffenListGen) { (verifyTypesList, previousDiffenList) =>
        getAllAssertions(verifyTypesList, previousDiffenList)
      }

    }
  }

  /** Encapsulates all the assertions for this testcase
    *
    * @param unusedVTypes List of DelimitedTableRowWithRecordType that composes Verify types
    * @param previousDiffenList List of DelimitedTableRowWithRecordType that composes the Previous DIFFEN
    * @return Consolidated assertion of all properties
    */
  def getAllAssertions(unusedVTypes: List[DelimitedTableRow], previousDiffenList: List[DelimitedTableRow]) = {

    implicit val sqlContext = spark.sqlContext

    //FIXME Figure out a way to add delete to a product in the middle of the record set instead of adding D records at the end of Verify Types
    val singleInstancePerKeyList = previousDiffenList
      .groupBy(_.drReason) //have only one instance per primarykey
      .map(_._2.head)
      .toList

    val previousDiffenSingleInstancePerKey =
      if (singleInstancePerKeyList.nonEmpty)
        singleInstancePerKeyList ++
          List(singleInstancePerKeyList.last.copy(rowId = s"${System.nanoTime()}_${UUID.randomUUID().toString}", drReason = "DELREC", s_startdt = dateFormat.print(december31st))) else List()

    //Simulating a previous DIFFEN
    val originalVerifyTypes = singleInstancePerKeyList.map(each => each.copy(rowId = s"${System.nanoTime()}_${UUID.randomUUID().toString}", s_startdt = dateFormat.print(januaryFirst)))
    val updatedRecord = originalVerifyTypes.head.copy(drReasonE = "Updated Record")
    val updatedVTypes = updatedRecord :: originalVerifyTypes.tail

    val allData = previousDiffenSingleInstancePerKey.union(updatedVTypes)

    val deletedPkList = previousDiffenSingleInstancePerKey.filter(_.drReason=="DELREC")
    val deletedPkRowsList = deletedPkList.map(_.rowId)

    val withoutDeletedPkRowsList = allData.filterNot(row => deletedPkRowsList.contains(row.rowId)).groupBy(_.drReason).map(_._2.head).toList

    val diffenOpenDataList = withoutDeletedPkRowsList
      .groupBy(row => row.drReason)
      .map { case (_, listRow) => listRow.last }
      .groupBy(_.drReason)
      .mapValues(groupList => groupList.maxBy(delimRow => delimRow.rowId))
      .values.toSet

    import sqlContext.implicits._

    val todaysVerifyTypesDF = spark.sparkContext.parallelize(updatedVTypes).toDF
    val previousDiffenDF = spark.sparkContext.parallelize(previousDiffenSingleInstancePerKey).toDF

    println("###################################### TODAY'S VERIFY TYPES ######################################")
    todaysVerifyTypesDF.show(100, false)
    println("######################################     PREVIOUS DIFFEN     ######################################")
    previousDiffenDF.show(false)

    val tableName = "DUMMY_TABLENAME"

    val processor = new NonRecordTypeBasedProcessor

    //Ref : Note that runType - fulldump/incremental doesnt make sense for transactional assertions
    val DiffenOutput(diffenOpen, diffenNonOpen, duplicatesOpt, prevDiffenVsVtypesDuplicates, _) =
      processor.processDiffen(tableName, dataCols, List("drReason"), todaysVerifyTypesDF, previousDiffenDF, metaInfo, TableType.DELTA, RunType.FULLDUMP, januaryFirst,  DateTimeFormat.forPattern("yyyy_MM_dd_HH"))(spark)


    println("######################################       DIFFEN OPEN       ######################################")
    diffenOpen.show(false)
    println("######################################     DIFFEN NON OPEN     ######################################")
    diffenNonOpen.show(false)

    val diffenOpenRowIds = diffenOpen.select("rowId").collect().map(row => row.get(0).toString).toSet
    val diffenNonOpenRowIds = diffenNonOpen.select("rowId").collect().map(row => row.get(0).toString).toSet
    val duplicatesRowIds = prevDiffenVsVtypesDuplicates match {
      case Some(df) =>
        println("###################################### DUPLICATE ######################################")
        df.show(false)
        df.select("rowId").collect().map(row => row.get(0).toString).toSet
      case None => Set[String]()
    }

    //DIFFEN OPEN = latestInVerifyTypes + PrevDIFFEN data which is not updated through verify types - D Records
    val diffenOpenCountCheck = diffenOpen.count() === diffenOpenDataList.size

    //DIFFEN NON_OPEN = prevDIFFEN Data which is updated through today's VTypes + Changes during the day in Vtypes + D records - bRecords
    val diffenNonOpenCountCheck = diffenNonOpen.count() === (1 +  deletedPkRowsList.size) //Updated record + Deleted records Pk records

    //Actual record checks
    val diffenOpenRowIdCheck = diffenOpenRowIds === diffenOpenDataList.map(_.rowId)

    val diffenNonOpenRowIdCheck = diffenNonOpenRowIds === deletedPkRowsList.toSet

    val prevDiffenVsVtypesDuplicateCheck = prevDiffenVsVtypesDuplicates.map(_.count()).getOrElse(0) === duplicatesRowIds.size

    println(s"diffenOpenCountCheck: $diffenOpenCountCheck   -> ${diffenOpen.count()} === ${diffenOpenDataList.size}")
    println(s"diffenNonOpenCountCheck: $diffenNonOpenCountCheck   -> ${diffenNonOpen.count()} === ${1 + deletedPkRowsList.size}")
    println(s"diffenOpenRowIdCheck: $diffenOpenRowIdCheck")
    println(s"diffenNonOpenRowIdCheck: $diffenNonOpenRowIdCheck")
    println(s"Duplicate check :  $prevDiffenVsVtypesDuplicateCheck -> ${prevDiffenVsVtypesDuplicates.map(_.count()).getOrElse(0)} === ${duplicatesRowIds.size}")

    diffenOpenCountCheck && diffenNonOpenCountCheck && diffenOpenRowIdCheck && diffenNonOpenRowIdCheck && prevDiffenVsVtypesDuplicateCheck
  }
}