/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.fixtures

import java.io.File

import org.apache.hadoop.fs.Path
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfterAll, Suite}

/** A minor extension of `SharedSparkContext` from the spark testing library to set parallelism */
trait DiffenSharedSparkContext extends BeforeAndAfterAll {
  self: Suite =>

  @transient private var _spark: SparkSession = _

  def spark: SparkSession = _spark

  val isWindows: Boolean = System.getProperty("os.name").contains("Windows")
  val windowBinariesLocation: String = "hadoop-2.7.2_winbinariesx64"

  if (isWindows) {
    val hadoopHome = new File(windowBinariesLocation).getAbsolutePath
    System.getProperties.put("hadoop.home.dir", hadoopHome)
    System.getProperties.put("java.library.path", hadoopHome + Path.SEPARATOR + "bin")
    System.getProperties.put("hadoop.tmp.dir", "target/tmp/hadoop")
    System.getProperties.put("java.io.tmpdir", "target/tmp/local")

    new File(windowBinariesLocation + "/bin")
      .listFiles
      .filter(_.getName.endsWith(".dll"))
      .foreach(file => System.load(file.getAbsolutePath))
  }
  System.getProperties.put("dfs.permissions.enabled", "false")

  val sparkConf = new SparkConf()
    .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    .set("mapreduce.fileoutputcommitter.algorithm.version", "2")
    .set("mapreduce.fileoutputcommitter.marksuccessfuljobs", "false")
    .set("spark.sql.autoBroadcastJoinThreshold", "-1")
    .set("spark.driver.extraJavaOptions", "-XX:+UseG1GC")
    .set("spark.executor.extraJavaOptions", "--XX:+UseG1GC")
    .set("spark.scheduler.mode", "FAIR")
    .set("spark.sql.shuffle.partitions", "1")
    .set("spark.default.parallelism", "1")
    .set("hive.exec.scratchdir", "target/tmp/hive")


  override def beforeAll() {
    _spark = SparkSession.builder()
      .appName("Test Context")
      .enableHiveSupport()
      .config(sparkConf)
      .master("local[*]")
      .getOrCreate()

    _spark.sparkContext.setCheckpointDir("target/tmp/checkpoint")

    _spark.sparkContext.setLogLevel("ERROR")
  }
}
