/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.prop

import java.util.UUID

import com.tdt.diffen.processor._
import com.tdt.diffen.processor.fixtures.{DelimitedTableRow, DiffenSharedSparkContext, NonCDCTestFixtures}
import com.tdt.diffen.utils.Enums.{RunType, TableType}
import org.joda.time.DateTime
import org.scalacheck.Prop._
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
import com.tdt.diffen.utils.DiffenConstants._
import org.joda.time.format.DateTimeFormat

class NonRecordTypeTransactionTest extends FunSuite with DiffenSharedSparkContext with Checkers with NonCDCTestFixtures {

  val dataCols = List("drReason", "drReasonE", "drReasonC", "drKind", "drDrrKind")
  val diffenAuditCols = List(StartDate, StartTime, EndDate, EndTime, DeleteFlag)
  val vTypesAuditCols = List[String]("rowId", "vds", "filename")
  val metaInfo = NonRecordTypeMetaInfo(timestampColName = StartTime, timestampColFormat = "yyyy-MM-dd HH:mm:ss", vTypesAuditCols, diffenAuditCols, List[String]())

  val januaryFirst = new DateTime(2017, 1, 1, 10, 0)
  val december31st = new DateTime(2016, 12, 31, 10, 0)

  test("Non RecordType Transaction Test") {

    spark.sparkContext.setLogLevel("ERROR")

    val todaysVerifyTypesListGen = DelimitedDataGenerators.createGeneratorWithoutRecordType(januaryFirst)

    val previousDiffenListGen = DelimitedDataGenerators.createGeneratorWithoutRecordType(december31st)
    check {
      forAllNoShrink(todaysVerifyTypesListGen, previousDiffenListGen) { (verifyTypesList, unusedPrevDiffen) =>
        getAllAssertions(verifyTypesList, unusedPrevDiffen)
      }
    }
  }

  def getAllAssertions(verifyTypesList: List[DelimitedTableRow], unusedPrevDiffen: List[DelimitedTableRow]) = {

    implicit val sqlContext = spark.sqlContext

    val duplicates = verifyTypesList.last.copy(rowId = s"${System.nanoTime()}_${UUID.randomUUID().toString}")

    val verifyTypesWithDups = verifyTypesList ++ List(duplicates)

    import sqlContext.implicits._

    val todaysVerifyTypesDF = spark.sparkContext.parallelize(verifyTypesWithDups).toDF

    println("###################################### TODAY'S VERIFY TYPES ######################################")
    todaysVerifyTypesDF.show(100, false)

    val errorRecord = DelimitedTableRow("ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR")
    val previousDiffenDFDUMMY = spark.sparkContext.parallelize(List(errorRecord)).toDF()
    val tableName = "DUMMY_TABLENAME"

    val processor = new NonRecordTypeBasedProcessor

    //TODO Duplicated across Testcase and assertions
    val DiffenOutput(diffenOpen, diffenNonOpen, duplicatesOpt, _, _) = processor.processDiffen(tableName, dataCols, List("drReason", "rowId"), todaysVerifyTypesDF,
      previousDiffenDFDUMMY, metaInfo, TableType.TXN, RunType.INCREMENTAL, januaryFirst, DateTimeFormat.forPattern("yyyy_MM_dd_HH"))(spark)

    println("######################################       DIFFEN OPEN       ######################################")
    diffenOpen.show(false)
    println("######################################     DIFFEN NON OPEN     ######################################")
    diffenNonOpen.show(false)
    println("######################################       DUPLICATES       ######################################")
    duplicatesOpt.show(false)

    val diffenOpenRowIds = diffenOpen.select("rowId").collect().map(row => row.get(0).toString).toSet
    val diffenNonOpenRowIds = diffenNonOpen.select("rowId").collect().map(row => row.get(0).toString).toSet

    val duplicatesRowIds = duplicatesOpt.select("rowId").collect().map(row => row.get(0).toString).toSet

    //DIFFEN OPEN = latestInVerifyTypes + PrevDIFFEN data which is not updated through verify types - D Records
    val diffenOpenCountCheck = diffenOpen.count() === (verifyTypesWithDups.size - duplicatesRowIds.size)

    //DIFFEN NON_OPEN = prevDIFFEN Data which is updated through today's VTypes + Changes during the day in Vtypes + D records - bRecords
    val diffenNonOpenCountCheck = diffenNonOpen.count() == 0

    //Actual record checks
    val diffenOpenRowIdCheck = diffenOpenRowIds === verifyTypesWithDups.map(_.rowId).toSet.diff(duplicatesRowIds)

    val diffenNonOpenRowIdCheck = diffenNonOpenRowIds.isEmpty

    println(s"diffenOpenCountCheck: $diffenOpenCountCheck")
    println(s"diffenNonOpenCountCheck: $diffenNonOpenCountCheck")
    println(s"diffenOpenRowIdCheck: $diffenOpenRowIdCheck")
    println(s"diffenNonOpenRowIdCheck: $diffenNonOpenRowIdCheck")

    diffenOpenCountCheck && diffenNonOpenCountCheck && diffenOpenRowIdCheck && diffenNonOpenRowIdCheck
  }
}