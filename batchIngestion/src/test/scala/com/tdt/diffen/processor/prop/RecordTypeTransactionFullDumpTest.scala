/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.prop

import java.util.UUID

import com.tdt.diffen.processor._
import com.tdt.diffen.processor.fixtures.{DelimitedTableRowWithRecordType, DiffenSharedSparkContext, NonCDCTestFixtures}
import com.tdt.diffen.utils.Enums.{RunType, TableType}
import org.apache.commons.lang3.StringUtils
import org.joda.time.DateTime
import org.scalacheck.Prop._
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
import com.tdt.diffen.utils.DiffenConstants._
import org.joda.time.format.DateTimeFormat

/** Tests the IABD (Insert, After, Before, Delete) or equivalent core logic for Record Type based transaction tables */
class RecordTypeTransactionFullDumpTest extends FunSuite with DiffenSharedSparkContext with Checkers with NonCDCTestFixtures {

  val dataCols = List("recordType", "drReason", "drReasonE", "drReasonC", "drKind", "drDrrKind")
  val januaryFirst = new DateTime(2017, 1, 1, 10, 0)
  val december31st = new DateTime(2016, 12, 31, 10, 0)

  val vTypesAuditCols = List[String]("rowId", "vds", "filename")
  val diffenAuditCols = List(StartDate, StartTime, EndDate, EndTime, DeleteFlag)
  val systemAuditCols = List()

  val metaInfo = RecordTypeMetaInfo(timestampColName = StartDate, timestampColFormat = "yyyy-MM-dd", operationTypeColName = "recordType",
    insertType = "D", beforeType = "", afterType = "", deleteType = "X", vTypesAuditCols, diffenAuditCols, systemAuditCols)

  test("RecordType Transaction Test") {

    spark.sparkContext.setLogLevel("ERROR")

    val (todaysVerifyTypesListGen, deletablesGen) = DelimitedDataGenerators.createGeneratorWithRecordType(januaryFirst, metaInfo)

    val (previousDiffenListGen, _) = DelimitedDataGenerators.createGeneratorWithRecordType(december31st, metaInfo)

    check {
      forAllNoShrink(todaysVerifyTypesListGen, previousDiffenListGen, deletablesGen) { (verifyTypesList, previousDiffenList, deletables) =>
        getAllAssertions(verifyTypesList, previousDiffenList, deletables)
      }

    }
  }


  /** Encapsulates all the assertions for this testcase
    *
    * @param verifyTypesList List of DelimitedTableRowWithRecordType that composes Verify types
    * @param previousDiffenList List of DelimitedTableRowWithRecordType that composes the Previous DIFFEN
    * @param mayBeDeletables List of DelimitedTableRowWithRecordType that ''may be'' a part of ''Delete'' records.  The reason why they are ''may be'' is because the Delete records are generated randomly, there is a possibility that
    *                        there may be ''Delete'' records for a primary key which aren't on PreviousDIFFEN or Verify types
    * @return Consolidated assertion of all properties
    */
  def getAllAssertions(verifyTypesList: List[DelimitedTableRowWithRecordType], previousDiffenList: List[DelimitedTableRowWithRecordType], mayBeDeletables: List[DelimitedTableRowWithRecordType]) = {

    implicit val sqlContext = spark.sqlContext

    val duplicates = verifyTypesList.last.copy(rowId = s"${System.nanoTime()}_${UUID.randomUUID().toString}")

    val verifyTypesWithDeletes = (verifyTypesList ++ List(duplicates))

    //logDatasetForDebugging(verifyTypesWithDeletes, previousDiffenSingleInstancePerKey)

    import sqlContext.implicits._

    val todaysVerifyTypesDF = spark.sparkContext.parallelize(verifyTypesWithDeletes).toDF

    println("###################################### TODAY'S VERIFY TYPES ######################################")
    todaysVerifyTypesDF.show(100, false)

    val errorRecord = DelimitedTableRowWithRecordType("ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR", "ERROR")
    val previousDiffenDFDUMMY = spark.sparkContext.parallelize(List(errorRecord)).toDF()
    val tableName = "DUMMY_TABLENAME"

    val processor = new RecordTypeBasedProcessor

    //TODO Duplicated across Testcase and assertions
    val DiffenOutput(diffenOpen, diffenNonOpen, duplicatesOpt, _, bRecordsOpt) = processor.processDiffen(tableName, dataCols, List("drReason"), todaysVerifyTypesDF, previousDiffenDFDUMMY, metaInfo, TableType.TXN, RunType.FULLDUMP, januaryFirst, DateTimeFormat.forPattern("yyyy-MM-dd"))(spark)

    println("######################################       DIFFEN OPEN       ######################################")
    diffenOpen.show(false)
    println("######################################     DIFFEN NON OPEN     ######################################")
    diffenNonOpen.show(false)
    println("######################################      DUPLICATES      ######################################")
    duplicatesOpt.show(false)

    val diffenOpenRowIds = diffenOpen.select("rowId").collect().map(row => row.get(0).toString).toSet
    val diffenNonOpenRowIds = diffenNonOpen.select("rowId").collect().map(row => row.get(0).toString).toSet

    val duplicatesRowIds = duplicatesOpt.select("rowId").collect().map(row => row.get(0).toString).toSet

    val updatedVerifyTypesSet = verifyTypesWithDeletes.toSet

    val latestPerKeyInVTypes = verifyTypesWithDeletes
      .groupBy(_.drReason)
      .mapValues(groupList => groupList.maxBy(delimRow => StringUtils.substringBefore(delimRow.rowId, "_").toLong))
      .values.toSet

    val duplicatesSet = verifyTypesWithDeletes.toSet.diff(latestPerKeyInVTypes)

    //DIFFEN OPEN = latestInVerifyTypes + PrevDIFFEN data which is not updated through verify types - D Records
    val diffenOpenCountCheck = diffenOpen.count() === latestPerKeyInVTypes.size

    //DIFFEN NON_OPEN = prevDIFFEN Data which is updated through today's VTypes + Changes during the day in Vtypes + D records - bRecords
    //val diffenNonOpenCountCheck = diffenNonOpen.count() === oldUpdatesDuringTheDay.size + deletables.size - duplicatesRowIds.size
    val diffenNonOpenCountCheck = diffenNonOpen.count() === 0

    //Actual record checks
    val diffenOpenRowIdCheck = diffenOpenRowIds === latestPerKeyInVTypes.map(_.rowId)

    //FIXME I am using the diffenoutput's duplicates row id for assertion. this is incorrect. should be using our calculated duplicates
    //val diffenNonOpenRowIdCheck = diffenNonOpenRowIds === (oldUpdatesDuringTheDay.map(_.rowId) ++ rowsWithDeletablesPK.map(_.rowId)).diff(duplicatesRowIds)
    val diffenNonOpenRowIdCheck = diffenNonOpenRowIds.isEmpty

    val duplicateCheck = duplicatesSet.size === duplicatesRowIds.size
    val duplicateRowIdCheck = duplicatesSet.map(_.rowId) === duplicatesRowIds


    println(s"diffenOpenCountCheck: $diffenOpenCountCheck")
    println(s"diffenNonOpenCountCheck: $diffenNonOpenCountCheck")
    println(s"diffenOpenRowIdCheck: $diffenOpenRowIdCheck")
    println(s"diffenNonOpenRowIdCheck: $diffenNonOpenRowIdCheck")
    println(s"duplicateCheck: $duplicateCheck")
    println(s"duplicateRowIdCheck: $duplicateRowIdCheck")

    diffenOpenCountCheck && diffenNonOpenCountCheck && diffenOpenRowIdCheck && diffenNonOpenRowIdCheck && duplicateCheck && duplicateRowIdCheck
  }
}