/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.prop

import java.util.UUID

import com.tdt.diffen.processor._
import com.tdt.diffen.processor.fixtures.{DelimitedTableRowWithRecordType, DiffenSharedSparkContext, NonCDCTestFixtures}
import com.tdt.diffen.utils.Enums.{RunType, TableType}
import org.joda.time.DateTime
import org.scalacheck.Prop._
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
import com.tdt.diffen.utils.DiffenConstants._
import org.joda.time.format.DateTimeFormat

/** Tests the IABD (Insert, After, Before, Delete) or equivalent core logic for Record Type based delta tables for Full dump/snapshot loads */
class RecordTypeMasterFullDumpTest extends FunSuite with DiffenSharedSparkContext with Checkers with NonCDCTestFixtures {

  val colList = List("recordType", "drReason", "drReasonE", "drReasonC", "drKind", "drDrrKind")
  val januaryFirst = new DateTime(2017, 1, 1, 10, 0)
  val december31st = new DateTime(2016, 12, 31, 10, 0)

  val vTypesAuditCols = List[String]("rowId", "vds", "filename")
  val diffenAuditCols = List(StartDate, StartTime, EndDate, EndTime, DeleteFlag)
  val systemAuditCols = List()

  val metaInfo = RecordTypeMetaInfo(timestampColName = StartDate, timestampColFormat = "yyyy-MM-dd HH:mm:ss", operationTypeColName = "recordType",
    insertType = "D", beforeType = "", afterType = "", deleteType = "X", vTypesAuditCols, diffenAuditCols, systemAuditCols)

  test("NonCDC RecordType Master FullDump Test") {

    spark.sparkContext.setLogLevel("ERROR")

    val (todaysVerifyTypesListGen, deletablesGen) = DelimitedDataGenerators.createGeneratorWithRecordType(januaryFirst, metaInfo)

    val (previousDiffenListGen, _) = DelimitedDataGenerators.createGeneratorWithRecordType(december31st, metaInfo)
    check {
      forAllNoShrink(todaysVerifyTypesListGen, previousDiffenListGen, deletablesGen) { (verifyTypesList, previousDiffenList, deletables) =>
        getAllAssertions(verifyTypesList, previousDiffenList, deletables)
      }
    }
  }

  /** Encapsulates all the assertions for this testcase
    *
    * @param unUsed List of DelimitedTableRowWithRecordType that composes Verify types
    * @param previousDiffenList List of DelimitedTableRowWithRecordType that composes the Previous DIFFEN
    * @param mayBeDeletables List of DelimitedTableRowWithRecordType that ''may be'' a part of ''Delete'' records.  The reason why they are ''may be'' is because the Delete records are generated randomly, there is a possibility that
    *                        there may be ''Delete'' records for a primary key which aren't on PreviousDIFFEN or Verify types
    * @return Consolidated assertion of all properties
    */
  def getAllAssertions(unUsed: List[DelimitedTableRowWithRecordType], previousDiffenList: List[DelimitedTableRowWithRecordType], mayBeDeletables: List[DelimitedTableRowWithRecordType]) = {

    implicit val sqlContext = spark.sqlContext

    //FIXME Figure out a way to add delete to a product in the middle of the record set instead of adding D records at the end of Verify Types
    val singleInstancePerKeyList = previousDiffenList
      .groupBy(_.drReason) //have only one instance per primarykey
      .map(_._2.head)
      .toList

    val previousDiffenSingleInstancePerKey = if (singleInstancePerKeyList.nonEmpty) singleInstancePerKeyList ++ List(singleInstancePerKeyList.last.copy(rowId = s"${System.nanoTime()}_${UUID.randomUUID().toString}", drReason = "DELREC", s_startdt = dateFormat.print(december31st))) else List()

    //Simulating a previous DIFFEN
    val verifyTypes = singleInstancePerKeyList.map(each => each.copy(rowId = s"${System.nanoTime()}_${UUID.randomUUID().toString}", s_startdt = dateFormat.print(januaryFirst)))

    val allData = previousDiffenSingleInstancePerKey.union(verifyTypes)
    val deletedPkList = previousDiffenSingleInstancePerKey.filterNot(row => verifyTypes.map(_.drReason).contains(row.drReason))
    val deletedPkRowsList = previousDiffenSingleInstancePerKey.filter(row => deletedPkList.map(_.drReason).contains(row.drReason))
    val diffenOpenDataList = allData.filterNot(row => deletedPkRowsList.map(_.drReason).contains(row.drReason)).groupBy(_.drReason).map(_._2.head).toList

    //logDatasetForDebugging(verifyTypes, previousDiffenSingleInstancePerKey)

    import sqlContext.implicits._

    val todaysVerifyTypesDF = spark.sparkContext.parallelize(verifyTypes).toDF
    val previousDiffenDF = spark.sparkContext.parallelize(previousDiffenSingleInstancePerKey).toDF

    println("###################################### TODAY'S VERIFY TYPES ######################################")
    todaysVerifyTypesDF.show(100, false)
    println("######################################     PREVIOUS DIFFEN     ######################################")
    previousDiffenDF.show(false)
    val tableName = "DUMMY_TABLENAME"

    val processor = new RecordTypeBasedProcessor

    //Ref : Note that runType - fulldump/incremental doesnt make sense for transactional assertions
    val DiffenOutput(diffenOpen, diffenNonOpen, duplicatesOpt, prevDiffenVsVtypesDuplicates, bRecordsOpt) =
      processor.processDiffen(tableName, colList, List("drReason"), todaysVerifyTypesDF, previousDiffenDF, metaInfo, TableType.DELTA, RunType.FULLDUMP, januaryFirst, DateTimeFormat.forPattern("yyyy-MM-dd"))(spark)


    println("######################################       DIFFEN OPEN       ######################################")
    diffenOpen.show(false)
    println("######################################     DIFFEN NON OPEN     ######################################")
    diffenNonOpen.show(false)

    val diffenOpenRowIds = diffenOpen.select("rowId").collect().map(row => row.get(0).toString).toSet
    val diffenNonOpenRowIds = diffenNonOpen.select("rowId").collect().map(row => row.get(0).toString).toSet
    val duplicatesRowIds = prevDiffenVsVtypesDuplicates match {
      case Some(df) =>
        println("###################################### DUPLICATE ######################################")
        df.show(false)
        df.select("rowId").collect().map(row => row.get(0).toString).toSet
      case None => Set[String]()
    }

    //DIFFEN OPEN = latestInVerifyTypes + PrevDIFFEN data which is not updated through verify types - D Records
    val diffenOpenCountCheck = diffenOpen.count() === diffenOpenDataList.size

    //DIFFEN NON_OPEN = prevDIFFEN Data which is updated through today's VTypes + Changes during the day in Vtypes + D records - bRecords
    val diffenNonOpenCountCheck = diffenNonOpen.count() === deletedPkRowsList.size

    //Actual record checks
    val diffenOpenRowIdCheck = diffenOpenRowIds === diffenOpenDataList.map(_.rowId).toSet

    val diffenNonOpenRowIdCheck = diffenNonOpenRowIds === deletedPkRowsList.map(_.rowId).toSet

    val prevDiffenVsVtypesDuplicateCheck = prevDiffenVsVtypesDuplicates.map(_.count()).getOrElse(0) === duplicatesRowIds.size

    println(s"diffenOpenCountCheck: $diffenOpenCountCheck   -> ${diffenOpen.count()} === ${diffenOpenDataList.size}")
    println(s"diffenNonOpenCountCheck: $diffenNonOpenCountCheck   -> ${diffenNonOpen.count()} === ${deletedPkRowsList.size}")
    println(s"diffenOpenRowIdCheck: $diffenOpenRowIdCheck")
    println(s"diffenNonOpenRowIdCheck: $diffenNonOpenRowIdCheck")
    println(s"Duplicate check :  $prevDiffenVsVtypesDuplicateCheck -> ${prevDiffenVsVtypesDuplicates.map(_.count()).getOrElse(0)} === ${duplicatesRowIds.size}")

    diffenOpenCountCheck && diffenNonOpenCountCheck && diffenOpenRowIdCheck && diffenNonOpenRowIdCheck && prevDiffenVsVtypesDuplicateCheck
  }

  def logDatasetForDebugging(updatedVerifyTypesList: List[DelimitedTableRowWithRecordType], previousDiffenList: List[DelimitedTableRowWithRecordType]) = {
    println("###################################### VERIFY TYPES (DEBUG) ######################################")
    updatedVerifyTypesList.foreach(printDelimitedTableRow)
    println()
    println("###################################### PREVIOUS DIFFEN (DEBUG) ######################################")
    previousDiffenList.foreach(printDelimitedTableRow)
    println()
  }

  def printDelimitedTableRow(each: DelimitedTableRowWithRecordType) = {
    println(
      s"""DelimitedTableRowWithRecordType("${each.rowId}", "${each.filename}", "${each.recordType}", "${each.drReason}", "${each.drReasonE}", "${each.drReasonC}", "${each.drKind}", "${each.drDrrKind}", "${each.s_startdt}", "${each.s_starttime}", "${each.s_enddt}", "${each.s_endtime}", "${each.s_deleted_flag}"),""")
  }
}