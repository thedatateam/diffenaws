/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.integration

import java.util.UUID

import com.tdt.diffen.models.DiffenParams
import com.tdt.diffen.processor.BootstrapProcessor
import com.tdt.diffen.processor.fixtures.{IntegrationTestDataHelpers, DiffenSharedSparkContext}
import com.tdt.diffen.utils.Enums.SourceType
import com.tdt.diffen.utils.{FileSystemUtils, SchemaUtils, DiffenUtils}
import com.tdt.diffen.utils.XmlParser.TableConfig
import com.tdt.diffen.utils.{SchemaUtils, DiffenUtils}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.scalatest.{FlatSpec, Matchers}

/** Tests the Transaction source type behavior for CDC/Record Type based tables */

class TransactionCDCIntegrationTest extends FlatSpec with Matchers with DiffenSharedSparkContext {
  "TxnCdcDataDiffenTest" should "createDiffenOpen_NonOpen" in {
      val id = UUID.randomUUID().toString.substring(1, 5)
      val base = "target/batchIngestion/" + id
      val tableName = "gps_all_txncdcdatadiffentest"

      val eodMarker = "2017-01-02 00:00:00"
      val businessDay = "2017-01-01 00:00:00"
      val partition: String = DiffenUtils.getBusinessDayPartition(businessDay, "yyyy-MM-dd HH:mm:ss", "yyyy-mm-dd").get
      val vTypesAuditColSchema = "rowId STRING NOT NULL ^filename STRING NOT NULL ^vds STRING NOT NULL"
      val diffenAuditColSchema = "s_startdt STRING NOT NULL ^s_starttime STRING NOT NULL ^s_enddt STRING NOT NULL ^s_endtime STRING NOT NULL ^s_deleted_flag STRING NOT NULL"
      val systemAuditColSchema = "c_journaltime STRING NOT NULL ^c_transactionid STRING NOT NULL ^c_operationtype STRING NOT NULL ^c_userid STRING NOT NULL"


      val diffenParams: DiffenParams = IntegrationTestDataHelpers.createDiffenParams(id, partition, partition, eodMarker, SourceType.CDC, base, vTypesAuditColSchema, diffenAuditColSchema, systemAuditColSchema,
        "c_journaltimestamp", "yyyy-MM-dd HH:mm:ss:SSSSSS", tableName.split("_")(2), reconTableName = "RECON_TABLE")
        .copy(recordTypeBatch = "Y", timestampCol = "c_journaltime", timestampColFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS")

      val tableDictionary: List[TableConfig] = IntegrationTestDataHelpers.cdcTableConfig(tableName)
        .map { x =>
          import x._
          TableConfig(name, "txn", keyCols.toLowerCase, colSchema,
            s"select count(*) from ${diffenParams.diffenOpenSchema}.$tableName", deleteIndex, deleteValue, "c_operationtype", "I", "B", "A", Some("incremental"))
        }


      IntegrationTestDataHelpers.prepareSampleCDCData(diffenParams, spark, base, tableName)

      val fs: FileSystem = FileSystemUtils.fetchFileSystem
      SchemaUtils.getOpsSchema(diffenParams).filterNot(_.startsWith("drop")).foreach(spark.sql)
      SchemaUtils.getBusinessTablesSchema(tableDictionary.head, diffenParams).filterNot(_.startsWith("drop")).foreach(spark.sql)

      BootstrapProcessor.processDiffen(diffenParams, fs, tableDictionary, new Configuration())(spark)


      spark.sql("select count(*) from " + diffenParams.diffenOpenSchema + "." + tableName + " where ods='2017-01-01'").take(1).map(_.getLong(0)).head should be(4)
      //spark.sql("select count(*) from " + diffenParams.diffenNonOpenSchema + "." + tableName + " where nds='2017-01-01'").take(1).map(_.getLong(0)).head should be(0)

      val eodMarker2 = "2017-01-03 00:00:00"
      val businessDay2 = "2017-01-02 00:00:00"

      val partition2: String = DiffenUtils.getBusinessDayPartition(businessDay2, "yyyy-MM-dd HH:mm:ss", "yyyy-mm-dd").get

      val diffenParams2: DiffenParams = IntegrationTestDataHelpers.createDiffenParams(id, partition2, partition2, eodMarker2, SourceType.CDC, base, vTypesAuditColSchema, diffenAuditColSchema, systemAuditColSchema,
        "c_journaltimestamp", "yyyy-MM-dd HH:mm:ss:SSSSSS", tableName.split("_")(2), reconTableName = "RECON_TABLE")
        .copy(recordTypeBatch = "Y", timestampCol = "c_journaltime", timestampColFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS")

      BootstrapProcessor.processDiffen(diffenParams2, fs, tableDictionary, new Configuration())(spark)

      spark.sql("select count(*) from " + diffenParams.diffenOpenSchema + "." + tableName + " where ods='2017-01-02'").take(1).map(_.getLong(0)).head should be(4)
      //spark.sql("select count(*) from " + diffenParams.diffenNonOpenSchema + "." + tableName + " where nds='2017-01-02'").take(1).map(_.getLong(0)).head should be(0)

      //Assert previous day as well
      spark.sql("select count(*) from " + diffenParams.diffenOpenSchema + "." + tableName + " where ods='2017-01-01'").take(1).map(_.getLong(0)).head should be(4)
  }

}
