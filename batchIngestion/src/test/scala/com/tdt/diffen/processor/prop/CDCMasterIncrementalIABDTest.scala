/*
 * Copyright 2019 The Data Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tdt.diffen.processor.prop

import com.tdt.diffen.processor.fixtures.{CDCDebugLogger, CDCTableRow, CDCTestFixtures, DiffenSharedSparkContext}
import com.tdt.diffen.processor.{DiffenOutput, RecordTypeBasedProcessor, RecordTypeMetaInfo}
import com.tdt.diffen.utils.Enums.{RunType, TableType}
import org.joda.time.{DateTime, Period}
import org.scalacheck.Prop._
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
import com.tdt.diffen.utils.DiffenConstants._
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}

/** Tests the IABD (Insert, After, Before, Delete) core logic for CDC based master tables */
class CDCMasterIncrementalIABDTest extends FunSuite with DiffenSharedSparkContext with Checkers with CDCTestFixtures with CDCDebugLogger {

  val dataCols = List("productId", "currencyVal", "description", "price")
  val vTypesAuditCols = List("rowId", "filename", "vds")
  val diffenAuditCols = List(StartDate, StartTime, EndDate, EndTime, DeleteFlag)
  val systemAuditCols = List("c_journaltime", "c_transactionid", "c_operationtype", "c_userid")
  val januaryFirst = new DateTime(2017, 1, 1, 10, 0)
  val december31st = new DateTime(2016, 12, 31, 10, 0)

  override val recordTypeMeta = RecordTypeMetaInfo(timestampColName = "c_journaltime", timestampColFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS",
    operationTypeColName = "c_operationtype", insertType = "I", beforeType = "B", afterType = "A", deleteType = "D", vTypesAuditCols, diffenAuditCols, systemAuditCols)

  test("CDC Master Incremental IABD") {

    spark.sparkContext.setLogLevel("ERROR")

    val range = Period.hours(5)
    val operationTypesForVTypes = List("B") //List("B") would mean (B,A) pairs
    val operationTypesForPrevDiffen = List("I") //Only I are available in the previous DIFFEN

    val productIds = List("PRODUCT1", "PRODUCT2", "PRODUCT3", "PRODUCT4", "PRODUCT5")

    val (todaysVerifyTypesListGen, deletablesGen, pkChangeGen) = CDCDataGenerators.createGenerator(januaryFirst, range, operationTypesForVTypes, productIds)

    val (previousDiffenListGen, _, _) = CDCDataGenerators.createGenerator(december31st, range, operationTypesForPrevDiffen, productIds)

    check {
      forAllNoShrink(todaysVerifyTypesListGen, previousDiffenListGen, deletablesGen, pkChangeGen) { (todaysVerifyTypesList, previousDiffenList, deletables, pkChange) =>
        getAllAssertions(dataCols, List("productId"), todaysVerifyTypesList, previousDiffenList, deletables, pkChange)
      }
    }
  }

  /** Encapsulates all the assertions for this testcase
    *
    * @param dataCols        All business columns for this table
    * @param pkCols          Primary Key columns for this table
    * @param verifyTypesList List of CDCTableRow that composes Verify types
    * @param previousDiffenList List of CDCTableRow that composes the Previous DIFFEN
    * @param mayBeDeletables List of CDCTableRow that ''may be'' a part of ''D'' records.  The reason why they are ''may be'' is because the Delete records are generated randomly, there is a possibility that
    *                        there may be ''D'' records for a primary key which aren't on PreviousDIFFEN or Verify types
    * @param mayBePkChanges  List of CDCTableRow that ''may be'' a part of records whose primary key has changed aka Records whose last entry is a 'B'.
    *                        Again, the reason why they are may be is because the PK changed records are generated randomly, there is a possibility that
    *                        there may be records for a primary key which aren't on PreviousDIFFEN or Verify types.  Also, a check is made so that the PKV records aren't part of the
    *                        ''D'' records as well
    * @return Consolidated assertion of all properties
    */
  def getAllAssertions(dataCols: List[String], pkCols: List[String], verifyTypesList: List[CDCTableRow], previousDiffenList: List[CDCTableRow], mayBeDeletables: List[CDCTableRow], mayBePkChanges: List[CDCTableRow]) = {

    implicit val sqlContext = spark.sqlContext

    //select just one D record for a single product which is available in the verifyTypes/previousDiffen. Isolated D for a product not in VType/Prev DIFFEN doesn't make sense
    val dRecords = mayBeDeletables.groupBy(_.productId).map(_._2.head)
      .filter(delProd => (verifyTypesList ++ previousDiffenList).map(_.productId).contains(delProd.productId)).toSet

    val pkVDeletables = mayBePkChanges.groupBy(_.productId).map(_._2.head)
      .filter(pkDProd => (verifyTypesList ++ previousDiffenList).map(_.productId).contains(pkDProd.productId)).toSet
      .filterNot(pkDProd => dRecords.map(_.productId).contains(pkDProd.productId))

    val deletables = dRecords ++ pkVDeletables

    val verifyTypesWithSpeciallyIntroducedTypes = verifyTypesList ++ deletables

    val previousDiffenSingleInstancePerKey = previousDiffenList
      .groupBy(_.productId) //have only one instance per primarykey
      .map(_._2.head)
      .toList

    //Enable this if you would like to log the case class for interactive debugging on spark-shell
    //logDatasetForDebugging(verifyTypesWithSpeciallyIntroducedTypes, previousDiffenSingleInstancePerKey)

    import sqlContext.implicits._
    val todaysVerifyTypesDF = spark.sparkContext.parallelize(verifyTypesWithSpeciallyIntroducedTypes).toDF
    val previousDiffenDF = spark.sparkContext.parallelize(previousDiffenSingleInstancePerKey).toDF

    println("###################################### TODAY'S VERIFY TYPES ######################################")
    todaysVerifyTypesDF.show(100, false)
    println("######################################     PREVIOUS DIFFEN     ######################################")
    previousDiffenDF.show(100, false)


    //TODO Duplicated across Testcase and assertions
    val januaryFirst = new DateTime(2017, 1, 1, 10, 0)
    val tableName = "DUMMY_TABLENAME"

    val recordTypeBasedProcessor = new RecordTypeBasedProcessor

    val DiffenOutput(diffenOpen, diffenNonOpen, duplicatesOpt, _, bRecordsOpt) = recordTypeBasedProcessor.processDiffen(tableName, dataCols, pkCols, todaysVerifyTypesDF, previousDiffenDF,
      recordTypeMeta, TableType.DELTA, RunType.INCREMENTAL, januaryFirst, DateTimeFormat.forPattern("yyyy-MM-dd"))(spark)

    println("######################################       DIFFEN OPEN       ######################################")
    diffenOpen.show(100, false)
    println("######################################     DIFFEN NON OPEN     ######################################")
    diffenNonOpen.show(100, false)
    println("######################################       DUPLICATES       ######################################")
    duplicatesOpt.show(100, false)
    /*println("######################################       B-RECORDS       ######################################")
    bRecordsOpt.foreach(_.show(false))*/

    val diffenOpenRowIds = diffenOpen.select("rowId").collect().map(row => row.get(0).toString).toSet
    val diffenNonOpenRowIds = diffenNonOpen.select("rowId").collect().map(row => row.get(0).toString).toSet

    val duplicatesRowIds = duplicatesOpt.select("rowId").collect().map(row => row.get(0).toString).toSet

    val verifyTypesWithoutBRecords = verifyTypesWithSpeciallyIntroducedTypes.toSet
    val bRecords = verifyTypesWithSpeciallyIntroducedTypes.filter(_.c_operationtype == "B").toSet

    //val latestPerKeyInVTypes = verifyTypesWithSpeciallyIntroducedTypes.groupBy(_.productId).map(_._2.last).toSet
    val latestPerKeyInVTypes = verifyTypesWithoutBRecords
      .groupBy(row => (row.c_journaltime, row.c_transactionid, row.c_operationtype, row.c_userid, row.productId, row.currencyVal, row.price))
      .map { case (_, listRow) => listRow.head }
      .groupBy(_.productId)
      .mapValues(groupList => groupList.maxBy(delimRow => delimRow.rowId))
      .values.toSet

    //All the rows with the same primary key as the deleted row
    val rowsWithDeletablesPK = latestPerKeyInVTypes.filter(dRow => deletables.map(_.productId).contains(dRow.productId))

    val oldAsAndDsDuringTheDay = verifyTypesWithoutBRecords.diff(latestPerKeyInVTypes)
    //Has to go into DIFFEN NON_OPEN -  Updates for I records (+ Old As for the day)
    val prevDiffenOpenProductsInVTypes = previousDiffenSingleInstancePerKey.filter(row => verifyTypesWithoutBRecords.map(_.productId).contains(row.productId)).toSet
    //Has to stay in DIFFEN OPEN - Previous Diffen open for which no update has been received (+ Latest update on Verify types)
    val prevDiffenOpenProductsNotInVTypes = previousDiffenSingleInstancePerKey.filterNot(row => verifyTypesWithoutBRecords.map(_.productId).contains(row.productId)).toSet

    //DIFFEN OPEN = latestInVerifyTypes + PrevDIFFEN data which is not updated through verify types - D Records
    val diffenOpenCountCheck = (latestPerKeyInVTypes.size + prevDiffenOpenProductsNotInVTypes.size) - deletables.size === diffenOpen.count()
    //DIFFEN NON_OPEN = prevDIFFEN Data which is updated through today's VTypes + Changes during the day in Vtypes + D records - bRecords
    val diffenNonOpenCountCheck = prevDiffenOpenProductsInVTypes.size + oldAsAndDsDuringTheDay.size + deletables.size - bRecords.size + pkVDeletables.size === diffenNonOpen.count()

    val diffenOpenRowIdCheck = (prevDiffenOpenProductsNotInVTypes.map(_.rowId) ++ latestPerKeyInVTypes.map(_.rowId)).diff(rowsWithDeletablesPK.map(_.rowId)) === diffenOpenRowIds
    val diffenNonOpenRowIdCheck = prevDiffenOpenProductsInVTypes.map(_.rowId) ++ oldAsAndDsDuringTheDay.map(_.rowId) ++ rowsWithDeletablesPK.map(_.rowId) -- bRecords.map(_.rowId) ++ pkVDeletables.map(_.rowId) === diffenNonOpenRowIds

    println(s"diffenOpenCountCheck: $diffenOpenCountCheck   -> ${diffenOpen.count()} === ${latestPerKeyInVTypes.size} + ${prevDiffenOpenProductsNotInVTypes.size} - ${deletables.size}")
    println(s"diffenNonOpenCountCheck: $diffenNonOpenCountCheck   -> ${diffenNonOpen.count()} === ${prevDiffenOpenProductsInVTypes.size} + ${oldAsAndDsDuringTheDay.size} + ${deletables.size} - ${duplicatesRowIds.size}")
    println(s"diffenOpenRowIdCheck: $diffenOpenRowIdCheck")
    println(s"diffenNonOpenRowIdCheck: $diffenNonOpenRowIdCheck")

    diffenOpenCountCheck && diffenNonOpenCountCheck && diffenOpenRowIdCheck && diffenNonOpenRowIdCheck
  }
}