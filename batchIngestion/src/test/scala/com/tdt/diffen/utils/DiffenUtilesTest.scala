package com.tdt.diffen.utils

import java.sql.Timestamp

import com.tdt.diffen.ConversionUtils
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkConf
import org.apache.spark.sql.functions.{col, lit, date_format}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.scalatest.{BeforeAndAfterAll, FlatSpec}

class DiffenUtilesTest extends FlatSpec with BeforeAndAfterAll {

  val sparkConf = new SparkConf()
  sparkConf.setAppName("DiffenUtilitesTest")
  sparkConf.setMaster("local[*]")

  implicit val spark = SparkSession.builder().config(sparkConf).enableHiveSupport().getOrCreate()
  val testBaseFolder = "test/resources/diffen_utilities_test"

  "persist hive table" should "persist data with applied formats" in {
        val tableName = "testTable"
    val tablePath = s"$testBaseFolder/$tableName"
    val testDB = "test_db"
    val partitionName = "test"
    val partitionValue = "part1"
    val timestampFormat = "yyyy-MM-dd HH:mm:ss.SSS"
    val testTime = "2020-07-14 01:01:01.111"

    import spark.implicits._
    case class Test(id: Int, name: String, updateTime: Timestamp)

    spark.sql(s"DROP database $testDB CASCADE")
    spark.sql(s"CREATE database $testDB")
    spark.sql(
      s"""CREATE EXTERNAL TABLE IF NOT EXISTS $testDB.$tableName (id INT, name STRING, updateTime TIMESTAMP)
         |PARTITIONED BY (`${partitionName}` string) ROW FORMAT SERDE   'org.apache.hadoop.hive.ql.io.orc.OrcSerde'
         |STORED AS INPUTFORMAT   'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'
         |OUTPUTFORMAT   'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
         |LOCATION   '${tablePath}'
         |""".stripMargin)

    val testData: DataFrame = List(
      (1, "user1", testTime),
      (2, "user2", testTime),
      (3, "user3", testTime)
    ).toDF()

    spark.createDataFrame(testData.rdd, Test.getClass)
    DiffenUtils.persistAsHiveTable(testData, tablePath, testDB, tableName,
      partitionName, partitionValue, SaveMode.Overwrite)

    val outputDF = spark.read.orc(tablePath)
    val outputTimestamp = outputDF.collect()(1).getTimestamp(2)
    val timestampString = outputTimestamp.toString
    assert(timestampString == testTime)
  }

  "parse udf" should "parse timestamp data" in {
    import spark.implicits._
    val testTime = "2020-07-14 01:01:01.111"
    val expectedOutput = "2020/07/14 01:01:01.111"
    val convertedFormat = "yyyy/MM/dd HH:mm:ss.SSS"
    val timestampFormat = "yyyy-MM-dd HH:mm:ss.SSS"
    val testData = List(testTime).toDF("ts")
    val transformedDate = testData.select(date_format(
      ConversionUtils.parseTS(col("ts"), lit(timestampFormat)), convertedFormat))
      .collect().apply(0).getString(0)
    assert(transformedDate == expectedOutput)
  }

  "parse udf" should "parse timestamp data with null" in {
    import spark.implicits._
    val testTime = null
    val expectedOutput = null
    val convertedFormat = "yyyy/MM/dd HH:mm:ss.SSS"
    val timestampFormat = "yyyy-MM-dd HH:mm:ss.SSS"

    val inputList: List[String] = List(testTime)

    val testData = inputList.toDF("ts")
    val transformedDate = testData.select(date_format(
      ConversionUtils.parseTS(col("ts"), lit(timestampFormat)), convertedFormat))
      .collect().apply(0).getString(0)
    assert(transformedDate == expectedOutput)
  }

  "parse udf" should "parse timestamp data with empty" in {
    import spark.implicits._
    val testTime = ""
    val expectedOutput = null
    val convertedFormat = "yyyy/MM/dd HH:mm:ss.SSS"
    val timestampFormat = "yyyy-MM-dd HH:mm:ss.SSS"

    val inputList: List[String] = List(testTime)

    val testData = inputList.toDF("ts")
    val transformedDate = testData.select(date_format(
      ConversionUtils.parseTS(col("ts"), lit(timestampFormat)), convertedFormat))
      .collect().apply(0).getString(0)
    assert(transformedDate == expectedOutput)
  }

  override protected def afterAll(): Unit = {
    val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
    fs.delete(new Path(testBaseFolder), true)
  }
}
