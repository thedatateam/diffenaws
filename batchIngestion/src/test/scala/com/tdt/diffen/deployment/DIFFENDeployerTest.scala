package com.tdt.diffen.deployment

import org.scalatest.{FlatSpec, Matchers}

class DIFFENDeployerTest extends FlatSpec with Matchers {

  "Deployed hash code" should "give the maximum number of instances" in {
    DIFFENDeployer.getMaxSourceCount("1302552720") == 10
  }

}
