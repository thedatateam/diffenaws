# Non Record Type 

- Expects a journal timestamp column to create DIFFEN  

## Delta Full Dump

##### Current Impl

1) Dedup verify types based on `PK`  
2) Union Previous Diffen with dedup data in step 1  
3) Finds deleted PK row ids from previous Diffen and verify types  
4) Dedup step 2 data based on `data cols` (include timestamp column)  
5) Diffen Open - latest record based on timestamp and rowid from dedup data of step 4 excluding the deleted PK rows in step 3  
6) Diffen Non Open - other data  

##### New Impl

1) Dedup verify types based on `data cols` (include timestamp column)  
2) Finds deleted PK row ids from previous Diffen and verify types  
3) Diffen Open - latest record based on timestamp and rowid from dedup data of step 1 records  
4) Diffen Non Open - other records from dedup and deleted rows  


## Delta Incremental

#### Current Impl

1) Dedup verify types data based on `PK`  
2) Union previous diffen with dedup data in step 1  
3) Dedup step 2 records based on `data cols` (includes the timestamp column)  
4) Diffen open - get latest record by timestamp col and rowId from dedup records in step 3  
5) Diffen nonopen - other records  


#### New Impl

1) Union pervious Diffen with verify types  
2) Dedup step 1 data with `data cols` (include timestamp column)  
3) Diffen Open - get latest dedup records from step 2  
4) Diffen Non Open - other records  

---

## Txn Full Dump

#### Current Impl

1) Dedup verify types based on `PK`    
2) Finds deleted PK row ids from previous Diffen and verify types  
3) Dedup step 1 data based on `data cols` (include timestamp column)  
4) Diffen Open - latest record based on timestamp and rowid from dedup data of step 4 excluding the deleted PK rows in step 3  
5) Diffen Non Open - other data  


#### New Impl

1) Dedup verify types based on `data cols` (include timestamp column)  
2) Finds deleted PK row ids from previous Diffen and verify types  
3) Diffen Open - latest record based on timestamp and rowid from dedup data of step 1 records  
4) Diffen Non Open - other records from dedup and deleted rows  


## Txn Incremental

#### Current Impl

1) Dedup verify types data based on `PK`    
2) Dedup step 1 records based on `data cols` (includes the timestamp column)  
3) Diffen open - get latest record by timestamp col and rowId from dedup records in step 3  
4) Diffen nonopen - other records  


#### New Impl
  
1) Dedup verify types with `data cols` (include timestamp column)  
2) Diffen Open - get latest record by timestamp col and rowId from dedup records in step 1  
3) Diffen Non Open - other records  


---
---

# Record Type

- Expects journal timestamp and record type column to create Diffen

## Delta Incremental

#### Current Impl

1) Dedup Verifytypes based on `dataCols` (including op type and journal time)
2) Diffen Open - get latest record based on 