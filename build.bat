call mvn clean install -f nifi-nar-bundles\pom.xml

cd batchIngestion
call sbt assembly

cd ..

if exist "bundle" rmdir /S /Q bundle
mkdir bundle\nifi-nars
mkdir bundle\lib
mkdir bundle\scripts
mkdir bundle\conf
mkdir bundle\demo

copy /Y nifi-nar-bundles\distribution\target\distribution-bin\*.nar bundle\nifi-nars
copy /Y batchIngestion\target\scala-2.11\IngestionProcessingV2-assembly-1.0.jar bundle\lib
copy /Y scripts\* bundle\scripts
copy /Y conf\* bundle\conf
copy /Y bfl_config\*.xml bundle\conf
xcopy demo bundle\demo /E /Y
